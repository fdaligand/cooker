import itertools
import os
import time
from collections import defaultdict
from enum import Enum
from operator import itemgetter, attrgetter

import click
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_, desc
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy import Index

from cooker.loader import JsonLoader

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:cooker@127.0.0.1:5432/cooker'  #sqlite:////tmp/test.db'
db = SQLAlchemy(app)

recipe_ingredient = db.Table('recipe_ingredient',
                db.Column('ingredient_id', db.Integer, db.ForeignKey('ingredient.id'), index=True),
                db.Column('recipe_id', db.Integer, db.ForeignKey('recipe.id')),
)

class Recipe(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))
    total_time = db.Column(db.String(80))
    difficulty = db.Column(db.String(80))
    image = db.Column(db.String(250))
    cook_time = db.Column(db.String(80))
    rate = db.Column(db.String(80))
    prep_time = db.Column(db.String(80))
    budget = db.Column(db.String(80))
    people_quantity = db.Column(db.String(80))
    ingredients = db.relationship('Ingredient', secondary=recipe_ingredient, backref=db.backref('recipe', lazy=True))


class Ingredient(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))
    user_input_id = db.Column(db.Integer, db.ForeignKey('user_input.id'))


class UserInput(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))
    ingredients = db.relationship('Ingredient', lazy=True)

class Association(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_inputs = db.Column(ARRAY(db.Text))
    all_ingredients = db.Column(ARRAY(db.Integer))
    matching_recipes = db.relationship('MatchingRecipe', backref=db.backref('association', lazy=True))

    def extract_all_ingredients(self, ingredients):
        """ Build a flatten list of all ingredient found in an associaiton"""
        all_ingredients = list(itertools.chain(*[ing for ing in ingredients.values()]))
        self.all_ingredients = list(map(lambda x: x.id, all_ingredients))

class MatchingRecipe(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe.id'))
    recipe = db.relationship('Recipe', uselist=False)
    confirmed_ingredient = db.Column(db.String)
    matching_score = db.Column(db.Float)
    missing_ingredient = db.Column(db.Integer)
    association_id = db.Column(db.Integer, db.ForeignKey('association.id'))


class DataSet(Enum):
    development = 'recipes_light.json'
    production = 'recipes.json'


@app.cli.command("init_db")
def init_db():

    # load field in memory
    db.create_all()
    if Recipe.query.first():
        app.logger.info('DB already set')
        return
    environment = os.environ.get('FLASK_ENV', 'production')
    try:
        env = DataSet[environment]
    except KeyError:
        app.logger.error(f"Wrong env params {environment}, don't load data")
        return
    loader = JsonLoader(os.path.join(os.path.dirname(__file__), 'data', env.value))
    loader.load_datas()
    start_time = time.perf_counter()
    for data in loader.datas:
        recipe = Recipe.query.filter(Recipe.name == data.get("name")).first()
        if not recipe:
            recipe = Recipe(
                name=data.get("name"),
                total_time=data.get("total_time"),
                difficulty=data.get("difficulty"),
                image=data.get("image"),
                cook_time=data.get("cook_time"),
                rate=data.get("rate"),
                prep_time=data.get("prep_time"),
                budget=data.get("budget"),
                people_quantity=data.get("people_quantity"),
            )

        for raw_ingredient in data.get("ingredients"):
            # build ingredient
            ingredient = Ingredient.query.filter(Ingredient.name == raw_ingredient).first()
            if not ingredient:
                ingredient = Ingredient(name=raw_ingredient)
            # associate ingredient with recipe
            recipe.ingredients.append(ingredient)

        db.session.add(recipe)
    db.session.commit()
    end_time = time.perf_counter()
    app.logger.info("load time =" + str(end_time-start_time))
    app.logger.info("load {} recipes".format(Recipe.query.count()))


def find_ingredients(inputs):
    """Build dict with all matching ingredients found in db for each user input"""
    ingredients = defaultdict(dict)
    for user_input in inputs:
        ingredients[user_input] = Ingredient.query.filter(
            Ingredient.name.like(f'%{user_input}%')
        ).all()
        app.logger.debug(f"Get {len(ingredients[user_input])} match for user input {user_input}")
        # hack to load recipe


    return ingredients


def save_user_input_ingredients(user_input_ingredients, requested_ingredients):
    """
        Save user input ingredient with its match in db.
        Return a dict object with a list of all matching ingredient by user input
    """
    ingredients = defaultdict(dict)
    for user_ingredient in user_input_ingredients:
        user_input = UserInput.query.filter(UserInput.name == user_ingredient).first()
        if user_input:
            app.logger.debug(f'user input {user_input} ind db, use it')
        else:
            user_input = UserInput(name=user_ingredient)
            # found all possible ingredient that match user input
            matching = requested_ingredients[user_ingredient]
            user_input.ingredients.extend(matching)
            db.session.add(user_input)
            db.session.commit()
        ingredients[user_input.name] = user_input.ingredients
    return ingredients


def save_matching_recipe(ingredients, association):
    """Save matching recipe in db for a given association"""

    # get all possible recipe for an ingredient
    recipes_by_ingredient = defaultdict(dict)
    app.logger.debug("get all possible recipe for an ingredient")
    for name, matching_ingredients in ingredients.items():
        recipes_by_ingredient[name] = set(itertools.chain(*[ing.recipe for ing in matching_ingredients]))

    app.logger.debug("try to find matching recipe")
    for recipe in set.intersection(*[matching for matching in recipes_by_ingredient.values()]):
        # use namedtuple for score
        user_confirmed_ingredient = set.intersection(set(association.all_ingredients), set(x.id for x in recipe.ingredients))
        if not MatchingRecipe.query.filter(MatchingRecipe.recipe == recipe).filter(MatchingRecipe.association == association).first():
            app.logger.debug("find new matching recipe")
            matching_recipe = MatchingRecipe(
                recipe=recipe,
                confirmed_ingredient=','.join(map(str, user_confirmed_ingredient)),
                matching_score=int((len(user_confirmed_ingredient) / len(recipe.ingredients))*100),
                missing_ingredient=len(user_confirmed_ingredient) - len(recipe.ingredients),
                association=association
            )
            db.session.add(matching_recipe)
            db.session.commit()


def get_possible_association(matching_ingredient):
    """
    Compute all possible combination or provided ingredient
    We don't search for association under 5 ingredient
    """
    # don't generate to much association to avoid performance issues
    starting_rank = 3 if len(matching_ingredient.keys()) < 5 else len(matching_ingredient.keys())-1
    for rank in range(starting_rank, len(matching_ingredient.keys())+1):
        # get all recipe that match rank for n ingredient
        possible_association = list(itertools.combinations(matching_ingredient.keys(), rank))
        for assoc in possible_association:
            association = Association.query.filter(Association.user_inputs == assoc).first()
            if association:
                app.logger.debug(f"association {assoc} already done, skip it.")
            else:
                # save new association in db for further use
                association = Association(user_inputs=assoc)
                app.logger.debug("search for matching ingredient")
                assoc_ingredient = {key: value for key, value in matching_ingredient.items() if key in assoc}
                app.logger.debug("compute all ingredient list")
                association.extract_all_ingredients(assoc_ingredient)
                db.session.add(association)
                db.session.commit()
                save_matching_recipe(assoc_ingredient, association)
                app.logger.debug(f"save new associaiton {assoc}")


def get_ranked_recipe(input_ingredients):
    """
    Retrieve all possible association from user input
    and return matching recipe order by matching score
    """

    ranked_recipes = db.session.query(MatchingRecipe) \
        .join(Association) \
        .filter(Association.user_inputs.contained_by(input_ingredients)) \
        .order_by(desc(MatchingRecipe.matching_score), desc(MatchingRecipe.missing_ingredient)) \
        .all()

    return ranked_recipes
