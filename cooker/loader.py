import json
import os
import logging
from json import JSONDecodeError

LOGGER = logging.getLogger(__name__)

class JsonLoader():
    """Load json data from a file"""

    def __init__(self, file_path):

        self.path = file_path
        self.datas = []

    def load_datas(self):

        if not os.path.isfile(self.path):
            LOGGER.error(f"{self.path} is not a valid file path, no datas will be loaded")
            return

        with open(self.path, 'r') as file:
            for i, line in enumerate(file):
                try:
                    self.datas.append(json.loads(line))
                except JSONDecodeError:
                    pass

