import itertools
import os
import time

from flask import render_template, request, session, jsonify
from cooker import models
from cooker.models import save_user_input_ingredients


def create_app():

    app = models.app

    # init db if not set
    app.logger.setLevel(os.environ.get('LEVEL_MODE', 'DEBUG'))
    app.secret_key = b'not_so_secret'


    @models.app.route('/')
    def search():
        return render_template('index.html')


    @models.app.route('/association', methods=["POST"])
    def association():
        input_ingredients = request.json.get('data', [])
        start_time = time.perf_counter()
        # get all matching ingredient from db
        possible_ingredients = models.find_ingredients(input_ingredients)
        save_user_input_ingredients(input_ingredients, possible_ingredients)
        get_ingredient_time = time.perf_counter()
        app.logger.debug(f"Get all ingredients in {get_ingredient_time - start_time}")
        models.get_possible_association(possible_ingredients)
        get_association_time = time.perf_counter()
        app.logger.debug(f"Get all association in {get_association_time - get_ingredient_time}")
        return jsonify(result='ok')


    @models.app.route('/recipes',  methods=["POST"])
    def recipes():
        """ Search for recipes according to input ingredient"""
        # get ingredient from the form
        input_ingredients = [x for x in request.form["ingredients"].split(',')]
        ranked_recipes = models.get_ranked_recipe(input_ingredients)
        return render_template('search_result.html', ingredients=input_ingredients, recipes=ranked_recipes)

    @models.app.route('/recipe/<int:recipe_id>', methods=["POST"])
    def details(recipe_id):
        recipe = models.Recipe.query.get(recipe_id)
        ingredient_id = [int(x) for x in request.form.get('ingredients', []).split(',')]
        for ing in recipe.ingredients:
            if ing.id in ingredient_id:
                ing.have_it = True
        return render_template('details.html', recipe=recipe)


    return app


