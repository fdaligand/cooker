--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Debian 12.1-1.pgdg100+1)
-- Dumped by pg_dump version 12.1 (Debian 12.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: association; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.association (
    id integer NOT NULL,
    user_inputs text[],
    all_ingredients integer[]
);


ALTER TABLE public.association OWNER TO postgres;

--
-- Name: association_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.association_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.association_id_seq OWNER TO postgres;

--
-- Name: association_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.association_id_seq OWNED BY public.association.id;


--
-- Name: ingredient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ingredient (
    id integer NOT NULL,
    name character varying(250),
    user_input_id integer
);


ALTER TABLE public.ingredient OWNER TO postgres;

--
-- Name: ingredient_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ingredient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ingredient_id_seq OWNER TO postgres;

--
-- Name: ingredient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ingredient_id_seq OWNED BY public.ingredient.id;


--
-- Name: matching_recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.matching_recipe (
    id integer NOT NULL,
    recipe_id integer,
    confirmed_ingredient character varying,
    matching_score double precision,
    missing_ingredient integer,
    association_id integer
);


ALTER TABLE public.matching_recipe OWNER TO postgres;

--
-- Name: matching_recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.matching_recipe_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.matching_recipe_id_seq OWNER TO postgres;

--
-- Name: matching_recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.matching_recipe_id_seq OWNED BY public.matching_recipe.id;


--
-- Name: recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe (
    id integer NOT NULL,
    name character varying(250),
    total_time character varying(80),
    difficulty character varying(80),
    image character varying(250),
    cook_time character varying(80),
    rate character varying(80),
    prep_time character varying(80),
    budget character varying(80),
    people_quantity character varying(80)
);


ALTER TABLE public.recipe OWNER TO postgres;

--
-- Name: recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipe_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipe_id_seq OWNER TO postgres;

--
-- Name: recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipe_id_seq OWNED BY public.recipe.id;


--
-- Name: recipe_ingredient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe_ingredient (
    ingredient_id integer,
    recipe_id integer
);


ALTER TABLE public.recipe_ingredient OWNER TO postgres;

--
-- Name: user_input; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_input (
    id integer NOT NULL,
    name character varying(250)
);


ALTER TABLE public.user_input OWNER TO postgres;

--
-- Name: user_input_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_input_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_input_id_seq OWNER TO postgres;

--
-- Name: user_input_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_input_id_seq OWNED BY public.user_input.id;


--
-- Name: association id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.association ALTER COLUMN id SET DEFAULT nextval('public.association_id_seq'::regclass);


--
-- Name: ingredient id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingredient ALTER COLUMN id SET DEFAULT nextval('public.ingredient_id_seq'::regclass);


--
-- Name: matching_recipe id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.matching_recipe ALTER COLUMN id SET DEFAULT nextval('public.matching_recipe_id_seq'::regclass);


--
-- Name: recipe id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe ALTER COLUMN id SET DEFAULT nextval('public.recipe_id_seq'::regclass);


--
-- Name: user_input id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_input ALTER COLUMN id SET DEFAULT nextval('public.user_input_id_seq'::regclass);


--
-- Data for Name: association; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.association (id, user_inputs, all_ingredients) FROM stdin;
\.


--
-- Data for Name: ingredient; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ingredient (id, name, user_input_id) FROM stdin;
1	600g de pâte à crêpe	\N
2	1/2 orange	\N
3	1/2 banane	\N
4	1/2 poire pochée	\N
5	1poignée de framboises	\N
6	75g de Nutella®	\N
7	1poignée de noisettes torréfiées	\N
8	1/2poignée d'amandes concassées	\N
9	1cuillère à café d'orange confites en dés	\N
10	2cuillères à café de noix de coco rapée	\N
11	1/2poignée de pistache concassées	\N
12	2cuillères à soupe d'amandes effilées	\N
13	1kg d'agneau	\N
14	300g de riz	\N
15	24 abricots secs	\N
16	1cuillère à soupe de sucre en poudre	\N
17	50g de matière grasse (saindoux, beurre ou huile)	\N
18	100g de beurre	\N
19	75cl de bouillon	\N
20	Poivre	\N
21	Sel	\N
22	1kg d'épaule d'agneau désossée	\N
23	3 oignons	\N
24	2 yaourts	\N
25	3dl de crème fraîche	\N
26	1 noix de gingembre frais	\N
27	8g de coriandre	\N
28	1cuillère à soupe de cumin	\N
29	Muscade	\N
30	75g de noix de cajou	\N
31	75g de raisin de Smyrne	\N
32	2 aubergines	\N
33	3 tomates	\N
34	700g d'épaule d'agneau	\N
35	200g de feta	\N
36	2 oignons	\N
37	2gousses d'ail	\N
38	1pincée de sel	\N
39	1pincée de poivre	\N
40	1pincée de cumin	\N
41	1filet d'huile d'olive	\N
42	1branche de thym	\N
43	1 gigot d'agneau	\N
44	6gousses d'ail	\N
45	100g de gingembre frais	\N
46	Huile d'olive	\N
47	1bouquet de coriandre fraîche	\N
48	3 mangues	\N
49	6cuillères à soupe de miel	\N
50	100g d'amandes effilées	\N
51	Piment	\N
52	1 épaule d'agneau roulée de 1,2 kg environ	\N
53	1kg de fève fraîches	\N
54	1bouquet de sarriette	\N
55	50g de beurre	\N
56	1.2kg d'agneau en morceaux	\N
57	6 tomates	\N
58	3gousses d'ail	\N
59	1cuillère à soupe d'huile d'olive	\N
60	1dose de safran	\N
61	1 céleri branche	\N
62	200g d'olive verte dénoyautées	\N
63	1 citron jaune	\N
64	1.5kg d'épaule d'agneau désossée et coupée en morceaux	\N
65	250g d'amandes mondées	\N
66	200g de raisins secs	\N
67	1cuillère à soupe de miel liquide	\N
68	1/2cuillère à café de cannelle	\N
69	6cuillères à soupe d'huile	\N
70	1kg d'agneau en morceaux	\N
71	6 fonds d'artichaut	\N
72	4 tomates	\N
73	1botte d'oignon	\N
74	1gousse d'ail	\N
75	1louche d'eau	\N
76	Huile	\N
77	Beurre	\N
78	Thym	\N
79	Laurier	\N
80	700g d'épaule d'agneau désossée	\N
81	700g de carotte	\N
82	1 ail	\N
83	1cuillère à café de sauge	\N
84	25cl de vin d'Anjou rouge	\N
85	1 cube de bouillon (de boeuf)	\N
86	2cuillères à soupe de persil haché	\N
87	2cuillères à soupe d'huile	\N
88	1.5kg d'agneau désossé	\N
89	2 échalotes	\N
90	250g de champignon	\N
91	250g de pomme de terre	\N
92	3branches de céleri	\N
93	25g d'orge perlé	\N
94	30cl de bouillon de volaille	\N
95	15cl de vin blanc sec	\N
96	3cuillères à soupe d'huile	\N
97	1noix de beurre	\N
98	1.6kg d'agneau	\N
99	1kg de poire	\N
100	100g de pistache	\N
101	1 coriandre	\N
102	15cl d'eau	\N
103	600g d'épaule d'agneau désossée	\N
104	600g de côtelette d'agneau	\N
105	40g de beurre	\N
106	2cuillères à soupe d'huile d'olive	\N
107	2 poivrons verts	\N
108	2 poivrons rouges	\N
109	2 tomates	\N
110	2 olives noires	\N
111	10cl de vin blanc sec	\N
112	1 bouquet garni	\N
113	1.5kg d'épaule d’agneau désossée	\N
114	120g de beurre	\N
115	15g de farine	\N
116	15g de paprika doux	\N
117	3 poivrons	\N
118	30cl de crème liquide	\N
119	1.5kg d'épaule d'agneau désossée et coupé en cubes	\N
120	50g de sucre	\N
121	50g d'amandes émondées	\N
122	20cl d'eau	\N
123	2cuillères à soupe de beurre	\N
124	1bâton de cannelle en morceaux	\N
125	1cuillère à soupe d'eau de fleur d'oranger	\N
126	1kg de viande d’agneau en morceaux	\N
127	1kg de pruneau d'Agen	\N
128	500g de figue fraîches et fermes	\N
129	125g de beurre	\N
130	200g de sucre	\N
131	1bâton de cannelle	\N
132	1cuillère à café de cannelle en poudre	\N
133	10 clous de girofle	\N
134	4grains de poivre noir	\N
135	Amandes effilées légèrement dorées (pour décorer)	\N
136	1kg d'agneau ou mouton (épaule de préférence)	\N
137	1verre d'eau de vinaigre d'alcool	\N
138	Sel /poivre	\N
139	4 oignons	\N
140	6cuillères à soupe de moutarde forte	\N
141	2 cubes de bouillon	\N
142	Piment / curry / paprika	\N
143	Sel /poivre	\N
144	500g de vermicelles	\N
145	2verres d'huile de tournesol	\N
146	400g de macédoine de légumes égouttée	\N
147	2poignées de gros sel	\N
148	1 épaule d'agneau	\N
149	Souris d'agneau par personne (du boucher ou surgelé(e)!)	\N
150	2 ail (selon goût)	\N
151	1cuillère à café rase de Ras el Hanout	\N
152	Curry /cumin moulu (voir remarques)	\N
153	1cuillère à café de fond de veau	\N
154	3cuillères à soupe de raisins secs	\N
155	4 carottes	\N
156	500g de pomme de terre nouvelles	\N
157	1poignée de pignons secs	\N
158	1/4tasse d'huile	\N
159	1/2cuillère à café de moutarde	\N
160	2gousses d'ail écrasées	\N
161	3 cardamome écrasées	\N
162	1cuillère à soupe de graines de coriandre en poudre	\N
163	1morceau de gingembre frais, pelé et finement haché	\N
164	1kg d'agneau maigre, coupé en dés de 2 1/2 cm	\N
165	1 oignon moyen finement émincé	\N
166	3 piments verts finement hachés	\N
167	1cuillère à café de sucre	\N
168	1cuillère à café de curcuma	\N
169	1kg d'épinard lavés et coupés menu	\N
170	1/2cuillère à café de sel et autant de poivre	\N
171	3cuillères à soupe de yaourt nature	\N
172	800g de sauté de veau d'agneau sans os	\N
173	350 pruneaux dénoyautés	\N
174	1 oignon	\N
175	1tablette de bouillon de volaille	\N
176	4cuillères à soupe d'huile d'olive	\N
177	1cuillère à soupe de confiture de figues	\N
178	1cuillère à café de coriandre moulue	\N
179	1cuillère à café de cumin moulu	\N
180	1pincée de safran	\N
181	2pincées de cannelle en poudre	\N
182	2 clous de girofle	\N
183	800g d'agneau coupé en gros dés	\N
184	10 pommes de terre	\N
185	30cl de vin blanc	\N
186	demi de citron jaune (ou jus prêt à l’emploi)	\N
187	Paprika	\N
188	2 feuilles de laurier (facultatif)	\N
189	8pièces d'agneau	\N
190	1cuillère à café de gingembre en poudre	\N
191	1/2 citron vert (le jus et le zeste)	\N
192	30cl d'huile d'olive	\N
193	1/2cuillère à café de purée de piment d'espelette	\N
194	1 épaule d'agneau désossée et coupée en dés	\N
195	1boîte de tomates pelées non égouttées	\N
196	3cuillères à soupe d'huile d'olive	\N
197	1tasse de thé d'eau	\N
198	2 clous de girofle écrasés	\N
199	Cannelle en poudre	\N
200	1 étoile de badiane	\N
201	1cuillère à café de coriandre en poudre	\N
202	Piment en poudre doux ou plus fort selon le goût	\N
203	1pincée de cumin en poudre	\N
204	1kg de porc (échine ou épaule) coupé en dés de 3 cm	\N
205	120g de raisins secs	\N
206	1 oignon émincé	\N
207	12 abricots secs coupés en oreillons et encore en deux	\N
208	5cl de vinaigre de cidre	\N
209	15cl de vin blanc	\N
210	1cuillère à soupe de miel	\N
211	1pincée de quatre-épices	\N
212	1filet d'huile	\N
213	2cuillères à soupe de vinaigre balsamique	\N
214	2cuillères à soupe de vin	\N
215	300g d'aiguillette de canard	\N
216	250g de figue sèche ou fraiche	\N
217	120g de fromage de chèvre	\N
218	1/2cube de Volailles	\N
219	2cuillères à soupe de sucre en poudre	\N
220	600g d'aiguillette de canard	\N
221	5 oranges juteuses	\N
222	1/2 poivron vert	\N
223	1/2 poivron rouge	\N
224	1.5verre de blé (type ebly)	\N
225	75cl de vin blanc	\N
226	600g d'aiguillette de canard (environ 3-4 par personne)	\N
227	1 échalote	\N
228	2cuillères à soupe de vin blanc	\N
229	5 abricots bien mûrs	\N
230	1 cube de bouillon	\N
231	Aiguillette de canard	\N
232	10g de sucre en poudre	\N
233	1petit verre de porto blanc	\N
234	2 figues bien mûres	\N
235	10cl de crème liquide	\N
236	2cuillères à soupe d'huile de tournesol	\N
237	Farine	\N
238	Poivre noir du moulin	\N
239	1noisette de beurre	\N
240	12 aiguillettes de canard	\N
241	1.2kg de figue surgelées	\N
242	4 pommes	\N
243	4cuillères à café de confiture de figues	\N
244	1pincée de cannelle moulue	\N
245	1noisette de beurre  salé	\N
246	10 aiguillettes de canard	\N
247	3 mandarines	\N
248	1cuillère à soupe de vinaigre balsamique	\N
249	Mélange 5 baies " moulues	\N
250	1boîte de pêche	\N
251	6filets de canard	\N
252	1cuillère à soupe de quatre-épices	\N
253	150g de beurre	\N
254	1cuillère à soupe de sucre	\N
255	150g de riz basmati	\N
256	150g de riz sauvage	\N
257	200g d'aiguillette de canard	\N
258	1 poire	\N
259	1cuillère à café de porto	\N
260	Fleur de sel	\N
261	Poivre moulin	\N
262	2 pommes	\N
263	3filets de dinde	\N
264	2 citrons jaunes	\N
265	Beurre de cuisson	\N
266	200g de blanc de poulet	\N
267	5cl de vinaigre balsamique	\N
268	10cl de crème fraîche légère semi-épaisse	\N
269	10g de beurre	\N
270	1cuillère à soupe de persil haché	\N
271	4 aiguillettes de poulet	\N
272	4 mandarines	\N
273	10cl de crème liquide (normale ou allégée)	\N
274	Coriandre	\N
275	250g d'aiguillette de poulet	\N
276	2 endives	\N
277	2cuillères à soupe de crème fraîche	\N
278	120g de riz (non cuit)	\N
279	400g d'aiguillette de poulet	\N
280	50g de morille séchées	\N
281	150g de Foie gras	\N
282	1 pâte feuilletée	\N
283	3 jaunes d'oeuf	\N
284	1 poire ferme	\N
285	6 pommes de terre rouges	\N
286	50g de sésame blond	\N
287	1 muscade	\N
288	1cuillère à soupe de moutarde	\N
289	1 cube de bouillon de poule	\N
290	450g de poulet (découpé en aiguillettes)	\N
291	1 citron	\N
292	100g de miel	\N
293	Estragon	\N
294	12 aiguillettes de poulet	\N
295	100g de chapelure	\N
296	2cuillères à soupe de moutarde classique	\N
297	2cuillères à soupe de moutarde à l'ancienne	\N
298	2 jaunes d'oeuf	\N
299	1cuillère à café de curry	\N
300	4 raies	\N
301	300g d'échalote	\N
302	80g de beurre	\N
303	1verre de vinaigre de vin	\N
304	Herbes de Provence	\N
305	Jus de citron	\N
306	1kg de poivron	\N
307	2 raies	\N
308	20cl de crème fraîche	\N
309	Herbes	\N
310	1kg de raie	\N
311	1l de court-bouillon	\N
312	2 jaunes d'oeuf durs	\N
313	2cuillères à soupe de câpres	\N
314	1/2 citron	\N
315	1 raie pour 2 personnes	\N
316	1 soja indonésienne ou son équivalent (miso allongé de sauce soya classique, d'un peu d'eau et de miel, par exemple)	\N
317	1 oignon blanc ou de la cive	\N
318	Mélange d'épices 5 parfums chinoises (ou tout autre mélange d'épices qui vous plaît)	\N
319	Pâte de piment pour accompagner (facultatif)	\N
320	6 poulets	\N
321	6cuillères à soupe de sauce soja	\N
322	3cuillères à soupe de confiture d'abricot	\N
323	Piment (ou poivre)	\N
324	500g de dinde	\N
325	1cuillère à café de curry en poudre	\N
326	1cuillère à soupe de concentré de tomates	\N
327	1/2 pomme	\N
328	25cl de vin blanc sec	\N
329	Poivre de cayenne	\N
330	200g de riz	\N
331	60g de raisins secs	\N
332	1.5 aile de poulet	\N
333	3cuillères à soupe de cannelle	\N
334	3cuillères à soupe de miel (selon les gôuts)	\N
335	2paquets d'aile de poulet	\N
336	1 oignon moyen	\N
337	1pot de crème fraîche épaisse	\N
338	15cl de sauce worcestershire	\N
339	1barquette de champignon de Paris	\N
340	4tasses de riz	\N
341	12 ailes de poulet	\N
342	5cl de sauce soja	\N
343	7.5cl de sirop d'érable	\N
344	Ketchup (selon les goûts)	\N
345	2cuillères à café de moutarde	\N
346	1cuillère à café de gingembre râpé	\N
347	Tabasco (selon les goûts)	\N
348	Miel (facultatif)	\N
349	1kg d'aile de poulet	\N
350	1gousse d'ail pilée	\N
351	2cuillères à soupe de vin blanc même sucré	\N
352	1cuillère à soupe de sauce soja	\N
353	1.5cuillère à soupe de miel liquide	\N
354	1pointe de piment selon le goût	\N
355	1cuillère à soupe d'huile d'arachide	\N
356	4 raies pelées	\N
357	10cl de vinaigre blanc	\N
358	1 feuille de laurier	\N
359	1feuille de thym	\N
360	Poivre en grains	\N
361	Gros sel	\N
362	60g de beurre doux	\N
363	40g de beurre demi-sel	\N
364	1cuillère à café de jus de citron	\N
365	2cuillères à soupe de câpres égouttés	\N
366	2branches de persil plat	\N
367	100g de chanterelle	\N
368	250g de riz basmati	\N
369	2 oeufs	\N
370	75g de chapelure	\N
371	20cl de vin blanc sec	\N
372	Crème fraîche	\N
373	Safran	\N
374	4 raies moyennes	\N
375	20 amandes fraîches (à défaut, sèches mais trempées dans le lait 24 h et épluchées)	\N
376	feuille de basilic vert géant d'Italie, des feuilles de basilic violet, des feuilles de basilic thaï (facultatif)	\N
377	50cl de beurre frais	\N
378	Lime (citron vert) de taille moyenne	\N
379	1cuillère à café d'Alcool blanc (pas trop parfumé)	\N
380	1l de bouillon de poisson	\N
381	50cl de lait	\N
382	Poivre du moulin	\N
383	2 raies assez épaisses	\N
384	1gros bouquet de herbes fraîches(ciboulette, persil, estragon...)	\N
385	20cl de vinaigre	\N
386	4 raies surgelées ou fraîches	\N
387	Crème fraîche légère ou non	\N
388	1verre de lait	\N
389	Curry en poudre	\N
390	1botte de poireau	\N
391	2 raies (eniron 400 g chacune)	\N
392	4tranches de jambon cru italien	\N
393	1 chicorée	\N
394	4cuillères à soupe de farine	\N
395	4 raies congelées	\N
396	1bocal de câpres (90 g)	\N
397	1kg de pomme de terre	\N
398	1l de bouillon minimum (quantité selon proportion d'ailes de raie)	\N
399	3cuillères à soupe de farine	\N
400	1kg de morue dessalée	\N
401	5 carottes	\N
402	6 pommes de terre moyennes	\N
403	4 oeufs	\N
404	300g de haricot vert extra-fins frais (ou surgelés)	\N
405	3 betteraves	\N
406	Bulot et des crevettes (facultatif)	\N
407	1 fenouil (facultatif)	\N
408	Huile d'arachide	\N
409	4gousses d'ail écrasées (pilées)	\N
410	1grosse cuillère à café de moutarde	\N
411	Vinaigre	\N
412	750g d'oie	\N
413	400g de salsifis	\N
414	500g de carotte	\N
415	6 cèpes	\N
416	300g de marrons	\N
417	1verre de bouillon de viande	\N
418	1cuillère à soupe de graisse d'oie	\N
419	Persil	\N
420	300g de tomme fraîche d'Auvergne	\N
421	500g de pomme de terre farineuses	\N
422	300g de Chair d'alligator	\N
423	360g de riz créole	\N
424	1.5 court-bouillon	\N
425	30g de beurre	\N
426	1 tomate moyenne	\N
427	1/2dl de vin blanc	\N
428	1pincée de farine	\N
429	2brins de persil	\N
430	Poivre de Cayenne moulu	\N
431	1 alose d'environ 1,2 kg (ou autre gros poisson blanc)	\N
432	100g d'oseille	\N
433	2cuillères à soupe de crème	\N
434	1tranche de pain de mie	\N
435	1tranche de lard de poitrine	\N
436	Chapelure	\N
437	4 escalopes de veau	\N
438	4 oeufs durs	\N
439	4tranches de jambon blanc ou fumé	\N
440	2cuillères d'huile	\N
441	25cl de vin blanc	\N
442	Champignon	\N
443	Oignon	\N
444	Ail	\N
445	20 pommes de terre variété amandines	\N
446	1boîte de 400 g de thon	\N
447	Crème fraîche épaisse	\N
448	1botte de ciboulette	\N
449	1 ananas	\N
450	4cuillères à soupe de Rhum Ambré LA MARTINIQUAISE	\N
451	4cuillères à soupe de lait de coco	\N
452	4cuillères à soupe de noix de coco rapée	\N
453	2cuillères à soupe de cassonade	\N
454	Anchois frais	\N
455	Vinaigre blanc	\N
456	6 andouillette	\N
457	1kg d'oignon	\N
458	1bouteille de vin blanc sec	\N
459	Gingembre	\N
460	Cannelle	\N
461	1 andouille	\N
462	500g de haricot blancs	\N
463	2 carottes	\N
464	2verres de vin blanc sec	\N
465	Bouquet garni	\N
466	750g d'andouille de Vire	\N
467	600g de pomme de terre	\N
468	40cl de crème liquide	\N
469	1pincée de muscade	\N
470	1 andouille coupée en morceaux	\N
471	2 oignons en lamelles	\N
472	1petite boîte de champignon de Paris (pieds et morceaux)	\N
473	25cl de crème liquide	\N
474	4 andouillette (de Cambrai de préférence)	\N
475	16cl de crème liquide	\N
476	3cuillères à soupe de chicorée liquide	\N
477	4 andouillette	\N
478	300g de poireau	\N
479	Vin blanc	\N
480	4 échalotes	\N
481	500g de champignon de Paris	\N
482	1/4bouteille de vin blanc	\N
483	1gros pot de crème fraîche	\N
484	1pot de crème fraîche	\N
485	2 citrons	\N
486	2 andouillette	\N
487	Crème fraîche légère	\N
488	Vin blanc sec	\N
489	Graines de coriandre	\N
490	Piment de Cayenne	\N
491	Mélange 5 baies	\N
492	4 andouillette de Troyes	\N
493	Moutarde forte de Dijon	\N
494	Endive	\N
495	Vinaigre balsamique	\N
496	150g de lentilles	\N
497	1 carotte	\N
498	50cl d'eau	\N
499	2 feuilles de laurier	\N
500	Pomme de terre	\N
501	3 andouillette de Troyes	\N
502	1 oignon jaune	\N
503	800g d'anguille	\N
504	5cuillères à soupe d'huile	\N
505	1botte de persil	\N
506	12tranches de bacon (0,5 cm d'épaisseur)	\N
507	120g de farine	\N
508	120g de chapelure	\N
509	1 oeuf	\N
510	5cl d'eau	\N
511	1 anguille de 1 kg	\N
512	1bouquet de persil	\N
513	4cuillères à soupe d'huile	\N
514	40cl de purée ou coulis de tomates	\N
515	1cuillère à soupe de citron haché	\N
516	250g de farine	\N
517	170g d'amandes moulues	\N
518	250g de sucre en poudre	\N
519	170g de beurre	\N
520	1 oeuf + 1 jaune	\N
521	2cuillères à soupe de rhum	\N
522	1 blanc d'oeuf	\N
523	Cassonade	\N
524	1kg d'encornet	\N
525	600g de tomate concassée (2 boites)	\N
526	500g de calamar congelés	\N
527	2 oignons blancs	\N
528	4 tomates mûres	\N
529	1 poivron vert	\N
530	1 persillade	\N
531	3gousses d'ail dans leur peau	\N
532	1pincée de piment	\N
533	1pincée d'épices à tagine	\N
534	1verre de vin blanc	\N
535	500g de pâtes (genre crozets)	\N
536	1 oignon hâché	\N
537	1 ail hâché	\N
538	1tasse d'huile d'olive	\N
539	200g de fromage râpé	\N
540	500g de lotte	\N
541	250g de maquereau	\N
542	250g de saurel	\N
543	250g de grondin	\N
544	250g de rascasse	\N
545	250g de crevette	\N
546	150g de chou	\N
547	4 pommes de terre	\N
548	400g de riz	\N
549	2 piments séchés	\N
550	4gousses d'ail	\N
551	2.5poignées de riz par personne	\N
552	1 banane (facultatif)	\N
553	20cl de tomate	\N
554	4 artichauts	\N
555	5 pommes de terre moyennes	\N
556	1barquette de lardons fumés	\N
557	1 reblochon	\N
558	9 coeurs d'artichaut	\N
559	8cl d'huile d'olive vierge	\N
560	1 jus de citron	\N
561	2gousses d'ail finement émincées	\N
562	12 artichauts violets	\N
563	6 carottes	\N
564	4 carré d'agneau	\N
565	100g de lardons fumés	\N
566	/branche de persil	\N
567	3cuillères d'huile d'olive	\N
568	8 artichauts à feuilles pointues	\N
569	1cuillère à soupe de persil	\N
570	3cuillères à soupe de chapelure	\N
571	8tranches de lard fines	\N
572	200g de jambon cuit	\N
573	150g de champignon	\N
574	4filets d'anchois à l'huile	\N
575	1poignée d'épinard	\N
576	3 oeufs (1 jaune d'oeuf cru + 2 oeufs durs)	\N
577	1 barde de lard	\N
578	20cl de vin blanc	\N
579	demi de citron	\N
580	2 pommes de terre	\N
581	75g de lard maigre	\N
582	25g de beurre	\N
583	35cl de bouillon	\N
584	2 artichauts	\N
585	200g de lardons	\N
586	Pignons de pin	\N
587	Eau	\N
588	1.2kg d'asperge blanches	\N
589	20cl de vin blanc d'alsace du style sylvaner ou pinot blanc	\N
590	15cl de crème liquide	\N
591	60g de fromage râpé	\N
592	Curry	\N
593	16 asperges	\N
594	1sachet de thé	\N
595	400g de fruits rouges	\N
596	400g de coquillettes "environ"	\N
597	1botte d'asperge	\N
598	50g de lardons	\N
599	15cl de crème fraîche	\N
600	1tranche de jambon blanc	\N
601	10 asperges	\N
602	1 oignon rouge	\N
603	100g de feta ou fromage de chèvre frais	\N
604	1filet de miel	\N
605	g de boeuf	\N
606	600g d'asperge blanches	\N
607	2 jaunes d'oeuf crus	\N
608	3 petits suisses	\N
609	15cl d'huile	\N
610	1cuillère à soupe de jus de citron	\N
611	1cuillère à soupe de cerfeuil haché	\N
612	400g de poisson (vous pouvez utiliser des tranches de poisson congelées)	\N
613	60g de câpres	\N
614	24g de gelée (1 sachet)	\N
615	130g de pâtes farfalle	\N
616	2 courgettes (jaunes de préférence)	\N
617	2cuillères à café de persil frais	\N
618	50g de salade roquette	\N
619	10 tomates cerise (de préférence rouges et jaunes pour la couleur)	\N
620	Parmesan	\N
621	1tranche de saumon fumé	\N
622	1 pomme de terre (selon la grosseur)	\N
623	Salade	\N
624	200g de lardons fumés	\N
625	20cl de crème fraîche liquide semi-épaisse	\N
626	4 aubergines	\N
627	1kg de tomate mûres	\N
628	1/2 oignon moyen	\N
629	100g de lardons ou 150 g de chair à saucisse	\N
630	1cube de bouillon	\N
631	3cuillères à soupe d'huile de tournesol	\N
632	5feuilles de persil ,	\N
633	1 aubergine	\N
634	20cl de sauce bolognaise	\N
635	75g de parmesan râpé	\N
636	2 tomates bien mûres	\N
637	1boîte de tomates pelées (425 ml soit environ 400 g)	\N
638	4 champignons de Paris frais	\N
639	100g de feta écrasée	\N
640	1gousse d'ail écrasée	\N
641	Basilic	\N
642	1 aubergine par personne	\N
643	300g de parmesan rapé	\N
644	500ml de coulis de tomate	\N
645	Poivre du moulin, épices pour spaghetti	\N
646	1 jaune d'oeuf	\N
647	Gruyère râpé	\N
648	2 aubergines bien brillantes	\N
649	100g de gruyère râpé	\N
650	Persil haché	\N
651	200g de pomme de terre	\N
652	1cuillère à soupe de fromage blanc	\N
653	2 tomates séchées	\N
654	1cuillère à soupe de pesto	\N
655	1poignée de noisettes	\N
656	100g de fromage blanc	\N
657	50g d'oignon émincés	\N
658	1cuillère à soupe de pignons	\N
659	50g de surimi	\N
660	2cuillères à café de concentré de tomates	\N
661	1tranche de fromage à raclette ou gruyère râpé	\N
662	3 aubergines	\N
663	2paquets de mozzarella	\N
664	500g de tomate italiennes en conserve	\N
665	Origan	\N
666	1tranche de roquefort (un premier prix suffira)	\N
667	100g de noix (environ)	\N
668	1petite boîte de concentré de tomates	\N
669	1 aubergine assez grosse	\N
670	1 tomate	\N
671	1/2 oignon	\N
672	1l d'huile de friture	\N
673	1pincée de cumin antillais	\N
674	1/2cuillère à café de piment en poudre	\N
675	1/2cuillère à café de coriandre en poudre	\N
676	1pincée de garam massala	\N
677	1cuillère à soupe d'huile	\N
678	1cuillère à soupe d'eau	\N
679	1/2cuillère à café de sel	\N
680	3cuillères à soupe de fromage blanc	\N
681	4 tomates séchées	\N
682	6 amandes	\N
683	1cuillère à soupe de raisins secs	\N
684	2 oignons rouges	\N
685	4 aubergines (700 g)	\N
686	2 poivrons verts (400 g)	\N
687	50g de pignons de pin ou de pistaches non salées	\N
688	Anis sauvage	\N
689	1kg de chair à saucisse	\N
690	1 poivron	\N
691	1grosse boîte de tomate épluchées	\N
692	1sachet de riz	\N
693	2 aubergines longues	\N
694	200g d'agneau ou chair à saucisse	\N
695	1verre de coulis de tomate	\N
696	2gousses d'ail dégermées	\N
697	40g de raisins secs	\N
698	1cuillère à café de cumin	\N
699	1pincée de piment en poudre	\N
700	Menthe ou coriandre fraîche	\N
701	1cuillère à soupe de chapelure	\N
702	500g de viande hachée	\N
703	2 oignons moyens	\N
704	1/2l de purée de tomate	\N
705	4cuillères à soupe de chapelure	\N
706	goutte de sauce soja et de jus de citron, herbes de Provence, piment doux ou autres épices selon les goûts	\N
707	200g de boeuf haché	\N
708	350g de tomates pelées (petite boîte)	\N
709	180g de feta	\N
710	Menthe fraîche	\N
711	Sucre en poudre	\N
712	2 courgettes biologiques	\N
713	3 oignons moyens	\N
714	2poignées de pignons de pins	\N
715	1petite boîte de ratatouille (environ 250 g)	\N
716	1cuillère à soupe de poudre d'amande	\N
717	250g de ricotta	\N
718	2cuillères à soupe de chapelure fine	\N
719	50g de parmesan râpé	\N
720	350g de viande hachée (bœuf ou porc)	\N
721	2 pommes de terre (coupées en dés ou surgelées, c'est la même chose)	\N
722	épices (au besoin..)	\N
723	2cuillères à soupe de vin blanc à cuisiner	\N
724	1boule de mozzarella	\N
725	1cuillère à soupe de citron	\N
726	1 blanc de poulet (sous vide)	\N
727	200g de chair à saucisse	\N
728	1petite boîte de champignon de Paris émincés	\N
729	10cl de lait	\N
730	10cl d'eau	\N
731	Gruyère râpé (facultatif)	\N
732	1pincée d'ail semoule	\N
733	1pincée de basilic	\N
734	250g de riz	\N
735	80g de raisin de Smyrne	\N
736	15g de sucre en poudre	\N
737	20g de beurre	\N
738	100g de chair à saucisse (ou rien, si vous ne voulez qu'avec des légumes)	\N
739	Condiments (sel, poivre, persil, ail, basilic, huile d'olive...)	\N
740	25g de gruyère râpé ...	\N
741	4cuillères à soupe de fromage blanc	\N
742	5 aubergines	\N
743	100g de pain de mie (5 petites tranches)	\N
744	6cuillères à soupe d'huile d'olive	\N
745	Fromage (comté, mozzarella, chèvre...)	\N
746	6 aubergines	\N
747	6 oignons moyens	\N
748	5 tomates dont une pour le décor	\N
749	1 poivron (décor)	\N
750	1verre de moutarde d'huile d'olive	\N
751	1/2l d'huile de friture	\N
752	40cl de sauce tomate maison (coulis avec herbes de Provence)	\N
753	1sachet de gruyère râpé	\N
754	400g de feta	\N
755	4cuillères à café de chapelure	\N
756	1pincée de coriandre	\N
757	400g de mouton	\N
758	100g de parmesan	\N
759	4 courgettes	\N
760	Aubergine	\N
761	150g de jambon de parme en tranches fines	\N
762	200g de mozzarella	\N
763	3cuillères à soupe de parmesan fraîchement râpé	\N
764	250g de pulpe de tomate	\N
765	10cl d'huile	\N
766	2pincées de sucre	\N
767	200g de brousse	\N
768	40g de parmesan râpé	\N
769	Ciboulette	\N
770	Pignons (facultatifs)	\N
771	4 aubergines (800 g)	\N
772	60g de fourme de montbrison (ou St Nectaire)	\N
773	25g de farine	\N
774	25cl de lait	\N
775	4feuilles de brick	\N
776	2 courgettes	\N
777	1 poivron rouge	\N
778	100g de lardons allumettes	\N
779	1branche de persil	\N
780	1 oeuf moyen	\N
781	60g de feta	\N
782	50g de crème liquide	\N
783	8filets de rouget	\N
784	4 poireaux	\N
785	4cuillères à soupe de fumet de poisson (poudre)	\N
786	10cl d'huile d'olive	\N
787	10cl de crème fraîche	\N
788	Pâte à crêpe (moitié farine blanche, moitié sarrasin)	\N
789	3 tomates pas trop mûres	\N
790	200g de champignon de Paris	\N
791	250g de boeuf haché	\N
792	1bouquet de ciboulette	\N
793	12 crêpes	\N
794	500g de pomme	\N
795	1cuillère à café de maïzena	\N
796	4cuillères à soupe de Calvados BUSNEL	\N
797	3cuillères à soupe de sucre	\N
798	1cuillère à café de cannelle	\N
799	4tranches de jambon cru	\N
800	2 carré frais	\N
801	2 poires bien mûres mais encore fermes	\N
802	Pruneau	\N
803	Datte et raisins secs	\N
804	2 vinaigre balsamique	\N
805	Beurre salé	\N
806	Ficelle alimentaire ou cure-dents	\N
807	8feuilles de brick	\N
808	1 blette (le vert)	\N
809	50g de lardons natures	\N
810	50g d'emmental râpé	\N
811	2 oeufs entiers	\N
812	150g de farine	\N
813	35cl de lait	\N
814	1cuillère à soupe de Cognac COURCEL	\N
815	1cuillère à café d'huile	\N
816	1gousse de vanille	\N
817	100g de chocolat	\N
818	20 noix de St jacques	\N
819	4feuilles de brique	\N
820	100g de julienne de légumes (surgelée c'est plus rapide)	\N
821	1cuillère à soupe de vin blanc ou d'armagnac	\N
822	200g de riz basmati	\N
823	4 persil et 4 branches de persil assez longues	\N
824	1cuillère à soupe d'huile de mais	\N
825	300g d'autruche	\N
826	20cl de vin rouge	\N
827	3cuillères à soupe de crème fraîche	\N
828	1/3cuillère à café de curry rouge	\N
829	1/3cuillère à café de curry jaune	\N
830	10 champignons shiitakés	\N
831	1grosse cuillère à soupe de farine	\N
832	400g de viande d'autruche (filet)	\N
833	Courgette	\N
834	Herbes diverses (thym, basilic, laurier...), ail, oignon	\N
835	1reste de pâtes de la veille	\N
836	2petites boîtes de champignon de Paris	\N
837	20cl de crème liquide à 15% de MG	\N
838	2cuillères à soupe d'oignon hachés	\N
839	Sel selon le goût	\N
840	2 avocats	\N
841	200g de Volailles ou de rôti (porc ou veau)	\N
842	4cuillères à soupe de mayonnaise	\N
843	1kg de viande (bœuf à braiser type bourguignon, agneau à braiser type poitrine collier, cuisses de poulet)	\N
844	1kg de semoule de blé dur moyenne (à ne pas confondre avec le couscous)	\N
845	2 navets	\N
846	1boîte de cardon ou de haricots (bocal d'1 litre)	\N
847	1boîte de pois chiches (370 g)	\N
848	1 courgette	\N
849	1bouquet de menthe	\N
850	1petit bouquet de coriandre et de persil frais	\N
851	1branche de céleri (facultatif)	\N
852	4cuillères à café de concentré de tomates	\N
853	5cuillères à café de paprika	\N
854	2cuillères à café de Ras el Hanout	\N
855	Huile classique (tournesol ou autre)	\N
856	150g de sucre en poudre	\N
857	1sachet de levure chimique	\N
858	3cuillères à soupe de lait	\N
859	3 oeufs	\N
860	1/4l d'eau	\N
861	1/4l de sirop de sucre de canne	\N
862	10cl de Rhum Ambré LA MARTINIQUAISE	\N
863	500g de morue	\N
864	500g de pomme de terre	\N
865	3 ail	\N
866	10 oeufs	\N
867	Feuille de laurier	\N
868	Olives noires	\N
869	3pavés de morue (600 g environ)	\N
870	6 oeufs	\N
871	4cuillères à soupe d'huile de tournesol	\N
872	12 pommes de terre	\N
873	500g d'échine de porc	\N
874	500g de gîte de bœuf	\N
875	500g d'épaule d’agneau	\N
876	1 queue de porc (facultatif)	\N
877	1.5kg de pomme de terre	\N
878	250g d'oignon	\N
879	1/2 vin blanc d’Alsace (Riesling ou Sylvaner)	\N
880	1 oignon piqué de clous de girofle	\N
881	8tranches épaisses de bacon	\N
882	1cuillère à soupe de persillade	\N
883	1pincée de herbes de Provence	\N
884	4tranches de Blanc de Poulet Nature conservation sans nitrite HERTA®️	\N
885	20feuilles de menthe	\N
886	150g de fromage de chèvre frais nature	\N
887	4 bagels	\N
888	1/2 oignon rouge	\N
889	2 pains bagels achetés surgelé ou faits maison (voir cette bonne recette de bagel)	\N
890	1bocal de poivron rouge	\N
891	60g de fromage frais ail et fines herbes (type Boursin ou Tartare)	\N
892	4 anchois marinés à l'huile	\N
893	1 poivron de chaque couleur (vert, jaune, rouge)	\N
894	200g de champignon frais	\N
895	Basilic frais	\N
896	2/3 baguette (selon la gourmandise de chacun)	\N
897	200g de gruyère râpé	\N
898	1 tube de Lait Concentré Sucré à pâtisser NESTLE 170 g	\N
899	50g de pistache non salées	\N
900	50g de graines de lin	\N
901	100g de flocons d'avoine	\N
902	1 avocat	\N
903	4 escalopes de poulet	\N
904	2cubes de bouillon cube de légume	\N
905	1kg de crevette fraîches	\N
906	1paquet de pâtes jaunes chinoises (environ 750 g)	\N
907	Sauce soja	\N
908	1 chou vert	\N
909	500g de haricot frais	\N
910	3 blancs de poulet	\N
911	Persil chinois	\N
912	3brins d'oignon verts	\N
913	Huile de tournesol	\N
914	1 banane	\N
915	1petit sachet de gruyère râpé	\N
916	2tranches de jambon	\N
917	Moutarde	\N
918	4 bananes	\N
919	Banane	\N
920	Banane jaunes, mûres ou pas)	\N
921	4tranches de jambon bas de gamme, genre épaule	\N
922	1demi pot de crème fraîche ou 20 cl de crème liquide	\N
923	1/2verre d'eau ou de lait	\N
924	3grosses cuillères à soupe de moutarde à l'estragon (par pitié n'utilisez QUE de la moutarde à l'estragon, ca ne rend rien avec une autre moutarde!!!)	\N
925	Riz	\N
926	4tranches de bacon	\N
927	100g de raisins secs	\N
928	10cl de rhum	\N
929	4filets de bar (environ 150 g par personne)	\N
930	150g de feta	\N
931	2 pommes (type golden)	\N
932	4filets de bar	\N
933	1/4l de cidre sec	\N
934	75cl de fumet de poisson	\N
935	1 céleri	\N
936	150g de crème	\N
937	1 fécule	\N
938	2 échalotes hachées fin	\N
939	1.5dl de vin blanc	\N
940	1/2dl de vinaigre de vin blanc	\N
941	200g de beurre froid coupé en dés	\N
942	Citron	\N
943	50g de caviar (vrai caviar ou oeufs de saumon)	\N
944	Légume verts au choix (petits pois, haricots, asperges, etc )	\N
945	2 bars de ligne	\N
946	1cuillère à café de vanille en poudre ou 2 gousses de vanille	\N
947	1/2cuillère à café d'ail semoule	\N
948	20cl de noilly prat ou du vin blanc sec	\N
949	15cl de crème fraîche entière	\N
950	Bar de 800 g environ	\N
951	3 carottes	\N
952	1 poireau	\N
953	500g de champignon frais	\N
954	10cl de crème fleurette	\N
955	1verre de vin blanc sec (12 cl environ)	\N
956	4 bars évidés et sans arêtes (250/300g)	\N
957	4tranches de truite fumée	\N
958	50g d'échalote (surgelée ou 5 échalotes)	\N
959	Aneth (quantité selon vos goûts)	\N
960	75g de fromage à la crème (type Elle&Vire)	\N
961	12.5cl de crème liquide	\N
962	Poivre (préférence 5 baies)	\N
963	1 bar portion par personne ou à défaut, un gros bar pour plusieurs convives	\N
964	Moutarde forte	\N
965	Romarin frais	\N
966	1 bar de 1,6 kg	\N
967	6 oignons blancs et leur tige verte	\N
968	1 fenouil	\N
969	1cuillère à café de noisettes en poudre	\N
970	2cuillères à soupe de raisin de Smyrne	\N
971	1/2cuillère à café de cannelle en poudre	\N
972	1/2cuillère à café de gingembre et de cumin en poudre	\N
973	1pincée de muscade râpée	\N
974	8grains de poivre noir	\N
975	2branches de persil	\N
976	800g de bar	\N
977	1 court-bouillon	\N
978	25cl de béchamel	\N
979	1/2pot de câpres	\N
980	1 bar (ou un loup) de 1,3 kg ou 2 bars de 800 g chacun	\N
981	1/2bouquet de persil plat	\N
982	2brins de fenouil ou 1 pincée de graines de fenouil	\N
983	1cuillère à café d'huile d'olive	\N
984	50g de gros sel gris de Guérande	\N
985	1 bar de 1,2 kg	\N
986	5cuillères d'huile	\N
987	1 bar entier de 1,2kg (vidé et préparé par le poissonnier)	\N
988	6bulbes de fenouil	\N
989	Graine de fenouil  4 à 5 cuillères à café	\N
990	Curcuma en poudre, 2 cuillères à café	\N
991	Bouillon de volaille  1 cub	\N
992	1 bar de 2 kg vidé	\N
993	2cuillères à soupe d'estragon haché	\N
994	2cuillères à soupe de cerfeuil haché	\N
995	2 barbue de 1 kg	\N
996	25cl de cidre sec	\N
997	Jus de cuisson du poisson	\N
998	200g de crème fraîche	\N
999	400g de crevette cuites et décortiquées	\N
1000	600g de barbue (4 morceaux de 150 g chacun)	\N
1001	120g de riz ( basmati c'est mieux!)	\N
1002	2bottes de radis roses	\N
1003	5cl de crème légère liquide	\N
1004	50g de fromage blanc à 0%	\N
1005	1cuillère à soupe de maïzena	\N
1006	4filets de barbue	\N
1007	2brins de thym	\N
1008	20 olives noires	\N
1009	350g de riz basmati	\N
1010	75cl d'eau	\N
1011	1 bouillon cube de légumes	\N
1012	1 poivron jaune	\N
1013	1kg de lotte	\N
1014	1bouquet de persil plat	\N
1015	1 queue de lotte	\N
1016	3gousses d'ail (3 à 4 gousses)	\N
1017	Vin blanc sec (1/ à 1 verre)	\N
1018	1boîte de coulis de tomate	\N
1019	reste de pomme de terre cuites	\N
1020	125g de lardons	\N
1021	100g de gruyère ou de comté	\N
1022	Poivre et muscade	\N
1023	2 bavettes	\N
1024	15g de beurre	\N
1025	15cl de crème épaisse	\N
1026	1cuillère à soupe de fond de veau	\N
1027	50cl de vin rouge	\N
1028	4 bavettes	\N
1029	8 échalotes	\N
1030	Margarine	\N
1031	Fond de sauce	\N
1032	1.4kg de bavette	\N
1033	3 oignons nouveaux	\N
1034	3 échalotes	\N
1035	110g de beurre	\N
1036	3 bécasses	\N
1037	1barquette de groseille	\N
1038	9fines tranches de lard fumé	\N
1039	1 raisin muscat	\N
1040	50cl de bouillon de boeuf	\N
1041	Cognac	\N
1042	30cl de vin rouge (Gamay)	\N
1043	3/4l de béchamel chaude	\N
1044	1/10l de lait	\N
1045	1morceau de beurre	\N
1046	1dl de vin blanc	\N
1047	10g de levure de boulanger fraîche	\N
1048	4g de sel	\N
1049	50g de beurre mou	\N
1050	35g de sucre en poudre	\N
1051	100ml de lait entier	\N
1052	gousse de vanille	\N
1053	zeste de citron	\N
1054	150g de Nutella®	\N
1055	Huile pour la friture	\N
1056	Sucre glace pour décorer	\N
1057	1kg de farine	\N
1058	1 yaourt nature	\N
1059	1pot de yaourt ) d'eau tiède	\N
1060	2barquettes de feta	\N
1061	2 fromages de chèvre	\N
1062	2cuillères à soupe de farine	\N
1063	1filet de truite (environ 300g)	\N
1064	5 graines de coriandre	\N
1065	10 cumin	\N
1066	1cuillère à soupe de farine	\N
1067	250g de viande de bison	\N
1068	1 échalote hachée	\N
1069	1cuillère à soupe d'estragon haché	\N
1070	1 oeuf + 1 jaune d'oeuf	\N
1071	5cl de porto	\N
1072	2 poireaux	\N
1073	20cl de crème liquide	\N
1074	200g de flocons d'avoine secs (ou autres céréales)	\N
1075	branche de persil (frais)	\N
1076	épices au choix	\N
1077	1 brique de coulis de tomate (30 cl)	\N
1078	1 sucre	\N
1079	1kg de bernique	\N
1080	1brin de persil	\N
1081	2 betteraves rouges crues	\N
1082	Pomme de terre ou pommes	\N
1083	1cuillère à soupe de vinaigre de vin	\N
1084	500g de fromage blanc	\N
1085	Poivre moulu	\N
1086	Fines herbes (persil, ciboulette)	\N
1087	Ail finement haché	\N
1088	1 oignon haché finement	\N
1089	800g de faux-filet de bœuf	\N
1090	4cuillères à soupe de vinaigre	\N
1091	1cuillère à soupe de curry	\N
1092	2 beef steak	\N
1093	20g de margarine	\N
1094	2cuillères à soupe de vinaigre	\N
1095	Maïzena roux (la quantité dépend de votre consistance de votre sauce)	\N
1096	1 chou rouge	\N
1097	200g de cèpes	\N
1098	3 gingembre frais	\N
1099	5 ail	\N
1100	500g de viande de dinde ou de poulet	\N
1101	250 saucisses fraîche CHORIZO	\N
1102	50g de concentré de tomates	\N
1103	2verres de schnaps	\N
1104	Brandy	\N
1105	3cuillères d'huile d'olive NON vierge	\N
1106	1 bimi (ou broccolini)	\N
1107	1cuillère à soupe de sésame	\N
1108	1cuillère à café de graines de coriandre	\N
1109	3cuillères à soupe de vinaigre de vin blanc ou balsamique	\N
1110	350g de carotte	\N
1111	50ml d'huile d'olive	\N
1112	1/2sachet de levure	\N
1113	Poivre et noix de muscade râpée	\N
1114	100g de raisins secs (gros grains de muscat de préférence)	\N
1115	20cl de crème liquide entière	\N
1116	200g de poudre de noisette	\N
1117	200g de sucre glace	\N
1118	200g de beurre	\N
1119	240g de farine	\N
1120	20g d'oeuf (la moitié d’un œuf entier battu)	\N
1121	pincée de sel	\N
1122	100g de Nutella®	\N
1123	140g de sucre glace	\N
1124	260g de beurre mou	\N
1125	440g de farine	\N
1126	210g de Nutella®	\N
1127	90g de farine	\N
1128	35g de poudre de noisette	\N
1129	75g de beurre	\N
1130	50g de sucre glace	\N
1131	Noisettes concassées	\N
1132	5 jaunes d'oeuf	\N
1133	50g de farine	\N
1134	50g de fécule de maïs ou de pomme de terre	\N
1135	100g de poudre de noisette	\N
1136	50g de beurre fondu	\N
1137	Sucre pour décorer	\N
1138	2 blancs d'oeuf	\N
1139	160g de sucre glace	\N
1140	2cuillères à café de cannelle	\N
1141	300g de poudre de noisette	\N
1142	120g de Nutella®	\N
1143	250g de beurre mou	\N
1144	150g de sucre glace	\N
1145	500g de farine	\N
1146	200g de Nutella®	\N
1147	4 blancs de dinde	\N
1148	150ml de lait de coco	\N
1149	1bocal d'ananas au naturel avec son jus	\N
1150	2 oignons blancs ou jaunes	\N
1151	10 noixs de cajou non salées	\N
1152	Pavot et sésame	\N
1153	Poivre 5 baies	\N
1154	4 blancs de poulet	\N
1155	6tranches d'ananas en boîte + 8 cuillères à soupe de jus	\N
1156	8cuillères à soupe de vinaigre balsamique	\N
1157	2 blancs de poulet	\N
1158	1grosse boîte de champignon de Paris (800 g)	\N
1159	Curcuma	\N
1160	Sel au cèleri	\N
1161	Fenugreek	\N
1162	100g de parmesan râpé	\N
1163	goutte de tabasco	\N
1164	6 blancs de poulet	\N
1165	6cuillères à soupe de farine	\N
1166	Poivre (mélange de baies)	\N
1167	1cuillère à café de moutarde	\N
1168	Champignon selon votre convenance	\N
1169	50cl de bouillon de volaille ou de légumes (fera très bien l'affaire)	\N
1170	1cuillère à café de concentré de tomates	\N
1171	Crème fraîche selon votre convenance	\N
1172	400g de blanc de poulet	\N
1173	10tranches de lard fines	\N
1174	125g de lardons fumés	\N
1175	1 endive	\N
1176	1petite boîte de champignon émincés	\N
1177	4cuillères à soupe de crème fraîche	\N
1178	1cuillère à soupe de cognac	\N
1179	4cuillères à café de persil	\N
1180	Sel et poivre	\N
1181	1boîte de champignon de Paris	\N
1182	2noix de beurre	\N
1183	1pot de sauce tomate nature	\N
1184	15 olives noires	\N
1185	2cuillères à café de cumin	\N
1186	3feuilles de basilic	\N
1187	2 escalopes de poulet	\N
1188	200g de flageolet	\N
1189	200g de champignon de Paris émincés	\N
1190	Poivre gris	\N
1191	8tranches de lard très fines	\N
1192	150g de courgette	\N
1193	150g de carotte	\N
1194	2cuillères à café de mélange 5 baies moulues	\N
1195	3cuillères à café de tapenade	\N
1196	Beurre pour le plat	\N
1197	1boîte d'ananas	\N
1198	2cuillères à café de coriandre moulue	\N
1199	1pincée de piment moulu	\N
1200	Coriandre pour la déco	\N
1201	2pincées de sucre poudre	\N
1202	2cuillères à soupe de siave	\N
1203	1cuillère à soupe de huître	\N
1204	1cuillère à soupe de manioc	\N
1205	4 escalopes de poulet bien épaisses	\N
1206	4tranches de fromage fondu (à croque-monsieur par exemple)	\N
1207	4cuillères à soupe de crème	\N
1208	60cl de crème fraîche liquide	\N
1209	40cl de fond de volaille	\N
1210	1 échalote coupée	\N
1211	3branches d'estragon	\N
1212	2cuillères à soupe de citron	\N
1213	Beurre (pour faire revenir le poulet)	\N
1214	2 bouillon cube	\N
1215	15cl d'huile de tournesol	\N
1216	4cuillères à soupe de sauce soja	\N
1217	1botte d'oignon nouveau	\N
1218	1bouquet de coriandre frais	\N
1219	Lait	\N
1220	Blanc de poulet	\N
1221	20cl de crème fraîche liquide	\N
1222	1boîte de champignon	\N
1223	8 escalopes de poulet	\N
1224	50cl de crème fraîche épaisse	\N
1225	400g de champignon frais	\N
1226	500g de comté	\N
1227	1 boursin "ail et fines herbes"	\N
1228	250g de riz blanc	\N
1229	825g de poire au sirop	\N
1230	1cuillère à café de graines d'anis (on peut les trouver en pharmarcie)	\N
1231	2bulbes de fenouil	\N
1232	3 pastis	\N
1233	20cl de crème fraîche épaisse	\N
1234	Piment doux en poudre	\N
1235	100g de gruyère suisse	\N
1236	4 morilles fraîches ou réhydratées	\N
1237	4feuilles de sauge	\N
1238	2tranches de jambon de parme	\N
1239	125g de mozzarella	\N
1240	1.3kg de blanc de poulet	\N
1241	500g de champignon	\N
1242	25cl de crème fraîche	\N
1243	7gousses d'ail	\N
1244	4 escalopes de poulet de 150 g pièce	\N
1245	100g d'amandes émondées	\N
1246	2cuillères à soupe de poudre d'amande	\N
1247	2cuillères à soupe de miel d'acacia	\N
1248	1bouquet de brocoli	\N
1249	1boîte de lait de coco	\N
1250	1 bouillon cube "poule"	\N
1251	Amandes effilées ou des graines de sésame	\N
1252	2 blancs de poulet ou 2 cuisses de poulet	\N
1253	4 coeurs d'artichaut (surgelés)	\N
1254	100g de lardons	\N
1255	2 champignons de Paris frais	\N
1256	1 oignon nouveau	\N
1257	4 tomates sechées	\N
1258	1cuillère à soupe de sauce worcestershire	\N
1259	4 endives	\N
1260	1 miel	\N
1261	5 champignons de Paris frais	\N
1262	2 échalotes ou 1 oignon	\N
1263	40cl de lait de coco ou de crème liquide	\N
1264	600g de blanc de poulet	\N
1265	12cl de vin blanc	\N
1266	150g d'épinard surgelés	\N
1267	100g d'oseille surgelée	\N
1268	Cerfeuil	\N
1269	500g d'oignon nouveau	\N
1270	1bouquet d'estragon	\N
1271	100g de chocolat noir amer rapé (75 %)	\N
1272	50g d'amandes grillées éfilées	\N
1273	150g de crème fleurette	\N
1274	Clou de girofle	\N
1275	1demi de gingembre	\N
1276	1demi de quatre-épices	\N
1277	2 poivrons rouges moyens	\N
1278	100g de cerneau de noix	\N
1279	100g de crème fraîche	\N
1280	100g de fromage double crème (type kiri, ou carré frais)	\N
1281	25cl de bouillon de poule	\N
1282	Piment d'Espelette	\N
1283	1kg de blanc de poulet	\N
1284	200g de tomate confites	\N
1285	20cl de concentré de tomates	\N
1286	3cuillères à café de sauce d'huître	\N
1287	1cuillère à café de piment doux	\N
1288	2cuillères à café de sucre	\N
1289	Roquette	\N
1290	Fromage de chèvre frais	\N
1291	4tranches de jambon fumé	\N
1292	2feuilles de sauge	\N
1293	4cuillères à soupe de bouillon de légumes	\N
1294	2 blancs de poulet pas trop épais	\N
1295	4tranches fines de poitrine fumée	\N
1296	3morceaux de tomate séchée	\N
1297	2feuilles de basilic frais	\N
1298	3 tomates mûres	\N
1299	1verre de vin blanc sec	\N
1300	4fines tranches de jambon de parme	\N
1301	Pesto rouge	\N
1302	Pesto vert	\N
1303	250ml de crème fraîche épaisse	\N
1304	6tranches de pain d'épices	\N
1305	2 figues séchées	\N
1306	4 abricots secs	\N
1307	2 blancs de poulet (de 200 g chacun)	\N
1308	2boîtes de tomate concassée	\N
1309	2bonnes poignées de champignon des bois surgelés	\N
1310	4tranches de blanc de poulet	\N
1311	2 cubes de bouillon de poule dégraissé	\N
1312	2cuillères à café de curry	\N
1313	4cuillères à café de crème fraîche légère (15%)	\N
1314	600g de seiche	\N
1315	800g de potiron	\N
1316	Coriandre en poudre	\N
1317	1kg de seiche (en lamelles c'est mieux)	\N
1318	6 Volailles	\N
1319	3cuillères à soupe de moutarde à l'ancienne	\N
1320	50g d'amandes effilées	\N
1321	75g d'amandes émincées	\N
1322	2cuillères à soupe de coulis de tomate	\N
1323	4 Volailles	\N
1324	800g de trompette de la mort	\N
1325	800g de blanquette de veau (morceaux maigres)	\N
1326	3 courgettes moyennes	\N
1327	2 poivrons	\N
1328	1grosse boîte de tomates pelées	\N
1329	4gousses d'ail épluchées	\N
1330	10tranches de chorizo doux ou fort selon votre goût	\N
1331	épices à l'Espagnole	\N
1332	800g de blanquette de veau extra-maigre	\N
1333	200g de brocoli	\N
1334	125g de champignon	\N
1335	50g de petit pois	\N
1336	1 courgette (pas obligatoire)	\N
1337	50cl de bouillon de poule	\N
1338	2cuillères à soupe de persil	\N
1339	Thym et 2 feuilles de laurier	\N
1340	Liant instantané pour sauce	\N
1341	400g de blanquette de veau maigre	\N
1342	400g de blanquette de veau ou de porc	\N
1343	8 endives (endives)	\N
1344	1tranche de pain	\N
1345	4cuillères à soupe de lait	\N
1346	Poivre ,	\N
1347	1.2kg de blanquette de veau	\N
1348	1 veau	\N
1349	1 bouquet garni (thym, romarin, persil frais, laurier, grains de poivre) enveloppé dans une compresse ficelée	\N
1350	2bottes d'asperge	\N
1351	400g de champignon de Paris	\N
1352	2 carottes émincées	\N
1353	1 oignon piqué d'un clou de girofle	\N
1354	50cl de crème fraîche ou 2 briques de crème liquide ou 2 yaourts	\N
1355	1 épaule d'agneau coupée en morceaux	\N
1356	5gousses d'ail (ou plus si vous aimez !)hachées	\N
1357	2cuillères d'huile d'olive	\N
1358	brin de persil	\N
1359	1 feuille de laurier (fraîche de préférence)	\N
1360	1 épaule d'agneau désossée et coupée en cubes	\N
1361	1 oignon et 1 carotte émincés finement	\N
1362	1/2 jus de citron	\N
1363	1petite branche de menthe fraîche	\N
1364	1tasse à café de crème	\N
1365	1tasse à café de vin blanc	\N
1366	800g de blanquette de dinde	\N
1367	8 champignons frais	\N
1368	1petit verre de vin blanc	\N
1369	1.5kg de dinde (j'ai pris des cuisses que j'ai débité en morceaux)	\N
1370	50g de margarine	\N
1371	180g de champignon	\N
1372	30g de farine	\N
1373	1 cube de bouillon de volaille	\N
1374	400g de blanquette de dinde	\N
1375	100g d'ortie fraîches (l’équivalent d’une essoreuse à salade)	\N
1376	6 escalopes de dinde	\N
1377	3 poireaux	\N
1378	1/2l de bouillon de volaille	\N
1379	2cuillères à soupe de maïzena	\N
1380	4 râbles de lapin fermier	\N
1381	250g de girolle	\N
1382	12 oignons blancs	\N
1383	100g de lardons 1/2 sel	\N
1384	60g de farine	\N
1385	30cl de vin blanc (Riesling)	\N
1386	70g de beurre	\N
1387	20cl de bouillon de volaille	\N
1388	150g de crème fraîche	\N
1389	Poivre blanc	\N
1390	800g de queue de lotte	\N
1391	8 pommes de terre	\N
1392	Champignon de Paris	\N
1393	1cuillère à soupe de fumet de poisson déshydraté	\N
1394	8 carottes	\N
1395	1 blanc de poireaux	\N
1396	4branches de céleri	\N
1397	1 oignon piqué de 5-6 clous de girofle	\N
1398	1cuillère à soupe de pastis (facultatif)	\N
1399	40g de beurre + 40 g de farine	\N
1400	400g de pointe d'asperge	\N
1401	30cl de crème fraîche	\N
1402	400g de lotte en filet	\N
1403	8 noix de saint-jacques	\N
1404	100g de crevette cuites	\N
1405	8 carottes (mini)	\N
1406	3 blancs de poireaux	\N
1407	5 échalotes	\N
1408	15ml de jus de citron	\N
1409	250ml d'eau	\N
1410	3g de fécule de pomme de terre	\N
1411	80ml de champagne	\N
1412	250g de crème fraîche	\N
1413	Baies roses	\N
1414	Graines de lin	\N
1415	6pavés de saumon	\N
1416	60g de beurre	\N
1417	40cl de crème fraîche	\N
1418	30cl de bouillon	\N
1419	800g de saumon frais	\N
1420	1branche de céleri	\N
1421	400g de moules (3 belles poignées)	\N
1422	1.5kg de seiche	\N
1423	2kg de moules	\N
1424	1sachet de court-bouillon pour poisson	\N
1425	200g de crevette décortiquées	\N
1426	250g de champignon de Paris	\N
1427	1dosette de safran	\N
1428	10cl de crème fraîche épaisse	\N
1429	60g de beurre sel poivre	\N
1430	800g de seiche	\N
1431	1l de vin blanc sec	\N
1432	400g de champignon de Paris frais	\N
1433	1.6kg de thon	\N
1434	1brin de céleri	\N
1435	4cuillères à soupe de vin blanc sec	\N
1436	1.5dl de crème fraîche	\N
1437	1.2kg d'épaule de veau (ou tendron, flanchet)	\N
1438	300g de champignon	\N
1439	2 oignons jaunes	\N
1440	200g d'oignon grelots	\N
1441	4 clous de girofle	\N
1442	2branches de thym	\N
1443	brin de cerfeuil	\N
1444	2branches de céleri	\N
1445	70g de farine	\N
1446	2kg de queue de veau (la qeue de veau est une viande absolument pas grasse)	\N
1447	3 étoiles de badiane , en pharmacie	\N
1448	500g de pâtes	\N
1449	1kg de blanquette de veau	\N
1450	1cuillère de farine	\N
1451	Carotte	\N
1452	Fond de veau	\N
1453	800g de blanquette de veau	\N
1454	1 ail entière	\N
1455	2kg de veau	\N
1456	8 champignons de Paris	\N
1457	1.5l de vin blanc	\N
1458	5cl d'huile	\N
1459	1boîte de champignon de Paris (en hiver)	\N
1460	300g de blanquette de veau	\N
1461	100ml de crème liquide	\N
1462	1 cube de bouillon de poulet	\N
1463	Poivre entier	\N
1464	30g de fromage bleu	\N
1465	33cl de bière St Feuillien brune	\N
1466	2 oignons émincés	\N
1467	2gousses d'ail pressées	\N
1468	400g de tomate en dés	\N
1469	1l de vin blanc sec (suffisamment pour recouvrir la viande)	\N
1470	1cuillère à café d'ail moulu	\N
1471	150g de crème fraîche épaisse	\N
1472	1cuillère à soupe de jus de citron vert	\N
1473	50cl de bouillon de volaille	\N
1474	15cl de pineau des charentes	\N
1475	5cl de fond de veau	\N
1476	1 pomme Golden	\N
1477	1 navet	\N
1478	1cuillère à dessert de concentré de tomates	\N
1479	2tranches de pain d'épices	\N
1480	1cuillère à café d'estragon frais	\N
1481	15grains de coriandre	\N
1482	1.2kg de veau	\N
1483	600g de pleurote	\N
1484	800g de veau découpé en cubes	\N
1485	70g d'oignon	\N
1486	70g de carotte	\N
1487	150g de châtaigne cuites	\N
1488	Fond de veau pour la cuisson de la viande	\N
1489	40g de farine	\N
1490	5cl de crème ou liqueur de châtaigne (alcool)	\N
1491	600g d'épaule de veau	\N
1492	4 navets	\N
1493	2 ail	\N
1494	1/4l de vin blanc sec + un verre de vin	\N
1495	1cuillère à soupe de crème fraîche	\N
1496	5grains de coriandre	\N
1497	5 baies de genévrier	\N
1498	1.5kg de blanquette de veau	\N
1499	1bouteille de vin blanc sec de bonne acidité (exemple, Picpoul de Pinet)	\N
1500	Cornichon	\N
1501	Maïzena	\N
1502	600g de blanc de poulet ou de dinde	\N
1503	200g de champignon (frais si possible)	\N
1504	2cuillères à soupe de fond de volaille déshydraté	\N
1505	3 filets de poulet	\N
1506	6 champignons de Paris	\N
1507	200g de blé dur précuit	\N
1508	20cl de crème épaisse	\N
1509	1 bouillon	\N
1510	Poivre et sel	\N
1511	150g de blé	\N
1512	75g de jambon blanc	\N
1513	75g de champignon de Paris	\N
1514	3cuillères à soupe de porto ou vin cuit	\N
1515	Beurre demi-sel	\N
1516	Salade (mâche)	\N
1517	Huile de noix ou noisettes	\N
1518	2verres de blé (type Ebly)	\N
1519	1 cube de bouillon (facultatif)	\N
1520	2grosses cuillères de crème fraîche	\N
1521	1briquette de coulis de tomate	\N
1522	450g de blé	\N
1523	2 cubes de bouillon (1 de légumes et 1 de poule)	\N
1524	350g de blé précuit	\N
1525	2 courgettes émincées	\N
1526	500g de lardons	\N
1527	180g de champignon émincés	\N
1528	1.5l de bouillon	\N
1529	7cl de vin rouge ou blanc	\N
1530	1pincée de parmesan	\N
1531	Blé précuit (suivre les proportion de la boîte)	\N
1532	400g de crevette surgelées	\N
1533	100g de feta	\N
1534	750g de blette	\N
1535	12filets d'anchois au sel ou à l'huile	\N
1536	1botte de blette ( 6 à 8 côtes )	\N
1537	300g de viande hachée	\N
1538	Huile d' olive	\N
1539	20cl de bouillon de boeuf	\N
1540	1botte de blette	\N
1541	1boîte de tomate entières avec leur jus	\N
1542	Coriandre moulue	\N
1543	Emmental râpé (ou tout au fromage à pâte dure)	\N
1544	1kg de blette	\N
1545	1filet d'huile d olive	\N
1546	300g de tomate	\N
1547	70g de gruyère râpé	\N
1548	1 blette (30 cm de haut)	\N
1549	1barquette de lardons	\N
1550	2cuillères à café de poivron	\N
1551	800g de blette (partie verte uniquement)	\N
1552	250g de tomate concassée	\N
1553	6filets d'anchois	\N
1554	2 Blinis	\N
1555	2portions de fromage à croque-monsieur	\N
1556	1tranche de jambon	\N
1557	2cuillères de crème fraîche épaisse	\N
1558	800g de boeuf à braiser (type macreuse, jumeaux,boeuf à bourguignon etc ...)	\N
1559	2 ail (c'est-à-dire 15 à 20 gousses)	\N
1560	1grosse boîte de tomate entières en conserve	\N
1561	750g de paleron de boeuf	\N
1562	1/2l de bouillon de poule	\N
1563	6cuillères à soupe de sauce tomate	\N
1564	570g d'ananas en rondelles	\N
1565	1kg de boeuf coupé en morceaux	\N
1566	3 échalotes émincées	\N
1567	1cuillère à soupe de persil ciselé de 2 gousses d'ail, dégermées	\N
1568	1boîte de tomates pelées 800 g mixées	\N
1569	2 sucre en morceaux + 1 pincée de bicarbonate (pour lever l'acidité )	\N
1570	20 olives noires et des olives vertes	\N
1571	8cuillères à soupe de curry indienne	\N
1572	2cuillères à soupe de herbes de Provence	\N
1573	1kg de boeuf à bourguignon	\N
1574	75cl de bière brune	\N
1575	12 champignons frais	\N
1576	600g de paleron de boeuf découpé en morceaux (pas trop petits, cubes de 7 à 8 cm de côté)	\N
1577	Vergeoise (sucre roux de betterave)	\N
1578	Genièvre (une poignée)	\N
1579	30cl de bière ambrée (Leffe par exemple)	\N
1580	600g de filet de boeuf	\N
1581	1.5 gingembre	\N
1582	5 piments séchés	\N
1583	5 clémentines	\N
1584	6cuillères à soupe de clémentine	\N
1585	1cuillère à soupe de poivre de sichuan	\N
1586	2cuillères à soupe de vin de riz	\N
1587	12.5cl de bouillon de poulet	\N
1588	400g de viande de boeuf à mijoter, ou grosse pièce de style gîte ou palette (au choix)	\N
1589	1/2 céleri rave	\N
1590	1/2 poireau	\N
1591	1 cube de bouillon de boeuf	\N
1592	Piment de Jamaique	\N
1593	Confiture d'airelles (ou de fruits rouges) (facultatif)	\N
1594	1filet de boeuf de 600 g	\N
1595	1 oignon piqué de 3 clous de girofle	\N
1596	3 pommes de terre	\N
1597	2l d'eau	\N
1598	Ficelle de cuisine + cuillère en bois	\N
1599	800g de contre-filet	\N
1600	3 bouillon	\N
1601	8 oignons	\N
1602	1/2bouquet de persil	\N
1603	8grains de poivre blanc	\N
1604	1botte de navet	\N
1605	1 oignon avec 2 clous de girofle	\N
1606	1 queue de boeuf tronçonnée et ficelée	\N
1607	1kg de viande de boeuf style aiguillette ou rumsteack laissé en 1 seul morceau ficelé mais sans barde	\N
1608	6 os à moelle	\N
1609	Bouillon	\N
1610	Eau pour le préparer (si pressé on peut remplacer le préparation du bouillon par des aides culinaires de type bouillon de pôt au feu)	\N
1611	1.4kg de boeuf à bourguignon	\N
1612	6tranches de pain d'épices tartinées de moutarde	\N
1613	50cl de Guinness	\N
1614	2cuillères à soupe de vergeoise	\N
1615	2 bouillon de cube de boeuf	\N
1616	1.5kg de boeuf à braiser coupé en cubes	\N
1617	150g de lardons fumés	\N
1618	6 pommes de terre de calibre moyen	\N
1619	1 poivron vert + 1 jaune + 1 rouge	\N
1620	300g de coeur d'artichaut surgelés	\N
1621	1cuillère à café de paprika	\N
1622	1cuillère à café de carvi	\N
1623	2cuillères à soupe de graisse d'oie ou d'huile	\N
1624	1 poivron vert émincé	\N
1625	675g de boeuf maigre haché	\N
1626	80g d'abricot secs hachés	\N
1627	125g de noix de cajou	\N
1628	25cl de fond de boeuf	\N
1629	400g de petit pois	\N
1630	125g de piment hachés	\N
1631	500g de boeuf haché	\N
1632	420g de haricots rouges en conserve	\N
1633	2l de bouillon de boeuf	\N
1634	2cuillères à café de coriandre en poudre	\N
1635	2cuillères à café de piment en poudre	\N
1636	2cuillères à café de cumin en poudre	\N
1637	1cuillère à soupe de sauce tomate	\N
1638	Crème fraîche (facultatif)	\N
1639	1kg de boeuf	\N
1640	1/2 veau	\N
1641	1dl d'eau ou de bouillon	\N
1642	1dl de vin blanc sec	\N
1643	1kg de carotte	\N
1644	1 bouquet garni (thym, laurier, persil)	\N
1645	600g de boeuf	\N
1646	1cuillère à soupe de moutarde forte	\N
1647	4 oignons nouveaux	\N
1648	1branche de basilic	\N
1649	500g d'oignon	\N
1650	100g de moutarde	\N
1651	8cl de vin blanc	\N
1652	1.5kg de paleron (ou un morceau du même genre) ou mieux encore, du taureau en tranches épaisses	\N
1653	4tranches d'épaule ou un coin de jambon	\N
1654	1pot de câpres	\N
1655	200g d'anchois à l’huile	\N
1656	Céleri en branches (les côtes, pas les feuilles)	\N
1657	1gros bouquet de persil	\N
1658	feuille de blette	\N
1659	2 citrons en tranches	\N
1660	3/4l de vin blanc sec	\N
1661	1verre de vinaigre de xérès ou de vinaigre balsamique	\N
1662	1/2verre d'huile d'olive	\N
1663	250g d'oignon saucier	\N
1664	300g de carotte	\N
1665	1kg de boeuf (viande pour ragoût ou daube)	\N
1666	10cl de vinaigre balsamique	\N
1667	3cuillères à soupe de concentré de tomates	\N
1668	1/2l de bouillon	\N
1669	3 romarin frais	\N
1670	10cl de marsala	\N
1671	10cl de vin rouge	\N
1672	2 steaks de bœuf	\N
1673	2cuillères à soupe de huître	\N
1674	1cuillère à soupe de soja	\N
1675	1poignée de coriandre fraîche	\N
1676	1verre d'eau	\N
1677	500g de boeuf coupé en tranches assez fines comme pour un carpaccio	\N
1678	1boîte d'ananas au sirop	\N
1679	Soja	\N
1680	2cuillères de sucre	\N
1681	Vinaigre balsamico di Modena (au caramel)	\N
1682	300g de viande de boeuf	\N
1683	100g de ketchup	\N
1684	2cuillères de soja	\N
1685	800g de viande de boeuf (type bourguignon...)	\N
1686	500g de pulpe de tomate	\N
1687	2cuillères à soupe de moutarde forte de Dijon	\N
1688	Poivre et huile d'olive selon goût	\N
1689	5 oignons	\N
1690	1bouteille de vin rouge	\N
1691	2grosses cuillères à soupe de vergeoise	\N
1692	1 paprika	\N
1693	Sel de celeri	\N
1694	3 courgettes	\N
1695	4 steaks hachés	\N
1696	1 bouillon pot au feu/légume/bœuf	\N
1697	200g de boeuf	\N
1698	200g d'épinard (ou autres légumes verts)	\N
1699	20g de gingembre coupé en lamelles	\N
1700	2.5cuillères à soupe d'huile	\N
1701	2cuillères à café d'huile de sésame	\N
1702	1/2cuillère à soupe de sauce satay	\N
1703	1/2cuillère à café de fécule de pomme de terre	\N
1704	2cuillères à café de sauce soja	\N
1705	1/4cuillère à café de sel	\N
1706	1cuillère à café de sauce satay	\N
1707	1/3cuillère à café de sucre	\N
1708	800g de boeuf à bourguignon (type collier de bœuf)	\N
1709	500g d'abricot au jus	\N
1710	50g d'oignon gratinée	\N
1711	6tranches de paleron	\N
1712	cl d'huile d'olive	\N
1713	gousse d'ail écrasées, 3 oignons, 2 clous de girofle, une pincée de muscade, 2 feuilles de laurier	\N
1714	4cuillères à soupe d'anchois	\N
1715	cuillère à soupe de farine	\N
1716	cuillère à soupe de vinaigre	\N
1717	100g de câpres et 10 cornichons	\N
1718	1 rôti de boeuf d'1,2 kg	\N
1719	1l d'eau	\N
1720	1/2bouteille de vin rouge	\N
1721	2tablettes de bouillon de boeuf	\N
1722	2cuillères à soupe de persil ciselé	\N
1723	750g de boeuf tendre	\N
1724	600g de carotte	\N
1725	2cuillères à café rases de gingembre en poudre	\N
1726	1cuillère à café d'épices " (badiane, girofle, cannelle, fenouil, poivre)	\N
1727	1 oignon (5-6cm de diamètre)	\N
1728	6 champignons type "champignon de Paris"	\N
1729	Herbes de type "herbes de Provence"	\N
1730	Poivre concassé (3 bonnes pincées)	\N
1731	Sel (selon les gouts)	\N
1732	15cl de crème fraîche liquide	\N
1733	3 steaks (taille en fonction de l'appétit carnivore)	\N
1734	Riz blanc (portion pour 3 personnes)	\N
1735	2cuillères à soupe de cumin	\N
1736	150g de riz	\N
1737	600g de boeuf coupé en cubes	\N
1738	3cuillères de paprika	\N
1739	450g de boeuf coupé en lamelles	\N
1740	1cuillère à soupe de sel	\N
1741	1 oignon haché	\N
1742	1cuillère à soupe de curry en poudre	\N
1743	2cuillères à soupe de ketchup	\N
1744	2 oignons cébettes	\N
1745	1kg d'épinard surgelés	\N
1746	400g de boeuf haché	\N
1747	250g de champignon émincés	\N
1748	1cuillère à soupe d'huile végétale	\N
1749	1pincée de thym	\N
1750	250g de gruyère râpé	\N
1751	1kg de viande de bœuf (type carbonnades maigres)	\N
1752	3cuillères à soupe d'huile de palme	\N
1753	6 gombo	\N
1754	1 pâte de graines de courges	\N
1755	Pili Pili	\N
1756	1.5kg de boeuf (bourguignon)	\N
1757	100g de persil frais haché	\N
1758	150g de basilic frais haché	\N
1759	100g de thym frais haché	\N
1760	3 feuilles de laurier	\N
1761	30cl de vin blanc sec	\N
1762	1 bouillon cube	\N
1763	700g de boeuf à bourguignon (collier)	\N
1764	5 navets	\N
1765	2cuillères à soupe de vinaigre à l'échalote	\N
1766	400g de boeuf coupé en lanières	\N
1767	4cuillères à café de maïzena	\N
1768	3cuillères à soupe d'huile d'arachide	\N
1769	2gousses d'ail finement hâchées	\N
1770	4 gingembre finement hâchée	\N
1771	4 oignons blancs ou rouges émincés	\N
1772	4cuillères à soupe de nuoc mam (sauce de poisson vietnamienne)	\N
1773	Citron vert	\N
1774	Coriandre fraîche ciselée	\N
1775	1kg de boeuf à braiser	\N
1776	700g d'oignon	\N
1777	gousse d'ail	\N
1778	Vinaigre de vin	\N
1779	1demi de vin rouge corsé	\N
1780	1/2bâton de cannelle	\N
1781	grain de poivre rouges (ou quelques grains de poivre 4 baies	\N
1782	2verres d'eau ou de bouillon de boeuf	\N
1783	4 steaks	\N
1784	1petit pot de crème fraîche	\N
1785	300g de boeuf à fondue	\N
1786	200g d'oignon	\N
1787	1cuillère à soupe de sauce nuoc mam	\N
1788	1.2kg de boeuf bourguignon (ou mieux, 700 g bourguignon + 500 g de gite gite).	\N
1789	Marjolaine	\N
1790	12 olives noires	\N
1791	1/2l de vin rouge (Côte du Rhône, ou Cahors).	\N
1792	2cuillères à soupe de jus de citron (possible sans, mais marinade 12 h).	\N
1793	3cuillères à café de paprika fort	\N
1794	2cuillères à café de piment fort (Espelette en poudre) ou 3 piments d'oiseaux bien secs	\N
1795	300g d'olive verte dénoyautées	\N
1796	700g de tomate concassée (2 boîtes, ou 1 kg tomates de fraîches pelées, et épépinées)	\N
1797	1 échalote rose	\N
1798	600g de boeuf spécial bourguignon	\N
1799	4 petits piments rouge selon votre goût (moi j'utilise du piment séché de l'île de la Réunion)	\N
1800	Mélange 5 baies du moulin	\N
1801	300g de boeuf haché	\N
1802	1 pomme (Golden ou autre)	\N
1803	1cuillère de gingembre	\N
1804	50g de saindoux ou d'huile	\N
1805	4cuillères de sauce soja	\N
1806	1verre de saké (ou, à défaut, de vin blanc sec)	\N
1807	250g de pousse de bambou en boîte	\N
1808	1coeur de céleri	\N
1809	150g de champignon frais	\N
1810	1kg de viande de boeuf bourguignon	\N
1811	1l de bière brune	\N
1812	16 oignons à sauce	\N
1813	Vinaigre pour déglacer	\N
1814	3 clous de girofle	\N
1815	2branches de sauge	\N
1816	5grains de poivre	\N
1817	3cuillères à soupe de farine ou maïzena	\N
1818	Bouillon de boeuf (en cas besoin)	\N
1819	400g de châtaigne	\N
1820	1tranche de pain de mie (sans croûte)	\N
1821	1kg de viande à bourguignon (coupé en petits morceaux par le boucher)	\N
1822	1l de sauce tomate	\N
1823	2 os à moelle	\N
1824	500g de boeuf à bourguignon	\N
1825	500g de joue de boeuf	\N
1826	200g de lard fumé	\N
1827	100g d'oignon blanc grelots	\N
1828	8gousses d'ail	\N
1829	1bouteille de bourgogne de table	\N
1830	1verre de cognac	\N
1831	Moutarde à l'ancienne de Meaux	\N
1832	Persil plat	\N
1833	Poivre du moulin blanc et noir	\N
1834	Mélange 4 épices	\N
1835	3morceaux de sucre	\N
1836	1kg de macreuse	\N
1837	Paleron	\N
1838	6 oignons rouges	\N
1839	150g de champignon de Paris frais.	\N
1840	1bouteille de vin de bourgogne (ou un autre vin corsé)	\N
1841	1 clou de girofle	\N
1842	10grains de genièvre	\N
1843	800g de boeuf à bourguignon coupés en cubes	\N
1844	300g de Lardons Fumés HERTA®	\N
1845	500ml de vin rouge	\N
1846	2cubes de Bouillon KUB Bœuf MAGGI®	\N
1847	300g de boeuf	\N
1848	1cuillère à soupe de sauce huître	\N
1849	1.25kg de boeuf à braiser en morceaux	\N
1850	2cuillères à soupe de farine assaisonnée	\N
1851	150ml de Guinness	\N
1852	225g de carotte en tronçons	\N
1853	1cuillère à soupe de persil hâché	\N
1854	1kg de viande de bœuf à braiser	\N
1855	300g d'épinards frais	\N
1856	250g de riz long grain	\N
1857	1l de bouillon de viande (2 cubes de viande)	\N
1858	1/2cuillère à café de cumin	\N
1859	1morceau de paleron ou de bourguignon lkg	\N
1860	1tranche de lard coupée en dés	\N
1861	Matière grasse	\N
1862	Sauge	\N
1863	1tablette de bouillon de boeuf	\N
1864	1.2kg de boeuf (rôti)	\N
1865	500g d'échalote	\N
1866	20petits morceaux de boeuf découpés en cubes ou lamelles	\N
1867	750g de collier de boeuf	\N
1868	15cl de jus d'orange	\N
1869	5 écorces d'orange	\N
1870	4 genièvre	\N
1871	1cuillère à café de baies roses	\N
1872	25cl de bouillon de boeuf	\N
1873	700g de boeuf à bourguignon	\N
1874	1bouteille de cidre brut	\N
1875	12brins de coriandre ou de persil	\N
1876	1 citron vert (zeste et jus)	\N
1877	5gousses d'ail	\N
1878	Sucre	\N
1879	Sauce nuoc mam	\N
1880	5 poivrons	\N
1881	2 piments rouges longs hachés	\N
1882	500g de steak de boeuf coupé en lamelles	\N
1883	1 poivron vert ou rouge coupé en petits morceaux	\N
1884	2 tomates coupées en quartiers	\N
1885	1/2cuillère à café de coriandre moulus	\N
1886	1/2cuillère à café de curcuma	\N
1887	2 gingembre haché	\N
1888	1 piment haché	\N
1889	1 Rosbif d environ 1 kg (plus ou moins selon les mangeurs)	\N
1890	1boîte de champignon sauvages vendus séchés	\N
1891	1boîte de champignon de Paris émincés	\N
1892	1 pâte brisée	\N
1893	1 brique de crème fraîche liquide	\N
1894	1kg de boeuf (pour bourguignon ou un steak) coupé en dé de 3 cm par 3 cm	\N
1895	1 oignon finement haché (ou plus selon les goûts)	\N
1896	6 carottes coupées en morceaux de 3 cm	\N
1897	1branche de céleri finement haché	\N
1898	100g de champignon	\N
1899	1boîte de tomate en morceaux (400g)	\N
1900	1 bouillon de boeuf	\N
1901	2cuillères à soupe de sauce worcestershire	\N
1902	Ail (facultatif)	\N
1903	1kg de paleron de bœuf	\N
1904	2 cive (facultatif)	\N
1905	3cuillères à soupe de fond de veau	\N
1906	1pincée de curry	\N
1907	3cuillères à soupe d'huile d'olive ou huile pimentée (c'est mieux ! )	\N
1908	Feuille de bananier de taille 20x15cm	\N
1909	500g de boeuf tendre	\N
1910	50g de lard haché	\N
1911	1cuillère à café de sel	\N
1912	1/2cuillère à café de poivre moulu	\N
1913	1cuillère à soupe d'échalote hachée	\N
1914	1/2cuillère à soupe de citronnelle finement hachée	\N
1915	10cl de vin blanc	\N
1916	100g de lardons (facultatif)	\N
1917	pointe de piment (facultatif)	\N
1918	2cuillères à soupe de crème fraîche épaisse	\N
1919	2 oignons jaunes ou blancs (ou des oignons nouveaux)	\N
1920	400g de viande hachée (on peut aussi prendre de la viande hachée surgelée)	\N
1921	10cl de coulis de tomate ou sauce tomate (on peut aussi utiliser du concentré de tomates dilué).	\N
1922	1 yaourt nature (de préférence de type 'bulgare')	\N
1923	1cuillère à soupe de paprika doux	\N
1924	1pincée de piment de Cayenne (facultatif)	\N
1925	1/2cuillère à café de maïzena (facultatif)	\N
1926	3cuillères à café d'huile	\N
1927	500g de petit pois	\N
1928	250g de haricot vert	\N
1929	1pincée de muscade en poudre	\N
1930	1brin de menthe fraîche (menthe poivrée de préférence)	\N
1931	8brins de coriandre fraîche	\N
1932	4cuillères à soupe d'huile d'arachide	\N
1933	2cuillères à soupe de sauce soja	\N
1934	400g de viande de boeuf coupée en lamelles	\N
1935	6cuillères à soupe de pineau des charentes rouge	\N
1936	2gousses d'ail écrasées ou hachées très finement	\N
1937	2cuillères à café de graines de coriandre écrasées ou de gingembre râpé	\N
1938	400g de boeuf (type tranche à fondue)	\N
1939	4cuillères à soupe de soja	\N
1940	1.5cuillère à soupe d'ail émincé (ou ail en semoule)	\N
1941	3cuillères à soupe d'oignon nouveau émincé	\N
1942	2cuillères à soupe d'huile de sésame	\N
1943	1cuillère à soupe de sésame grillées	\N
1944	350g de boeuf à mijoter	\N
1945	3 carottes moyennes	\N
1946	1/2 cube de bouillon de bœuf	\N
1947	2cuillères à café de paprika doux	\N
1948	1/2cuillère à café de muscade	\N
1949	1kg de viande hachée	\N
1950	3 poivrons (2 rouges et un vert)	\N
1951	500g de tomate	\N
1952	100g d'olive verte dénoyautées	\N
1953	2cuillères à café d'origan	\N
1954	8cuillères à soupe d'huile olive	\N
1955	150g de brocoli	\N
1956	100g de petit pois	\N
1957	400g de faux-filet	\N
1958	3 oignons nouveaux (ou un gros oignon)	\N
1959	150g de champignon de Paris	\N
1960	3cuillères à soupe de sauce soja	\N
1961	1gousse d'ail haché	\N
1962	100g de châtaigne d'eau en boîte (facultatif)	\N
1963	1botte de coriandre	\N
1964	Piment rouge	\N
1965	2 steaks	\N
1966	3 échalotes (ou 1 oignon)	\N
1967	1cuillère à soupe de sésame (ou un peu d'huile de sésame après cuisson)	\N
1968	Persil ou de coriandre pour décorer	\N
1969	Champignon de Paris ou des champignons noirs coupés en lanières	\N
1970	1kg de filet de boeuf	\N
1971	2cuillères à soupe de moutarde	\N
1972	8 oignons blancs	\N
1973	1/2l de crème fraîche	\N
1974	1.5kg de boeuf à mijoter (paleron, macreuse)	\N
1975	1cuillère à café de gros sel	\N
1976	1cuillère à café de poivre	\N
1977	1 gingembre frais	\N
1978	2 cubes de bouillon (boeuf ou légumes, selon votre goût)	\N
1979	4 bavettes de boeuf (100 g par personne)	\N
1980	1bonne poignée d'arachide	\N
1981	1 concombre	\N
1982	Spaghetti type Capellini (très fins)	\N
1983	feuille de salade	\N
1984	Nuoc mam	\N
1985	1morceau de sucre	\N
1986	2 steaks hachés	\N
1987	1tranche de pain rassi + lait	\N
1988	Persil frais à convenance	\N
1989	Cumin en poudre	\N
1990	1 brique de coulis de tomate	\N
1991	1kg de saucisse	\N
1992	Herbes aromatiques (thym, persil, ciboulette, romarin ect...)	\N
1993	Curry (quantité selon les goûts)	\N
1994	500ml de purée de tomate	\N
1995	3verres de lentilles	\N
1996	Piment (facultatif)	\N
1997	2pavés de saumon	\N
1998	20g d'estragon frais	\N
1999	4cuillères à soupe de lait concentré non sucré ou de crème liquide allégée	\N
2000	150g de sauce tomate	\N
2001	1cuillère à soupe de poudre à colombo	\N
2002	60g de beurre mou	\N
2003	50g de cassonade	\N
2004	60g de miel	\N
2005	190g de farine	\N
2006	1cuillère à café d'épices à pain d’épices (2 g)	\N
2007	1/2cuillère à café de bicarbonate (2,5 g)	\N
2008	1 bonite d'environ 2 kg	\N
2009	50g de gingembre frais	\N
2010	1 piment frais	\N
2011	2 bananes bien mûres	\N
2012	40cl de lait de coco	\N
2013	1g de safran	\N
2014	Betterave crues (2 ou 3, selon la taille)	\N
2015	1.5l d'eau salée	\N
2016	Cumin (selon votre goût)	\N
2017	Bouillon de boeuf (1 cube)	\N
2018	500g d'escalope de veau épaisse	\N
2019	100g de champignon de votre cueillette ou de saison	\N
2020	3cl de jus de citron	\N
2021	4 bouchées feuilletées à garnir	\N
2022	35cl de crème fraîche épaisse ou de crème liquide épaisse	\N
2023	1petite cuillère de fond de veau	\N
2024	5cl de vin blanc sec ou moelleux selon votre goût	\N
2025	1barquette de légume pot au feu (comprenant poireau, carottes, oignon, céleri, persil, navet....)	\N
2026	4 champignons de Paris	\N
2027	2boîtes de quenelle de volaille nature	\N
2028	5cl de vin blanc	\N
2029	4 bouchées à la reine	\N
2030	500g de blanc de poulet	\N
2031	brin de thym citronné	\N
2032	65g de farine	\N
2033	65g de beurre	\N
2034	50cl de bouillon de poule + légumes	\N
2035	250g de lardons fumés (allumettes)	\N
2036	50cl de crème fraîche liquide	\N
2037	1cuillère à soupe de crème fraîche épaisse	\N
2038	4 bouchées à la reine (ou 8 pour 4 gros appétits)	\N
2039	250g de saumon	\N
2040	250g de lotte	\N
2041	1boîte de champignon forestiere	\N
2042	1sachet de béchamel	\N
2043	160g de farine	\N
2044	160g de beurre	\N
2045	800g de fruit de mer surgelés (moules, calamars, noix de st jacques, etc...)	\N
2046	400g de quenelle ou quenelles coupées en morceaux	\N
2047	Aneth	\N
2048	4dosettes de safran	\N
2049	2verres de vin blanc	\N
2050	8 bouchées à la reine	\N
2051	3 knacks alsaciennes	\N
2052	3 pâtes feuilleté)	\N
2053	3gouttes de tabasco (falcutatif)	\N
2054	8 coquilles saint-jacques	\N
2055	1 saumon	\N
2056	100g de crevette	\N
2057	3cuillères à soupe de crème fraîche épaisse	\N
2058	Safran "	\N
2059	4 feuilletés toute prêtes dans le commerce	\N
2060	1 blanc de poulet ou 2 petits blancs	\N
2061	1cuillère à soupe de margarine (meilleur goût que l'huile)	\N
2062	1petite boîte de champignon de Paris	\N
2063	6 olives vertes	\N
2064	1cuillère à café de fond de volaille en poudre ou 1 petit cube de bouillon de volaille	\N
2065	1/2cuillère de café de maïzena	\N
2066	1pincée d'ail en poudre	\N
2067	1kg d'épaule de veau	\N
2068	1/2 poule	\N
2069	200g de quenelle de veau	\N
2070	1l de bouillon	\N
2071	10cl de crème	\N
2072	2 jaunes d'oeuf pour la liaison	\N
2073	8 bouchées à la reine feuilletées (boulangerie ou supermarché)	\N
2074	6 bouchées à la reine	\N
2075	6tranches de jambon blanc	\N
2076	6tranches de jambon fumé	\N
2077	20dl de crème fraîche	\N
2078	3/4cuillère à café de fond de veau	\N
2079	500g de fruit de mer (surgelés)	\N
2080	10cl de jus de citron (en bouteille)	\N
2081	demi de muscade	\N
2082	cuillère à café de persil	\N
2083	demi d'estragon	\N
2084	2/3gousse d'ail	\N
2085	1 échalote (selon les goûts)	\N
2086	4 pâtes feuilletées achetées dans le commerce	\N
2087	100g de crevette cuites décortiquées	\N
2088	25g de petit pois cuits ou en conserve	\N
2089	15cl de lait	\N
2090	reste de Volailles (du poulet rôti de midi, par exemple)	\N
2091	1petite boîte de quenelle de volaille au naturel	\N
2092	Olive verte	\N
2093	1/2l de lait	\N
2094	30g de maïzena	\N
2095	8 bouchées à la reine feuilletées	\N
2096	300g de rumsteak de veau	\N
2097	200g de champignon (boîte)	\N
2098	Quenelle de veau	\N
2099	45g de beurre	\N
2100	75cl de bouillon de volaille	\N
2101	6 pâtes feuilletées	\N
2102	200g de chocolat	\N
2103	12 cerises dénoyautées (au sirop ou surgelées)	\N
2104	3cuillères à soupe de Kirsch SYLVAIN	\N
2105	7cl de crème	\N
2106	6 vol-au-vent	\N
2107	6 quenelles	\N
2108	300g de fruit de mer surgelés	\N
2109	75g de gruyère	\N
2110	1cuillère à café de fond de poisson (facultatif, mais meilleur)	\N
2111	2 escalopes de volaille	\N
2112	4tranches de lard fumé	\N
2113	150g de comté doux	\N
2114	1 raisin noir	\N
2115	8 noix	\N
2116	300g de pâte feuilletée	\N
2117	2dl de vin blanc du Jura	\N
2118	200g de comté doux	\N
2119	1 poulet (1 à 2 kg)	\N
2120	250g de quenelle de veau	\N
2121	100g de champignon de Paris	\N
2122	15cl de crème	\N
2123	1filet de jus de citron	\N
2124	2 bouchées à la reine	\N
2125	250g de poisson blanc type cabillaud, lotte...	\N
2126	12 crevettes fraîches ou cuites	\N
2127	20cl de crème semi-épaisse	\N
2128	4 boudins noirs nature	\N
2129	4 châtaignes	\N
2130	10tranches de bacon	\N
2131	4 pommes golden	\N
2132	Cumin	\N
2133	2 boudins blancs (achetés de préférence chez un boucher)	\N
2134	25cl de crème fraîche liquide	\N
2135	125g de champignon au choix	\N
2136	2 boudins blancs	\N
2137	1 échalote (voir 2 pour ceux aiment)	\N
2138	20cl de crème semi épaisse	\N
2139	4 boudins blancs	\N
2140	10 champignons de Paris moyens	\N
2141	3cuillères à café de sucre roux	\N
2142	2cuillères à soupe de crème semi-épaisse	\N
2143	Lait écrémé	\N
2144	5 boudins blancs	\N
2145	7 pommes	\N
2146	2boules de mozzarella	\N
2147	4 pommes fruits	\N
2148	40g de raisin de Corinthe	\N
2149	5 pommes	\N
2150	3 boudins blancs	\N
2151	1/2verre de porto ou de vin blanc	\N
2152	2 boudins noirs aux oignons ou pas	\N
2153	800g de pomme de terre (cette proportion est au choix, selon l'appétit de chacun! Il faut de quoi faire une purée!)	\N
2154	250g de boudin noir à l'oignon (ou nature)	\N
2155	3 pommes reinettes ou golden	\N
2156	1demi litre de vin blanc sec	\N
2157	1pointe de piment de Cayenne	\N
2158	1cuillère d'huile d'olive	\N
2159	1boîte d'ananas en conserve (ou un ananas frais)	\N
2160	1petit morceau de gingembre frais	\N
2161	1pincée de curry en poudre.	\N
2162	6 boudins blancs	\N
2163	3 pommes	\N
2164	3cuillères à soupe de miel	\N
2165	4 boudins noirs	\N
2166	800g d'oignon	\N
2167	600g de lotte	\N
2168	600g de rascasse	\N
2169	600g de congre	\N
2170	600g de saint-pierre	\N
2171	600g de vive	\N
2172	600g de grondin rouge	\N
2173	600g de cigale de mer ou de langoustines	\N
2174	10 tomates mûres	\N
2175	8 oignons moyens	\N
2176	10brins de persil simple	\N
2177	1cuillère à café de safran en filaments	\N
2178	8brins de thym	\N
2179	1/2 feuille de laurier	\N
2180	25cl d'huile d'olive	\N
2181	3/4cuillère à soupe de gros sel	\N
2182	15grains de poivre noir	\N
2183	1/4 orange séchée	\N
2184	2 petits piments rouges	\N
2185	20cl d'huile d'olive	\N
2186	1.5kg d'épinard	\N
2187	6 pommes de terre	\N
2188	1brin de fenouil	\N
2189	1.5l d'eau	\N
2190	6 oeufs bien frais	\N
2191	6tranches de pain de campagne	\N
2192	1 oignon blanc	\N
2193	2 pommes de terre à chair ferme	\N
2194	1 courgette ferme(de 20 cm)	\N
2195	50cl de bouillon de légumes	\N
2196	Fenouil en graines (20 graines) ou ½ cuillère à café d’anis apéritif (pastis)	\N
2197	20 safran	\N
2198	feuille de Fleurs de thym	\N
2199	1gousse d'ail hachée (pas prévue pour être rissolée)	\N
2200	250g de petit pois écossés frais (ou 1 mini boite de conserve)	\N
2201	10 asperges vertes fraîches ou surgelées en tronçons	\N
2202	2 tomates moyennes bien en chair	\N
2203	2 oeufs bien frais dans leur coquille (ou 2 filets de poisson)	\N
2204	1.2kg de morue	\N
2205	2 fenouil	\N
2206	2 tomates (ou une boîte de tomates concassées)	\N
2207	2pincées de safran (en filaments)	\N
2208	1cuillère à soupe de persil ciselé	\N
2209	1 rôti de porc (préalablement cuit au four et refroidi) de 400 g	\N
2210	3 tomates fraiches	\N
2211	1cuillère à café de herbes de Provence	\N
2212	10cl de vin blanc à cuisiner	\N
2213	60cl d'eau chaude (= 60 cl)	\N
2214	Vermicelles	\N
2215	Persil ciselé, ciboulette	\N
2216	2 cuisses de poulet	\N
2217	300g de farine	\N
2218	1 poulet coupé en huit	\N
2219	400g de pois chiches	\N
2220	1/2botte de céleri	\N
2221	Oeuf	\N
2222	épices (persil, paprika, feuille de laurier,thym et basilic)	\N
2223	1 poulet (après un poulet rôti par exemple)	\N
2224	1paquet de ravioli	\N
2225	Parmesan râpé selon gout	\N
2226	4plaques de raviole de Royan	\N
2227	1botte de carotte nouvelles	\N
2228	4 navets oranges	\N
2229	1 poulet	\N
2230	Coriandre en grains	\N
2231	Cube de bouillon de volaille dilués dans 1 l d'eau bouillante	\N
2232	4 tomates coupées en quartiers	\N
2233	150g de tofu coupés en dés	\N
2234	2 oeufs battus en omelette	\N
2235	1boîte de champignon de paille	\N
2236	1 porc pour le bouillon	\N
2237	1 oignon coupé en quartiers	\N
2238	3brins de ciboulette	\N
2239	4bols d'eau	\N
2240	4 prunes salées (facultatif)	\N
2241	4 oignons nouveaux (ou 1 ou 2 oignons blancs)	\N
2242	3cuillères à soupe de vermicelles	\N
2243	1 cube de bouillon de votre choix	\N
2244	Persil plat (facultatif)	\N
2245	1cuillère à soupe de parmesan râpé	\N
2246	800g de blanc de poulet	\N
2247	Poivre doux	\N
2248	Ail en poudre	\N
2249	Gingembre en poudre	\N
2250	800g de légume  maïs, petits-pois, carottes et haricots verts découpés	\N
2251	200g d'oignon à découper en carrés	\N
2252	2gousses d'ail à hacher	\N
2253	2 poivrons verts découpés en carrés	\N
2254	20g de sauce soja	\N
2255	50g d'huile de tournesol	\N
2256	1kg de viande hachée (porc / veau)	\N
2257	Chapelure tex/mex	\N
2258	1 farine	\N
2259	Beurre + huile de tournesol	\N
2260	1 bière d'Orval	\N
2261	2cuillères à soupe de vin de Madère	\N
2262	1boîte de concentré de tomates	\N
2263	2cuillères à café de basilic séché	\N
2264	2cuillères à café de thym	\N
2265	3cuillères à café de laurier	\N
2266	Tomate fraiches	\N
2267	Champignon (faculatifs)	\N
2268	300g de porc	\N
2269	4 biscottes s	\N
2270	1 fond d'assiette à soupe de lait	\N
2271	Sirop de Liège	\N
2272	Sauce brune )	\N
2273	Raisins secs	\N
2274	600g de viande de porc crue hachée	\N
2275	400g de viande de boeuf crue ou bouillie	\N
2276	200g de pain	\N
2277	20cl de lait	\N
2278	300g de haché de veau	\N
2279	1tranche de pain de mie , sans la croûte	\N
2280	1cuillère à café de thym	\N
2281	1cuillère à café de thym sec ou frais de préférence	\N
2282	200g de viande hachée de boeuf	\N
2283	1cuillère à soupe de persil ou de coriandre haché	\N
2284	4cuillères à soupe d'arachide écrasées	\N
2285	1verre d'huile d'olive	\N
2286	400g de boeuf haché (ou de cheval)	\N
2287	1grand verre d'eau froide environ	\N
2288	8 cornichons (selon vos goûts)	\N
2289	500g de viande hachée à 5%	\N
2290	2cuillères à café de semoule fine	\N
2291	4cuillères à café de pignons de pins	\N
2292	400g de coulis de tomate	\N
2293	1.5cuillère à café de gingembre	\N
2294	250g de porc	\N
2295	250g de boeuf	\N
2296	1cuillère à soupe de sirop de Liège	\N
2297	500g de porc	\N
2298	500g de boeuf	\N
2299	4tranches de pain de mie (sans la croûte) trempées dans du lait	\N
2300	5 oignons finement hachés	\N
2301	1bouquet de persil finement haché	\N
2302	Cassonade brune	\N
2303	Vinaigre de vin rouge	\N
2304	1l de bouillon de viande	\N
2305	Genièvre	\N
2306	Raisin de corinthe	\N
2307	500g de haché de veau	\N
2308	500g de viande hachée de bœuf	\N
2309	5 oignons (2 pour la sauce et 3 pour les boulettes)	\N
2310	800g de sauce tomate	\N
2311	1boule de mozzarella (125 g)	\N
2312	1 cube de bouillon de bœuf	\N
2313	12.5cl de vin rouge	\N
2314	800g de viande hachée (environ)	\N
2315	Champignon (conserves ou frais)	\N
2316	4 pommes de terre cuites de taille moyenne	\N
2317	2tranches de jambon blanc	\N
2318	2tranches de bacon (ou ce qu'il vous reste de la raclette, jambon fumé, cru...)	\N
2319	50g de fromage à raclette	\N
2320	125g de chapelure	\N
2321	Huile de friture	\N
2322	300g de boeuf haché ou 4 steaks hachés	\N
2323	3cuillères à soupe de persil haché	\N
2324	3gousses d'ail écrasées	\N
2325	50g de pain râpé	\N
2326	1reste de poulet	\N
2327	1reste de macédoine de légumes	\N
2328	400g de boeuf haché préparé	\N
2329	350g de cerise du nord dénoyautées	\N
2330	6 pommes de terre de taille moyenne	\N
2331	Beurre ou margarine à cuire	\N
2332	600g de viande hachée (mélange porc et veau, ou porc et boeuf)	\N
2333	8 endives	\N
2334	1 bière (25 cl)	\N
2335	500g de viande mélangées	\N
2336	1/4botte de persil plat	\N
2337	3feuilles de sauge fraîche	\N
2338	6cuillères à soupe de chapelure de pain	\N
2339	3 steaks hachés	\N
2340	2 saucisses	\N
2341	5 biscottes	\N
2342	1grosse poignée de persil	\N
2343	2cuillères à soupe de sésame	\N
2344	2cuillères à café d'épices  cumin, cnanelle, coriandr	\N
2345	Sauce tomate nature	\N
2346	Couscous " de céréales blé - seigle - épeautre	\N
2347	800g de viande d’agneau (épaule ou haut de gigot)	\N
2348	3gousses d'ail hachées	\N
2349	2 oignons hachés	\N
2350	2cuillères à soupe de menthe hachée	\N
2351	2cuillères à soupe de coriandre hachée	\N
2352	1pointe de couteau de piment de Cayenne	\N
2353	2cuillères à soupe de chapelure	\N
2354	15cl d'huile d'olive	\N
2355	1 céleri-rave	\N
2356	600g d'épaule d'agneau hachée	\N
2357	1 oeuf (battu en omelette)	\N
2358	1brin de menthe	\N
2359	3tranches de pain de mie	\N
2360	5cl de lait	\N
2361	200g de viande hachée	\N
2362	2verres de pain dur	\N
2363	1cuillère de café de sel	\N
2364	1boîte de sauce tomate	\N
2365	1cuillère de café d'herbes de Provence	\N
2366	Paprika doux	\N
2367	200g de pain rassis	\N
2368	300g de tomate (coulis, frais, en boîte, selon les goûts)	\N
2369	1demi d'oignon	\N
2370	Parmesan râpé (facultatif)	\N
2371	4tranches d'ananas	\N
2372	1boîte de 400 g de tomate concassée	\N
2373	4cuillères à soupe de miel	\N
2374	750g de boeuf haché	\N
2375	1 piment vert frais épépiné et haché	\N
2376	3cuillères à soupe de coriandre fraîche hachée	\N
2377	1cuillère à café de garam massala (mélange d'épices)	\N
2378	1 oeuf battu	\N
2379	1boîte de tomates pelées (400 g)	\N
2380	1bâton de cannelle de 2 cm	\N
2381	2gousses d'ail hachées	\N
2382	5 gingembre frais haché	\N
2383	1cuillère à soupe de cumin moulu	\N
2384	2cuillères à soupe de coriandre moulus	\N
2385	1cuillère à café de piment moulu	\N
2386	150g de chair à saucisse	\N
2387	300g de coulis de tomate	\N
2388	Olive verte et noires	\N
2389	1bouquet de coriandre	\N
2390	2cuillères à soupe de concentré de tomates	\N
2391	400g de petit pois (en conserve)	\N
2392	500g de viande de bœuf haché	\N
2393	250g de sauce tomate	\N
2394	75g d'oignon	\N
2395	1cuillère à soupe d'ail haché	\N
2396	1/2cuillère à soupe de cumin en poudre	\N
2397	600g de viande hachée	\N
2398	1cuillère à soupe de cumin en poudre	\N
2399	1 oeuf entier	\N
2400	1cuillère à soupe de persil haché très finement	\N
2401	1brin de thym	\N
2402	3 yaourts brassés	\N
2403	2cuillères à soupe de menthe fraîche hachée	\N
2404	800g de petit pois	\N
2405	150g de pâtes en forme de pépins de melon)	\N
2406	Curcuma (facultatif)	\N
2407	1kg de viande hachée (boeuf, agneau)	\N
2408	2 oignons moyens hachés	\N
2409	2 tomates pelées et coupées en cubes (ou 20 cl de coulis de tomates)	\N
2410	Persil ou coriandre hachées	\N
2411	2filets de poisson (cabillaud, grenadier, ou ce que vous aimez!)	\N
2412	reste de légume cuits à la vapeur (100 à 150 g) ou une petite boîte de macédoine de légumes	\N
2413	2pincées de gros sel	\N
2414	Farine disposée dans une assiette (pour enrober les boulettes)	\N
2415	2petites boîtes de tomate	\N
2416	1petite boîte de tomates pelées	\N
2417	1pincée de paprika	\N
2418	1kg d'escalope de poulet	\N
2419	1petite poignée d'arachide	\N
2420	Beurre de cacahuètes	\N
2421	1cuillère à soupe de nuoc mam	\N
2422	3cuillères à soupe de coriandre finement coupée.	\N
2423	5cuillères à soupe de sauce soja	\N
2424	5cuillères à soupe de vermouth blanc sec	\N
2425	1cuillère à café de cinq parfums chinois	\N
2426	4cuillères à soupe de sésame	\N
2427	1cuillère à café de sucre roux	\N
2428	300g de chair à saucisse ou de viande hachée	\N
2429	50g de gruyère râpé	\N
2430	1kg de haché de veau	\N
2431	5branches de céleri	\N
2432	2l d'eau + 1 bouillon cube	\N
2433	1cuillère à soupe de fécule de maïs	\N
2434	600g de veau	\N
2435	200g de gruyère	\N
2436	50cl de coulis de tomatesBR>	\N
2437	12 veau	\N
2438	390g de tomates pelées concassées	\N
2439	1pincée de piment d'Espelette	\N
2440	200g de lardons natures	\N
2441	1cuillère à soupe de canard	\N
2442	1branche de laurier	\N
2443	7 poivrons	\N
2444	5 tomates	\N
2445	750g de veau	\N
2446	150g de gruyère	\N
2447	400g de haché de veau ou de porc	\N
2448	1cuillère à soupe de thym fraîches	\N
2449	200g de viande de boeuf hachée	\N
2450	1/2verre de vin blanc sec (10 cl)	\N
2451	200g de viande hachée de porc	\N
2452	2cuillères à soupe de parmesan rapé	\N
2453	1cuillère à café de piment doux en poudre	\N
2454	Pulpe de tomate	\N
2455	Poivre .	\N
2456	250g de chair à saucisse	\N
2457	1 oignon haché fin	\N
2458	1gousse d'ail hachée fin	\N
2459	1bouquet de coriandre hachée	\N
2460	Piment frais ou pincée de piment en poudre	\N
2461	20g de farine	\N
2462	1grande boîte de tomates pelées	\N
2463	1 oignon râpé	\N
2464	1cuillère à café de cumin (ou plus suivant les goûts)	\N
2465	1pincée de gingembre	\N
2466	1poignée de persil	\N
2467	100g de beurre (ou un peu moins suivant les goûts)	\N
2468	Piment doux (facultatif)	\N
2469	500g de farce (mélange porc/boeuf)	\N
2470	1verre de riz	\N
2471	Aneth (au choix et au goût de chacun)	\N
2472	Concentré de tomates	\N
2473	1 carotte (optionnel)	\N
2474	600g de boeuf haché ou veau	\N
2475	200g de pecorino ou parmesan râpé	\N
2476	Menthe	\N
2477	1cuillère à soupe de beurre	\N
2478	4cuillères à soupe d'oignon finement hachés	\N
2479	1 pomme de terre bouillie, réduite en purée (une tasse)	\N
2480	3cuillères à soupe de chapelure (fine)	\N
2481	500g de boeuf haché maigre	\N
2482	5cuillères à soupe de crème épaisse	\N
2483	Persil haché (au goût)	\N
2484	cuillère à soupe de beurre et 2 cuillères à soupe d'huile	\N
2485	250g de champignon de Paris ou de champignons variés.	\N
2486	4branches de persil	\N
2487	4 tomates rondes	\N
2488	500g de chair à saucisse	\N
2489	pincée de herbes de Provence	\N
2490	400g de chair à saucisse	\N
2491	200g d'épinard hachés surgelés	\N
2492	Huile neutre (pour la cuisson)	\N
2493	4 steaks haché	\N
2494	1sachet d'olives	\N
2495	1petit morceau de pain	\N
2496	1cuillère à soupe de lait	\N
2497	1boîte de petit pois /carottes	\N
2498	2tranches de pain de mie	\N
2499	150cl d'eau	\N
2500	2g de safran (soit 2 dosettes)	\N
2501	2cuillères à soupe d'ail et persil surgelé	\N
2502	branche de ciboulette ciselée	\N
2503	1poignée de riz cru	\N
2504	4portions de Vache qui rit®	\N
2505	Carré frais	\N
2506	Bleu ...	\N
2507	Huile à frire	\N
2508	1bol de chapelure	\N
2509	1bol de farine	\N
2510	700g de viande hachée	\N
2511	12 tomates	\N
2512	5cuillères à soupe de basilic ciselé	\N
2513	1cuillère à café de muscade rapée	\N
2514	16 tomates cerise	\N
2515	400g de porc	\N
2516	20g de pain	\N
2517	8 boulettes de viande orientales (épicées)	\N
2518	Thym frais	\N
2519	Herbes aromatiques	\N
2520	650g de haché de veau	\N
2521	50g de chapelure	\N
2522	2cuillères à soupe de persil plat haché	\N
2523	1l de coulis de tomate	\N
2524	1 cube de bouillon dégraissé	\N
2525	75g de boulgour	\N
2526	33cl de lait	\N
2527	33cl d'eau	\N
2528	1petite poignée de raisins secs	\N
2529	200g de boulgour moyen	\N
2530	5 courgettes	\N
2531	150g de raisins secs	\N
2532	150g d'olives noires avec ou sans noyaux	\N
2533	1cuillère à soupe de curcuma	\N
2534	2verres de boulgour	\N
2535	1/2 potimarron	\N
2536	2 panais	\N
2537	2cuillères à soupe d'huile de colza	\N
2538	1cuillère à soupe d'huile de sésame	\N
2539	1cuillère à café de quatre-épices (ou autres selon les goûts)	\N
2540	Graines de courge	\N
2541	Graines de tournesol	\N
2542	250g de boulgour	\N
2543	1pincée de piment d’Espelette en poudre	\N
2544	800g de crevette ) crus (ou 400 g cuits décortiqués)	\N
2545	4 poireaux (gros)	\N
2546	4filets d'anchois	\N
2547	2pincées de poivre	\N
2548	1petite pincée de sel	\N
2549	1 coeur de boeuf	\N
2550	2 rognons de boeuf	\N
2551	1 foie de boeuf	\N
2552	500g de boeuf bourguignon	\N
2553	2l de vin de Bourgogne pinot noir (12.5°)	\N
2554	250g de lardons fumés	\N
2555	100g de morille	\N
2556	100g de cèpes	\N
2557	10 genièvre	\N
2558	2bâtons de cannelle entier	\N
2559	1.4kg de boeuf pour bourguignon	\N
2560	1boîte de tomates pelées	\N
2561	4cuillères à soupe d'ail	\N
2562	Persil surgelé ou de persillade maison	\N
2563	3 poivrons rouges assez gros	\N
2564	2cuillères à soupe de cinq-épices chinoises	\N
2565	2cuillères à soupe de curry	\N
2566	500g de bourguignon de cerf (coupé dans des parties telle que le cuissot ou l'épaule en dés)	\N
2567	1l de vin	\N
2568	3 feuilles de laurier sèches	\N
2569	2branches de thym sèches	\N
2570	1 oignon rouge (gros)	\N
2571	2 échalotes de taille moyenne	\N
2572	1petite branche de romarin fraiche	\N
2573	500g de cèpes des bois	\N
2574	Farine (pour épaissir la sauce en fin de cuisson)	\N
2575	1.5cuillère à café de fond de veau	\N
2576	1bouquet d'oseille fraîche	\N
2577	4 échalotes finement hachées	\N
2578	4 pommes de terre (de taille moyenne) coupées en cubes	\N
2579	1bonne cuillère à soupe de beurre	\N
2580	1 bouillon de volaille dilué dans 1 L d'eau	\N
2581	4tranches de pain de campagne rassis	\N
2582	1petit bouquet de cerfeuil ciselé	\N
2583	600g de boeuf (paleron par exemple) coupé en cubes	\N
2584	600g de carotte coupées en rondelles	\N
2585	200g de lard fumé coupé en lardons	\N
2586	1/2l de vin rouge	\N
2587	1/2l de bouillon (ou d'eau + 1 bouillon-cube)	\N
2588	6 crottins de chavignol bien blancs	\N
2589	300g de saumon fumé	\N
2590	Salade mélangée	\N
2591	Cerneau de noix concassés	\N
2592	1cuillère de miel	\N
2593	6cuillères d'huile de noisette	\N
2594	2cuillères de vinaigre de xèrès	\N
2595	1cuillère de vinaigre balsamique	\N
2596	600g de brandade toute préparée	\N
2597	500g d'asperge blanches du marché	\N
2598	7 pommes de terre	\N
2599	400g de saumon	\N
2600	1cuillère à soupe d'aneth	\N
2601	50g de fromage râpé	\N
2602	250g de cabillaud	\N
2603	5centimètres de chorizo fort	\N
2604	8 pommes de terre assez grosses	\N
2605	12gousses d'ail	\N
2606	3centimètres de gingembre	\N
2607	8 olives noires	\N
2608	3centimètres de harissa	\N
2609	Pain de campagne	\N
2610	2 escalopes de dinde	\N
2611	25cl de ketchup	\N
2612	3cuillères à soupe de cassonade	\N
2613	échalote	\N
2614	1/2 reblochon	\N
2615	500g de pomme de terre congelées préfrites ou pommes de terre fraîches si pas pressé	\N
2616	1paquet de lardons	\N
2617	100g de beaufort	\N
2618	100g de ricotta	\N
2619	épices au choix (moi c'est deux petites pincées d'estragon, mais la coriandre c'est bon aussi pas exemple)	\N
2620	Huile pour la friture des bricks	\N
2621	4tranches de saumon fumé	\N
2622	2cuillères à café d'aneth hachée	\N
2623	2 boudins noirs	\N
2624	6feuilles de brick	\N
2625	Beurre fondu	\N
2626	Coriandre fraîche	\N
2627	Ficelle alimentaire ou papier aluminium	\N
2628	3cl de cognac	\N
2629	50cl de crème fraîche	\N
2630	1 feuille de brick par personne	\N
2631	1petite boîte de lentilles cuisinées	\N
2632	800g d'épaule d'agneau désossée et coupée en dés	\N
2633	10feuilles de brick	\N
2634	3cuillères à soupe de raisin blonds	\N
2635	1 citron non traité	\N
2636	2cuillères à soupe de miel	\N
2637	1pincée de cannelle	\N
2638	1paquet de brick carrées = papier filo (épiceries asiatiques et orientales)	\N
2639	200g de dinde	\N
2640	2verres de riz basmati	\N
2641	3cuillères à soupe de chutney de mangue	\N
2642	Huile de cuisson	\N
2643	1 feuille de brick	\N
2644	5cl d'huile d'olive	\N
2645	50g d'olive verte	\N
2646	4portions de fromage fondu type Kiri	\N
2647	20feuilles de brick rondes	\N
2648	140g de concentré de tomates	\N
2649	1boîte de thon	\N
2650	Fromage de chèvre	\N
2651	500g de courgette	\N
2652	15feuilles de brick	\N
2653	200g de viande de boeuf	\N
2654	200g de viande de porc (échine )	\N
2655	1petite gousse d'ail	\N
2656	12 feuilles de coriandre fraîche	\N
2657	2cuillères à soupe de curcuma	\N
2658	1cuillère à soupe de gingembre moulu	\N
2659	500g de viande hachée (chez le boucher)	\N
2660	1bouquet de menthe (5 brins pour la prépa et le reste pour la déco)	\N
2661	10feuilles de salade verte	\N
2662	1paquet de brick	\N
2663	200g de poulet	\N
2664	200g de maroilles	\N
2665	1 bûche de chèvre	\N
2666	3cuillères à soupe de miel liquide	\N
2667	1cuillère à café de sirop d'érable	\N
2668	1cuillère à café de persil	\N
2669	1cuillère à soupe de moutarde mi-forte	\N
2670	1 laitue	\N
2671	3/4 tomate	\N
2672	Olives	\N
2673	1kg d'épinard en branches surgelés	\N
2674	300g de feta de brebis	\N
2675	1gros bouquet de coriandre	\N
2676	1pot de yaourt nature	\N
2677	5feuilles de brick	\N
2678	4 pommes de terre (suivant la grosseur)	\N
2679	1pincée de fromage râpé	\N
2680	6 poireaux	\N
2681	200g de lardons nature	\N
2682	3feuilles de brick	\N
2683	200g de carotte	\N
2684	75g de petit pois	\N
2685	40g de bacon en fines tranches	\N
2686	4 pruneaux dénoyautés	\N
2687	2feuilles de brick	\N
2688	30g de chèvre	\N
2689	30g de beurre liquide pour beurrer les feuilles	\N
2690	1 courgette coupée en rondelles	\N
2691	200g de champignon frais coupés en fines lamelles	\N
2692	1 poivron rouge coupé en morceaux	\N
2693	1 oignon moyen émincé	\N
2694	5belles branches de persil mixées	\N
2695	100g de lardons allumettes natures ou fumés	\N
2696	Sel (il y a déjà les lardons), du poivre	\N
2697	Matière grasse pour dorer	\N
2698	4pavés de saumon	\N
2699	4grosses cuillères à soupe de sauce tomate au basilic	\N
2700	12 asperges vertes fraîches et crues	\N
2701	1 coulommier (ou 1 part de brie de Meaux)	\N
2702	40g de truffe	\N
2703	100g de mascarpone	\N
2704	1pincée de fleur de sel	\N
2705	4 brioches à tête	\N
2706	5 oeufs	\N
2707	1/2bouquet de ciboulette	\N
2708	350g de farine	\N
2709	175g de beurre froid coupé en dés	\N
2710	1cube de levure de boulanger fraîche	\N
2711	2cuillères à soupe d'eau	\N
2712	100g de pépite en chocolat	\N
2713	30cl de lait	\N
2714	1sachet de levure de boulanger	\N
2715	1.5cuillère à café de SEL DES ALPES	\N
2716	8 briochettes	\N
2717	200g de dés de jambon	\N
2718	50g d'emmental	\N
2719	100g de lardons fumées	\N
2720	20cuillères de crème fraîche	\N
2721	1 fromage de chèvre	\N
2722	1 brochet (la taille dépend de la pêche)	\N
2723	4cuillères à soupe d'estragon	\N
2724	400ml de crème fraîche	\N
2725	200ml de vin blanc (Riesling, pinot blanc)	\N
2726	2 échalotes grises	\N
2727	1 brochet préparé, de 2 kg environ	\N
2728	1cuillère à soupe de Fines herbes hachées (persil, cerfeuil, ciboulette)	\N
2729	1 brochet	\N
2730	1 pain de campagne rassis	\N
2731	200g de lard gras ou de saindoux	\N
2732	1/2l de vin blanc	\N
2733	250ml de crème fraîche liquide	\N
2734	1petite boîte de champignon de Paris ou quelques girolles	\N
2735	600g d'espadon	\N
2736	1petit bocal de tomate séchée (confites)	\N
2737	1 Magret de Canard	\N
2738	4 pêches en conserve (préférez de la bonne qualité)	\N
2739	Cerise (plus facile avec des congelées)	\N
2740	Miel	\N
2741	2 Magrets de Canard	\N
2742	Jus d'orange	\N
2743	1 orange	\N
2744	2kg de crevette crues	\N
2745	1 mangue	\N
2746	Jus d'orange (20 cl)	\N
2747	Piment de Cayenne en poudre (1 pincée)	\N
2748	12 gambas	\N
2749	1/2cuillère à soupe d'aneth	\N
2750	1/2cuillère à soupe de persil plat haché	\N
2751	1/2cuillère à soupe de ciboulette	\N
2752	10cl d'huile de tournesol	\N
2753	2 citrons verts	\N
2754	1cuillère à café de moutarde forte	\N
2755	1cuillère à café de miel liquide	\N
2756	2 pommes golden	\N
2757	1kg de poisson ferme (marlin, espadon, thon...)	\N
2758	3cuillères à soupe de pâte d'arachide	\N
2759	1bouquet d'oignon pays (cives, cebettes) ou (ciboulette + échalotes)	\N
2760	2 citrons (verts)	\N
2761	2cuillères à soupe de gingembre coupé en copeaux	\N
2762	1trait de tabasco (selon goût)	\N
2763	2 poulets coupées en 2	\N
2764	8tranches de bacon	\N
2765	1/2tasse d'huile	\N
2766	1/4tasse de sauce soja (soya)	\N
2767	2cuillères à soupe de vinaigre de vin rouge	\N
2768	2cuillères à soupe de sirop d'érable	\N
2769	1gousse d'ail hachée finement	\N
2770	250g de poitrine fumée	\N
2771	2 poivrons soit 1 rouge et 1 vert	\N
2772	Tabasco	\N
2773	14 noix de saint-jacques	\N
2774	7tranches de poitrine fumée	\N
2775	20cl de crème fleurette	\N
2776	Romarin	\N
2777	1kg de veau	\N
2778	1/2cuillère de curcuma	\N
2779	1/2 piment doux	\N
2780	1/3cuillère à café de gingembre	\N
2781	1cuillère à soupe de coriandre hachée	\N
2782	400g de fromage edam	\N
2783	4tranches de viande à fondue très fines	\N
2784	1 filet mignon	\N
2785	12 saucisses cocktail	\N
2786	12 pruneaux	\N
2787	1 poivron jaune ou rouge	\N
2788	4 quenelles natures ou à ce qui vous fait envie	\N
2789	16 coeurs de canard	\N
2790	16morceaux de Magret de Canard en gros cubes (laisser la peaux)	\N
2791	Ail haché	\N
2792	4cuillères de miel	\N
2793	4cuillères d'huile (au choix arachide,colza ou d'olive)	\N
2794	2cuillères de quatre-épices	\N
2795	12 noisettes d'agneau	\N
2796	feuille de menthe fraîche	\N
2797	500g de gigot d'agneau désossé	\N
2798	2cuillères à soupe de vinaigre de vin blanc	\N
2799	2cuillères à soupe de soja	\N
2800	1cuillère à café d'huile de sésame (ou de maïs)	\N
2801	12 côtes d'agneau	\N
2802	8 figues sèches (ou 4 fraîches à couper en 2)	\N
2803	4 abricots frais à couper en 2 (ou 8 abricots secs)	\N
2804	10feuilles de basilic frais	\N
2805	1cuillère à café de miel	\N
2806	1pincée de gingembre en poudre	\N
2807	200g de semoule	\N
2808	50g de pois chiches	\N
2809	50 raisins secs	\N
2810	2petites boîtes de concentré de tomates	\N
2811	3 tomates entières	\N
2812	1bonne pincée de Ras el Hanout	\N
2813	400g d'agneau (épaule)	\N
2814	1 citron (pour son jus)	\N
2815	1kg d'épaule d’agneau	\N
2816	Laurier frais	\N
2817	20 aiguillettes de canard	\N
2818	2 pommes fruit	\N
2819	8 pruneaux d'Agen	\N
2820	5brins de romarin	\N
2821	150g de viande d'iguane	\N
2822	1 tomate mure	\N
2823	2 poivrons (rouge et jaune)	\N
2824	1cuillère à soupe de tabasco	\N
2825	1/3tasse de mangue	\N
2826	1 piment vert	\N
2827	25cl d'huile de friture	\N
2828	2 maïs	\N
2829	1 banane plantain	\N
2830	750g de boeuf (filet ou rumsteack)	\N
2831	2 citronnelle	\N
2832	1 petit piment vert	\N
2833	1petit morceau de gingembre	\N
2834	2cuillères à soupe de nuoc mam	\N
2835	1/2 citron vert	\N
2836	400g de tagliatelles fraîches	\N
2837	feuille de menthe	\N
2838	Feuilles de coriandre	\N
2839	2cuillères à soupe d'huile d'arachide	\N
2840	Sel et gros sel	\N
2841	500g de filet de boeuf ou autre morceau tendre	\N
2842	20g de chapelure (ou de chapelure japonaise Panko)	\N
2843	4cuillères de sauce soja japonaise	\N
2844	2cuillères de mirin	\N
2845	1cuillère à soupe d'huile de friture	\N
2846	16 bambous de 15 cm de longueur	\N
2847	60g de ketchup	\N
2848	1cuillère à café de soja japonaise	\N
2849	4cl de ketchup	\N
2850	4cl de sauce soja	\N
2851	4cl de sauce barbecue	\N
2852	4cl d'eau	\N
2853	2cl de vinaigre	\N
2854	2cl d'huile	\N
2855	2gousses d'ail mixées	\N
2856	2cuillères à soupe de sucre	\N
2857	2 oignons finement hachés	\N
2858	6cl de chapelure	\N
2859	570g d'ananas en tranches	\N
2860	450g de boeuf coupé en cubes	\N
2861	150g de marrons cuits	\N
2862	12 pommes type golden	\N
2863	600g de boudin noir paysan	\N
2864	10cl de vinaigre de cidre	\N
2865	pincée de piment d'Espelette	\N
2866	8 abricots bien sucrés	\N
2867	2tranches épaisses de lard	\N
2868	2filets de canard (ou magrets)	\N
2869	3 pêches blanches ou pêches de vigne pas trop mûres (à défaut, prendre des pêches en boîte au naturel)	\N
2870	600g de crevette grise surgelées crues (gambas)	\N
2871	50g de pignons de pins	\N
2872	50g de raisins secs	\N
2873	300g de riz long américain	\N
2874	1/2verre d'huile	\N
2875	24 crevettes	\N
2876	2cuillères à café de mélange d'épices indiennes (garam masala)	\N
2877	24 queues de crevettes crues	\N
2878	1 poivron rouge coupé en dés	\N
2879	6gousses d'ail pilé	\N
2880	2 échalotes coupées en rondelles	\N
2881	600g de blanc de dinde	\N
2882	6cl d'huile	\N
2883	200g de girolle	\N
2884	70g de lard ou de poitrine	\N
2885	2cuillères à soupe de beurre fondu (30 g)	\N
2886	Persil frais à volonté	\N
2887	600g de filet mignon de porc	\N
2888	1cuillère à café de sésame	\N
2889	2cuillères à café d'oignon frits (on en trouve tout prêts en grande surface, ils sont secs et croustillants)	\N
2890	1kg de foie d'agneau	\N
2891	6 courgettes	\N
2892	9cuillères à soupe d'huile d'olive	\N
2893	3cuillères à soupe de jus de citron	\N
2894	1.5gousse d'ail	\N
2895	1branche de romarin ciselé	\N
2896	3branches de thym émiétté	\N
2897	1bonne pincée de piment de Cayenne	\N
2898	12 gambas de calibre 16/800 congelées	\N
2899	1paquet de chapelure	\N
2900	8 gambas	\N
2901	1 citron vert	\N
2902	10g de gingembre	\N
2903	100g de riz	\N
2904	8morceaux de lapin assez petits	\N
2905	8petites tranches de lard fumé	\N
2906	1pot de tapenade	\N
2907	1kg de poisson (de préférence de la lotte ou de l'espadon)	\N
2908	1demi de piment fort	\N
2909	1demi de cumin en poudre	\N
2910	20tranches fines de poitrine fumée	\N
2911	12 champignons de Paris	\N
2912	Huile olive	\N
2913	1 porc de 1,8 à 2 kg	\N
2914	1 filet mignon de porc	\N
2915	2 bananes pas trop mures	\N
2916	8tranches de lard fumé	\N
2917	1/2 poivron	\N
2918	5cuillères de vin blanc	\N
2919	700g de filet mignon de porc	\N
2920	8cuillères à soupe de moutarde fine de Dijon	\N
2921	4tranches épaisses de lard fumé (poitrine fumée)	\N
2922	1kg de porc ou d'échine sans os	\N
2923	Girofle en poudre	\N
2924	Poivre , Cayenne	\N
2925	3 oignons émincés	\N
2926	12 olives vertes hachées	\N
2927	1cuillère à soupe de vinaigre de cidre	\N
2928	1cuillère à café de paprika doux	\N
2929	1cuillère à café de miel ou de sucre	\N
2930	Sel , Cayenne	\N
2931	1bouquet de romarin	\N
2932	3belles tranches d'ananas bien mûr	\N
2933	1brin de ciboulette	\N
2934	50cl de jus d'ananas	\N
2935	6 filets de poulet	\N
2936	4 poivrons	\N
2937	1cuillère à soupe de vinaigre	\N
2938	3cuillères à soupe de jus d'ananas	\N
2939	2 escalopes de poulet (environ 400 g)	\N
2940	2cuillères à soupe de miel d’acacia	\N
2941	1cuillère à soupe de sucre roux	\N
2942	1cuillère à soupe de gingembre râpé finement	\N
2943	1gousse d'ail écrasée finement	\N
2944	2cuillères à soupe de vinaigre de xérès	\N
2945	1poignée de sésame	\N
2946	Tomate cerise	\N
2947	1/2cuillère à café d'anis vert	\N
2948	1/2cuillère à café d'origan	\N
2949	1/2cuillère à café de muscade rapée	\N
2950	2 poivrons rouges coupés en carrés	\N
2951	4 blancs de poulet coupés en cubes	\N
2952	4tranches de lard	\N
2953	1 mangue dénoyautée et coupée en gros cubes	\N
2954	25g de yaourt nature	\N
2955	Citron vert râpé	\N
2956	20g de coriandre fraîche hachée	\N
2957	1 piment épépiné et haché	\N
2958	2 filets de poulet	\N
2959	1 maïs	\N
2960	2 figues (les figues de barbarie ne conviennent pas ici)	\N
2961	Huile d'olive (généreusement)	\N
2962	Paprika en poudre (3 cuillères à café)	\N
2963	Curry en poudre (3 cuillères à café)	\N
2964	5 gingembre frais	\N
2965	Aneth ciselé	\N
2966	4cuillères à soupe de moutarde à l'ancienne	\N
2967	4 blancs de poulet de 150 g chacun	\N
2968	5brins de thym	\N
2969	100g de cacahuète grillée	\N
2970	1 yaourt nature brassé	\N
2971	3cuillères à soupe de sésame	\N
2972	10cuillères à soupe de sauce soja ou de sauce à marinade japonaise (magasins asiatiques)	\N
2973	1demi de paprika doux	\N
2974	200g de noisettes décortiquées	\N
2975	100g de poivron rouge et vert	\N
2976	10cl de jus d'orange	\N
2977	10feuilles de menthe fraîche (ou plus)	\N
2978	2 poivrons (vert et jaune pour la couleur)	\N
2979	demi de citron vert	\N
2980	5 blancs de poulet	\N
2981	1bouquet de thym frais	\N
2982	1bouquet de laurier frais (si possible de jeunes feuilles)	\N
2983	1bouquet d'origan frais	\N
2984	1.2kg de blanc de poulet	\N
2985	4 yaourts	\N
2986	1petit bouquet de coriandre fraîche	\N
2987	1cuillère à café de piment de Cayenne	\N
2988	1cuillère à café d'ail semoule	\N
2989	1kg de poulet	\N
2990	8 oranges	\N
2991	6brins de persil	\N
2992	Piment de Cayenne en poudre	\N
2993	250g de miel liquide	\N
2994	4 filets de poulet	\N
2995	1cuillère à café de thym en poudre	\N
2996	1cuillère à café de romarin en poudre	\N
2997	1cuillère à café d'ail en poudre	\N
2998	4 quenelles (natures ou autres, selon votre goût)	\N
2999	16tranches de lard fumé	\N
3000	1sachet de pruneau dénoyautés	\N
3001	16 noix de st-jacques surgelées ou fraîches	\N
3002	1.8kg de saumon frais	\N
3003	16 champignons de Paris	\N
3004	300g de poivron	\N
3005	1 citron (1/2)	\N
3006	250g de lard salé	\N
3007	1/2l de crème fleurette	\N
3008	6 sot-l'y-laisse de dinde	\N
3009	1cuillère à soupe de persil frais	\N
3010	1petite boîte de champignon de Paris ou 3 à 4 champignons de Paris frais (émincés)	\N
3011	2cuillères à soupe de crème fraîche (de préférence liquide)	\N
3012	1paquet de Steak Gourmand Poivre et Persil LE BON VEGETAL HERTA®	\N
3013	100g de quinoa	\N
3014	100g de chou rouge	\N
3015	1cuillère à soupe de graines de sésame	\N
3016	1.5cuillère à soupe de miel	\N
3017	2cuillères à soupe de sauce soja salée	\N
3018	1pincée de curry moulu	\N
3019	10 feuilles de coriandre hâchées	\N
3020	800g de thon	\N
3021	3branches de thym	\N
3022	1cuillère à soupe de baies roses	\N
3023	600g de grenadin de veau de veau	\N
3024	2cuillères à soupe de curry en poudre	\N
3025	500g de veau coupé en petits cubes	\N
3026	1cuillère à soupe de thym	\N
3027	1cuillère à café de harissa	\N
3028	4cuillères à soupe d'huile d´olive	\N
3029	1 poivron rouge "facultatif"	\N
3030	1 poivron vert "facultatif"	\N
3031	8 Volailles (dinde ou poulet)	\N
3032	1pincée de piment fort	\N
3033	1/2 oignon haché finement	\N
3034	300g de semoule de couscous	\N
3035	2cuillères à soupe de raisins secs	\N
3036	1cuillère à soupe de coriandre fraîche hachée	\N
3037	1barquette d'Emincés nature Soja LE BON VEGETAL HERTA®	\N
3038	1cuillère à soupe de yaourt nature	\N
3039	1cuillère à soupe de curry doux	\N
3040	2 brocolis	\N
3041	Gruyère	\N
3042	1 brocoli frais	\N
3043	4 saucisses fumées selon leur taille	\N
3044	1kg de brocoli surgelés	\N
3045	2boîtes de pulpe de tomate concassées (soit 400 g environ)	\N
3046	Herbes de Provence (quantité selon votre goût)	\N
3047	1 cube de bouillon culinaire (type Kub'Or)	\N
3048	8 saucisses type chipolatas	\N
3049	500g de brocoli frais (ou surgelés)	\N
3050	2cuillères à soupe de boursin (ail etamp ; fines herbes)	\N
3051	100g de champignon émincés	\N
3052	12 oeufs	\N
3053	1 truffe	\N
3054	1 poivron jaune (le style paprika est meilleur)	\N
3055	2tranches de lard ou, à défaut, poitrine fumée	\N
3056	2g de margarine ou de graisse de porc	\N
3057	250g de patate douce	\N
3058	80g de farine	\N
3059	50g de poudre de noisette	\N
3060	50g de cacao en poudre non sucré	\N
3061	70g de sucre roux	\N
3062	60g d'huile de coco	\N
3063	1cuillère d'arôme vanille	\N
3064	4tranches de pain de campagne	\N
3065	2 tomates bien mûre	\N
3066	Mozzarella	\N
3067	Parmesan fraîchement râpé	\N
3068	4tranches de pain au sésame	\N
3069	200g de potiron	\N
3070	150g de fromage frais	\N
3071	500g de bucatini	\N
3072	4tranches de jambon	\N
3073	50cl de lait demi-écrémé	\N
3074	50g de maïzena	\N
3075	1pincée de muscade moulue	\N
3076	75g de farine	\N
3077	75g de sucre	\N
3078	2cuillères à soupe de Rhum Ambré LA MARTINIQUAISE	\N
3079	190g de crème de marron	\N
3080	80g de chocolat	\N
3081	25g de sucre glace	\N
3082	3cuillères à soupe d'eau	\N
3083	3 citrons (pour le jus)	\N
3084	160g de sucre semoule	\N
3085	40g de noix de coco rapée	\N
3086	40g de sucre	\N
3087	20g de Fleur de Maïs Maïzéna®	\N
3088	4 blancs d'oeuf	\N
3089	120g de sucre semoule	\N
3090	130g de farine	\N
3091	70g de beurre fondu	\N
3092	200g d'amandes	\N
3093	70g de sucre en poudre	\N
3094	8 blancs d'oeuf	\N
3095	225g de Nutella®	\N
3096	100g de sucre	\N
3097	250g de lait entier	\N
3098	300g de crème fleurette	\N
3099	2gousses de vanille	\N
3100	16g de gélatine en feuilles	\N
3101	18 boudoirs	\N
3102	450g de pomme	\N
3103	270g de Nutella®	\N
3104	5cl de Calvados BUSNEL	\N
3105	3 blancs d'oeuf	\N
3106	175g de sucre glace	\N
3107	80g de poudre d'amande	\N
3108	1cuillère à café d'extrait d'amande	\N
3109	200g de mascarpone	\N
3110	150g de spéculoos	\N
3111	90g de beurre	\N
3112	600g de myrtilles surgelées	\N
3113	350g de crème	\N
3114	3feuilles de gélatine	\N
3115	1tablette de chocolat blanc	\N
3116	60g de crème liquide	\N
3117	1cuillère à soupe d'arôme vanille	\N
3118	150g de crème liquide	\N
3119	5 oeufs entiers Simplement bon et bio®️	\N
3120	110g de sucre Sucandise®️	\N
3121	60g de maïzena	\N
3122	1pincée de sel Portland®️	\N
3123	200g de chocolat dessert corsé Scholetta®️	\N
3124	20cl de crème fraîche liquide Milsani®️	\N
3125	40g de beurre Chantoiseau®️ ramolli	\N
3126	90g de sucre	\N
3127	Nutella®	\N
3128	Coco en poudre	\N
3129	2feuilles de gélatine	\N
3130	2 citrons verts pour le zeste et le jus	\N
3131	250g de mascarpone	\N
3132	2 Escalopes Soja & Blé LE BON VEGETAL HERTA®	\N
3133	1/2 concombre	\N
3134	2poignées de salade	\N
3135	2 yaourts de soja	\N
3136	4cuillères à soupe de jus de citron	\N
3137	2kg de bulot	\N
3138	4bottes de citronnelle	\N
3139	4 gingembre	\N
3140	10feuilles de bergamote	\N
3141	1cuillère à soupe de piment	\N
3142	Sauce au gingembre	\N
3143	800g de blanquette de veau ( de la dinde ou du filet de poulet fond aussi l'affaire )	\N
3144	1 cube de bouillon de légume	\N
3145	1petite boîte de champignon (coupés)	\N
3146	4 pains pour hamburger	\N
3147	4tranches de cheddar	\N
3148	2 pains pour hamburger	\N
3149	2 steaks de boeuf	\N
3150	75g de roquefort	\N
3151	6 champignons blancs	\N
3152	2cuillères à café de vinaigre balsamique	\N
3153	30g de roquette	\N
3154	4 pommes de terre moyennes	\N
3155	1cuillère à soupe de mayonnaise	\N
3156	1cuillère à soupe de ketchup	\N
3157	2cuillères à café de farine	\N
3158	125g de farine de sarrasin	\N
3159	125g de farine de froment	\N
3160	35cl d'eau froide	\N
3161	brin de ciboulette	\N
3162	250g de saumon frais	\N
3163	feuille de mâche	\N
3164	Salade verte	\N
3165	4 steaks hachés charolais	\N
3166	25g de roquefort	\N
3167	15cl de crème fraîche semi-épaisse	\N
3168	Huile végétale ou de pépin de raisins	\N
3169	2tranches de jambon , soit une divisé en 4	\N
3170	6 pains pour hamburger	\N
3171	4cuillères d'huile	\N
3172	Ketchup (facultatif)	\N
3173	100g de fromage	\N
3174	4tranches de pain de mie	\N
3175	25cl de bière brune	\N
3176	1/4 chou rouge	\N
3177	2 muffins complets	\N
3178	1cuillère à café d'huile d'olive « fruité noir » Le Mas de l’Ange	\N
3179	2 blancs de poulet fermier	\N
3180	2cuillères à soupe d'olives noires au thym et à la sarriette, la grossane	\N
3181	8 noisettes décortiquées	\N
3182	2tranches d'aubergine nappées d’huile d’olive et de thym	\N
3183	feuille de cresson ou d’épinard	\N
3184	4 tomates mi-séchée de Provence	\N
3185	Poivre noir Ceylan	\N
3186	100g de roquette	\N
3187	10 tomates cerise	\N
3188	400g de pois chiches en boîte égouttés	\N
3189	Haricots rouges	\N
3190	Pois chiches	\N
3191	1cuillère à soupe d'origan	\N
3192	2cuillères à soupe de mayonnaise	\N
3193	250g de lentilles verte	\N
3194	60g de chapelure	\N
3195	60g de comté	\N
3196	2 pains complets pour burger	\N
3197	2 Steaks Soja & Blé LE BON VEGETAL HERTA®	\N
3198	100g de tomate confite à l’huile	\N
3199	1/4 oignon rouge	\N
3200	20g de roquette	\N
3201	20g de pignons de pin	\N
3202	2cuillères à soupe de vinaigre balsamique (crème)	\N
3203	4 tortillas au blé complet	\N
3204	250g de haricots noirs en conserve, égouttés	\N
3205	200g de quinoa	\N
3206	1boîte de tomate concassée	\N
3207	200g de maïs	\N
3208	1filet d'huile de colza	\N
3209	4 tortillas (grandes)	\N
3210	1paquet d'Emincés nature Soja LE BON VEGETAL HERTA®	\N
3211	1petite boîte de haricots rouges	\N
3212	50g de salade	\N
3213	2 cornichons aigre-doux	\N
3714	10 olives noires	\N
3214	2cuillères à soupe de crème fraîche 30% de matière grasse	\N
3215	500g de tripes cuites	\N
3216	75g de lardons	\N
3217	300g de pomme de terre	\N
3218	200g de chou	\N
3219	200g de poireau	\N
3220	200g de céleri -boule	\N
3221	1 cumin	\N
3222	1brin de marjolaine	\N
3223	2cuillères à soupe de basilic hâché	\N
3224	Bouillon cube	\N
3225	1 butternut de taille normale	\N
3226	4 quenelles natures de taille moyenne	\N
3227	125g de comté	\N
3228	1 courge de type Butternut	\N
3229	1 butternut moyen	\N
3230	15 châtaignes fraîches ou en conserve	\N
3231	400g de cèpes frais	\N
3232	150g de viande de bœuf hachée	\N
3233	Persil frais haché	\N
3234	1beau morceau de lieu noir (choisir de préférence un gros morceau épais à couper en 2)	\N
3235	10 oignons verts (oignons nouveaux, cébettes...)	\N
3236	Poivre de Cayenne	\N
3237	600g de lieu noir	\N
3238	2 bananes (pas trop mures)	\N
3239	230g de tomates pelées	\N
3240	8morceaux de gingembre confit	\N
3241	1cuillère à soupe de pâte de curry rouge	\N
3242	2cuillères à soupe d'huile de sésame (ou de tournesol à défaut)	\N
3243	1 lieu noir de 500 g	\N
3244	10cl de crème UHT (ou 40 g de beurre)	\N
3245	1cuillère à soupe de câpres fines au vinaigre	\N
3246	2tranches de lieu noir (frais ou surgelé)	\N
3247	Poivre (à votre goût)	\N
3248	1beau morceau de lieu noir d'environ 500 g	\N
3249	1pointe de curry	\N
3250	1filet de lieu noir congelé	\N
3251	1 figue	\N
3252	1rondelle de chèvre frais	\N
3253	800g de lieu noir	\N
3254	33cl de vin blanc demi-sec	\N
3255	4tranches de lieu noir	\N
3256	600g de lieu noir sans la peau	\N
3257	20cl de fond de poisson	\N
3258	10cl de lait de coco	\N
3259	1bouquet d'oignon nouveau	\N
3260	1 piment rouge	\N
3261	1/4botte de coriandre	\N
3262	1cuillère à soupe de sauce soja claire	\N
3263	500g de lieu noir en morceaux	\N
3264	400g de champignon de Paris ( 1 grosse boîte )	\N
3265	1kg de lieu noir	\N
3266	800g de cabillaud	\N
3267	10 pommes de terre Charlotte	\N
3268	50g de beurre demi-sel	\N
3269	2 oranges	\N
3270	Baies roses (facultatif)	\N
3271	1/2bouteille de Chablis	\N
3272	20cl de crème	\N
3273	6filets de lieu noir	\N
3274	6 oignons	\N
3275	20 olives noires dénoyautées	\N
3276	4morceaux de lieu noir ( soit environ 700 g)	\N
3277	4 abricots , bien mûrs	\N
3278	1 citron , découpé en rondelles	\N
3279	Poivron jaune	\N
3280	1 oignon , à découper en rondelles	\N
3281	100g d'amandes entières	\N
3282	125g de crème fraîche	\N
3283	1cuillère à soupe de carvi	\N
3284	1cuillère à soupe de gingembre	\N
3285	1cuillère à café de câpres	\N
3286	Poivre vert	\N
3287	4pavés de lieu noir de 200g env., sans peau	\N
3288	40g de beurre 1/2 sel	\N
3289	1branche de menthe	\N
3290	1/2bâton de réglisse fin	\N
3291	4filets de lieu noir	\N
3292	400g d'épinard hachés	\N
3293	Béchamel	\N
3294	100g de farine	\N
3295	4 lieu noir	\N
3296	500g de pomme de terre grenailles	\N
3297	80g de framboises	\N
3298	10cuillères à soupe d'huile d'olive	\N
3299	100ml de vinaigre de framboise	\N
3300	3cuillères à soupe de sirop de grenadine	\N
3301	2 lieu noir	\N
3302	2poignées de gambas (ou crevettes)	\N
3303	1 concentré de tomates	\N
3304	boîte de tomate concassée	\N
3305	1poignée de tomate cerise	\N
3306	1botte de coriandre fraiche	\N
3307	1 oignon finement haché	\N
3308	1branche de céleri finement hachée	\N
3309	1/2 poivron vert épépiné et finement haché	\N
3310	90g de noix hachées	\N
3311	6cuillères à soupe de chapelure blanche sèche	\N
3312	1cuillère à café de sauce worcestershire	\N
3313	1cuillère à café d'estragon sec	\N
3314	12.5cl de crème fraîche	\N
3315	125g de beurre fondu	\N
3316	25cl d'eau	\N
3317	3brins de persil	\N
3318	6grains de poivre	\N
3319	1/2 citron (jus)	\N
3320	2 oeufs durs hachés	\N
3321	120g d'olive verte (dénoyautées)	\N
3322	Autres épices à poisson, selon votre goût	\N
3323	2 lieu noir de 300g chacun	\N
3324	8tranches de lard	\N
3325	1 poivron rouge coupé en dès	\N
3326	2 échalotes émincées	\N
3327	4pavés de lieu noir frais, sans peau	\N
3328	750g de pomme de terre à chair ferme	\N
3329	150g de tomate semi-confites	\N
3330	2cuillères à soupe de pistou	\N
3331	12cl d'huile d'olive	\N
3332	1petit bouquet de Fines herbes mélangées	\N
3333	2filets de lieu noir	\N
3334	2 tomates épépinées et coupées en morceaux	\N
3335	1 échalote (selon le goût)	\N
3336	1gousse d'ail (selon le goût)	\N
3337	Fromage râpé (style gruyère)	\N
3338	3filets de lieu noir	\N
3339	3 pommes de terre nouvelles par personnes	\N
3340	6tranches de chorizo	\N
3341	1 citronnelle coupée en rondelles ou 1 citron vert	\N
3342	1cuillère à café de pâte à citronnelle	\N
3343	50g de pulpe de tomate en conserve	\N
3344	20cl de lait de coco non sucré	\N
3345	1.2kg de lieu noir	\N
3346	1l de moules	\N
3347	50g de beurre (sauce) + 20 g (plat)	\N
3348	50cl de lait (=1/2 litre)	\N
3349	200g de raclette (fromage)	\N
3350	Muscade râpée (assaisonnement)	\N
3351	6tranches de lieu noir	\N
3352	3 citrons	\N
3353	1 courgette de taille moyenne	\N
3354	4g de lieu noir en tranche	\N
3355	4filets d'anchois à l'huile d'olive	\N
3356	2branches de basilic ou de persil plat ou frisé	\N
3357	4filets de lieu noir (ou de skrei, cabillaud norvégien proposé en février sur nos étals)	\N
3358	50g de farine (plus ou moins selon la taille des filets de poisson)	\N
3359	1 pamplemousse rose	\N
3360	1 orange non traitée	\N
3361	1 mandarine	\N
3362	25g de beurre sans sel	\N
3363	1pincée de sucre de canne (ou un trait de miel)	\N
3364	4filets de lieu noir de 200 g	\N
3365	150g de farine tamisée	\N
3366	20cl de bière blonde	\N
3367	8 navets	\N
3368	200g de roquefort	\N
3369	20cl de crème fraîche (ou de soja cuisine)	\N
3370	1cuillère à soupe d'armagnac	\N
3371	5cuillères à soupe de Maury (ou un autre vin doux rouge peu sucré)	\N
3372	1kg de pomme de terre qui tiennent bien à la cuisson	\N
3373	60g de beurre environ, ou du saindoux	\N
3374	Vin blanc (facultatif)	\N
3375	1boîte de Lait Concentré Sucré sans Lactose NESTLÉ	\N
3376	120cl de café fort et chaud	\N
3377	3cuillères à café de cacao amer en poudre NESTLÉ DESSERT	\N
3378	4 cailles	\N
3379	1cube de bouillon volaille	\N
3380	1pincée de cannelle en poudre	\N
3381	4 cailles (vidées, sans têtes...et tout et tout)	\N
3382	Pêche aux sirops	\N
3383	4 cailles (vidées et sans tête de préférence)	\N
3384	3gousses d'ail en chemise préalablement lavées	\N
3385	20grains de poivre noir	\N
3386	3cl de vinaigre	\N
3387	3cl d'huile d'olive	\N
3388	1cuillère de gros sel	\N
3389	4 figues	\N
3390	20g de raisin blonds secs	\N
3391	20cl de fond de volaille	\N
3392	50cl de vin d’épices (un bon vin rouge charpenté, auquel on adjoint gingembre, cannelle, cumin, girofle selon son goût)	\N
3393	4cl de hydromel	\N
3394	1cl d'huile	\N
3395	1noisette de beurre .	\N
3396	4 cailles moyennes	\N
3397	1cube de fond de veau	\N
3398	Baie de genévrier	\N
3399	15cl d'Alcool à base de baies de genévrier)	\N
3400	8 cailles	\N
3401	2cuillères à soupe de moutarde forte	\N
3402	4 cailles (vidées et préparées par votre boucher)	\N
3403	Huile de tournesol (de quoi napper le fond du fait-tout)	\N
3404	Beurre doux (50g)	\N
3405	1verre de vin blanc (juste pour donner du goût) (J'ai utilisé du Gewurztraminer)	\N
3406	1 noisettes de margarine	\N
3407	2cuillères à café de miel doux	\N
3408	25cl de vin blanc doux	\N
3409	4 cailles prêtes à cuire	\N
3410	1/2l de fond de volaille	\N
3411	8feuilles de sauge	\N
3412	2feuilles de menthe	\N
3413	100g de pancetta (ou lardons fumés)	\N
3414	200g de brocciu de brousse ou fromage frais	\N
3415	4 cailles non farcies	\N
3416	125g de riz	\N
3417	22cl d'eau	\N
3418	1 petit suisse nature	\N
3419	1grosse cuillère à soupe de miel toutes fleurs	\N
3420	1.5cuillère à café de quatre-épices	\N
3421	1 vin blanc de cuisine	\N
3422	4 cailles prêtes à rôtir	\N
3423	16 abricots frais	\N
3424	Amandes effilées	\N
3425	2cuillères à soupe de fond de volaille	\N
3426	15cl d'amaretto	\N
3427	20g de beurre mou	\N
3428	4 cailles , plumées et étêtées	\N
3429	1branche de romarin	\N
3430	20 genièvre	\N
3431	2cuillères à soupe de gelée de pomme ou de coing selon ce que vous avez	\N
3432	6 cailles	\N
3433	6tranches de lard	\N
3434	1boîte de pulpe de tomate	\N
3435	1boîte de sauce tomate cuisinée	\N
3436	1/2verre d'armagnac	\N
3437	Cèpes séchés, ou morilles, ou girolles	\N
3438	1/2verre de vin rouge	\N
3439	1.5cuillère à café de maïzena	\N
3440	1bonne poignée d'olives noires de Nice	\N
3441	2 cailles	\N
3442	4 cailles prête à cuire	\N
3443	1bouquet de herbes ( thym, romarin,origan, laurier )	\N
3444	6 figues épluchées	\N
3445	200ml de vin blanc	\N
3446	1cuillère à soupe de chocolat amer , rapé	\N
3447	Sel et poivrz	\N
3448	15 cerneaux de noix	\N
3449	15 abricots secs	\N
3450	1 échalote moyenne ou 2 petites	\N
3451	100g de lardons en dés, non fumés	\N
3452	Tomate cerise pour la décoration	\N
3453	1verre d'eau bouillante	\N
3454	2boîtes de lardons	\N
3455	2verres de porto (20 cl)	\N
3456	2 mandarines	\N
3457	500g de marrons (en boîte, ou sous vide)	\N
3458	250g de champignon (au choix, plus ils sont parfumés mieux c'est, cela dépend de vos finances...)	\N
3459	2 briques (= 2 x 20 cl) de crème fraîche liquide	\N
3460	10 noix fraiches	\N
3461	1/2 poivron (rouge ou orange)	\N
3462	20g de raisins secs	\N
3463	2cuillères d'huile de tournesol ou de raisin	\N
3464	125g d'oignon grelots	\N
3465	1cuillère d'huile	\N
3466	200g de foie -gras mi-cuit	\N
3467	500g de pleurote grises	\N
3468	10brins de persil	\N
3469	6 poivrons	\N
3470	Petit salé	\N
3471	Ficelle	\N
3472	1petite boîte de Foie gras ou de mousse de foie gras (120 g environ)	\N
3473	250g de pruneau d'Agen	\N
3474	150g de poitrine de porc maigre	\N
3475	1 thym	\N
3476	1boîte de raisin au naturel	\N
3477	2verres de liqueur de cognac	\N
3478	2dl de crème fraîche	\N
3479	2 raisin noir et blanc	\N
3480	100g de beurre mou	\N
3481	4tranches de Foie gras	\N
3482	600g de chou vert émincé	\N
3483	400g de chanterelle (ou autre champignons des bois)	\N
3484	12 asperges	\N
3485	4 poires + poudre de cannelle	\N
3486	Beurre ou margarine	\N
3487	4 cailles (sans la tête)	\N
3488	4 pommes acidulées	\N
3489	1poignée de raisins secs	\N
3490	15cl de cognac	\N
3491	1grosse cuillère à soupe de miel	\N
3492	3tranches de jambon de pays	\N
3493	50g de cantal entre-deux	\N
3494	1 cube de bouillon de légumes	\N
3495	4 cailles ,	\N
3496	100g de porc bœuf	\N
3497	100g de foie de volaille	\N
3498	1/2botte de coriandre fraîche	\N
3499	1 petit suisse	\N
3500	4 fruits de la passion	\N
3501	Jus de cuisson	\N
3502	2cuillères de pineau des charentes	\N
3503	500g de raisin verts	\N
3504	20cl de Sauternes (soit environ 2 verres)	\N
3505	200g de Foie gras de canard cru	\N
3506	10cl d'armagnac	\N
3507	4cuillères à soupe de canard	\N
3508	8 cailles vidées	\N
3509	500g de raisin blanc type Chasselas	\N
3510	200g de Foie gras de canard	\N
3511	20cl de Sauternes	\N
3512	300g de chair à saucisse	\N
3513	3tranches de pain rassi	\N
3514	15cl de porto	\N
3515	75cl de fond de veau léger	\N
3516	500g de marrons	\N
3517	10 pommes de terre (type grenaille)	\N
3518	4 cailles (compter 2 par personnes)	\N
3519	150g de Foie gras mi-cuit	\N
3520	50g de haché de veau	\N
3521	4 poires bien mûres	\N
3522	8 cailles bien dodues prêtes à cuire	\N
3523	Poivre au citron	\N
3524	Aromate	\N
3525	8gousses d'ail hachées	\N
3526	16feuilles de sauge	\N
3527	1poignée de champignon noir chinois	\N
3528	6 champignons chinois parfumés moyens	\N
3529	2paquets de vermicelles de soja (transparents)	\N
3530	2cuillères à soupe de glutamate	\N
3531	4 Fleurs d'anis	\N
3532	1/4l de soja	\N
3533	12 fonds d'artichaut	\N
3534	4 cailles préparées (vidées, ététées)	\N
3535	1/2 ananas	\N
3536	6cl de rhum brun	\N
3537	33cl de pineau des charentes	\N
3538	2cuillères à soupe de fond de veau	\N
3539	1/2cuillère à café d'extrait de vanille	\N
3540	1pincée de poivre du moulin	\N
3541	6 cailles préparées par votre boucher	\N
3542	10cl de vin blanc sec (type Sauvignon)	\N
3543	1.2kg de blette	\N
3544	1.4kg de viande de porc	\N
3545	1.6kg de lard maigre	\N
3546	Crépine de porc	\N
3547	3cuillères à café de sel par kg (15 g/kg)	\N
3548	1pincée de poivre par kg (1 g/kg)	\N
3549	Herbes de Provence (thym, laurier...)	\N
3550	1 laitue (environ 200 g)	\N
3551	150g d'épinard	\N
3552	50g de roquette	\N
3553	500g de viande de porc	\N
3554	250g de bacon (ou lardons ou poitrine fumée)	\N
3555	2 salades frisées	\N
3556	1 batavia	\N
3557	4feuilles de blette	\N
3558	600g d'épinards frais	\N
3559	1/2cuillère à café de quatre-épices	\N
3560	2tranches de poitrine fumée	\N
3561	feuille de sauge	\N
3562	80g de gruyère râpé	\N
3563	8cl d'huile d'olive	\N
3564	200g d'escalope de poulet	\N
3565	80g de raisins secs	\N
3566	180g de farine de sarrasin	\N
3567	180g d'andouille	\N
3568	150g de tomme de brebis	\N
3569	25cl de lait (entier de préférence)	\N
3570	1sachet de levure	\N
3571	200g de farine de châtaignes	\N
3572	4 jaunes d'oeuf	\N
3573	5 blancs d'oeuf	\N
3574	12.5cl de lait	\N
3575	1poignée de gruyère râpé	\N
3576	75g de mimolette vieille	\N
3577	1pot de tomate séchée	\N
3578	200g de farine	\N
3579	125g de beurre demi-sel	\N
3580	250g de julienne de légumes surgelée	\N
3581	100g de gruyère	\N
3582	1verre de lait (environ 15 cl)	\N
3583	1 poivre	\N
3584	120g de farine de maïs	\N
3585	1 carré frais "gervais"	\N
3586	1petit bouquet de persil et de menthe finement hachés	\N
3587	20cl d'eau froide	\N
3588	8cl d'huile d'arachide	\N
3589	Ciboulette ciselée	\N
3590	100g de truite fumée (à peu près 3 tranches)	\N
3591	200g de chèvre	\N
3592	250g de viande hachée de boeuf	\N
3593	4portions de fromage frais (type Vache qui rit)	\N
3594	150g de bleu d'Auvergne (ou fourme, roquefort...)	\N
3595	75g de raisins secs	\N
3596	3cuillères à soupe de porto (ou moscatel, pineau, vin blanc doux,...)	\N
3597	200g de farine (blanche ou moitié blanche/moitié farine complète)	\N
3598	120g de crème épaisse	\N
3599	150g de parmesan râpé	\N
3600	150g d'abricot secs moelleux	\N
3601	Origan ou basilic si on veut	\N
3602	Tomate confites (à l'huile) ou séchées	\N
3603	180g de farine	\N
3604	10cl de lait demi écrémé	\N
3605	3cuillères à soupe d'huile de colza	\N
3606	100g de jambon en dés (de porc ou, pour ceux qui n'en mangent pas, de dinde halal)	\N
3607	500g de courgette crues	\N
3608	1cuillère à soupe de herbes aromatiques (au choix, ciboulette, herbes de Provence, basilic, persil)	\N
3609	1cuillère à café de thym séché	\N
3610	4tranches de jambon blanc	\N
3611	4 saucisses de strasbourg	\N
3612	9cl d'huile d'olive	\N
3613	12cl de lait	\N
3614	1boîte de thon au naturel(140g)	\N
3615	2petites boîtes de thon à la catalane (135g chacune) ou une grosse	\N
3616	150g de saumon frais	\N
3617	150g de saumon fumé	\N
3618	10cl de beurre fondu	\N
3619	150g de gruyère râpé	\N
3620	1paquet de levure	\N
3621	60g de poivron rouge	\N
3622	60g de poivron vert	\N
3623	60g de poivron jaune	\N
3624	3cuillères à soupe d'aneth	\N
3625	4 oeufs entier + 1 jaune	\N
3626	1/2verre de lait tiède	\N
3627	60g d'emmental	\N
3628	1cuillère à soupe de cognac (et plus si affinité)	\N
3629	1.2kg de carotte	\N
3630	200g de lard fumé en tranches fines	\N
3631	60g de coquillettes	\N
3632	100g de fromage râpé	\N
3633	300g de courgette épluchées coupées en lamelles fines	\N
3634	1gousse d'ail émincée	\N
3635	1 poivron rouge émincé	\N
3636	170g de farine	\N
3637	1.5cuillère à café de sel fin	\N
3638	1cuillère à café de miel de Savoie	\N
3639	7cl de lait	\N
3640	1 patate douce	\N
3641	1cuillère à soupe de garam massala sous forme de pâte	\N
3642	130g de gruyère râpé	\N
3643	1 crottin de chèvre	\N
3644	100g de poulet	\N
3645	200g de foie de volaille	\N
3646	1cuillère à soupe de porto	\N
3647	50g de pignons de pin	\N
3648	2pincées de sel et de poivre	\N
3649	10g de levure fraîche de boulanger	\N
3650	22cl de lait	\N
3651	500g de farine à pain	\N
3652	120g de sucre en poudre	\N
3653	1sachet de sucre vanillé	\N
3654	1cuillère à café de cannelle poudre	\N
3655	250g de fruits secs ou confits sectionnés en petits dés soit l'équivalent d'un bol de petit-déjeuner (J'ai utilisé des raisins blancs et noirs, fraises, pruneaux et ananas -tous secs ou confits bien sûr-)	\N
3656	125g de sucre	\N
3657	1/2sachet de levure chimique	\N
3658	250g de fruits secs abricots, bananes, raisins, pruneaux, figues,…) ou confits (poires, cerises,…)	\N
3659	125ml de rhum	\N
3660	200g de poulet rôti	\N
3661	200g de légume de printemps, 1 poignée de haricots verts, 1 carotte nouvelle, 1 poignée de pois gourmands, 2 petits navets	\N
3662	13cl de lait écrémé	\N
3663	Persil et ciboulette	\N
3664	1 moules	\N
3665	120g de poireau	\N
3666	10cl de crème fraîche liquide	\N
3667	1grosse cuillère à soupe d'huile d'olive	\N
3668	1/2 levure	\N
3669	20 olives vertes dénoyautées	\N
3670	Sel du poivre	\N
3671	1 ortie (la valeur d'une assiette pleine environ)	\N
3672	Lardons	\N
3673	50g de fécule de pomme de terre	\N
3674	Poivre (ou piment d'espelette)	\N
3675	1boîte de petit pois	\N
3676	Poivre & basilic	\N
3677	100g de maïzena	\N
3678	1/2boîte de tomate	\N
3679	Fromage râpé	\N
3680	Papier sulfurisé	\N
3681	10 saucisses knaki	\N
3682	180g de gruyère râpé	\N
3683	11g de levure	\N
3684	200g de tomate séchée	\N
3685	50g de câpres	\N
3686	120g de sucre Sucandise®️	\N
3687	90g de beurre Chantoiseau®️	\N
3688	1cuillère à café de levure chimique	\N
3689	40cl de Bordeaux rouge AOC 75 CL* Fontagnac®️	\N
3690	5 poires	\N
3691	1cuillère à café de muscade en poudre	\N
3692	1 boîte de Lait Concentré Sucré Nestlé 397 g	\N
3693	30g de graine de pavot	\N
3694	1cuillère à soupe de crème fraîche épaisse 30% de matière grasse ou mascarpone	\N
3695	250g de tomme de chèvre	\N
3696	15cl d'huile d'olive + pour le plat	\N
3697	180g de jambon	\N
3698	Poulet ou lardons (en dés donc)	\N
3699	180g de fromage râpé	\N
3700	1sachet de pistache (ou plus selon les préférences)	\N
3701	200g de Volailles découpés en gros dés	\N
3702	2 oignons émincés + 1 gousse d'ail	\N
3703	80g de ricotta	\N
3704	2cuillères à soupe de coriandre fraîche hachée	\N
3705	2cuillères à soupe de persil frais haché	\N
3706	1cuillère à soupe de noix de coco rapée	\N
3707	1cuillère à café de sel fin + poivre au goût	\N
3708	20g de beurre pour le moule	\N
3709	7cl d'huile	\N
3710	200g d'épinard	\N
3711	100g de chèvre	\N
3712	1poignée de basilic haché frais ou 1 cuillère à soupe de basilic déshydraté	\N
3713	10 cerneaux de noix concassés	\N
3715	3 oeufs entiers	\N
3716	70g de parmesan râpé	\N
3717	10 tomates séchées marinées	\N
3718	175g de farine	\N
3719	1botte de basilic	\N
3720	100g de coppa	\N
3721	300g de tomate séchée	\N
3722	100g d'olives à la grecque	\N
3723	12 olives noires dénoyautées	\N
3724	4 saucisses de Strasbourg	\N
3725	Gruyère ou comté en petits cubes (plus ou moins selon votre goût)	\N
3726	3/4cuillère à café de sel	\N
3727	2cuillères à soupe de farine et un peu d'huile (ou une noix de beurre) pour le moule	\N
3728	2 poivrons bien charnus	\N
3729	2 courgettes très fermes	\N
3730	2 aubergines moyennes	\N
3731	50g de parmesan frais	\N
3732	200g de farine (ou 20 g de farine de riz et 80 g de Maïzena pour les intolérants au gluten)	\N
3733	12.5cl de lait (ou du lait d'amande pour les intolérants au lactose)	\N
3734	100g d'emmental râpé et 80 g de mélange oméga Boost / power mix Gretel	\N
3735	1pincée de sel et de poivre	\N
3736	1 bûche de chèvre découpée en dés	\N
3737	100g de tomate séchée	\N
3738	2 tomates fraîches découpées en petits cubes	\N
3739	1l de lait (entier, c'est meilleur)	\N
3740	250g de pâtes (type coquillages)	\N
3741	13cl de lait	\N
3742	75g de noisettes	\N
3743	5tranches de pancetta	\N
3744	150g de gorgonzola	\N
3745	12cl de lait entier	\N
3746	2cuillères à café de gingembre	\N
3747	1kg de calamar	\N
3748	1 cube de bouillon de poisson	\N
3749	1kg de calamar frais	\N
3750	500g d'oignon émincés	\N
3751	2boîtes de tomate concassée (celles de 425 ml)	\N
3752	1petit verre de cognac	\N
3753	cuillère à soupe de homard concentrée (facultatif)	\N
3754	600g de calamar frais (avec leur encre )	\N
3755	4 poivrons , de deux couleurs, par exemple 2 jaunes et deux rouges	\N
3756	1gros bouquet de persil hâché	\N
3757	1kg de calamar frais ou décongelés	\N
3758	1 oignon doux	\N
3759	1kg de tomate	\N
3760	1/2cuillère à café de curry	\N
3761	1bonne pincée de sel	\N
3762	1cuillère à café de basilic	\N
3763	1cuillère à soupe de persil - huile d'olive (ou beurre)	\N
3764	Pâtes	\N
3765	800g de calamar	\N
3766	1petit bouquet de persil	\N
3767	3brins de thym	\N
3768	750g d'encornet surgelés	\N
3769	1grosse boîte de tomates pelées au jus	\N
3770	3cuillères à soupe de paprika	\N
3771	1pincée de piment de Cayenne	\N
3772	500g de calamar	\N
3773	1kg de tomate fraîches ou pelées	\N
3774	2cuillères à soupe de gingembre frais en poudre	\N
3775	1 piment d'Espelette	\N
3776	1kg de calamar encornés ou lamelles	\N
3777	70g de concentré de tomates	\N
3778	30cl de crème fraîche épaisse	\N
3779	1pointe de piment fort	\N
3780	10cl d'armagnac ou cognac	\N
3781	1.2kg de calamar ou encornets	\N
3782	2kg d'épinard	\N
3783	600g de calamar	\N
3784	100g de bleu	\N
3785	100g de chèvre frais	\N
3786	4 Vache qui rit®	\N
3787	400g de pulpe de tomate	\N
3788	1cuillère à soupe de piment d'Espelette	\N
3789	1.5kg de calamar bien frais	\N
3790	200g d'olives noires	\N
3791	1cuillère à café de graine de fenouil	\N
3792	1bulbe de fenouil	\N
3793	1trait de pastis	\N
3794	500g de moules	\N
3795	2kg de calamar nettoyés	\N
3796	1kg de viande hachée (porc et veau)	\N
3797	1botte de persil plat ou frisé	\N
3798	2 oignons paille	\N
3799	3boîtes de tomates pelées entières (ou concassées)	\N
3800	8cuillères à soupe d'huile d'olive	\N
3801	600g de seiche ou calamars	\N
3802	150g de fromage blanc	\N
3803	50g d'huile	\N
3804	1kg de calamar frais ou surgelés	\N
3805	300g de palette de porc hachée	\N
3806	5g de champignon noir	\N
3807	1 échalote finement hachée	\N
3808	1boîte de 400 g de pulpe de tomate	\N
3809	2pincées de thym	\N
3810	2 calamars	\N
3811	1sachet de tomate à l’italienne (Picard)	\N
3812	5 petits pains suédois	\N
3813	2cuillères à café de miel	\N
3814	16 calamars surgelés	\N
3815	250g d'épinard hachés surgelés	\N
3816	8 champignons de Paris frais	\N
3817	8 calamars ou 12 moyens	\N
3818	200g de riz cuit	\N
3819	1/2botte de coriandre	\N
3820	4branches de menthe	\N
3821	1/2botte de persil	\N
3822	4feuilles de céleri	\N
3823	4 anchois	\N
3824	750g de sauce tomate	\N
3825	1 calamar d'1 kg	\N
3826	300g de saucisse	\N
3827	300g de viande de boeuf hachée	\N
3828	600g de calamar (chipirones)	\N
3829	2 oignons coupés en lamelles	\N
3830	3gousses d'ail , pelées et hachées	\N
3831	1/2cuillère à café de Pili Pili , réduit en flocons	\N
3832	1cuillère à soupe de paprika	\N
3833	4 tomates épépinées et coupées en dés	\N
3834	2cuillères à soupe de basilic haché	\N
3835	6 seiches	\N
3836	2 tomates grappe	\N
3837	1petite boîte de champignon de Paris en lamelles (soit 115 g égouttés)	\N
4082	1 oignon entier	\N
3838	1boîte de tomate en cubes (ou 3 ou 4 tomates fraîches coupées en dés)	\N
3839	Ail et oignons (selon vos goûts) coupées finement	\N
3840	Olives noires niçoises (dénoyautées si possible)	\N
3841	1 poivron vert (coupés en dés ou en lanières fines)	\N
3842	2kg de calamar en lamelles	\N
3843	952g de tomates pelées	\N
3844	4branches de thym (et/ou de sarriette)	\N
3845	1feuille de céleri	\N
3846	1/2cuillère à café de piment de Cayenne (ou d'Espelette)	\N
3847	1cuillère à soupe de pâte de curry	\N
3848	2cuillères à café de sauce nuoc mam	\N
3849	1pincée de sucre	\N
3850	1kg de calamar congelés	\N
3851	1cuillère à soupe de câpres	\N
3852	2pincées de piment de Cayenne ou d'Espelette	\N
3853	500g d'épinard	\N
3854	1sachet de calamar (ça fond à la décongélation)	\N
3855	150g d'oignon émincés	\N
3856	4gousses d'ail écrasées	\N
3857	12 calamars nettoyés par le poissonier	\N
3858	6 tomates fraîches	\N
3859	2 calamars (au rayon frais) avec leurs tentacules (les faire nettoyer de préférence)	\N
3860	4 tomates en grappes moyennes	\N
3861	4cuillères à soupe de persil haché frais	\N
3862	2 pâtes feuilletées (ou pâtes à pizza)	\N
3863	2 poivrons jaunes	\N
3864	5tranches de fromage de chèvre	\N
3865	1 camembert bien fait dans une boite en bois	\N
3866	Armagnac	\N
3867	Pain grille	\N
3868	5 pommes de terre	\N
3869	1 camembert	\N
3870	40g d'amandes émondées	\N
3871	1cuillère à café de Ras el Hanout	\N
3872	1bonne cuillère à soupe de miel liquide	\N
3873	100g de pois frais	\N
3874	100g de haricot vert	\N
3875	200g d'olives	\N
3876	125g de gruyère râpé	\N
3877	10cl d'huile olive	\N
3878	1cuillère à café de muscade râpée	\N
3879	1petite boîte de canard	\N
3880	300g de raisin (muscat)	\N
3881	10cl de vin blanc sec fruité (muscat d'Alsace)	\N
3882	8tranches de pain de mie	\N
3883	50cl de béchamel	\N
3884	2filets de canard sans la peau	\N
3885	Huile d'arachide pour la friture	\N
3886	8 champignons chinois déssechés et trempés dans l'eau 15 à 20 min et finement émincés	\N
3887	2 petits piments verts finement émincés	\N
3888	60cl de bouillon de poule	\N
3889	250g d'ananas frais coupés en morceaux	\N
3890	1 piment rouge doux émincé	\N
3891	3gousses d'ail finement hachées	\N
3892	4 échalotes finement émincées	\N
3893	6cuillères à soupe de poisson vietnamienne (nuoc-mâm)	\N
3894	brin de coriandre	\N
3895	Cacahuète grillée écrasées	\N
3896	1 canard	\N
3897	50cl de jus d'orange pressé	\N
3898	1/2 cube de bouillon	\N
3899	1/2 Magret de Canard ou 1 magret de canard par personne, selon les appétits	\N
3900	3 oranges	\N
3901	2 fond brun (gibier,..) liquide	\N
3902	5cuillères à soupe de sucre	\N
3903	2cuillères à soupe de vinaigre blanc	\N
3904	1 canette	\N
3905	450g de canard sans peau	\N
3906	400g d'oignon	\N
3907	2cuillères à café d'épices tandoori	\N
3908	4gousses de cardamome	\N
3909	1 étoile de badiane ( anis étoilé)	\N
3910	1 canard de 1,5 kg à 2 kg découpé	\N
3911	1morceau de gingembre	\N
3912	33cl de bière Bourbon ou autre	\N
3913	1 canard de barbarie	\N
3914	Orange	\N
3915	Sucre en poudre 200 g	\N
3916	3cuillères à soupe de vinaigre	\N
3917	2 cuisses de canard	\N
3918	240g de riz	\N
3919	Piment (purée possible à doser selon la totérance des convives)	\N
3920	Citron vert (ou vinaigre)	\N
3921	200g de canard	\N
3922	15 pruneaux (suivant vos goûts)	\N
3923	40cl de crème fraîche liquide	\N
3924	1cuillère à café d'armagnac	\N
3925	morceau de cerneau de noix	\N
3926	Magret de Canard	\N
3927	2 mangues bien mûres	\N
3928	1 canard assez gros ( ben , j'sais pas moi..... un canard , quoi !!!!!)	\N
3929	1kg de pomme	\N
3930	1pot de confiture de groseille .	\N
3931	30cl de vinaigre de bonne qualité	\N
3932	1 canard de 2.5 kg(environ)	\N
3933	8 abricots	\N
3934	2cuillères à soupe de miel liquide	\N
3935	1cuillère à soupe de mélange d'épices 5 parfums chinois	\N
3936	1/2cuillère à café de gingembre en poudre	\N
3937	5morceaux de sucre	\N
3938	15cl de fond de volaille	\N
3939	5cl de vinaigre de vin	\N
3940	1 canard de 2kg	\N
3941	200g d'amandes entières	\N
3942	30cl de cognac	\N
3943	5cuillères à soupe de soja	\N
3944	2cuillères à soupe de fécule .	\N
3945	1fine tranche de lard fumé	\N
3946	1kg de cerise	\N
3947	1verre de liqueur de cognac	\N
3948	1 canard gras (type mulard, avec son foie)	\N
3949	150g de figue	\N
3950	150g de farce fine de porc	\N
3951	45cl de vin de fronton	\N
3952	2 cuisses de canard confites	\N
3953	1 canard (ou six cuisses de canard)	\N
3954	50g de saindoux (ou graisse de canard)	\N
3955	50cl de bouillon	\N
3956	12 navets	\N
3957	1 canard (moi je préfère des cuisses ou des magrets) de 2 kg environ	\N
4083	4quartiers d'ail	\N
4084	Paprika rouge, laurier et thym	\N
3958	250g d'olive verte (dénoyautées ou non selon le goût, moi je trouve qu'avec le noyau, le goût du canard est meilleur, bien que je ne mange pas les olives!)	\N
3959	2 tomates (ou tomates pelées en boîte pour se faciliter la vie)	\N
3960	1cuillère à soupe rase de farine	\N
3961	5 pêches de vigne	\N
3962	100g de raisin	\N
3963	25cl de Lillet blanc (ou Porto blanc à défaut)	\N
3964	200g de porc et veau	\N
3965	200g de Volailles haché	\N
3966	3grosses cuillères de crème fraîche	\N
3967	1petit verre de calvados	\N
3968	1 canard environ 3 kg	\N
3969	50cl de vin blanc (muscadet)	\N
3970	8 pommes reinette	\N
3971	12 cerneaux de noix	\N
3972	75g de lardons fumés	\N
3973	Sauceline	\N
3974	500g de champignon de forêt	\N
3975	Pruneau secs dénoyautés	\N
3976	Cannelle en bâtons et cannelle en poudre	\N
3977	Crème liquide	\N
3978	1 canard sauvage	\N
3979	3cl d'huile	\N
3980	1grosse cuillère à soupe de beurre	\N
3981	1/2cuillère à café de sucre	\N
3982	30cl de vinaigre	\N
3983	800g de navet	\N
3984	50g de lard	\N
3985	10cl de bouillon	\N
3986	20cl de vin blanc doux	\N
3987	100g d'olives (1 boîte)	\N
3988	1 canard ou volaille (peut se faire avec un chapon, oie, voire un poulet bien gras), si possible avec le cou	\N
3989	700g de rhubarbe (de préférence Cherry ou Canada Red, évitez la victoria qui est verte)	\N
3990	150g de sucre roux	\N
3991	Gros sel de Guérande	\N
3992	1 canard de Barbarie tendre	\N
3993	175g de pain rassis (1 tasse)	\N
3994	5cl d'armagnac	\N
3995	1 canard avec ses abats	\N
3996	150g de poudre d'amande	\N
3997	2 petits suisses	\N
3998	1pincée de thym moulu	\N
3999	10cl de cognac	\N
4000	1 canard de Barbarie de 2,5 kg	\N
4001	1kg de navet nouveaux	\N
4002	24 oignons	\N
4003	30g de sucre semoule	\N
4004	8cl de porto	\N
4005	250g de lard ou de poitrine fumée	\N
4006	2 foie de volaille	\N
4007	2 brioches	\N
4008	50g de crème fraîche	\N
4009	7cl de porto	\N
4010	3tranches de toast	\N
4011	100g de fruits secs (figues, abricots, prunes)	\N
4012	100g de marrons pelés	\N
4013	25g d'amandes mondées	\N
4014	25g de noix	\N
4015	1cuillère de sirop (genre sirop d’érable)	\N
4016	1 piment de Cayenne	\N
4017	Céleri	\N
4018	800g de bouillon	\N
4019	4cl de porto	\N
4020	1branche de romarin pour la décoration	\N
4021	1 canard de 2,5 kg environ	\N
4022	1.5kg de pomme	\N
4023	1.5cuillère à soupe de thym	\N
4024	1 canard de Barbarie	\N
4025	30g d'épices	\N
4026	4cl de miel liquide	\N
4027	1pincée de glutamate	\N
4028	1 canard élevé en plein air de 2 kg	\N
4029	1gousse d'ail "péi" ou français	\N
4030	3 patates douces	\N
4031	3grosses cuillères à soupe de confiture "mangue miel"	\N
4032	1/2gousse de vanille de La Réunion	\N
4033	5cuillères à soupe de vinaigre balsamique	\N
4034	Massalé	\N
4035	3filets de Magret de Canard	\N
4036	600g de potimarron (1 belle tranche)	\N
4037	50cl de sauce soja	\N
4038	20cl de miel	\N
4039	1pincée de piment doux	\N
4040	1pincée de gingembre (selon vos goûts)	\N
4041	1 canard sauvage d'environ 2,5 kg	\N
4042	1noix de beurre  manié	\N
4043	10cl de madère (facultatif)	\N
4044	1 canard sauvage préparé	\N
4045	4 pommes qui tiennent à la cuisson (royal gala par exemple)	\N
4046	50cl de cidre	\N
4047	4 Magrets de Canard	\N
4048	2cuillères à soupe de satay en poudre	\N
4049	4branches de poivre vert	\N
4050	4gousses d'ail émincées en fines lamelles	\N
4051	1paquet de basilic thaïlandais	\N
4052	3cuillères à soupe de sauce nuoc mam	\N
4053	1cuillère à soupe de piment en purée (si possible thaïlandais)	\N
4054	1 metton (acheter les paquets bleus et laisser murir le metton, pas de trop)	\N
4055	20g de beurre (ma grand mère en mettait beaucoup, mais c'était délicieux)	\N
4056	1petit verre de cointreau ou Grand Marnier	\N
4057	2 oranges et 2 citrons	\N
4058	1 canette d'environ 2 kg	\N
4059	Orange (ou 2 si petites) + le zeste	\N
4060	1 orange en tranches	\N
4061	30cl d'eau	\N
4062	3cuillères à café de bouillon de volaille	\N
4063	2petites boîtes de champignon de Paris entiers	\N
4064	Fleur de sel aux épices grillés de Madagascar	\N
4065	Curry de Madras	\N
4066	1 canette de barbarie d'1.5 kg	\N
4067	1boîte de pêche au sirop	\N
4068	12 figues sèches	\N
4069	150g de foie de volaille	\N
4070	1 canette de 1,2 kg environ	\N
4071	1petit bocal d'olive verte dénoyautées	\N
4072	2cuillères à café de farine ou maïzena	\N
4073	1gros morceaux de gingembre frais	\N
4074	50cl de lait entier	\N
4075	3cuillères à soupe de rhum	\N
4076	1gousse de vanille ou 1 cuillère à soupe de vanille liquide	\N
4077	20g de beurre doux	\N
4078	40ml de rhum	\N
4079	4feuilles de lasagnes (en boîte)	\N
4080	feuille de basilic	\N
4081	1kg de viande de boeuf haché	\N
4085	12feuilles de lasagnes	\N
4086	400g de brousse	\N
4087	1paquet de lardons de saumon fumé	\N
4088	1briquette de coulis de tomate (1/2 litre)	\N
4089	Tomate	\N
4090	12 canneloni à farcir	\N
4091	350g de viande hachée	\N
4092	25cl de coulis de tomate	\N
4093	Huile d'olive (ou de pépins de raisin)	\N
4094	300g de poivron en cubes (surgelés)	\N
4095	350g de porc /veau	\N
4096	350g de ricotta	\N
4097	2 tomates épépinées en cubes	\N
4098	2dl de crème fraîche allégée	\N
4099	3dl de lait	\N
4100	250g de canneloni	\N
4101	200g d'épinard congelés	\N
4102	1l de béchamel	\N
4103	12 canneloni	\N
4104	1 bûche de chèvre frais	\N
4105	Gruyère râpé (ou parmesan)	\N
4106	400g de filet de poulet	\N
4107	plaque de lasagnes fraîche ou rouleau de cannelloni	\N
4108	Tomate sechées	\N
4109	Coulis de tomate	\N
4110	Champignon surgelés (genre "mélange forestier" qu'on trouve dans tous les hypers) d'environ 500 g	\N
4111	Canneloni prêts à farcir (3-4 par personne) qu'on trouve également en hypermarché	\N
4112	Béchamel préparée avec votre recette préférée	\N
4113	Pain	\N
4114	8 canneloni	\N
4115	1poignée de basilic frais	\N
4116	150g de chèvre frais	\N
4117	Parmesan râpé finement	\N
4118	1bûchette de chèvre	\N
4119	1paquet de lasagnes fraiches (12 feuilles)	\N
4120	150g de lardons	\N
4121	100g de comté	\N
4122	10ml de crème liquide	\N
4123	1boîte de canneloni précuites	\N
4124	1boîte de tomates pelées , en dés	\N
4125	1petit sachet de gruyère	\N
4126	12feuilles de lasagnes sèches	\N
4127	1.2kg d'épinard	\N
4128	500g de ricotta	\N
4129	4cuillères d'huile d'olive	\N
4130	Thym et laurier	\N
4131	375g de lasagnes fraîches	\N
4132	1gousse d'ail , écrasées	\N
4133	1kg d'épinard , finement hachés	\N
4134	650g de ricotta fraîche, battue	\N
4135	2 oeufs , battus	\N
4136	1pincée de muscade fraîchement moulue	\N
4137	1 oignon moyen, haché	\N
4138	2gousses d'ail , finement hachées	\N
4139	500g de tomate bien mûres, hachées	\N
4140	2cuillères à soupe de concentré de tomates (double)	\N
4141	150g de mozzarella , en petits morceaux	\N
4142	16plaques de lasagnes	\N
4143	400g de brocciu	\N
4144	3cuillères à soupe de pesto vert	\N
4145	40cl de coulis de tomate	\N
4146	10 canneloni à farcir	\N
4147	150g d'escalope de porc	\N
4148	150g de comté râpé	\N
4149	1boîte de canneloni	\N
4150	1sachet de julienne de légumes surgelée	\N
4151	Bouillon de légumes	\N
4152	800g d'épinards frais	\N
4153	6tranches de jambon	\N
4154	100g de feta en dés ou gorgonzola	\N
4155	1boîte de canneloni à farcir	\N
4156	250g de viande hachée	\N
4157	Huile neutre (tournesol ou pépins de raisin)	\N
4158	250g de canneloni crus (22 tubes)	\N
4159	250g de veau	\N
4160	150g de porc	\N
4161	2cuillères à soupe de parmesan	\N
4162	4 biscottes écrasées ou 2 à 3 verres de chapelure	\N
4163	Piment doux	\N
4164	500g de tomates pelées au jus	\N
4165	200g de sauce tomate fraîche aux herbes de Provence (type Tomacouli Panzani)	\N
4166	500g de boeuf haché ou 4 steaks hachés	\N
4167	250g de haché de veau	\N
4168	1l de purée de tomate	\N
4169	250g de mozzarella	\N
4170	450g de ricotta	\N
4171	60g de parmesan râpé	\N
4172	Persil italien	\N
4173	30 canneloni en tube à cuire	\N
4174	1paquet de canneloni précuits	\N
4175	1boîte de canneloni secs	\N
4176	300g de chair à saucisse ou farce à légumes	\N
4177	1 fromage de chèvre frais, type "petit Billy"	\N
4178	1bouteille de sauce tomate	\N
4179	1gousse d'ail (facultatif)	\N
4180	350g de boeuf haché ("américain" en Belgique)	\N
4181	1 tomate et/ou concentré et/ou un "pasata" de tomates	\N
4182	15cl de vin rouge	\N
4183	1 cube de bouillon de viande	\N
4184	8feuilles de lasagnes	\N
4185	1paquet de brocoli ou choux fleur surgelé	\N
4186	6 canneloni	\N
4187	1 viande hachée	\N
4188	Bouillon de volaille	\N
4189	4cuillères à soupe de concentré de tomates	\N
4190	1boîte de tomate	\N
4191	1kg de champignon (mélange forestier)	\N
4192	4 canneloni (prêt à farcir) par personne	\N
4193	1bouquet de herbes fraiches (basilic, ciboulette, persil)	\N
4194	120g de parmesan râpé	\N
4195	1kg de coulis de tomate	\N
4196	3feuilles de sauge	\N
4197	20 canneloni à farcir	\N
4198	6tranches de saumon fumé	\N
4199	500g de mascarpone (2 boîtes)	\N
4200	400g de saumon en boite sans peau, ni arrêtes (4 boîtes environ)	\N
4201	570g de pesto rosso (3 bocaux)	\N
4202	8 canneloni (compter 1 cannelloni/personne lorsque c'est servi en entrée)	\N
4203	300g de colin d'Alaska (ou tout autre poisson du même type)	\N
4204	20 scampis	\N
4205	100g de crevette rose	\N
4206	100g de crevette grise	\N
4207	5 scampis par personne	\N
4208	1boîte de homard	\N
4209	3 andouillette	\N
4210	3cuillères à soupe de moutarde	\N
4211	1 béchamel	\N
4212	100g d'emmental râpé	\N
4213	1 blette	\N
4214	2filets de carrelet	\N
4215	6 champignons	\N
4216	1 pomme	\N
4217	500g de viande hachée (porc et veau)	\N
4218	300g de jambon	\N
4219	1cuillère de persil	\N
4220	1petite cuillère de basilic séché	\N
4221	1petite cuillère d'origan séché	\N
4222	3petites boîtes de tomates pelées (3 x 400g)	\N
4223	Poivre …	\N
4224	Canneloni à garnir	\N
4225	2 poivrons pelés et épépinés	\N
4226	1boîte de tomate entières	\N
4227	1 canette coupée en morceaux	\N
4228	30cl de crème fraîche épaisse entière	\N
4229	150g de champignon de Paris frais (de préférence)	\N
4230	5cl de calvados	\N
4231	Fond de volaille	\N
4232	150g de capellini	\N
4233	250g de carotte	\N
4234	300g de courgette	\N
4235	3 bouillon -cubes	\N
4236	1 poisson de 2 kg	\N
4237	1 beurre	\N
4238	l de lait	\N
4239	1/2l d'eau	\N
4240	1branche de thym et une de persil	\N
4241	500g de jambon fromage	\N
4242	2 bouillon cubes de poule	\N
4243	3 yaourts natures à la grecque	\N
4244	3cuillères à café de concentré de tomates	\N
4245	500g d'autres pâtes	\N
4246	150g de tomate confites à l'huile d'olive	\N
4247	copeau de parmesan	\N
4248	1kg de potiron	\N
4249	15cl de crème liquide très froide	\N
4250	1/2cuillère à café d'anis	\N
4251	2g de poisson mauricien à chair délicate)	\N
4252	2cl d'huile d'olive	\N
4253	25cl de noilly prat -Prat	\N
4254	1bonne poignée de feuille de curry vertes	\N
4255	50cl de crème liquide	\N
4256	1cuillère à soupe de moutarde à l'ancienne	\N
4257	1cuillère à soupe de moutarde de dijon	\N
4258	1cuillère de jus de citron	\N
4259	1pointe de couteau de cardamome verte en poudre	\N
4260	1 piment oiseau	\N
4261	10 tomates	\N
4262	1.5kg de jarret de boeuf en cubes de 3 cm de côté	\N
4263	2 farine	\N
4264	1tranche de pain rassi	\N
4265	50cl de bière brune	\N
4266	50g de beurre ou de saindoux	\N
4267	Poivre noir	\N
4268	1cuillère à café de fécule de maïs	\N
4269	600g de boeuf (collier, épaule,...)	\N
4270	25cl de bière "Chouffe"	\N
4271	1cuillère à soupe de sirop de Liège (à tartiner)	\N
4272	1.2kg de boeuf	\N
4273	600g de rognons de porc	\N
4274	1l de bière bruxelloise	\N
4275	300g de viande de boeuf (style viande pour fondue) coupée en cubes	\N
4276	1tranche de pain de campagne tartinée de moutarde à l'ancienne	\N
4277	20cl de bière brune forte	\N
4278	2 spéculoos	\N
4279	800g de porc	\N
4280	1bocal de cerise du nord dénoyautées	\N
4281	1 bière brune ou trapiste	\N
4282	700g de boeuf (morceaux pour sauté ou pour carbonades)	\N
4283	1 bière (3 monts, par exemple)	\N
4284	2tranches de pain de campagne	\N
4285	Cassonade et vinaigre	\N
4286	1.6kg de paleron de Boeuf	\N
4287	2cuillères à soupe de cassonade brune	\N
4288	3tranches de pain rassis	\N
4289	1l de bière de garde	\N
4290	100g de lard	\N
4291	400g de crevette cuites décortiquées	\N
4292	500g de quinoa	\N
4293	Riz ou maïs	\N
4294	200g de haricot vert	\N
4295	20cl de crème de soja	\N
4296	850g de boeuf à braiser (macreuse ou jumeau)	\N
4297	1cuillère à café de cassonade	\N
4298	50cl de bière Guinness	\N
4299	50cl d'eau ou de bouillon	\N
4300	1kg de macreuse de boeuf (ou viande à bourguignon)	\N
4301	25cl de bière	\N
4302	kg de pomme de terre	\N
4303	750g de macreuse (viande de boeuf type pour bourguignon)	\N
4304	3cuillères à soupe de cassonade ou vergeoise	\N
4305	250g de lardons allumettes	\N
4306	2tranches de pain blanc couvertes abondamment de moutarde à l'ancienne	\N
4307	10 baies roses	\N
4308	2 bière brune (Chimay Bleue)	\N
4309	2cuillères à soupe de beurre salé	\N
4310	2gousses d'ail taillées en lamelles	\N
4311	1 viande (boeuf maigre) coupés en gros dés. comptez +/- 250gr/pers.	\N
4312	1 champignon frais	\N
4313	1bouteille de bière (Préférez une bière brune, type trapiste genre chimay...)	\N
4314	3tranches de pain d'épices	\N
4315	Poivre et de sucre	\N
4316	1kg de viande de boeuf pour daube	\N
4317	1l de fond de boeuf	\N
4318	pincée de sucre ou cassonade	\N
4319	Vinaigre balsamique (4/5 cuillères à soupe)	\N
4320	Laurier de thym	\N
4321	Chou de bruxelles (une trentaine)	\N
4322	33cl de bière brune	\N
4323	Bière d'abbaye	\N
4324	1kg de porc	\N
4325	2 poires	\N
4326	1.5kg de boeuf en morceaux	\N
4327	5 bière brune	\N
4328	4 oignons moyens finement émincés	\N
4329	4cuillères à soupe de vinaigre de vin rouge	\N
4330	4cuillères à soupe de cassonade	\N
4331	2tranches de pain	\N
4332	250g de beurre	\N
4333	750g de boeuf	\N
4334	1boîte de champignon coupés	\N
4335	4cuillères à soupe de confiture de fraises	\N
4336	1/2 pain recouverte de moutarde	\N
4337	1/2 pain recouverte de confiture de fraises	\N
4338	5cuillères à soupe de maïzena expresse pour sauces brunes	\N
4339	600g de viande de bœuf pour carbonnades	\N
4340	25cl d'eau bouillante	\N
4341	2cl de bière Chimay Bleue	\N
4342	2cuillères à café de sucre brun	\N
4343	2cuillères à café de sel d’épices	\N
4344	1cuillère à café de purée de tomate	\N
4345	1gousse d'ail pressée	\N
4346	600g de viande pour carbonnades	\N
4347	1tranche de pain rassis	\N
4348	1l de bouillon de boeuf (préparé avec des cubes)	\N
4349	20g de chocolat noir	\N
4350	33cl de bière chimay rouge	\N
4351	6 pommes de terre à chair ferme	\N
4352	2kg de cardon	\N
4353	4 citrons	\N
4354	3filets d'anchois	\N
4355	1 truffe de 30 g brossée	\N
4356	2 cardons	\N
4357	10 anchois au sel	\N
4358	25cl de bouillon de volaille	\N
4359	1filet de vinaigre	\N
4360	30 crevettes	\N
4361	1 noix de coco rapée ou en boîte	\N
4362	3cuillères à soupe de curry	\N
4363	300g de poulet (escalopes épaisses)	\N
4364	Poudre à colombo (ou massalé)	\N
4365	Tomate en boîte	\N
4366	6 cuisses de poulet (ou un poulet découpé)	\N
4367	700g de tomate entières ou en morceaux	\N
4368	1 pâte de vinday (achetée dans une épicerie asiatique)	\N
4369	5 patates douces	\N
4370	20cl de lait de coco	\N
4371	1kg de poulet frais (1 beau poulet)	\N
4372	3 oignons rouges (émincés)	\N
4373	4gousses d'ail écrasées (ajouter du sel à l'ail écrasé)	\N
4374	1cuillère à soupe d'huile de tournesol (pas olive)	\N
4375	4 tomates bien rouges coupées en petits morceaux	\N
4376	3cuillères à café de curcuma (safran réunion)	\N
4377	4 feuilles de laurier	\N
4378	3 petits piments (optionnel)	\N
4379	800g de coque , fraîches ou surgelées	\N
4380	100g de saindoux	\N
4381	750g de carotte	\N
4382	1 jambon	\N
4383	1 bouillon de volaille	\N
4384	1poignée de gros sel	\N
4385	6 carottes de taille moyenne	\N
4386	50g de beurre (ou 3 cuillères à soupe d’huile)	\N
4387	1cuillère à café de farine	\N
4388	1kg de carotte nouvelles	\N
4389	12.5cl de crème fraîche épaisse	\N
4390	1.5cuillère à café de sucre en poudre	\N
4391	1pincée de thym en poudre	\N
4392	30g de carotte cuites	\N
4393	1cuillère à soupe de crème liquide	\N
4394	4 carottes de taille moyenne	\N
4395	1barquette de lardons de 100 g (fumés de préférence)	\N
4396	400g de carotte	\N
4397	5feuilles d'oseille fraîche	\N
4398	100g de brique de vache	\N
4399	100g de brique de chèvre	\N
4400	3 oignons (ou 3 poireaux)	\N
4401	1cuillère à café de curcuma (facultatif)	\N
4402	2cuillères à café de sucre en poudre	\N
4403	220g de carotte râpées (3 tasses)	\N
4404	25cl de sucre fin (1 tasse)	\N
4405	25cl d'eau (1 tasse)	\N
4406	1 jus de citron frais	\N
4407	800g de carotte	\N
4408	1cuillère à soupe d'estragon ciselé	\N
4409	30g de beurre ,	\N
4410	2 carottes (150 g)	\N
4411	2tranches de bacon	\N
4412	Fromage blanc	\N
4413	24tranches de carpaccio de boeuf	\N
4414	12tranches d'aubergine grillées surgelées	\N
4415	600g de saumon (soit environ 150 g par personne)	\N
4416	4 citrons verts	\N
4417	2cuillères à soupe de baies roses	\N
4418	600g de thon très frais	\N
4419	3 avocats	\N
4420	1cuillère à soupe de vinaigre de riz chinois (noir)	\N
4421	2pavés de saumon sans peau	\N
4422	2cl de Vodka POLIAKOV	\N
4423	1 pomme verte	\N
4424	1pincée de jeunes pousses de salade	\N
4425	1 carpe	\N
4426	Oeuf de carpe	\N
4427	300g de champignon de Paris	\N
4428	1cuillère à café de crème d'anchois	\N
4429	1 carré d'agneau désossé	\N
4430	1 carré d'agneau entier (1,2 kg)	\N
4431	2g de gingembre frais	\N
4432	4g de curry	\N
4433	1g de marjolaine	\N
4434	1g de paprika	\N
4435	2g de romarin	\N
4436	1cuillère à soupe d'huile d'olive + 1 cuillère à café de jus de citron	\N
4437	50g de céleri branche	\N
4438	30g de câpres	\N
4439	3g de persil	\N
4440	3feuilles de menthe	\N
4441	1carré d'agneau (12 à 16 côtes)	\N
4442	1 pamplemousse	\N
4443	2cuillères à soupe de coriandre en poudre	\N
4444	Beurre ou huile d’olive	\N
4445	2 carré d'agneau de 6 côtes premières prétranchées	\N
4446	2brins de sarriette	\N
4447	1cuillère à café de sucre en poudre	\N
4448	1dl de bouillon de volaille	\N
4449	Carré d'agneau et sa carcasse	\N
4450	10 carottes fanes	\N
4451	5 oignons nouveaux	\N
4452	Ail rose de lautrec	\N
4453	500g de figue sèches	\N
4454	Farce aux herbes	\N
4455	1bouquet de cerfeuil	\N
4456	2branches d'estragon	\N
4457	2 côtelettes d'agneau par personne	\N
4458	1 carré d'agneau de 16 côtes	\N
4459	1cuillère à soupe de gros sel	\N
4460	Herbes (Romarin, thym, sarriette, sauge…)	\N
4461	2cuillères à soupe de pavot	\N
4462	Anis ...	\N
4463	1bouquet de persil (feuilles)	\N
4464	80g de beurre pommade	\N
4465	1cuillère à soupe de savora	\N
4466	2 étoiles de badiane	\N
4467	1cuillère à soupe de pastis	\N
4468	1cuillère à soupe de vinaigre de xérès	\N
4469	400g de flageolet frais (sinon faites-les tremper selon les indications, d’habitude entre 8 h et 24 h)	\N
4470	8 tomates	\N
4471	8 navets nouveaux	\N
4472	8 grenailles	\N
4473	Ratte …)	\N
4474	2 agneau et un os de veau	\N
4475	Thym et de la fleur de thym	\N
4476	1bouquet de persil (tiges)	\N
4477	1 oignon piqué de 2 clous de girofle	\N
4478	1kg de porc , faire donner un coup de scie tous les 3cm pour faciliter le découpage	\N
4479	3cuillères à soupe de vinaigre de vin	\N
4480	2 cornichons	\N
4481	1carré de porc d'1,5 kg (côtes premières)	\N
4482	6 oranges	\N
4483	7cl de cognac	\N
4484	carré de carré de veau pour 6	\N
4485	Moutarde à l'ancienne	\N
4486	6 poires	\N
4487	12 pruneaux dénoyautés	\N
4488	900g de carré de veau	\N
4489	15cl de béchamel	\N
4490	45g de gruyère râpé	\N
4491	350g de champignon	\N
4492	10g de farine	\N
4493	65g de crème fraîche	\N
4494	2 carrelet de 700 g ététés par votre poisonnier	\N
4495	1.2kg de poireau	\N
4496	2verres de cidre brut	\N
4497	5 carottes (environ 500 g)	\N
4498	100g de noix	\N
4499	200g de cream cheese	\N
4500	100g de sucre glace	\N
4501	800g de veau émincé	\N
4502	1 curry	\N
4503	1demi de vin blanc sec	\N
4504	5cl de crème épaisse	\N
4505	10tranches d'ananas en boîte	\N
4506	1demi de pomme Golden	\N
4507	200g de lentilles corail	\N
4508	3 carottes en rondelles	\N
4509	6 pommes de terre moyenne en dés	\N
4510	2 courgettes en rondelles	\N
4511	1l de bouillon de légumes	\N
4512	1boîte d'escargot Achatines	\N
4513	500g de girolle	\N
4514	50cl de crème fleurette	\N
4515	1botte de cerfeuil	\N
4516	1/2botte de persil plat	\N
4517	850g de lotte	\N
4518	18 noix de saint-jacques	\N
4519	1/4l de lait écrémé	\N
4520	150cl de crème fraîche épaisse	\N
4521	10cl de vin blanc type Chardonnay	\N
4522	2cuillères à soupe d'aneth déshydratée	\N
4523	200g de chapelure de pain	\N
4524	1cuillère à café d'ail haché	\N
4525	10g de beurre fondu	\N
4526	2cuillères à soupe de jus de citron	\N
4527	50ml de sirop d'érable	\N
4528	1boîte de tomate en dés	\N
4529	4filets de saumon de 125 g chacun	\N
4530	8 crevettes décortiquées	\N
4531	12 noix de pétoncle	\N
4532	Fines herbes à votre goût	\N
4533	100g de saumon	\N
4534	100g de lieu noir	\N
4535	4 tomates cerise	\N
4536	500g de noix de pétoncle	\N
4537	4 escalopes poulet ou dinde	\N
4538	200g de céleri-rave	\N
4539	200g de courgette	\N
4540	50cl de bouillon de volaille (eau + cube)	\N
4541	4brins de persil	\N
4542	1000g de haricot (lingots) secs ;	\N
4543	8 cuisses de canard confites;	\N
4544	800g de saucisse fraîche ;	\N
4545	500g de travers de porc ) ;	\N
4546	5 saucisses de couenne ;	\N
4547	500g de couenne fraîches ;	\N
4548	3 tomates ;	\N
4549	2 carottes ;	\N
4550	3 oignons ;	\N
4551	2gousses d'ail ;	\N
4552	1 bouquet garni ;	\N
4553	4 clous de girofle ;	\N
4554	3tranches de pain de mie (ou chapelure) ;	\N
4555	750g de haricot blancs secs	\N
4556	100g de couenne fraîches	\N
4557	300g de lard de poitrine demi-sel	\N
4558	1kg d'épaule de mouton	\N
4559	1 oie ou 1 canard	\N
4560	1 saucisson cru à l'ail	\N
4561	200g de graisse d'oie ou de saindoux	\N
4562	1boîte de purée de tomate	\N
4563	12 saucisses de Strasbourg	\N
4564	1 saucisson à l'ail fumé	\N
4565	2boîtes de haricot blancs	\N
4566	500g de purée de tomate  fraîche	\N
4567	200g de lardons fumés ou (2 tranches)	\N
4568	500g de flageolet nature en conserve	\N
4569	4 saucisses de Toulouse	\N
4570	4tranches de saucisson à l'ail	\N
4571	4morceaux de confit de canard en conserve	\N
4572	2tranches de poitrine de porc fumée	\N
4573	2feuilles de laurier	\N
4574	600g de cabillaud en filets	\N
4575	400g de haricot blanc	\N
4576	400g de tomates pelées (1 boîte)	\N
4577	375g de haricots secs	\N
4578	375g de champignon de Paris (ou de cèpes ou de morilles, selon vos goûts... et la saison !)	\N
4579	400g d'olives noires dénoyautées	\N
4580	240g de tomate épluchées	\N
4581	1 chorizo fort	\N
4582	4 queues de lottes (ou pavés de saumon)	\N
4583	Palourde	\N
4584	Moules	\N
4585	4 cervelas	\N
4586	6 cervelas selon appétit	\N
4587	1/4l de crème entière	\N
4588	2 cervelle de porc	\N
4589	3gousses d'ail haché	\N
4590	6cl de vinaigre + 20 cl d'eau froide	\N
4591	6 pommes de terre vapeur	\N
4592	6cl d'eau	\N
4593	200g d'oignon rouge	\N
4594	1pincée de piment “dedo de moça” haché	\N
4595	500g de sole	\N
4596	10 citrons	\N
4597	1pincée de piment “biquinho” (pour décorer)	\N
4598	6 cuisses de poulet	\N
4599	230g de champignon de Paris	\N
4600	Farine (facultatif)	\N
4601	1barquette de chair à saucisse	\N
4602	1sachet de riz (basmati de préférence)	\N
4603	1grand verre de vin blanc	\N
4604	450g de chair à saucisse	\N
4605	100g de concentré de tomates	\N
4606	2 dinde coupées en dés	\N
4607	6 tomates coupées en dés	\N
4608	4 piments verts coupés en petits morceaux	\N
4609	1/2cuillère à soupe de paprika	\N
4610	1/2 poivre noir	\N
4611	100g de pointe d'asperge	\N
4612	150g de queue d'écrevisse	\N
4613	2 oignons nouveaux ou échalotes	\N
4614	500g de champignon de couche (ou de Paris)	\N
4615	2tranches de jambon cru	\N
4616	8 champignons de Paris gros calibres (8 à 9 cm de diamètre)	\N
4617	1boîte de corned beef dans sa gelée (soit 200 g)	\N
4618	80g de parmesan râpé	\N
4619	2 escalopes de poulet fermier (ou de dinde)	\N
4620	2cuillères à soupe de persil frais	\N
4621	2gousses d'ail pilées	\N
4622	100g de champignon de Paris miniatures	\N
4623	20g de roquefort fondu	\N
4624	1louche de légume du soleil/chèvre	\N
4625	4 champignons à farcir (ou plus s'il n'y en a pas)	\N
4626	2 tomates fraîches	\N
4627	100g de germes de soja	\N
4628	1/2 poivron jaune	\N
4629	100g d'oignon	\N
4630	2filets de thon	\N
4631	4 champignons à farcir	\N
4632	1poignée d'emmental râpé	\N
4633	8 champignons	\N
4634	1petite boîte de thon	\N
4635	250g de ricotta = 1 pot	\N
4636	2cuillères de concentré de tomates	\N
4637	400g de sauce tomate avec petits légumes	\N
4638	100g de riz basmati	\N
4639	100g de tomme de chèvre sans croûte	\N
4640	2brins de romarin	\N
4641	2 cuisses de poulet (ou escalopes)	\N
4642	1bouquet de herbes ( persil, basilic, coriandre, ciboulette...)	\N
4643	1morceau de pain trempé dans du lait (ou 3 biscottes)	\N
4644	50g de pain rassis	\N
4645	1paquet de lardons nature	\N
4646	Gingembre moulu	\N
4647	1cuillère à café de cumin en grains	\N
4648	2gousses d'ail écrasées ou 2 cuillères à café d'ail moulu	\N
\.


--
-- Data for Name: matching_recipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.matching_recipe (id, recipe_id, confirmed_ingredient, matching_score, missing_ingredient, association_id) FROM stdin;
\.


--
-- Data for Name: recipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipe (id, name, total_time, difficulty, image, cook_time, rate, prep_time, budget, people_quantity) FROM stdin;
1	6 ingrédients que l’on peut ajouter sur une crêpe au Nutella®	25 min	très facile	https://assets.afcdn.com/recipe/20171006/72810_w420h344c1cx2544cy1696cxt0cyt0cxb5088cyb3392.jpg	10 min	5	15 min	bon marché	6
2	Agneau à l'abricot (recette turque)	3 h	Niveau moyen		1 h	3	2 h	Coût moyen	6
3	Agneau à l'oriental	1h15	très facile		1 h	4.6	15 min	bon marché	6
4	Agneau à la grec (aubergine & feta)	1h45	facile		1 h	5	45 min	bon marché	4
5	Agneau à la mangue et aux amandes	1h30	très facile		1 h	5	30 min	Coût moyen	4
6	Agneau à la sarriette et aux fèves fraîches	2 h	Niveau moyen		1 h		1 h	Coût moyen	6
7	Agneau au céleri, à la coriandre et aux olives	1h35	facile		1 h		35 min	Coût moyen	6
8	Agneau aux amandes et raisins secs	1h20	très facile	https://assets.afcdn.com/recipe/20130405/47393_w420h344c1cx256cy192.jpg	1 h	4.7	20 min	Coût moyen	6
9	Agneau aux artichauts et à la tomate	1h15	facile		45 min	4.8	30 min	Coût moyen	4
10	Agneau aux carottes en cocotte	1 h	facile		40 min	4.8	20 min	Coût moyen	4
11	Agneau aux échalotes et champignons	2h30	facile		2 h		30 min	Coût moyen	6
12	Agneau aux poires et aux pistaches	1 h	facile		45 min	5	15 min	Coût moyen	6
13	Agneau aux poivrons et olives en cocotte	1h40	facile		1 h	5	40 min	Coût moyen	6
14	Agneau aux poivrons et sa sauce rose au paprika	1h25	facile		1 h		25 min	Coût moyen	4
15	Agneau aux pruneaux et aux amandes	1h15	facile		55 min	4	20 min	Coût moyen	4
16	Agneau aux pruneaux et figues fraîches ( L'ham Lahlou de ma maman)	1h45	Niveau moyen		1h30		15 min	Coût moyen	8
17	Agneau aux vermicelles	2h30	Niveau moyen		2 h		30 min	Coût moyen	6
18	Agneau confit aux pommes de terres nouvelles	3h15	facile	https://assets.afcdn.com/recipe/20150901/17089_w420h344c1cx1500cy2250.jpg	3 h	4.9	15 min	Coût moyen	6
19	Agneau épicé aux épinards	1h30	facile		1 h	4.8	30 min	Coût moyen	4
20	AGNEAU ÉPICÉ AUX PRUNEAUX	1h25	facile		1 h	4.3	25 min	Coût moyen	4
21	Agneau et ses pommes de terre à la portugaise	1h10	facile		55 min	5	15 min	bon marché	4
22	Agneau mariné aux épices	25 min	très facile		10 min	4	15 min	Coût moyen	4
23	Agneau sauté aux épices	40 min	très facile		15 min	4.6	25 min	bon marché	4
24	Aigre-doux de porc aux fruits secs de Cat	1h20	facile		1 h	4.8	20 min	bon marché	4
25	Aiguillette de canard au chèvre à la compoté d'oignon et de miel	1 h	facile		30 min		30 min	bon marché	2
26	Aiguillettes de canard à l'orange	42 min	très facile	https://assets.afcdn.com/recipe/20160722/63991_w420h344c1cx2713cy1808.jpg	12 min	4.2	30 min	Coût moyen	4
27	Aiguillettes de canard à ma façon	15 min	très facile	https://assets.afcdn.com/recipe/20150719/29538_w420h344c1cx1500cy1000.jpg	5 min	4.7	10 min	bon marché	4
28	Aiguillettes de canard aux abricots	25 min	facile	https://assets.afcdn.com/recipe/20140626/45909_w420h344c1cx2953cy4429.jpg	10 min	4.6	15 min	Coût moyen	2
29	Aiguillettes de canard aux figues et au porto	25 min	facile		15 min	4.7	10 min	assez cher	2
30	Aiguillettes de canard aux figues surgelées et aux pommes	11 min	très facile		6 min		5 min	bon marché	4
31	Aiguillettes de canard aux mandarines	25 min	facile		10 min	4.8	15 min	Coût moyen	10
32	Aiguillettes de canard aux pêches	20 min	facile	https://assets.afcdn.com/recipe/20190312/89343_w420h344c1cx2016cy1367cxt0cyt0cxb4032cyb2734.jpg	10 min	4.2	10 min	Coût moyen	6
33	Aiguillettes de Canard aux poires et quatre épices	20 min	très facile	https://assets.afcdn.com/recipe/20120503/11580_w420h344c1cx1140cy1750.jpg	5 min	4.4	15 min	Coût moyen	2
34	Aiguillettes de canard aux pommes	25 min	très facile	https://assets.afcdn.com/recipe/20131224/28383_w420h344c1cx1024cy768.jpg	20 min	4.6	5 min	bon marché	2
35	Aiguillettes de filet de dinde marinées à l'ail et au citron	2h30	facile		15 min	4.3	2h15	bon marché	2
36	Aiguillettes de poulet à l'ail et vinaigre balsamique	25 min	très facile	https://assets.afcdn.com/recipe/20190909/98012_w420h344c1cx1224cy1530cxt0cyt0cxb2448cyb3060.jpg	20 min	4.8	5 min	bon marché	2
37	Aiguillettes de Poulet à la mandarine	25 min	très facile	https://assets.afcdn.com/recipe/20100120/31825_w420h344c1cx256cy192.jpg	10 min	4.5	15 min	bon marché	4
38	Aiguillettes de poulet aux endives	30 min	facile	https://assets.afcdn.com/recipe/20141109/9299_w420h344c1cx1960cy1102.jpg	20 min	4.8	10 min	bon marché	2
39	Aiguillettes de poulet aux morilles et foie gras en chausson	50 min	Niveau moyen		25 min		25 min	Coût moyen	4
40	Aiguillettes de poulet marinées au miel et à l'estragon	25 min	très facile		10 min	4.3	15 min	bon marché	4
41	Aiguillettes de poulet panées à la moutarde et au miel	15 min	très facile		5 min	5	10 min	bon marché	4
42	Aile de Raie à l'échalote	1h10	facile	https://assets.afcdn.com/recipe/20170619/69545_w420h344c1cx300cy278.jpg	25 min	4.4	45 min	bon marché	4
43	Aile de Raie à l'huile d'olive	35 min	très facile	https://assets.afcdn.com/recipe/20150529/25682_w420h344c1cx1296cy972.jpg	25 min	5	10 min	bon marché	4
44	Aile de raie et beurre aux câpres	35 min	facile	https://assets.afcdn.com/recipe/20120130/21504_w420h344c1cx256cy192.jpg	20 min	4.3	15 min	Coût moyen	4
45	Aile de Raie grillée à l'asiatique	21 min	facile		6 min	5	15 min	Coût moyen	2
46	Ailerons confits à la confiture d'Abricots	30 min	très facile		25 min		5 min	bon marché	4
47	Ailerons de dinde au curry à l'indienne	1h40	facile	https://assets.afcdn.com/recipe/20160128/15502_w420h344c1cx1500cy1125.jpg	1 h	4.4	40 min	Coût moyen	4
48	Ailes de poulet à la cannelle et au miel	35 min	très facile		30 min		5 min	bon marché	4
49	Ailes de poulet à la crème	1h30	facile		1 h	3	30 min	bon marché	4
50	Ailes de poulet à la sauce soja et au sirop d'érable	55 min	facile	https://assets.afcdn.com/recipe/20130509/3326_w420h344c1cx256cy192.jpg	40 min		15 min	Coût moyen	4
51	Ailes de poulet sautées à l'asiatique	45 min	facile	https://assets.afcdn.com/recipe/20100120/51425_w420h344c1cx256cy192.jpg	30 min	4.3	15 min	bon marché	4
52	Ailes de raie aux câpres	20 min	facile	https://assets.afcdn.com/recipe/20170626/69851_w420h344c1cx2592cy1728.jpg	15 min	4.7	5 min	Coût moyen	4
53	Ailes de raie panées à la crème et aux chanterelles	1h30	Niveau moyen	https://assets.afcdn.com/recipe/20111128/48203_w420h344c1cx256cy192.jpg	30 min	3	1 h	Coût moyen	4
54	Ailes de Raie pochées au basilic et aux amandes fraîches	35 min	Niveau moyen		15 min	5	20 min	Coût moyen	4
55	Ailes de raie vapeur aux herbes	20 min	facile	https://assets.afcdn.com/recipe/20120728/52380_w420h344c1cx256cy192.jpg	5 min	4.9	15 min	Coût moyen	4
56	ailes de raies à la sauce curry et fondue de poireaux	25 min	facile		20 min	4.3	5 min	Coût moyen	4
57	Aîles de raies aux câpres et jambon italien	45 min	facile		15 min		30 min	Coût moyen	2
58	Ailes de raies aux câpres et pommes de terre	50 min	très facile	https://assets.afcdn.com/recipe/20170216/48719_w420h344c1cx360cy545.jpg	30 min	4	20 min	bon marché	4
59	Aïoli provençal à ma façon	50 min	très facile	https://assets.afcdn.com/recipe/20161220/7200_w420h344c1cx1871cy1710.jpg	30 min		20 min	bon marché	4
60	Alicot d'oie aux salsifis	4h35	facile		4 h		35 min	Coût moyen	4
61	Aligot à l'ancienne	1 h	facile	https://assets.afcdn.com/recipe/20160616/20429_w420h344c1cx1500cy2248.jpg	30 min	4.4	30 min	Coût moyen	6
62	Alligator à l'étouffée (Louisiane)	2h30	Niveau moyen		2 h		30 min	assez cher	2
63	Alose farcie à l'oseille	1 h	facile		40 min		20 min	Coût moyen	4
64	Alouettes sans têtes à la cocotte minute	50 min	très facile		30 min	4.6	20 min	bon marché	4
65	Amandines soufflées et crème de thon à la ciboulette	1h05	très facile		50 min	4.5	15 min	bon marché	6
66	Ananas rôti au Rhum Ambré LA MARTINIQUAISE façon Piña Colada	33 min	facile	https://assets.afcdn.com/recipe/20171117/75211_w420h344c1cx3680cy2456cxt0cyt0cxb7360cyb4912.jpg	18 min		15 min	bon marché	4
67	Anchois à l'huile	15 min	très facile		0	2.3	15 min	Coût moyen	1
68	Andouilettes au vin blanc, à la purée d'oignons et aux pommes	1h45	facile		45 min	5	1 h	assez cher	6
69	Andouille aux haricots	4h15	très facile		4 h	4.5	15 min	Coût moyen	6
70	Andouille chaude aux pommes de terreà la lyonnaise	25 min	très facile		5 min		20 min	bon marché	4
71	Andouille, oignons, champignons comme à la foire	20 min	très facile	https://assets.afcdn.com/recipe/20130515/58220_w420h344c1cx256cy192.jpg	10 min	4.7	10 min	bon marché	4
72	Andouillette à la chicorée	20 min	facile		10 min		10 min	Coût moyen	4
73	Andouillette aux poireaux	20 min	très facile	https://assets.afcdn.com/recipe/20141206/6155_w420h344c1cx1824cy1368.jpg	15 min	4.5	5 min	Coût moyen	4
74	Andouillettes à la crème et aux champignons	45 min	très facile		15 min	4.8	30 min	bon marché	4
75	Andouillettes à la crème facile	35 min	très facile	https://assets.afcdn.com/recipe/20120904/27608_w420h344c1cx256cy192.jpg	30 min	4.5	5 min	bon marché	4
76	Andouillettes à la crème	15 min	facile		10 min		5 min	Coût moyen	2
77	Andouillettes à la moutarde sur lit d'endive	20 min	très facile	https://assets.afcdn.com/recipe/20151230/62082_w420h344c1cx1500cy1125.jpg	5 min	3.5	15 min	bon marché	4
78	Andouillettes aux lentilles	1h10	très facile	https://assets.afcdn.com/recipe/20160917/48627_w420h344c1cx2064cy1161.jpg	1 h	4.5	10 min	bon marché	2
79	andouillettes aux pommes de terre	1h30	très facile	https://assets.afcdn.com/recipe/20150526/11565_w420h344c1cx1296cy972.jpg	1 h	4.8	30 min	Coût moyen	2
80	Anguille à l'anglaise en brochettes	40 min	facile		20 min		20 min	bon marché	6
81	Anguilles aux tomates et au persil	35 min	Niveau moyen		20 min	4	15 min	Coût moyen	4
82	Anneaux aux amandes	38 min	facile		8 min		30 min	bon marché	20
83	Anneaux d'encornet à la tomate	45 min	facile		40 min		5 min	bon marché	5
84	Anneaux de calamars à la méridionale	50 min	très facile	https://assets.afcdn.com/recipe/20140207/46426_w420h344c1cx1000cy750.jpg	35 min	4.8	15 min	bon marché	4
85	Arni Youvetsi - agneau aux pâtes au four (Grèce)	2h20	facile		2 h	4.4	20 min	bon marché	6
86	Arròs a banda	1h30	Niveau moyen		30 min		1 h	Coût moyen	4
87	Arroz a la cubana ( riz)	47 min	facile		17 min	5	30 min	bon marché	2
88	Arti'flette (tartiflette aux artichauts)	1h20	Niveau moyen		20 min		1 h	bon marché	4
89	Artichauts à l'huile d'olive et au citron	40 min	très facile	https://assets.afcdn.com/recipe/20130730/26298_w420h344c1cx1504cy999.jpg	20 min	4.6	20 min	bon marché	4
90	Artichauts à la barigoule et collier d'agneau lardons	1 h	facile		40 min		20 min	Coût moyen	4
91	Artichauts à la barigoule	1h10	Niveau moyen	https://assets.afcdn.com/recipe/20140828/10458_w420h344c1cx1296cy968.jpg	45 min	4	25 min	Coût moyen	4
92	Artichauts à la provençale aux anchois	1h10	Niveau moyen	https://assets.afcdn.com/recipe/20130730/6273_w420h344c1cx996cy1504.jpg	30 min		40 min	Coût moyen	4
93	Artichauts aux pommes de terre et au lard	55 min	facile		40 min	5	15 min	Coût moyen	4
94	Artichauts barigoulettes aux pignons de pin	35 min	très facile	https://assets.afcdn.com/recipe/20130731/40825_w420h344c1cx1000cy1504.jpg	20 min		15 min	bon marché	4
95	Artichauts fondants au vin blanc et aux petits légumes	1h05	facile	https://assets.afcdn.com/recipe/20130801/33484_w420h344c1cx1504cy999.jpg	45 min		20 min	bon marché	4
96	Asperges à la crème d'asperge gratinée	55 min	très facile	https://assets.afcdn.com/recipe/20170411/23656_w420h344c1cx400cy267.jpg	20 min	4.7	35 min	bon marché	4
97	Asperges à ma façon...	50 min	Niveau moyen		30 min		20 min	bon marché	4
98	Coquillettes aux asperges	38 min	facile	https://assets.afcdn.com/recipe/20150511/46945_w420h344c1cx968cy1296.jpg	18 min	4	20 min	bon marché	4
99	Asperges de saison aux poires	1h20	facile	https://assets.afcdn.com/recipe/20160416/47137_w420h344c1cx1500cy1125.jpg	35 min	5	45 min	bon marché	2
100	Asperges et mayonnaise aux petits-suisses	50 min	facile		20 min		30 min	bon marché	4
101	Aspic de poisson aux câpres (recette diététique)	45 min	Niveau moyen		15 min	5	30 min	Coût moyen	4
102	Assiette de pâtes à l'italienne	30 min	facile		20 min	5	10 min	bon marché	2
103	Assiette de saumon fumée à l'oeuf	40 min	facile		30 min		10 min	Coût moyen	1
104	Aubergine à la carbonara	25 min	très facile	https://assets.afcdn.com/recipe/20170106/34815_w420h344c1cx1224cy1632.jpg	15 min	4.1	10 min	bon marché	4
105	Aubergine à la Catalane selon Mamie	2h45	Niveau moyen		2 h	5	45 min	bon marché	6
106	Aubergine à la parmesane	1h15	facile	https://assets.afcdn.com/recipe/20180703/80157_w420h344c1cx2934cy1786cxt0cyt0cxb5868cyb3572.jpg	30 min	4.7	45 min	bon marché	2
107	Aubergine à la provençale et aux champignons	35 min	facile		20 min	4	15 min	bon marché	3
214	blanc de poulet à la chapelure  et au parmesan	50 min	très facile		35 min	4.2	15 min	bon marché	4
108	Aubergines à la feta	40 min	très facile	https://assets.afcdn.com/recipe/20120508/56354_w420h344c1cx256cy192.jpg	30 min	4.3	10 min	bon marché	2
109	Aubergines à la parmesane faciles	1h15	très facile		30 min	3.3	45 min	bon marché	4
110	Aubergines à la provençale	25 min	très facile	https://assets.afcdn.com/recipe/20170106/49828_w420h344c1cx2580cy1722.jpg	15 min	4.3	10 min	bon marché	4
111	Aubergines à la purée au pesto	1h15	facile		1 h		15 min	bon marché	2
112	aubergines à ma façon	1h15	très facile		45 min	4.1	30 min	bon marché	1
113	Aubergines au four à la mozzarella	1h25	facile	https://assets.afcdn.com/recipe/20170106/34704_w420h344c1cx2736cy1824.jpg	45 min	5	40 min	bon marché	4
114	aubergines au roquefort et aux noix	35 min	très facile		25 min	4.5	10 min	Coût moyen	4
115	Aubergines aux 5 épices	15 min	très facile	https://assets.afcdn.com/recipe/20150312/60353_w420h344c1cx1250cy1875.jpg	10 min	4.5	5 min	bon marché	2
116	Aubergines aux épices indiennes	25 min	facile		15 min	5	10 min	Coût moyen	2
117	Aubergines aux fruits secs	55 min	facile		45 min		10 min	bon marché	2
118	Aubergines aux poivrons et pignons de pin	1 h	très facile		45 min		15 min	bon marché	4
119	Aubergines et chair à saucisses gratinées	1h15	facile		45 min	4.5	30 min	bon marché	6
120	Aubergines farcies à l'orientale	50 min	très facile	https://assets.afcdn.com/recipe/20111212/8179_w420h344c1cx256cy192.jpg	30 min	4.8	20 min	bon marché	4
121	Aubergines farcies à la bolognaise	25 min	très facile	https://assets.afcdn.com/recipe/20130327/31855_w420h344c1cx256cy192.jpg	10 min	4.5	15 min	bon marché	2
122	Aubergines farcies à la crétoise	2 h	Niveau moyen	https://assets.afcdn.com/recipe/20190205/87243_w420h344c1cx4480cy2010cxt0cyt0cxb7000cyb4660.jpg	1 h	4.6	1 h	Coût moyen	2
123	Aubergines farcies à la feta	40 min	très facile		15 min	4	25 min	bon marché	2
124	Aubergines farcies à la ratatouille et aux raisins secs	1 h	facile		45 min		15 min	bon marché	2
125	aubergines farcies à la ricotta	1 h	facile	https://assets.afcdn.com/recipe/20170531/3073_w420h344c1cx1632cy918.jpg	40 min	4	20 min	bon marché	2
126	Aubergines farcies à la viande et à la mozzarella	1h05	facile		50 min		15 min	bon marché	2
127	Aubergines farcies au poulet et chair à saucisse	1 h	facile		45 min	4.7	15 min	bon marché	2
128	Aubergines farcies au riz à l'orientale	45 min	facile	https://assets.afcdn.com/recipe/20120502/41700_w420h344c1cx946cy1420.jpg	15 min	3.9	30 min	bon marché	6
129	Aubergines farcies (aux légumes ou mixte)	55 min	Niveau moyen	https://assets.afcdn.com/recipe/20180208/77437_w420h344c1cx1866cy2800cxt0cyt0cxb3733cyb5600.jpg	40 min	4.8	15 min	bon marché	2
130	Aubergines farcies aux oignons caramélisés	55 min	très facile		45 min		10 min	bon marché	2
131	Aubergines farcies aux petits légumes	1h45	facile		1 h	4.3	45 min	bon marché	4
132	Aubergines farcies aux tomates	1 h	facile		25 min		35 min	bon marché	6
133	Aubergines frites à la sauce tomate	1h20	Niveau moyen		20 min	4.3	1 h	Coût moyen	6
134	Aubergines gratinées à la feta	55 min	très facile		45 min	3.3	10 min	bon marché	4
135	Aubergines ou courgettes à la farce de mouton, saveur de l'Orient	50 min	facile		30 min		20 min	bon marché	4
136	Aubergines roulées à la mozzarella	1h15	Niveau moyen	https://assets.afcdn.com/recipe/20120502/61533_w420h344c1cx1947cy1294.jpg	30 min	4.6	45 min	Coût moyen	6
137	aubergines soufflées à la brousse	1 h	très facile		30 min	3.8	30 min	bon marché	2
138	Aubergines soufflées à la fourme	1h30	facile		30 min	4	1 h	bon marché	8
139	Aumônière aux 3 légumes	30 min	facile	https://assets.afcdn.com/recipe/20110803/65162_w420h344c1cx192cy256.jpg	10 min	4.3	20 min	bon marché	4
140	Aumonière de rouget à la fondue de poireaux	1h20	facile	https://assets.afcdn.com/recipe/20141027/62230_w420h344c1cx816cy612.jpg	20 min	4.6	1 h	Coût moyen	4
141	Aumonières aux aubergines et à la ricotta	1h45	Niveau moyen		15 min	5	1h30	Coût moyen	6
142	Aumônières aux pommes flambées au Calvados BUSNEL	35 min	facile	https://assets.afcdn.com/recipe/20161205/63079_w420h344c1cx1334cy2072.jpg	5 min		30 min	bon marché	6
143	Aumônières campagnardes de poires aux fruits secs	1h05	facile		20 min	5	45 min	bon marché	4
144	Aumonières de blettes à la brousse	40 min	facile		20 min	5	20 min	bon marché	4
145	Aumônières de crêpes au Cognac COURCEL et sauce chocolat	40 min	facile	https://assets.afcdn.com/recipe/20191119/102675_w420h344c1cx1167cy1167cxt0cyt0cxb2334cyb2334.jpg	20 min		20 min	bon marché	6
146	Aumônières de St jacques aux petits légumes	30 min	Niveau moyen	https://assets.afcdn.com/recipe/20141231/26039_w420h344c1cx1152cy768.jpg	10 min	5	20 min	Coût moyen	4
147	Autruche à la crème au vin	40 min	facile		10 min	3	30 min	Coût moyen	3
148	Autruche à la provençale	40 min	très facile		20 min	4.7	20 min	Coût moyen	4
149	Avec un reste de pâtes ou pâtes à la campagnarde	20 min	très facile		15 min	5	5 min	bon marché	4
150	Avocat à la volaille (pour utiliser les restes)	15 min	très facile		0		15 min	bon marché	4
151	Azbane ( boulettes de semoule parfumées et bouillon aux légumes et à la viande)	2 h	Niveau moyen	https://assets.afcdn.com/recipe/20140716/21627_w420h344c1cx129cy97.jpg	1 h	3.8	1 h	Coût moyen	6
152	Baba au Rhum Ambré LA MARTINIQUAISE	45 min	facile	https://assets.afcdn.com/recipe/20161205/34697_w420h344c1cx2153cy2965.jpg	25 min		20 min	Coût moyen	1
153	Bacalhau à Brás (Morue à Brás)	1 h	facile		30 min		30 min	Coût moyen	6
154	bacalhau a bras (morue à la portugaise)	50 min	facile	https://assets.afcdn.com/recipe/20170215/2336_w420h344c1cx1500cy1000.jpg	10 min	4.9	40 min	Coût moyen	6
155	Bäckeoffe à l'alsacienne	3h30	facile	https://assets.afcdn.com/recipe/20130316/48380_w420h344c1cx192cy256.jpg	3 h	4.9	30 min	Coût moyen	6
156	Bacon grillé à la provençale	40 min	très facile		30 min	5	10 min	bon marché	4
157	Bagels au poulet, chèvre, courgettes, menthe	30 min	très facile	https://assets.afcdn.com/recipe/20190731/95819_w420h344c1cx2448cy1632cxt0cyt0cxb4896cyb3264.jpg	15 min		15 min	bon marché	4
158	Bagels aux poivrons	5 min	très facile	https://assets.afcdn.com/recipe/20180330/78291_w420h344c1cx2808cy1872cxt0cyt0cxb5616cyb3744.jpg	0	4.5	5 min	bon marché	2
159	Baguette de pain aux poivrons	40 min	facile		10 min	4.8	30 min	bon marché	4
215	Blanc de poulet à la normande	45 min	facile		25 min	3.3	20 min	bon marché	6
160	Ball Cereals au lait concentré sucré	1h10	facile	https://assets.afcdn.com/recipe/20190509/92131_w420h344c1cx613cy345cxt0cyt0cxb1227cyb690.png	0		10 min	bon marché	6
161	Ballotin de poulet à l'avocat	1h10	très facile	https://assets.afcdn.com/recipe/20190112/85911_w420h344c1cx1008cy756cxt0cyt0cxb2016cyb1512.jpg	50 min		20 min	bon marché	4
162	Bami aux pâtes jaunes	40 min	Niveau moyen		15 min		25 min	Coût moyen	6
163	Banane congolaise à la poêle	25 min	très facile		10 min	5	15 min	bon marché	2
164	Bananes au jambon, moutarde à l'estragon	35 min	très facile		30 min	3.3	5 min	bon marché	2
165	Bananes rôties au bacon et  riz aux raisins	45 min	facile		20 min	3	25 min	Coût moyen	4
166	Papillotes de bar à la feta et aux pommes	40 min	facile		25 min	5	15 min	Coût moyen	4
167	Bar à la normande	1h05	facile		45 min		20 min	Coût moyen	4
168	Bar à la sauce 'de luxe'	1 h	Niveau moyen		15 min	4.8	45 min	Coût moyen	4
169	Bar à la vanille	1h06	facile		6 min		1 h	Coût moyen	4
170	Bar aux petits légumes	45 min	facile	https://assets.afcdn.com/recipe/20130128/31706_w420h344c1cx256cy192.jpg	30 min	5	15 min	bon marché	4
171	Bar en papillote à la truite fumée	45 min	facile		30 min		15 min	bon marché	4
172	Bar en papillotte à la moutarde et au romarin	35 min	très facile	https://assets.afcdn.com/recipe/20150521/33934_w420h344c1cx1500cy1000.jpg	30 min	4.7	5 min	assez cher	1
173	Bar farci aux herbes et aux épices	1h10	facile		20 min	5	50 min	Coût moyen	8
174	Bar gratiné aux câpres	30 min	facile		20 min		10 min	Coût moyen	4
175	Bar grillé aux herbes	36 min	facile	https://assets.afcdn.com/recipe/20131211/32578_w420h344c1cx780cy585.jpg	16 min	4.8	20 min	Coût moyen	6
176	Bar rôti à la tomate	45 min	facile		20 min	4.9	25 min	Coût moyen	4
177	Bar rôti aux 2 fenouils	45 min	facile		30 min		15 min	Coût moyen	4
178	Bar rôti aux herbes	40 min	facile	https://assets.afcdn.com/recipe/20190824/96725_w420h344c1cx375cy500cxt0cyt0cxb750cyb1000.jpg	30 min	5	10 min	Coût moyen	6
179	Barbue au cidre et sa sauce aux crevettes	50 min	facile		30 min	5	20 min	Coût moyen	8
180	Barbue aux radis roses et crème à l'ail	45 min	facile		25 min		20 min	bon marché	4
181	Barbues à la tomate et aux olives	40 min	facile		20 min	5	20 min	bon marché	4
182	Basmati aux poivrons façon risotto	50 min	facile	https://assets.afcdn.com/recipe/20140927/56796_w420h344c1cx1224cy1632.jpg	30 min	3.5	20 min	bon marché	4
183	Baudroie à la beffroi	25 min	facile		15 min		10 min	Coût moyen	4
184	Baudroie (lotte) à la sétoise	1h15	Niveau moyen		1 h	5	15 min	assez cher	2
185	Bauen Fruhstuck : poêlée de pommes de terre aux lardons et aux oignons	1h05	facile	https://assets.afcdn.com/recipe/20170607/68275_w420h344c1cx1560cy2080.jpg	25 min	4	40 min	bon marché	4
186	Bavette à l'échalote à ma façon	30 min	facile		20 min	4.7	10 min	bon marché	2
187	bavette à l'échalote sauce vin rouge	35 min	facile	https://assets.afcdn.com/recipe/20170310/63843_w420h344c1cx1800cy1200.jpg	30 min	4.4	5 min	assez cher	2
188	Bavette à l'échalote	25 min	très facile	https://assets.afcdn.com/recipe/20131215/15104_w420h344c1cx2000cy1500.jpg	15 min	4.7	10 min	bon marché	4
189	Bavette cuite au four et aux oignons	35 min	facile		10 min	5	25 min	Coût moyen	4
190	Bécasse à la Fressinoise (groseilles, lard)	55 min	facile	https://assets.afcdn.com/recipe/20130118/53982_w420h344c1cx256cy192.jpg	30 min		25 min	bon marché	3
191	Béchamel aux oeufs durs	35 min	très facile		15 min		20 min	bon marché	4
192	Beignets au Nutella®	1h05	Niveau moyen	https://assets.afcdn.com/recipe/20171009/72899_w420h344c1cx1000cy666cxt0cyt0cxb2000cyb1333.jpg	5 min	3.2	1 h	bon marché	10
193	Beignets aux 3 fromages (Beureg)	1h20	facile		20 min		1 h	bon marché	8
194	Beignets aux herbes	20 min	très facile		10 min		10 min	bon marché	2
195	Beignets de Truite aux épices	25 min	facile		10 min	2	15 min	Coût moyen	2
196	Beignets fourrés à la viande de bison, sauce brunoise	1 h	facile		20 min		40 min	Coût moyen	4
197	Beignets végétariens à la tomate	30 min	facile	https://assets.afcdn.com/recipe/20160928/30697_w420h344c1cx2808cy1872.jpg	10 min	4.3	20 min	bon marché	4
198	Berniques à la poêle	20 min	facile		5 min	5	15 min	Coût moyen	4
199	Betteraves à la polonaise	1h20	très facile	https://assets.afcdn.com/recipe/20190108/85725_w420h344c1cx768cy1024cxt0cyt0cxb1536cyb2048.jpg	1 h	4.1	20 min	bon marché	4
200	BIBBELESKÄS  (fromage blanc à la paysanne)	10 min	très facile		0	4.5	10 min	bon marché	4
201	Bifteck a la créole	25 min	facile	https://assets.afcdn.com/recipe/20131120/9612_w420h344c1cx640cy480.jpg	15 min	4.8	10 min	bon marché	4
202	Biftecks à la sauce vinaigrée	35 min	très facile		20 min		15 min	bon marché	2
203	Bigos à l'espagnole	1h15	très facile		1 h	4.5	15 min	assez cher	6
204	Bimi sauté aux amandes	43 min	facile	https://assets.afcdn.com/recipe/20180518/79156_w420h344c1cx2376cy1584cxt0cyt0cxb4752cyb3168.jpg	38 min		5 min	bon marché	4
205	Biscuit roulé de carottes à la crème de poireaux	1 h	Niveau moyen	https://assets.afcdn.com/recipe/20140115/35589_w420h344c1cx1945cy1176.jpg	30 min	4.5	30 min	bon marché	6
206	Biscuits “baci di dama” au Nutella®	1h08	Niveau moyen	https://assets.afcdn.com/recipe/20171004/72701_w420h344c1cx4344cy2896cxt0cyt0cxb8688cyb5792.jpg	18 min	4	50 min	bon marché	24
207	Biscuits de Noël fourrés au Nutella®	1h20	facile	https://assets.afcdn.com/recipe/20181116/83901_w420h344c1cx512cy341cxt0cyt0cxb1024cyb683.jpg	15 min	4.2	1h05	bon marché	14
208	Biscuits double sensation au Nutella®	2 h	Niveau moyen	https://assets.afcdn.com/recipe/20171009/72898_w420h344c1cx666cy1000cxt0cyt0cxb1333cyb2000.jpg	25 min		1h35	bon marché	20
209	Biscuits étoilés à la cannelle et au Nutella®	1h05	Niveau moyen	https://assets.afcdn.com/recipe/20161128/34828_w420h344c1cx455cy341.jpg	10 min		55 min	bon marché	30
210	Biscuits sandwichs au Nutella®	1h53	très facile	https://assets.afcdn.com/recipe/20191106/102002_w420h344c1cx960cy540cxt0cyt0cxb1920cyb1080.png	10 min	1	28 min	bon marché	25
211	Blanc de dinde à l'ananas, lait de coco et épices	50 min	facile		30 min	4.8	20 min	bon marché	4
212	Blanc de poulet à l'ananas	20 min	facile		10 min	5	10 min	bon marché	4
213	Blanc de poulet à l'indienne au four	1h20	très facile		1 h	4	20 min	bon marché	2
534	Bricks aux pruneaux et au chèvre	25 min	très facile		15 min	4	10 min	bon marché	1
216	Blanc de poulet aux courgettes et au lard	50 min	très facile	https://assets.afcdn.com/recipe/20140308/24651_w420h344c1cx1632cy1224.jpg	30 min	4.2	20 min	Coût moyen	2
217	Blanc de poulet aux endives, champignon et lardons au Companion	15 min	très facile		12 min		3 min	bon marché	4
218	Blanc de poulet aux endives, champignon et lardons au Cookeo	15 min	très facile		9 min		6 min	bon marché	4
219	Blanc de poulet aux endives, champignon et lardons au Cooking Chef	15 min	très facile		12 min		3 min	bon marché	4
220	Blanc de poulet aux endives, champignon et lardons au Monsieur Cuisine		très facile		0			bon marché	
221	Blanc de poulet aux endives, champignon et lardons au Thermomix	15 min	très facile		12 min		3 min	bon marché	4
222	Blanc de poulet aux endives, champignons et lardons	55 min	très facile	https://assets.afcdn.com/recipe/20131118/38246_w420h344c1cx1250cy1875.jpg	40 min	4.8	15 min	bon marché	4
223	Blancs de poulet aux olives noires	25 min	facile		10 min	4	15 min	bon marché	4
224	Blanc de poulet émincé aux flageolets	35 min	facile		20 min	3.6	15 min	bon marché	2
225	Blanc de poulet farci aux carottes et courgettes	1h10	facile		50 min	4	20 min	bon marché	4
226	Blanc de poulet marinés à la tapenade	35 min	facile		25 min	4	10 min	bon marché	4
227	Blanc de poulet, sauce à l'ananas	35 min	facile		25 min	2.5	10 min	Coût moyen	4
228	Blanc de poulet sauté à la chinoise	20 min	très facile		10 min		10 min	bon marché	4
229	Blancs de poulet à l'alsacienne	35 min	facile	https://assets.afcdn.com/recipe/20171124/75508_w420h344c1cx936cy1664cxt0cyt0cxb1872cyb3328.jpg	20 min	4.4	15 min	bon marché	4
230	Blancs de poulet à l'estragon	35 min	très facile	https://assets.afcdn.com/recipe/20130822/65698_w420h344c1cx1296cy968.jpg	20 min	4.4	15 min	bon marché	4
231	Blancs de Poulet à l'orientale	20 min	très facile		10 min		10 min	bon marché	2
232	Blancs de poulet à la béchamel	20 min	très facile	https://assets.afcdn.com/recipe/20170412/38364_w420h344c1cx2144cy1424.jpg	15 min	4.9	5 min	bon marché	4
233	Blancs de poulet a la crème et aux champignons	20 min	très facile	https://assets.afcdn.com/recipe/20170323/63455_w420h344c1cx2464cy1632.jpg	15 min	4.6	5 min	Coût moyen	4
234	blancs de poulet à la franc comtoise	35 min	très facile		20 min	4.2	15 min	bon marché	6
235	Blancs de poulet au boursin et aux poires	20 min	très facile	https://assets.afcdn.com/recipe/20150205/57927_w420h344c1cx1224cy1632.jpg	18 min	4.6	2 min	Coût moyen	4
236	Blancs de poulet au fenouil à la vapeur d'anis	40 min	facile	https://assets.afcdn.com/recipe/20161020/27196_w420h344c1cx2136cy1424.jpg	25 min	4	15 min	bon marché	4
237	Escalopes de poulet au gruyère et aux morilles	30 min	facile		15 min	5	15 min	assez cher	2
238	blancs de poulet au jambon de Parme et à la mozzarella	30 min	très facile		15 min	5	15 min	bon marché	4
239	Blancs de poulet au persil et à l'ail	45 min	facile	https://assets.afcdn.com/recipe/20130420/50938_w420h344c1cx256cy192.jpg	30 min	4	15 min	bon marché	6
240	Blancs de poulet aux amandes	30 min	très facile		10 min	4.6	20 min	bon marché	4
241	Blancs de poulet aux brocolis sauce coco	50 min	très facile	https://assets.afcdn.com/recipe/20130527/42277_w420h344c1cx256cy192.jpg	20 min	4.8	30 min	bon marché	4
242	Poulet aux coeurs d'artichauts	45 min	très facile	https://assets.afcdn.com/recipe/20130704/63285_w420h344c1cx1162cy1750.jpg	30 min	4.4	15 min	bon marché	2
243	Blancs de poulet aux endives champignons et  lait de coco	1h15	facile		45 min	4.3	30 min	bon marché	4
244	Blancs de poulet aux herbes	1h05	très facile		35 min	4.3	30 min	bon marché	4
245	Blancs de poulet aux oignons nouveaux	40 min	Niveau moyen	https://assets.afcdn.com/recipe/20190710/95005_w420h344c1cx393cy540cxt0cyt0cxb786cyb1080.jpg	20 min	5	20 min	Coût moyen	4
246	Blancs de poulet aux pépites de chocolat amer et amandes grillées	30 min	facile		15 min		15 min	Coût moyen	4
247	Blancs de poulet aux poivrons rouges et noix	1h10	facile		30 min		40 min	Coût moyen	4
248	Blancs de poulet aux tomates confites	55 min	très facile	https://assets.afcdn.com/recipe/20150831/63750_w420h344c1cx1728cy2592.jpg	40 min	4.4	15 min	Coût moyen	6
249	Blancs de poulet farcis à la roquette et au chèvre	40 min	facile		20 min	4.6	20 min	bon marché	2
250	Blancs de poulet farcis à la sauge	32 min	facile		12 min	5	20 min	bon marché	4
251	Blancs de poulet farcis à la tomate séchée et à la mozzarella	1 h	facile	https://assets.afcdn.com/recipe/20140713/52151_w420h344c1cx240cy320.jpg	45 min	4.7	15 min	bon marché	2
252	Blancs de poulet farcis aux deux pestos et à la ricotta	55 min	très facile	https://assets.afcdn.com/recipe/20100120/26401_w420h344c1cx256cy192.jpg	40 min	4.5	15 min	bon marché	4
253	Blancs de poulet farcis aux fruits secs et pain d'épice	40 min	facile		15 min	3.5	25 min	bon marché	4
254	Blancs de poulet minceur à ma façon	45 min	très facile	https://assets.afcdn.com/recipe/20190321/89767_w420h344c1cx1995cy1330cxt0cyt0cxb3988cyb2658.jpg	30 min	4.3	15 min	bon marché	2
255	Blancs de poulet minceur au curry et à la tomate	22 min	facile	https://assets.afcdn.com/recipe/20140528/49358_w420h344c1cx1632cy1224.jpg	7 min	4.8	15 min	bon marché	4
256	Blancs de seiche aux épices et potiron	28 min	facile		8 min	4	20 min	Coût moyen	4
257	blancs de seiche aux petites légumes	1h05	très facile		45 min	4	20 min	Coût moyen	3
258	blancs de volaille aux amandes	30 min	très facile		15 min	3	15 min	bon marché	6
259	blancs de volaille sautés aux amandes	35 min	facile		20 min	5	15 min	bon marché	6
260	Blancs de volailles aux trompettes de la mort	30 min	très facile		20 min		10 min	bon marché	4
261	Blanquette de veau à l'espagnole, au chorizo	1h30	facile	https://assets.afcdn.com/recipe/20140501/37575_w420h344c1cx410cy291.jpg	1 h	4.8	30 min	Coût moyen	6
262	Blanquette à la Véro	1h30	facile		1 h	4	30 min	Coût moyen	4
263	Blanquette aux chicons ( endives )	1h20	très facile		1 h	4.6	20 min	bon marché	4
264	Blanquette aux pointes d'asperges de la baronne	2h35	facile		2 h	4.7	35 min	Coût moyen	6
265	blanquette d'agneau à l'ancienne	1h15	facile		45 min	4.4	30 min	Coût moyen	4
266	Blanquette d'agneau à la menthe	50 min	facile		30 min	4.3	20 min	Coût moyen	4
267	Blanquette de dinde aux champignons	1h15	facile	https://assets.afcdn.com/recipe/20130609/52847_w420h344c1cx256cy192.jpg	1 h	4.7	15 min	bon marché	6
268	Blanquette de dinde aux lardons	40 min	facile	https://assets.afcdn.com/recipe/20130601/53860_w420h344c1cx256cy192.jpg	20 min	5	20 min	bon marché	6
269	Blanquette de dinde aux orties	1 h	facile		30 min	2.5	30 min	Coût moyen	4
270	Blanquette de dinde aux poireaux	35 min	facile	https://assets.afcdn.com/recipe/20100120/17718_w420h344c1cx256cy192.jpg	25 min	4.7	10 min	bon marché	6
271	Blanquette de lapin au riesling et aux girolles	1h20	très facile	https://assets.afcdn.com/recipe/20120502/6885_w420h344c1cx1140cy1750.jpg	55 min	4.8	25 min	bon marché	4
272	Blanquette de lotte aux petits légumes	50 min	facile	https://assets.afcdn.com/recipe/20170414/21054_w420h344c1cx2880cy1920.jpg	30 min	5	20 min	assez cher	4
273	Blanquette de lotte aux pointes d'asperges	35 min	Niveau moyen		15 min	4.4	20 min	assez cher	4
274	Blanquette de lotte, Saint-Jacques et crevettes au champagne	25 min	facile	https://assets.afcdn.com/recipe/20191121/102755_w420h344c1cx1891cy2837cxt0cyt0cxb3783cyb5675.jpg	0		25 min	Coût moyen	4
275	Blanquette de saumon à ma façon	1 j 6 h 45 min	facile	https://assets.afcdn.com/recipe/20151213/37513_w420h344c1cx1632cy918.jpg	45 min	5	1 j 6 h	Coût moyen	6
276	Blanquette de saumon aux moules	1h45	facile	https://assets.afcdn.com/recipe/20180122/76986_w420h344c1cx1824cy2328cxt0cyt0cxb3648cyb4657.jpg	45 min	4.8	1 h	Coût moyen	4
277	blanquette de seiche au safran et aux champignons	1h15	facile		50 min	4.9	25 min	Coût moyen	4
278	Blanquette de seiche aux champignons frais	1h30	très facile	https://assets.afcdn.com/recipe/20100120/10352_w420h344c1cx256cy192.jpg	1 h	4.7	30 min	Coût moyen	4
279	Blanquette de thon à l'ancienne	1h30	Niveau moyen		1 h	4.2	30 min	Coût moyen	8
280	Blanquette de veau à l'ancienne	2h10	facile	https://assets.afcdn.com/recipe/20181017/82831_w420h344c1cx1781cy2461cxt0cyt0cxb3563cyb4922.jpg	1h40	4.8	30 min	Coût moyen	6
281	blanquette de veau à l'estragon et à la badiane	3 h	très facile		2 h	4	1 h	bon marché	4
282	Blanquette de veau à la bière Tongerlo à ma façon	1h50	facile	https://assets.afcdn.com/recipe/20170204/66411_w420h344c1cx960cy540.jpg	1h30		20 min	Coût moyen	4
283	Blanquette de veau à la catalane	1h45	facile		45 min	5	1 h	Coût moyen	4
284	Blanquette de veau à la provençale	2 h	facile		1h45	3	15 min	bon marché	6
285	Blanquette de veau à la ratatouille	1h15	très facile		45 min	3.4	30 min	Coût moyen	5
286	Blanquette de veau à la sauce cremeuse au bleu	55 min	très facile		45 min	5	10 min	bon marché	2
287	Blanquette de veau à la St Feuillien	35 min	très facile		20 min	4.8	15 min	bon marché	4
288	'Blanquette' de veau à la tomate et courgettes	1h30	très facile	https://assets.afcdn.com/recipe/20160109/31997_w420h344c1cx1296cy776.jpg	1 h	4.7	30 min	Coût moyen	6
289	Blanquette de veau à la vanille	2h20	facile	https://assets.afcdn.com/recipe/20150216/31455_w420h344c1cx2304cy1296.jpg	2 h	4.9	20 min	Coût moyen	6
290	Blanquette de veau à ma façon	1h20	Niveau moyen	https://assets.afcdn.com/recipe/20190319/89637_w420h344c1cx2050cy1938cxt0cyt0cxb3838cyb3838.jpg	50 min	3.7	30 min	Coût moyen	6
291	Blanquette de veau améliorée aux pleurotes	2 h	facile		1 h	4.8	1 h	Coût moyen	6
292	Blanquette de veau aux châtaignes	1h30	facile		1 h	4.4	30 min	Coût moyen	4
293	Blanquette de veau aux pleurotes et légumes de saison	2h20	Niveau moyen		2 h	4.8	20 min	Coût moyen	4
294	Blanquette de veau narbonnaise aux cornichons	16 min	très facile		1 min	4.6	15 min	Coût moyen	4
295	Blanquette de volaille aux champignons	50 min	très facile	https://assets.afcdn.com/recipe/20160524/32100_w420h344c1cx1632cy1224.jpg	30 min	4.8	20 min	bon marché	4
296	Blé au poulet et aux légumes	1h10	facile		40 min	4.5	30 min	bon marché	4
297	Blé aux champignons et jambon	35 min	très facile		20 min	4.2	15 min	bon marché	2
298	Blé aux légumes pour enfants	13 min	très facile		8 min	4.8	5 min	bon marché	4
299	Blé aux légumes simple	1h05	facile		50 min		15 min	bon marché	4
300	Blésotto à l'italienne	50 min	facile	https://assets.afcdn.com/recipe/20190306/88845_w420h344c1cx2016cy1512cxt0cyt0cxb4032cyb3024.jpg	40 min		10 min	bon marché	4
301	'blésotto' aux crevettes et à la féta	20 min	très facile	https://assets.afcdn.com/recipe/20190219/88010_w420h344c1cx2880cy1920cxt0cyt0cxb5758cyb3833.jpg	15 min	4.2	5 min	bon marché	4
302	Blettes à l'ail et aux anchois	50 min	facile	https://assets.afcdn.com/recipe/20190411/90733_w420h344c1cx540cy720cxt0cyt0cxb1080cyb1440.jpg	20 min	4.5	30 min	Coût moyen	4
303	Blettes à la bolognaise	55 min	facile	https://assets.afcdn.com/recipe/20150327/2413_w420h344c1cx1224cy1632.jpg	25 min	4.8	30 min	bon marché	4
304	Blettes à la tomate et au fromage	40 min	facile		30 min	5	10 min	bon marché	4
305	Blettes aux tomates et aux lardons fumés	1h05	très facile	https://assets.afcdn.com/recipe/20171026/73920_w420h344c1cx266cy480cxt0cyt0cxb533cyb960.jpg	45 min	4.5	20 min	bon marché	4
306	Blettes et poivrons braisés aux lardons	40 min	très facile	https://assets.afcdn.com/recipe/20100120/43177_w420h344c1cx256cy192.jpg	30 min	4.5	10 min	bon marché	2
307	Blettes mijotées à la tomate	1 h	très facile		30 min	4	30 min	bon marché	4
308	BLINIS A L AVOCAT	25 min	très facile		20 min	2	5 min	Coût moyen	2
309	Blinis aux pommes de terre et à la ciboulette	25 min	facile		20 min		5 min	Coût moyen	1
310	Boeuf à l'ail et à la tomate	2h10	très facile		2 h	4.3	10 min	bon marché	4
311	Boeuf à l'ananas	2h20	Niveau moyen		2 h	4	20 min	Coût moyen	4
312	Boeuf à l'indienne, très provençal	3h10	facile	https://assets.afcdn.com/recipe/20150924/49091_w420h344c1cx241cy320.jpg	3 h		10 min	Coût moyen	4
313	Boeuf à la bière brune	1h20	très facile		1 h	4	20 min	Coût moyen	6
314	Boeuf à la bière	1h40	facile	https://assets.afcdn.com/recipe/20100120/21423_w420h344c1cx256cy192.jpg	40 min	4.7	1 h	Coût moyen	4
315	Boeuf à la clémentine	45 min	très facile	https://assets.afcdn.com/recipe/20190228/88519_w420h344c1cx1224cy1496cxt0cyt0cxb2448cyb2992.jpg	25 min	4.9	20 min	bon marché	4
316	Boeuf à la crème (recette tchèque)	2h25	facile		2 h	4	25 min	Coût moyen	4
317	Boeuf à la ficelle et ses légumes	40 min	facile		20 min		20 min	Coût moyen	4
318	Boeuf à la ficelle rapide	1h30	Niveau moyen		1 h		30 min	assez cher	4
319	Boeuf à la ficelle	4h30	facile		4 h	4.7	30 min	Coût moyen	6
320	Boeuf à la Guinness façon Isa	3h15	facile		3 h		15 min	Coût moyen	6
321	Boeuf à la hongroise	1h10	très facile	https://assets.afcdn.com/recipe/20161129/13951_w420h344c1cx1632cy1224.jpg	40 min	4.9	30 min	bon marché	6
322	Boeuf à la javanaise	45 min	très facile	https://assets.afcdn.com/recipe/20141230/33949_w420h344c1cx2064cy1161.jpg	30 min	4.7	15 min	bon marché	8
323	Boeuf à la mexicaine	40 min	facile	https://assets.afcdn.com/recipe/20161118/36006_w420h344c1cx1448cy2576.jpg	15 min	4	25 min	Coût moyen	4
324	Boeuf à la mode (braisé)	1h20	très facile	https://assets.afcdn.com/recipe/20190130/86920_w420h344c1cx1512cy1823cxt0cyt0cxb3024cyb3646.jpg	1 h	4.4	20 min	bon marché	6
325	Boeuf à la moutarde rapide	30 min	facile	https://assets.afcdn.com/recipe/20140917/12965_w420h344c1cx1280cy960.jpg	10 min	3.5	20 min	bon marché	4
326	Boeuf à la moutarde	1h35	très facile	https://assets.afcdn.com/recipe/20171019/73616_w420h344c1cx2304cy1536cxt0cyt0cxb4608cyb3072.jpg	1h30	4	5 min	bon marché	4
327	Boeuf à la Saint-Gilloise (Gard)	1h30	très facile		1 h	4.1	30 min	Coût moyen	6
328	Boeuf à la sauce balsamique	3h30	facile	https://assets.afcdn.com/recipe/20100120/10259_w420h344c1cx256cy192.jpg	3 h	4.8	30 min	Coût moyen	4
329	Boeuf à la sauce d'huîtres	30 min	très facile	https://assets.afcdn.com/recipe/20191023/101091_w420h344c1cx3575cy2116cxt0cyt0cxb5600cyb3738.jpg	15 min	5	15 min	bon marché	2
330	boeuf à la sauce de soja	1h40	facile		40 min	4.5	1 h	bon marché	4
331	Boeuf à la tomate et au piment	20 min	facile		10 min		10 min	bon marché	2
332	Boeuf à la tomate	20 min	très facile		15 min	2.8	5 min	bon marché	4
333	Boeuf à ma façon	1h30	facile		1 h	4.3	30 min	Coût moyen	4
334	Boeuf aux courgettes poêlé	40 min	facile		20 min		20 min	bon marché	4
335	Boeuf au satay et aux épinards (Taïwan)	40 min	facile		10 min	4.8	30 min	Coût moyen	2
336	Boeuf aux abricots	2h15	très facile		2 h		15 min	bon marché	4
337	Boeuf aux anchois et aux câpres	2h15	très facile	https://assets.afcdn.com/recipe/20181211/84781_w420h344c1cx584cy359cxt0cyt0cxb1168cyb719.jpg	2 h	4.7	15 min	Coût moyen	6
338	Boeuf aux carottes braisé	3h10	facile		2h10	4	1 h	Coût moyen	6
339	Boeuf aux carottes et aux épices	1h20	très facile	https://assets.afcdn.com/recipe/20120502/47929_w420h344c1cx1140cy1750.jpg	1 h	4.8	20 min	bon marché	6
340	Boeuf aux champignons	1 h	facile		30 min	4	30 min	Coût moyen	2
341	Boeuf aux courgettes et au cumin	30 min	très facile	https://assets.afcdn.com/recipe/20100120/29814_w420h344c1cx256cy192.jpg	20 min	4.3	10 min	bon marché	4
342	Boeuf aux courgettes et pommes de terre	45 min	facile		30 min	3	15 min	bon marché	4
343	Boeuf aux épices	30 min	facile		15 min	2	15 min	Coût moyen	4
344	Boeuf aux épinards en gratin	1 h	très facile		40 min	4.5	20 min	bon marché	5
345	Boeuf aux graines de courges	3h10	Niveau moyen	https://assets.afcdn.com/recipe/20161006/29919_w420h344c1cx1824cy2736.jpg	2h30		40 min	Coût moyen	6
346	Boeuf aux herbes	2h30	facile		2 h		30 min	Coût moyen	6
347	Boeuf aux légumes tout fondant	3h20	facile		3 h	4	20 min	Coût moyen	4
348	Boeuf aux oignons à la chinoise	35 min	très facile	https://assets.afcdn.com/recipe/20130509/49391_w420h344c1cx256cy192.jpg	15 min	4.3	20 min	bon marché	4
349	boeuf aux oignons (recette grecque)	1h30	très facile	https://assets.afcdn.com/recipe/20161017/8756_w420h344c1cx2808cy1872.jpg	1 h	5	30 min	Coût moyen	4
350	Boeuf aux oignons très facile	25 min	très facile		10 min	3.5	15 min	bon marché	4
351	Boeuf aux oignons	35 min	très facile	https://assets.afcdn.com/recipe/20150616/43920_w420h344c1cx1632cy1224.jpg	20 min	4.4	15 min	bon marché	2
352	Boeuf aux olives	3 h	facile	https://assets.afcdn.com/recipe/20160325/5317_w420h344c1cx2728cy1784.jpg	1h30	3	1h30	assez cher	6
353	Boeuf aux petits piments	1h25	facile		1 h		25 min	bon marché	4
354	Boeuf aux pommes et gingembre	25 min	très facile		15 min	4.3	10 min	bon marché	3
355	Boeuf aux pousses de bambou	1 h	facile		20 min	4	40 min	Coût moyen	4
356	Boeuf Bourguignon à la biere brune et aux châtaignes	4 h	facile	https://assets.afcdn.com/recipe/20120211/39887_w420h344c1cx256cy192.jpg	3h30	4.8	30 min	bon marché	4
357	Boeuf bourguignon à la tomate	1h20	facile	https://assets.afcdn.com/recipe/20161025/42855_w420h344c1cx1224cy1632.jpg	1 h	4	20 min	bon marché	6
358	Boeuf bourguignon à ma façon	1h30	Niveau moyen	https://assets.afcdn.com/recipe/20100120/60306_w420h344c1cx256cy192.jpg	1 h	4.6	30 min	Coût moyen	4
359	Boeuf bourguignon aux oignons rouges	3 h	facile		2h30		30 min	Coût moyen	4
360	Boeuf bourguignon	1h40	facile	https://assets.afcdn.com/recipe/20190705/94830_w420h344c1cx1775cy2362cxt0cyt0cxb3550cyb4724.jpg	1h25	4.5	15 min	bon marché	6
361	Boeuf braisé à l'oignon	12 min	facile		2 min		10 min	bon marché	2
362	Boeuf braisé à la Guinness	1h30	très facile	https://assets.afcdn.com/recipe/20161006/11930_w420h344c1cx2304cy1536.jpg	1 h	3.8	30 min	Coût moyen	5
363	Boeuf braisé au riz, aux épinards et aux tomates	1h25	facile		1h15	4	10 min	Coût moyen	4
364	Boeuf braisé aux carottes	1h20	très facile	https://assets.afcdn.com/recipe/20180622/79920_w420h344c1cx1944cy2592cxt0cyt0cxb3888cyb5184.jpg	1 h	4.8	20 min	bon marché	6
365	Boeuf braisé aux échalotes	3h25	facile	https://assets.afcdn.com/recipe/20130719/24866_w420h344c1cx1000cy1504.jpg	3 h	4.2	25 min	Coût moyen	6
366	Boeuf caramélisé aux oignons	25 min	très facile		20 min	3.7	5 min	bon marché	2
367	Boeuf carottes à l'orange	2h55	facile		40 min	4	2h15	bon marché	4
368	Boeuf carottes à la normande	1 h	très facile	https://assets.afcdn.com/recipe/20160617/8080_w420h344c1cx2220cy1820.jpg	40 min	4.8	20 min	bon marché	4
369	Boeuf émincé à la chinoise	1h10	facile	https://assets.afcdn.com/recipe/20141014/22658_w420h344c1cx1936cy1296.jpg	45 min	3.3	25 min	Coût moyen	4
370	Boeuf émincé aux poivrons et aux épices	30 min	facile	https://assets.afcdn.com/recipe/20120502/40568_w420h344c1cx972cy1296.jpg	15 min	4.3	15 min	Coût moyen	4
371	Boeuf en croûte aux champignons sauvages	50 min	facile		30 min	4.8	20 min	Coût moyen	4
372	Boeuf en sauce à l'irlandaise	4h30	facile	https://assets.afcdn.com/recipe/20100120/57726_w420h344c1cx256cy192.jpg	4 h	4.9	30 min	Coût moyen	6
373	Boeuf en sauce facile aux épices	2h20	facile		2 h	5	20 min	bon marché	4
374	Boeuf grillé aux feuilles de bananier	40 min	Niveau moyen		20 min	3	20 min	bon marché	6
375	Boeuf haché aux carottes	40 min	facile		30 min	3.9	10 min	bon marché	4
376	Boeuf haché aux poivrons (Hongrie)	25 min	très facile	https://assets.afcdn.com/recipe/20140814/3309_w420h344c1cx900cy1350.jpg	10 min	4.4	15 min	bon marché	4
377	Boeuf haché épicés aux légumes	40 min	facile	https://assets.afcdn.com/recipe/20150224/38334_w420h344c1cx450cy256.jpg	15 min	5	25 min	Coût moyen	4
378	Boeuf mariné à la chinoise	15 min	très facile		10 min	4.4	5 min	bon marché	4
379	Boeuf mariné à la coréenne	40 min	facile	https://assets.afcdn.com/recipe/20181206/84672_w420h344c1cx2016cy1512cxt0cyt0cxb4032cyb3024.jpg	10 min	4.9	30 min	Coût moyen	4
380	Boeuf mijoté aux 3 épices	3h20	très facile	https://assets.afcdn.com/recipe/20190602/93363_w420h344c1cx2016cy1512cxt0cyt0cxb4032cyb3024.jpg	3 h	4	20 min	Coût moyen	4
381	Boeuf mijoté aux aubergines et olives vertes	1h30	facile		1 h	4	30 min	bon marché	8
382	Boeuf sauté à l'orientale	45 min	facile		15 min	5	30 min	Coût moyen	4
383	boeuf sauté aux échalotes à la chinoise	35 min	très facile		15 min	4.3	20 min	bon marché	3
384	Boeuf sauté aux poivrons et graines de sésame	1 h	facile	https://assets.afcdn.com/recipe/20100120/9858_w420h344c1cx256cy192.jpg	30 min	4.5	30 min	bon marché	3
385	Bœuf Strogonoff aux pommes de terre	1 h	facile		40 min		20 min	Coût moyen	8
386	Boeuf tortue comme à La Réunion	3h15	très facile	https://assets.afcdn.com/recipe/20120123/18919_w420h344c1cx256cy192.jpg	3 h	4.9	15 min	Coût moyen	6
387	Bol chinois au boeuf et aux spaghettis	25 min	très facile		5 min	4.3	20 min	bon marché	4
388	Bolognaise à l'arménienne	1h05	facile	https://assets.afcdn.com/recipe/20171117/75227_w420h344c1cx1989cy1328cxt0cyt0cxb3978cyb2656.jpg	40 min	4.3	25 min	Coût moyen	2
389	Bolognaise à ma façon	50 min	très facile	https://assets.afcdn.com/recipe/20171115/75066_w420h344c1cx2464cy1632cxt0cyt0cxb4928cyb3264.jpg	30 min	4	20 min	bon marché	6
390	Bolognaise aux lentilles	1h10	très facile	https://assets.afcdn.com/recipe/20160223/37987_w420h344c1cx250cy375.jpg	50 min	4.5	20 min	bon marché	4
391	Bolognaise de saumon à la crème d'estragon	15 min	très facile		10 min	4.3	5 min	Coût moyen	2
392	Bonhommes aux épices de Noël et au Nutella®	1h09	Niveau moyen	https://assets.afcdn.com/recipe/20171009/72894_w420h344c1cx1000cy666cxt0cyt0cxb2000cyb1333.jpg	9 min	4.6	1 h	bon marché	20
393	Bonite à la balinaise	2 h	Niveau moyen	https://assets.afcdn.com/recipe/20141116/2926_w420h344c1cx320cy240.jpg	30 min	4.7	1h30	bon marché	6
394	Bortsch aux betteraves (Pologne)	20 min	très facile		10 min	1	10 min	bon marché	4
395	Bouché à la reine au veau	55 min	Niveau moyen		25 min	1	30 min	bon marché	5
396	Bouchée à la reine façon nadine	1h30	très facile		1 h		30 min	bon marché	4
397	Préparation au poulet mariné à l'ail pour bouchée à la reine	3 h	facile		30 min		2h30	bon marché	4
398	Bouchées à la reine à ma façon	20 min	facile		10 min		10 min	bon marché	2
399	bouchées à la reine au poisson	20 min	facile		15 min	4.8	5 min	Coût moyen	4
400	bouchées à la reine aux fruits de mer et safran	1 h	facile		30 min	4.5	30 min	Coût moyen	8
401	Bouchées à la reine aux knacks alsaciennes	20 min	très facile		5 min	4.3	15 min	bon marché	3
402	Bouchées à la reine aux st-jacques et saumon	40 min	très facile		20 min	4.6	20 min	Coût moyen	4
403	Bouchées à la reine cathy	20 min	très facile		10 min		10 min	bon marché	2
404	Bouchées à la reine comme en Alsace	2h10	facile	https://assets.afcdn.com/recipe/20181128/84330_w420h344c1cx4025cy2400cxt0cyt0cxb8650cyb5762.jpg	1h30	4.9	40 min	Coût moyen	8
405	Bouchées à la reine façon normande	25 min	très facile	https://assets.afcdn.com/recipe/20130330/47382_w420h344c1cx192cy256.jpg	10 min	4.6	15 min	bon marché	6
406	Bouchées à la reine fruits de mer et béchamel	1h45	facile	https://assets.afcdn.com/recipe/20170602/28111_w420h344c1cx3680cy2456.jpg	45 min	5	1 h	bon marché	6
407	Bouchées à la reine Newburg	40 min	Niveau moyen		20 min	4	20 min	Coût moyen	4
408	Bouchées à la reine (restes de volaille)	25 min	très facile	https://assets.afcdn.com/recipe/20160920/59987_w420h344c1cx2592cy1728.jpg	5 min	4.8	20 min	bon marché	4
409	Bouchées à la reine	1 h	facile	https://assets.afcdn.com/recipe/20181031/83530_w420h344c1cx1817cy2423cxt0cyt0cxb3631cyb4842.jpg	40 min	4.5	20 min	Coût moyen	6
410	Bouchées au chocolat fourrées à la cerise et au Kirsch SYLVAIN	1h48	facile	https://assets.afcdn.com/recipe/20171117/75204_w420h344c1cx2600cy1733cxt0cyt0cxb5200cyb3467.jpg	5 min		28 min	bon marché	6
411	Bouchées aux fruits de mer et quenelles	1h15	très facile		30 min	4.4	45 min	Coût moyen	6
412	Bouchées de volaille sauce au comté et aux raisins	1 h	très facile		30 min	5	30 min	bon marché	4
413	Bouchées feuilletées à la reine	2h40	facile		2 h	4.2	40 min	bon marché	6
414	Bouchées poisson/crevettes aux petits légumes	1 h	facile		20 min	4.8	40 min	Coût moyen	2
415	Boudin aux pommes à ma façon	40 min	très facile		25 min	5	15 min	bon marché	4
416	Boudin blanc à la crème et aux champignons	30 min	très facile	https://assets.afcdn.com/recipe/20150621/48988_w420h344c1cx1296cy968.jpg	20 min	4.2	10 min	bon marché	2
417	Boudin blanc à la moutarde et aux échalotes	20 min	très facile	https://assets.afcdn.com/recipe/20141011/226_w420h344c1cx320cy240.jpg	10 min	4.3	10 min	bon marché	2
418	Boudin blanc aux oignons caramélisés	20 min	facile		10 min	4.3	10 min	bon marché	4
419	Boudin blanc aux pommes et à la mozzarella	50 min	facile		20 min	5	30 min	bon marché	4
420	Boudin blanc aux pommes et aux raisins	35 min	Niveau moyen		20 min	4.8	15 min	Coût moyen	4
421	boudin blanc aux pommes	45 min	facile	https://assets.afcdn.com/recipe/20100120/56096_w420h344c1cx192cy256.jpg	30 min	4.7	15 min	Coût moyen	3
422	Boudin noir aux pommes et miel	1h45	facile		45 min	4.9	1 h	bon marché	2
423	Boudin noir poêlé aux pommes	1h15	facile	https://assets.afcdn.com/recipe/20131017/17657_w420h344c1cx1920cy2880.jpg	45 min	4.7	30 min	bon marché	2
424	Boudins blanc à la provençale	1h15	facile	https://assets.afcdn.com/recipe/20100120/788_w420h344c1cx256cy192.jpg	45 min	4.5	30 min	Coût moyen	4
425	Boudins blancs à l'ananas	30 min	très facile		10 min	4	20 min	Coût moyen	4
426	Boudins blancs aux pommes	25 min	très facile	https://assets.afcdn.com/recipe/20111212/35258_w420h344c1cx256cy192.jpg	10 min	4.7	15 min	bon marché	6
427	Boudins noirs aux oignons	40 min	facile	https://assets.afcdn.com/recipe/20151008/25295_w420h344c1cx997cy1066.jpg	30 min	4.8	10 min	Coût moyen	4
428	Bouillabaisse comme à Marseille	4 h	difficile	https://assets.afcdn.com/recipe/20140326/56899_w420h344c1cx256cy192.jpg	2 h	4.1	2 h	assez cher	10
429	Bouillabaisse de légumes aux oeufs	50 min	facile		30 min	3.7	20 min	Coût moyen	6
430	Bouillabaisse de légumes d’été aux fines saveurs	30 min	très facile	https://assets.afcdn.com/recipe/20110906/14856_w420h344c1cx256cy192.jpg	15 min		15 min	bon marché	2
431	Bouillabaisse de morue aux pommes de terre	1h10	facile	https://assets.afcdn.com/recipe/20181207/84684_w420h344c1cx1944cy1296cxt0cyt0cxb3883cyb2588.jpg	50 min	5	20 min	Coût moyen	6
432	Bouillette à la provençale	30 min	Niveau moyen		20 min		10 min	bon marché	4
433	Bouillon aux petites pâtes	17 min	très facile	https://assets.afcdn.com/recipe/20151002/49428_w420h344c1cx1632cy1224.jpg	15 min	4.6	2 min	bon marché	1
434	Bouillon de poule aux pâtes fraiches maison	2 h	Niveau moyen	https://assets.afcdn.com/recipe/20100120/21430_w420h344c1cx256cy192.jpg	1 h	4.6	1 h	bon marché	4
435	Bouillon de poulet à l'orientale	2h30	facile		2 h	3.5	30 min	Coût moyen	6
436	Bouillon de poulet aux raviolis	2h10	très facile		2 h		10 min	bon marché	4
437	Bouillon de ravioles aux petits légumes	50 min	très facile	https://assets.afcdn.com/recipe/20130924/49512_w420h344c1cx2592cy1728.jpg	30 min	5	20 min	Coût moyen	2
438	Bouillon de tomates aux dés de tofu	50 min	très facile		20 min	5	30 min	bon marché	4
439	Bouillon express aux oignons nouveaux	15 min	facile		10 min	4	5 min	bon marché	2
440	Boules de poulet aux legumes	1 h	facile		45 min		15 min	bon marché	4
441	Boulets à la bière d'Orval	1h30	très facile		1 h	3	30 min	bon marché	1
442	Boulets à la liégeoise - recette de ma grand-mère	1h20	Niveau moyen		1 h		20 min	Coût moyen	4
443	Boulets à la liégeoise	1h25	Niveau moyen	https://assets.afcdn.com/recipe/20100120/392_w420h344c1cx256cy192.jpg	45 min	4.4	40 min	Coût moyen	6
444	Boulettes de veau à l'indienne	50 min	facile		30 min		20 min	Coût moyen	4
445	Boulette façon kefta aux cacahuètes	35 min	très facile		15 min		20 min	bon marché	2
446	Boulettes à l'ail maison	28 min	facile		8 min	3	20 min	bon marché	4
447	Boulettes à l'orientale	50 min	très facile		20 min	4.3	30 min	bon marché	4
448	Boulettes à la liégeoise	55 min	très facile	https://assets.afcdn.com/recipe/20130106/11856_w420h344c1cx192cy256.jpg	45 min	4.4	10 min	bon marché	4
449	Boulettes à la mozza (polpettes)	1h45	facile	https://assets.afcdn.com/recipe/20151024/57700_w420h344c1cx1224cy1632.jpg	1h15	4.5	30 min	bon marché	8
450	Boulettes à la sauce au champignons de Jérémy	20 min	très facile		10 min	5	10 min	bon marché	4
451	Boulettes à la tomate (restes de raclette)	35 min	Niveau moyen		5 min	4	30 min	Coût moyen	10
452	Boulettes au parmesan à la sauce tomate	1h10	facile		1 h	4.4	10 min	bon marché	4
453	Boulettes de poulet et légumes (à partir des restes)	15 min	très facile		10 min	5	5 min	bon marché	4
454	Boulettes aux cerises (plat médiéval)	30 min	facile	https://assets.afcdn.com/recipe/20190526/92952_w420h344c1cx375cy500cxt0cyt0cxb750cyb1000.jpg	15 min	4	15 min	bon marché	2
455	Boulettes aux endives et à la bière	1h20	très facile	https://assets.afcdn.com/recipe/20130719/33086_w420h344c1cx1000cy1504.jpg	1 h	4.6	20 min	bon marché	4
456	Boulettes aux herbes	30 min	très facile		15 min		15 min	bon marché	4
457	Boulettes aux saveurs orientales	15 min	très facile		5 min	4	10 min	bon marché	3
458	Boulettes d'agneau à l'orientale	2 h	facile		1 h	1	1 h	assez cher	6
459	Boulettes d'agneau à la menthe	1 h	Niveau moyen	https://assets.afcdn.com/recipe/20130915/13563_w420h344c1cx320cy212.jpg	30 min	4	30 min	Coût moyen	4
460	Boulettes d'agneau à la sauce tomate	1 h	facile		30 min		30 min	Coût moyen	6
461	Boulettes d'oeufs à la napolitaine (restes de pain)	1 h	facile		30 min	4.2	30 min	bon marché	4
462	Boulettes de boeuf à l'ananas gerdilien (recette créole)	1 h	facile		30 min	4.2	30 min	Coût moyen	4
463	Boulettes de boeuf à la sauce épicée	1h20	facile		1 h	3.8	20 min	Coût moyen	4
464	Boulettes de boeuf à la sauce tomate	30 min	facile		25 min	4.1	5 min	Coût moyen	4
465	Boulettes de boeuf à la tunisienne	45 min	très facile	https://assets.afcdn.com/recipe/20150510/64864_w420h344c1cx1280cy960.jpg	30 min	4.9	15 min	bon marché	4
466	Boulettes de bœuf aux légumes poêlés	50 min	facile		30 min		20 min	bon marché	3
467	Boulettes de boeuf aux oignons et au cumin	20 min	facile	https://assets.afcdn.com/recipe/20190316/89467_w420h344c1cx375cy500cxt0cyt0cxb750cyb1000.jpg	15 min	3	5 min	Coût moyen	4
468	Boulettes de boeuf, riz aux oignons et sauce au yourt	45 min	facile		15 min	4.7	30 min	Coût moyen	4
469	Boulettes de boeufs aux becs d'oiseaux et petit pois	1 h	facile		30 min	4	30 min	Coût moyen	4
470	Boulettes de kefta à la marocaine	1 h	très facile	https://assets.afcdn.com/recipe/20161114/55224_w420h344c1cx1360cy1360.jpg	40 min	4.7	20 min	bon marché	6
471	Boulettes de poisson aux légumes	40 min	très facile	https://assets.afcdn.com/recipe/20160223/37620_w420h344c1cx250cy375.jpg	10 min	4.7	30 min	bon marché	2
472	Boulettes de porc à la bolognaise	1h30	très facile	https://assets.afcdn.com/recipe/20171117/75222_w420h344c1cx2052cy1368cxt0cyt0cxb4104cyb2736.jpg	1 h	2	30 min	bon marché	4
473	Boulettes de poulet aux cacahuètes	50 min	facile		20 min	3.5	30 min	Coût moyen	24
474	Boulettes de poulet aux cinq parfums	30 min	facile		15 min		15 min	Coût moyen	4
475	Boulettes de purée gratinées farcies à la viande	1h50	très facile	https://assets.afcdn.com/recipe/20180504/79024_w420h344c1cx1632cy918cxt0cyt0cxb3264cyb1836.jpg	50 min	3.8	1 h	bon marché	4
476	Boulettes de veau à la crème	45 min	facile	https://assets.afcdn.com/recipe/20100120/967_w420h344c1cx256cy192.jpg	20 min	4.1	25 min	Coût moyen	6
477	Boulettes de veau à la sauce tomate	20 min	facile		10 min		10 min	bon marché	6
478	Boulettes de veau à la tomate et aux lardons	45 min	facile		30 min	3	15 min	bon marché	2
479	Boulettes de veau au gruyère, sauce aux poivrons	1h35	facile		1 h	2	35 min	Coût moyen	6
480	boulettes de veau aux pommes	55 min	facile	https://assets.afcdn.com/recipe/20160913/30225_w420h344c1cx2184cy1456.jpg	35 min	4.4	20 min	Coût moyen	2
481	Boulettes de viande à l'espagnole	14 h	facile		1 h	4.5	13 h	Coût moyen	4
482	Boulettes de viande à l'italienne	35 min	très facile		20 min	3.5	15 min	bon marché	4
483	Boulettes de viande à la coriandre et sauce tomate	50 min	Niveau moyen		20 min	4.9	30 min	bon marché	4
484	Boulettes de viande à la marocaine - kefta b'matecha (Maroc)	45 min	très facile	https://assets.afcdn.com/recipe/20130828/63737_w420h344c1cx2128cy1416.jpg	30 min	4.5	15 min	bon marché	5
485	Boulettes de viande à la russe	25 min	très facile	https://assets.afcdn.com/recipe/20160830/31502_w420h344c1cx1500cy1000.jpg	15 min	4.8	10 min	bon marché	6
486	Boulettes de viande à la Sicilienne	20 min	très facile	https://assets.afcdn.com/recipe/20160804/60376_w420h344c1cx375cy279.jpg	15 min	4.8	5 min	bon marché	4
487	Boulettes de viande à la suédoise	38 min	facile	https://assets.afcdn.com/recipe/20111106/40280_w420h344c1cx192cy256.jpg	8 min	4.6	30 min	bon marché	8
488	Boulettes de viande aux champignons	25 min	très facile	https://assets.afcdn.com/recipe/20170606/19611_w420h344c1cx600cy506.jpg	10 min	4.3	15 min	bon marché	4
489	Boulettes de viande aux courgettes et tomates	55 min	facile	https://assets.afcdn.com/recipe/20190907/97903_w420h344c1cx1632cy1224cxt0cyt0cxb3264cyb2448.jpg	50 min	3	5 min	bon marché	4
490	Boulettes de viande aux épinards	45 min	très facile		15 min	4.4	30 min	bon marché	30
491	Boulettes de viande aux olives	45 min	très facile		30 min	4.3	15 min	bon marché	4
492	Boulettes de viande aux petits pois carottes	50 min	très facile		30 min	4.5	20 min	bon marché	4
493	Boulettes de viande hachée à la vache qui rit	1h05	très facile		5 min		1 h	Coût moyen	2
494	Boulettes de viande légères à la tomate.	1h45	facile	https://assets.afcdn.com/recipe/20130924/12313_w420h344c1cx1424cy2144.jpg	1 h	4.2	45 min	bon marché	4
495	Boulettes farcies aux tomates cerise	50 min	facile		20 min		30 min	bon marché	4
496	Boulettes orientales aux légumes d'été	40 min	difficile	https://assets.afcdn.com/recipe/20100120/49297_w420h344c1cx256cy192.jpg	20 min	4.7	20 min	Coût moyen	2
497	boulettes surprises à la mozzarella	1 h	facile	https://assets.afcdn.com/recipe/20161006/24959_w420h344c1cx2000cy3000.jpg	45 min	4.4	15 min	bon marché	4
498	Boulgour aux bananes et aux raisins secs	25 min	facile	https://assets.afcdn.com/recipe/20120502/3148_w420h344c1cx1140cy1750.jpg	10 min	3.7	15 min	bon marché	2
499	boulgour aux courgettes	40 min	très facile	https://assets.afcdn.com/recipe/20110608/1507_w420h344c1cx192cy256.jpg	30 min	4.5	10 min	bon marché	4
500	Boulgour aux légumes épicés	30 min	très facile		20 min		10 min	bon marché	2
501	Boulgour aux tomates	35 min	facile		20 min	4	15 min	bon marché	2
502	Bouquets à la diable	55 min	très facile	https://assets.afcdn.com/recipe/20160301/14423_w420h344c1cx800cy600.jpg	35 min		20 min	Coût moyen	4
503	Bourguignon à l' ancienne	2 h	facile		1 h	5	1 h	Coût moyen	6
504	Bourguignon à ma façon	2h25	très facile	https://assets.afcdn.com/recipe/20100120/6318_w420h344c1cx256cy192.jpg	2 h	2	25 min	Coût moyen	6
505	Bourguignon de cerf à ma façon	1 j 15 h	facile	https://assets.afcdn.com/recipe/20131026/30256_w420h344c1cx1632cy1224.jpg	3 h	5	1 j 12 h	Coût moyen	4
506	Bourriquette - Soupe à l'oseille et aux oeufs pochés	55 min	facile	https://assets.afcdn.com/recipe/20140918/32843_w420h344c1cx1024cy768.jpg	35 min	4.8	20 min	Coût moyen	4
507	Braisé de boeuf aux carottes	2h45	très facile	https://assets.afcdn.com/recipe/20140306/831_w420h344c1cx1250cy1317.jpg	2 h	4.7	45 min	bon marché	4
508	Brandade de chèvre frais à la menthe et au saumon fumé	30 min	facile		0	4	30 min	bon marché	4
509	Brandade de morue aux asperges	45 min	très facile		15 min	4.3	30 min	Coût moyen	4
510	Brandade de saumon à l'aneth	1h05	facile		20 min		45 min	bon marché	4
511	Brandade saumon - cabillaud	1h10	très facile	https://assets.afcdn.com/recipe/20190528/93064_w420h344c1cx2016cy1512cxt0cyt0cxb4031cyb3024.jpg	45 min	5	25 min	Coût moyen	4
512	Brick à la sauce québécoise	40 min	facile		20 min		20 min	bon marché	4
513	Brick à la tartiflette	55 min	facile		15 min	4	40 min	bon marché	2
514	Brick au poulet et aux deux fromages	35 min	facile		10 min	4.6	25 min	bon marché	4
515	Bricks au saumon fumé et aux pommes	20 min	facile	https://assets.afcdn.com/recipe/20150227/33512_w420h344c1cx918cy918.jpg	10 min	4.3	10 min	bon marché	4
516	Brick aux 2 boudins et aux pommes	40 min	très facile		20 min	4.3	20 min	bon marché	4
517	Brick de rougets aux légumes et purée maison	1h40	Niveau moyen		40 min	3.7	1 h	Coût moyen	4
518	brick de saumon à la crème de lentilles	50 min	facile		30 min	4.2	20 min	Coût moyen	4
519	Bricks aux pommes de terre, lardons et champignons	25 min	facile		15 min		10 min	bon marché	2
520	Bricks à l'indienne	1h10	facile		50 min	4.8	20 min	Coût moyen	8
521	Bricks à la dinde et au riz, parfumés à la mangue et au basilic	1 h	facile	https://assets.afcdn.com/recipe/20100120/56134_w420h344c1cx256cy192.jpg	30 min	4.5	30 min	assez cher	2
522	Bricks à la pomme de terre, viande hachée, olives vertes et kiri	1h25	Niveau moyen		40 min	5	45 min	bon marché	4
523	Bricks à la tomate thon et courgette	1h05	très facile		5 min	4.3	1 h	bon marché	4
524	Bricks à la viande épicée	40 min	très facile		10 min	4.9	30 min	bon marché	3
525	Bricks à la viande	25 min	très facile	https://assets.afcdn.com/recipe/20150108/18378_w420h344c1cx960cy1280.jpg	10 min	4.3	15 min	bon marché	5
526	Bricks au poulet et à la ricotta	35 min	Niveau moyen	https://assets.afcdn.com/recipe/20130503/55030_w420h344c1cx752cy500.jpg	20 min	4.2	15 min	bon marché	4
527	Bricks au poulet et aux maroilles	40 min	très facile		10 min	5	30 min	bon marché	4
528	Bricks aux deux fromages sur un lit de salade	20 min	facile	https://assets.afcdn.com/recipe/20181201/84478_w420h344c1cx1024cy768cxt0cyt0cxb2048cyb1536.jpg	5 min	5	15 min	bon marché	5
529	Bricks aux épinards, feta et coriandre	35 min	très facile	https://assets.afcdn.com/recipe/20130922/66600_w420h344c1cx1296cy968.jpg	15 min	4.6	20 min	bon marché	4
530	bricks aux légumes faciles	35 min	facile		15 min	4	20 min	bon marché	4
531	Bricks aux légumes	20 min	très facile	https://assets.afcdn.com/recipe/20190212/87644_w420h344c1cx3775cy1725cxt0cyt0cxb5608cyb3741.jpg	5 min	4.1	15 min	bon marché	2
532	Bricks aux poireaux	1h30	facile	https://assets.afcdn.com/recipe/20160727/38585_w420h344c1cx1632cy918.jpg	30 min	3.7	1 h	bon marché	4
533	Bricks aux pommes de terre, carottes et aux petits pois	1 h	facile	https://assets.afcdn.com/recipe/20190417/90986_w420h344c1cx1745cy2390cxt0cyt0cxb56527cyb37727.jpg	35 min	5	25 min	bon marché	4
535	Bricks de légumes aux lardons d'Elvire	45 min	facile	https://assets.afcdn.com/recipe/20100120/9831_w420h344c1cx256cy192.jpg	15 min	4.7	30 min	bon marché	4
536	Bricks de saumon aux asperges	35 min	très facile		25 min		10 min	bon marché	4
537	Brie de Meaux ou Coulommiers à la truffe maison	15 min	facile	https://assets.afcdn.com/recipe/20150116/13592_w420h344c1cx1824cy1368.jpg	0		15 min	Coût moyen	4
538	Brioche aux asperges	45 min	très facile		20 min	4.3	25 min	bon marché	4
539	Brioche moelleuse aux pépites de chocolat	4h30	difficile	https://assets.afcdn.com/recipe/20190704/94726_w420h344c1cx351cy530cxt0cyt0cxb702cyb1060.jpg	20 min		40 min	bon marché	6
540	Brioche tressée au SEL DES ALPES	3h30	facile	https://assets.afcdn.com/recipe/20190909/98007_w420h344c1cx2840cy1893cxt0cyt0cxb5680cyb3786.jpg	40 min		20 min	bon marché	6
541	Briochettes au jambon et à la crème	30 min	facile		15 min	5	15 min	bon marché	4
542	Briquette aux lardons et chevre	45 min	facile		30 min	4	15 min	bon marché	4
543	Brochet à la romande	1h30	Niveau moyen		1 h	4.8	30 min	Coût moyen	6
544	Brochet à la sauce au vin blanc et aux champignons	35 min	facile	https://assets.afcdn.com/recipe/20180218/77607_w420h344c1cx1632cy918cxt0cyt0cxb3264cyb1836.jpg	20 min	4.5	15 min	Coût moyen	6
545	Brochet farci à la Périgourdine	1 h	Niveau moyen	https://assets.afcdn.com/recipe/20141206/64107_w420h344c1cx1725cy2066.jpg	45 min		15 min	assez cher	8
546	Brrochettes d'espadon aux tomates confites	40 min	très facile		25 min		15 min	Coût moyen	4
547	brochette de canard aux fruits	40 min	très facile		10 min	4	30 min	Coût moyen	2
548	Brochette de canard  mariné à l'orange	30 min	très facile		10 min	5	20 min	Coût moyen	4
549	Brochette de crevettes à la mangue	2h25	Niveau moyen		25 min		2 h	Coût moyen	4
550	Brochette de crevettes grillées aux herbes	25 min	facile	https://assets.afcdn.com/recipe/20170613/69375_w420h344c1cx768cy1280.jpg	5 min	5	20 min	Coût moyen	4
551	Brochette de lotte aux épices	20 min	très facile		10 min		10 min	Coût moyen	4
552	Brochette de poisson mariné aux arachides	2h38	très facile		8 min	5	2h30	bon marché	5
553	Brochette de poitrine de poulet à l'érable marinée	1h15	très facile		45 min	4.8	30 min	Coût moyen	4
554	Brochette de porc aux poivrons	1h15	très facile	https://assets.afcdn.com/recipe/20170627/69854_w420h344c1cx1456cy2184.jpg	15 min	4	1 h	bon marché	4
555	Brochette de Saint-Jacques au lard, papillote de légumes à la crème	40 min	très facile	https://assets.afcdn.com/recipe/20140215/17033_w420h344c1cx1086cy927.jpg	10 min	4.8	30 min	Coût moyen	2
556	Brochette de veau à la marocaine	30 min	facile		15 min		15 min	bon marché	5
557	Brochette de viande fondue à l'edam	40 min	facile	https://assets.afcdn.com/recipe/20130322/28663_w420h344c1cx256cy192.jpg	15 min	4	25 min	Coût moyen	2
558	Brochette filet mignon à la moutarde	1h20	très facile		50 min	4	30 min	bon marché	4
559	Brochettes aux quenelles et ses légumes	40 min	facile		20 min		20 min	bon marché	2
560	Brochettes coeur et magret de canard à la marinade sucrée/salée	35 min	très facile		15 min	4.8	20 min	bon marché	8
561	Brochettes d'agneau à la menthe	20 min	très facile	https://assets.afcdn.com/recipe/20140814/48126_w420h344c1cx900cy1350.jpg	10 min	5	10 min	Coût moyen	4
562	Brochettes d'agneau à la pékinoise	15 min	très facile		5 min		10 min	bon marché	4
563	Brochettes d'agneau aux figues et abricots	30 min	facile	https://assets.afcdn.com/recipe/20190630/94421_w420h344c1cx2016cy980cxt0cyt0cxb4032cyb1960.jpg	10 min		20 min	bon marché	4
564	Brochettes d'agneau aux légumes	2h30	très facile	https://assets.afcdn.com/recipe/20150613/33401_w420h344c1cx1548cy1019.jpg	1 h	5	1h30	bon marché	4
565	Brochettes d'agneau marinées aux herbes fraîches	50 min	très facile		20 min	5	30 min	Coût moyen	4
566	Brochettes d'aiguillettes de canard aux pommes et pruneaux	35 min	très facile		20 min		15 min	Coût moyen	4
567	Brochettes d'iguane à la cubaine	2h24	Niveau moyen		10 min		2h14	Coût moyen	4
568	Brochettes de boeuf à la thaïlandaise	25 min	facile	https://assets.afcdn.com/recipe/20170623/69509_w420h344c1cx240cy323.jpg	5 min	4.8	20 min	Coût moyen	4
569	Brochettes de bœuf aux oignons japonaises	25 min	très facile		5 min		20 min	Coût moyen	4
570	Brochettes de boeuf haché à l'ananas	1 j 20 min	très facile	https://assets.afcdn.com/recipe/20170627/69896_w420h344c1cx1500cy1125.jpg	20 min	4.3	1 j	bon marché	4
571	Brochettes de boeuf mariné aux marrons et aux poivrons	39 min	facile		4 min	4.3	35 min	Coût moyen	4
572	Brochettes de boudin paysan aux pommes	1 h	très facile	https://assets.afcdn.com/recipe/20150827/50520_w420h344c1cx1296cy728.jpg	30 min		30 min	bon marché	4
573	Brochettes de canard aux abricots	30 min	très facile		0	4.4	30 min	Coût moyen	4
574	Brochettes de canard aux pêches	25 min	très facile	https://assets.afcdn.com/recipe/20120502/61480_w420h344c1cx1162cy1750.jpg	15 min	4.6	10 min	Coût moyen	4
575	Brochettes de crevettes à la romaine	35 min	facile		20 min	5	15 min	Coût moyen	4
576	Brochettes de crevettes aux épices	23 min	facile		8 min		15 min	bon marché	8
577	Brochettes de crevettes marinées à l'ail	1 h	facile	https://assets.afcdn.com/recipe/20160509/3488_w420h344c1cx1724cy2587.jpg	30 min	4.8	30 min	bon marché	4
578	Brochettes de dinde et poêlée de girolles à la crème	1h05	facile	https://assets.afcdn.com/recipe/20120502/580_w420h344c1cx1162cy1750.jpg	35 min	4.5	30 min	Coût moyen	6
579	Brochettes de filet mignon à la japonaise	30 min	facile	https://assets.afcdn.com/recipe/20150118/25905_w420h344c1cx1296cy864.jpg	15 min		15 min	Coût moyen	6
580	Brochettes de foie d'agneau aux courgettes et aux aubergines	55 min	facile	https://assets.afcdn.com/recipe/20100120/45058_w420h344c1cx256cy192.jpg	5 min	5	50 min	bon marché	6
581	Brochettes de gambas à la mode ajacienne	1h05	facile		20 min		45 min	Coût moyen	2
582	Brochettes de gambas marinées et riz à la mangue	1h15	facile	https://assets.afcdn.com/recipe/20140814/64106_w420h344c1cx900cy1350.jpg	45 min	4.9	30 min	assez cher	2
583	Brochettes de lapin à la tapenade	35 min	très facile		30 min	5	5 min	bon marché	4
584	Brochettes de poisson aux épices	20 min	facile		5 min	4.5	15 min	Coût moyen	8
585	Brochettes de poitrine fumée à la mozzarella	20 min	très facile		10 min	3	10 min	bon marché	4
838	Canard aux pruneaux et à la cannelle	30 min	très facile		15 min	5	15 min	Coût moyen	2
586	Brochettes de porc à l'ananas - économique et facile  !	1h15	très facile	https://assets.afcdn.com/recipe/20170627/69826_w420h344c1cx3000cy2000.jpg	15 min	4.6	1 h	bon marché	6
587	Brochettes de porc à la banane	30 min	très facile	https://assets.afcdn.com/recipe/20130502/52176_w420h344c1cx192cy256.jpg	15 min	4.8	15 min	bon marché	4
588	Brochettes de porc à la moutarde	30 min	facile		15 min	3.5	15 min	bon marché	4
589	Brochettes de porc marinées aux pommes	45 min	facile		20 min		25 min	bon marché	6
590	Brochettes de poulet à l'ananas et aux poivrons rouges	25 min	Niveau moyen		10 min	4	15 min	Coût moyen	4
591	Brochettes de poulet à l'ananas	40 min	très facile	https://assets.afcdn.com/recipe/20170531/51368_w420h344c1cx1872cy2808.jpg	10 min	4.6	30 min	bon marché	6
592	Brochettes de poulet à la marinade	35 min	facile	https://assets.afcdn.com/recipe/20170627/69905_w420h344c1cx1944cy1296.jpg	20 min	4	15 min	bon marché	4
593	Brochettes de poulet à la menthe	45 min	très facile	https://assets.afcdn.com/recipe/20160926/27383_w420h344c1cx2527cy1685.jpg	15 min	4.8	30 min	bon marché	6
594	Brochettes de poulet à la sauce aux fruits pimentée	20 min	très facile		10 min	4.6	10 min	Coût moyen	4
595	Brochettes de poulet au curry et aux trois délices	55 min	très facile	https://assets.afcdn.com/recipe/20140814/63630_w420h344c1cx900cy1350.jpg	10 min	4.7	45 min	bon marché	2
596	Brochettes de poulet marinées au miel et à la moutarde à l'ancienne	30 min	facile	https://assets.afcdn.com/recipe/20130807/19244_w420h344c1cx1296cy968.jpg	15 min	4.7	15 min	bon marché	4
597	Brochettes de poulet aux 3 poivrons et au thym	40 min	facile		20 min	4	20 min	bon marché	4
598	Brochettes de poulet aux cacahuètes	25 min	très facile	https://assets.afcdn.com/recipe/20120509/17560_w420h344c1cx1162cy1750.jpg	15 min	4.1	10 min	bon marché	4
599	brochettes de poulet aux graines de sésame	25 min	très facile	https://assets.afcdn.com/recipe/20160118/53631_w420h344c1cx2064cy1161.jpg	10 min	4.7	15 min	bon marché	4
600	brochettes de poulet aux noisettes	44 min	facile	https://assets.afcdn.com/recipe/20150329/42371_w420h344c1cx1224cy1632.jpg	9 min	4.3	35 min	bon marché	4
601	Brochettes de poulet caramélisées aux agrumes	25 min	facile	https://assets.afcdn.com/recipe/20150426/7285_w420h344c1cx1632cy1224.jpg	10 min	3.5	15 min	bon marché	4
602	Brochettes de poulet mariné à la menthe	25 min	très facile	https://assets.afcdn.com/recipe/20150708/53005_w420h344c1cx960cy1280.jpg	15 min	4	10 min	bon marché	6
603	Brochettes de poulet mariné au citron et aux herbes	30 min	très facile	https://assets.afcdn.com/recipe/20170404/1076_w420h344c1cx1569cy1107.jpg	15 min	3.8	15 min	bon marché	4
604	Brochettes de poulet mariné épicé à la coriandre	35 min	facile		15 min	3.5	20 min	bon marché	6
605	Brochettes de poulet piquant à l'orange	40 min	très facile	https://assets.afcdn.com/recipe/20170627/69796_w420h344c1cx2781cy1854.jpg	10 min	4	30 min	bon marché	5
606	Brochettes de poulet mariné aux herbes et aux légumes	45 min	facile	https://assets.afcdn.com/recipe/20141012/2563_w420h344c1cx1280cy960.jpg	15 min		30 min	bon marché	4
607	Brochettes de quenelles au lard et aux pruneaux	35 min	très facile	https://assets.afcdn.com/recipe/20140814/16514_w420h344c1cx900cy1350.jpg	20 min	4.7	15 min	bon marché	4
608	Brochettes de Saint-Jacques à l'ail	20 min	facile		5 min	4.4	15 min	Coût moyen	4
609	Brochettes de saumon au lard et aux poivrons	30 min	Niveau moyen		0	5	30 min	bon marché	8
610	Brochettes de sot l'y laisse à la normande	40 min	très facile		30 min	4	10 min	Coût moyen	2
611	Brochettes de steak végétal	30 min	très facile	https://assets.afcdn.com/recipe/20190705/94828_w420h344c1cx2448cy1632cxt0cyt0cxb4896cyb3264.jpg	10 min		20 min	bon marché	2
612	Brochettes de thon aux poivrons	25 min	facile		15 min		10 min	assez cher	4
613	Brochettes de veau à l'indienne	50 min	très facile		30 min	4	20 min	bon marché	4
614	Brochettes de veau à l'orientale	17 min	très facile	https://assets.afcdn.com/recipe/20120502/38844_w420h344c1cx1181cy1771.jpg	7 min	4.5	10 min	bon marché	4
615	Brochettes de volaille à la marocaine	20 min	très facile		10 min	4.6	10 min	bon marché	8
616	Brochettes grillées aux épices	35 min	très facile		15 min	3.3	20 min	bon marché	4
617	Brochettes végétariennes au curry	20 min	très facile	https://assets.afcdn.com/recipe/20190705/94829_w420h344c1cx2448cy1632cxt0cyt0cxb4896cyb3264.jpg	10 min		10 min	bon marché	2
618	Brocoli aux lardons	30 min	facile	https://assets.afcdn.com/recipe/20130326/21634_w420h344c1cx256cy192.jpg	20 min	4.7	10 min	bon marché	4
619	Brocolis aux saucisses fumées	25 min	très facile	https://assets.afcdn.com/recipe/20161212/57332_w420h344c1cx360cy560.jpg	15 min	5	10 min	bon marché	3
620	Brocolis aux saucisses	1h05	très facile		50 min	3.8	15 min	bon marché	4
621	Brocolis d'amoureux à la crème de Boursin aux champignons	15 min	très facile		10 min	4.6	5 min	bon marché	2
622	Brouillade à la truffe noire	20 min	très facile		5 min		15 min	Coût moyen	6
623	Brouillade d'oeufs aux deux poivrons	45 min	très facile		25 min	5	20 min	bon marché	2
624	Brownie vegan au chocolat et patate douce	1 h	facile	https://assets.afcdn.com/recipe/20191121/102752_w420h344c1cx1920cy2880cxt0cyt0cxb3840cyb5760.jpg	50 min		10 min	bon marché	4
625	Bruschetta aux aubergines	40 min	très facile	https://assets.afcdn.com/recipe/20120502/5277_w420h344c1cx1162cy1750.jpg	10 min	4.4	30 min	bon marché	2
626	Bruschetta fromage frais potiron rôti	30 min	très facile	https://assets.afcdn.com/recipe/20191015/100679_w420h344c1cx2808cy1872cxt0cyt0cxb5616cyb3743.jpg	20 min		10 min	bon marché	4
627	Bucatini aux poireaux sauce jambon-béchamel	45 min	facile	https://assets.afcdn.com/recipe/20190302/88617_w420h344c1cx1280cy720cxt0cyt0cxb2560cyb1440.jpg	20 min		25 min	bon marché	4
628	Bûche crème de marron et Rhum Ambré LA MARTINIQUAISE	58 min	facile	https://assets.afcdn.com/recipe/20171205/75845_w420h344c1cx2000cy3000cxt0cyt0cxb4000cyb6000.jpg	27 min		31 min	bon marché	6
629	Bûche de Noël au citron coco meringuée	4 h	facile	https://assets.afcdn.com/recipe/20191121/102754_w420h344c1cx1920cy2880cxt0cyt0cxb3840cyb5760.jpg	15 min		45 min	bon marché	4
630	Bûche de Noël au Nutella®	1h08	Niveau moyen	https://assets.afcdn.com/recipe/20171009/72893_w420h344c1cx1000cy666cxt0cyt0cxb2000cyb1333.jpg	8 min	4.6	1 h	bon marché	15
631	Bûche de Noël Pomme Vanille et Nutella®	1h05	facile	https://assets.afcdn.com/recipe/20181116/83903_w420h344c1cx4344cy2896cxt0cyt0cxb8688cyb5792.jpg	25 min	5	40 min	bon marché	8
632	Bûche façon tatin au Calvados BUSNEL	2h31	facile	https://assets.afcdn.com/recipe/20171205/75846_w420h344c1cx222cy148cxt0cyt0cxb445cyb297.jpg	43 min	4	48 min	bon marché	6
633	Bûche revisitée façon cheesecake	1h30	facile	https://assets.afcdn.com/recipe/20191015/100684_w420h344c1cx1920cy2880cxt0cyt0cxb3840cyb5760.jpg	20 min		1h10	bon marché	6
634	Bûche roulée au chocolat	40 min	facile	https://assets.afcdn.com/recipe/20191129/103142_w420h344c1cx2951cy1967cxt0cyt0cxb5902cyb3935.jpg	15 min		25 min	bon marché	8
635	Bûche roulée mangue citron vert au Nutella®	50 min	facile	https://assets.afcdn.com/recipe/20191106/102014_w420h344c1cx960cy540cxt0cyt0cxb1920cyb1080.png	10 min		40 min	bon marché	6
636	Buddah Bowl végétarien	35 min	très facile	https://assets.afcdn.com/recipe/20190708/94908_w420h344c1cx2448cy1632cxt0cyt0cxb4896cyb3264.jpg	10 min		25 min	bon marché	2
637	Bulots à la citronnelle et au gingembre (Vietnam)	1h20	facile	https://assets.afcdn.com/recipe/20161018/46632_w420h344c1cx2808cy1872.jpg	1 h	5	20 min	bon marché	6
638	Burger à la blanquette de veau	1h45	facile	https://assets.afcdn.com/recipe/20130913/66674_w420h344c1cx320cy320.jpg	1h30	5	15 min	Coût moyen	4
639	Burger à la française au roquefort	25 min	très facile	https://assets.afcdn.com/recipe/20160713/16166_w420h344c1cx2000cy3000.jpg	5 min	4.7	20 min	bon marché	2
640	Burger aux galettes de pommes de terre	25 min	très facile	https://assets.afcdn.com/recipe/20160218/8248_w420h344c1cx861cy641.jpg	10 min	4.8	15 min	bon marché	2
641	Burger breton aux galettes de sarrasin	35 min	facile	https://assets.afcdn.com/recipe/20160914/22035_w420h344c1cx2000cy3000.jpg	10 min	3.3	25 min	Coût moyen	4
642	Burger charolais à la poire	20 min	facile		10 min		10 min	bon marché	4
643	Burger egg (Hamburger à l'oeuf)	35 min	facile		5 min	5	30 min	bon marché	6
644	Burger perdu au maroilles et à la bière brune	1 h	facile	https://assets.afcdn.com/recipe/20141117/60200_w420h344c1cx816cy458.jpg	30 min		30 min	bon marché	2
645	Burger Provençal Maison à la Pulpe d'Olive	45 min	facile		25 min	5	20 min	Coût moyen	2
646	Burger savoureux aux pois chiches et avocats	20 min	facile	https://assets.afcdn.com/recipe/20160423/22725_w420h344c1cx1224cy1461.jpg	10 min	5	10 min	bon marché	4
647	Burger vegan aux haricots rouges	27 min	très facile	https://assets.afcdn.com/recipe/20170219/44233_w420h344c1cx1494cy1494.jpg	7 min	3.4	20 min	bon marché	4
648	Burger végétarien aux lentilles	55 min	très facile	https://assets.afcdn.com/recipe/20170320/13294_w420h344c1cx2144cy1424.jpg	30 min	5	25 min	bon marché	4
649	Burger végétarien	25 min	très facile	https://assets.afcdn.com/recipe/20190708/94911_w420h344c1cx2287cy1525cxt0cyt0cxb4575cyb3050.jpg	10 min		15 min	bon marché	2
650	Burrito maïs, poivron, haricot noir, quinoa, persil	35 min	facile	https://assets.afcdn.com/recipe/20191015/100666_w420h344c1cx2464cy1632cxt0cyt0cxb4928cyb3264.jpg	20 min		15 min	bon marché	2
651	Burritos végétariens	40 min	très facile	https://assets.afcdn.com/recipe/20190708/94912_w420h344c1cx2851cy1833cxt0cyt0cxb5702cyb3667.jpg	20 min		20 min	bon marché	4
652	Busecca soupe aux tripes	1h15	très facile		45 min	4	30 min	bon marché	4
653	Butternut au comté et à la quenelle	1 h	facile		45 min	4	15 min	bon marché	4
654	Butternut aux lardons micro-ondes	25 min	facile		15 min		10 min	bon marché	2
655	Butternut farci aux cèpes et aux châtaignes	1h45	facile	https://assets.afcdn.com/recipe/20121104/50261_w420h344c1cx256cy192.jpg	30 min	3.8	1h15	Coût moyen	4
656	Cabillaud à l'oignon vert	45 min	facile		25 min	3	20 min	Coût moyen	2
657	Cabillaud à la banane en petit curry	40 min	facile		20 min	5	20 min	bon marché	4
658	Cabillaud à la bretonne	40 min	facile		10 min	4.7	30 min	Coût moyen	4
659	Cabillaud à la bulgare	25 min	très facile	https://assets.afcdn.com/recipe/20100120/1232_w420h344c1cx256cy192.jpg	20 min	4.3	5 min	bon marché	2
660	Cabillaud à la crème de ciboulette	15 min	très facile	https://assets.afcdn.com/recipe/20161106/57231_w420h344c1cx1026cy766.jpg	10 min	4	5 min	bon marché	2
661	Cabillaud à la figue et au chèvre	20 min	facile		15 min	5	5 min	Coût moyen	1
662	Cabillaud à la sauce au curry	50 min	facile	https://assets.afcdn.com/recipe/20160110/38558_w420h344c1cx716cy839.jpg	30 min	4.7	20 min	Coût moyen	4
663	Cabillaud à la sauce basilic	45 min	très facile	https://assets.afcdn.com/recipe/20131118/46625_w420h344c1cx1250cy1875.jpg	15 min	4.7	30 min	bon marché	4
664	Cabillaud à la sauce coco	40 min	Niveau moyen		15 min	5	25 min	bon marché	4
665	Cabillaud à la sauce moutardée	30 min	très facile		20 min	4.3	10 min	bon marché	4
666	Cabillaud à la tomate et aux champignons	15 min	facile	https://assets.afcdn.com/recipe/20140506/18485_w420h344c1cx718cy534.jpg	10 min	3	5 min	bon marché	4
667	Cabillaud au beurre d'orange et pommes de terre au four en 30 minutes	30 min	Niveau moyen	https://assets.afcdn.com/recipe/20190828/96897_w420h344c1cx810cy540cxt0cyt0cxb1620cyb1080.jpg	8 min	3.3	22 min	assez cher	4
668	Cabillaud au Chablis et aux champignons	45 min	facile		25 min		20 min	Coût moyen	4
669	Cabillaud au four à la provençale	1 h	facile	https://assets.afcdn.com/recipe/20190204/87191_w420h344c1cx3844cy2322cxt0cyt0cxb7077cyb4722.jpg	45 min	4.7	15 min	bon marché	6
670	Cabillaud aux abricots	1 h	très facile		45 min	3.8	15 min	Coût moyen	4
671	Cabillaud aux cinq saveurs	25 min	très facile		10 min		15 min	bon marché	4
672	Cabillaud aux épinards béchamel	40 min	facile		30 min		10 min	bon marché	4
673	cabillaud aux framboises	50 min	facile		30 min	3	20 min	Coût moyen	4
674	Cabillaud aux gambas avec tomates concassées et coriandre	40 min	facile		20 min		20 min	Coût moyen	4
675	Cabillaud aux noix	1h20	facile		50 min		30 min	Coût moyen	6
676	Cabillaud aux olives et au vin blanc	40 min	très facile	https://assets.afcdn.com/recipe/20161010/49710_w420h344c1cx2736cy1824.jpg	30 min	4.3	10 min	bon marché	4
677	Cabillaud aux parfums boisés	2 j 2 h	Niveau moyen		1 j 6 h	4	20 h	Coût moyen	4
678	Cabillaud aux poivrons et échalotes	40 min	très facile	https://assets.afcdn.com/recipe/20141130/44244_w420h344c1cx1632cy1224.jpg	20 min	4.6	20 min	Coût moyen	4
679	Cabillaud aux tomates et au pistou et purée à l'huile d'olive	35 min	très facile		20 min	4.9	15 min	bon marché	4
680	Cabillaud aux tomates (et fromage)	20 min	très facile	https://assets.afcdn.com/recipe/20120914/56814_w420h344c1cx256cy192.jpg	10 min	4.3	10 min	bon marché	2
681	Cabillaud au chorizo et aux pommes de terre poêlées	40 min	facile	https://assets.afcdn.com/recipe/20170417/67324_w420h344c1cx1500cy1125.jpg	20 min	5	20 min	bon marché	4
682	Cabillaud coco à la samana (République Dominicaine)	40 min	facile	https://assets.afcdn.com/recipe/20100120/40880_w420h344c1cx256cy192.jpg	20 min	4.8	20 min	Coût moyen	4
683	Cabillaud et sa sauce aux moules	50 min	facile		10 min	4.3	40 min	Coût moyen	4
684	Cabillaud gratiné à la crème et à la raclette	55 min	facile		35 min	4.2	20 min	Coût moyen	6
685	Cabillaud gratiné aux oignons	35 min	facile		15 min	4	20 min	Coût moyen	6
686	Cabillaud (morue) en papillote à l'italienne	25 min	facile		15 min	5	10 min	Coût moyen	4
687	Cabillaud (ou skrei) à l'orange	40 min	très facile	https://assets.afcdn.com/recipe/20191017/100789_w420h344c1cx1024cy694cxt0cyt0cxb2048cyb1389.jpg	30 min		10 min	Coût moyen	4
688	Cabillaud pané à la bière	15 min	très facile		10 min	4.6	5 min	bon marché	4
689	Cabillaud sauce roquefort aux 3 légumes	1h10	Niveau moyen		40 min	5	30 min	Coût moyen	4
690	Cacasse à cul nu de ma grand-mère	2 h	très facile		1 h	5	1 h	bon marché	2
691	Café viennois au Lait Concentré Sucré Sans Lactose	5 min	très facile	https://assets.afcdn.com/recipe/20190829/96918_w420h344c1cx2000cy2867cxt0cyt0cxb4000cyb5734.jpg	0	5	5 min	bon marché	6
692	Caille à l'orange et aux épices	40 min	Niveau moyen	https://assets.afcdn.com/recipe/20160831/37830_w420h344c1cx1494cy2656.jpg	30 min	5	10 min	Coût moyen	4
693	Caille en papillote aux pêches	1 h	facile		30 min	4.3	30 min	Coût moyen	4
694	Cailles à l'escabèche	15 min	facile	https://assets.afcdn.com/recipe/20160320/36341_w420h344c1cx1500cy1125.jpg	10 min	4.7	5 min	bon marché	4
695	Cailles à l’hydromel façon Moyen Age	1h15	Niveau moyen		15 min		1 h	Coût moyen	4
696	Cailles à la cocotte aux échalotes caramélisées	35 min	facile		25 min	4.4	10 min	Coût moyen	2
697	cailles à la façon de papy	1h15	facile		30 min	5	45 min	Coût moyen	4
698	Cailles à la moutarde et au miel	1h10	très facile	https://assets.afcdn.com/recipe/20141105/12074_w420h344c1cx1250cy1292.jpg	1 h	4.8	10 min	Coût moyen	4
699	Cailles à la paysanne faciles	45 min	facile	https://assets.afcdn.com/recipe/20151007/22282_w420h344c1cx1280cy960.jpg	30 min	4.5	15 min	bon marché	4
700	Cailles à la provençale	1h30	très facile		1 h	2	30 min	bon marché	4
701	Cailles à la sauge farcies au brocciu (fromage corse)	1 h	Niveau moyen		40 min		20 min	Coût moyen	4
702	Cailles au four et riz pilaf aux lardons	1 h	très facile	https://assets.afcdn.com/recipe/20171106/74448_w420h344c1cx2016cy1512cxt0cyt0cxb4032cyb3024.jpg	30 min	4.4	30 min	bon marché	4
703	Cailles au miel et aux mandarines	55 min	facile		45 min		10 min	Coût moyen	4
704	Cailles aux abricots et aux amandes	30 min	très facile	https://assets.afcdn.com/recipe/20180501/78965_w420h344c1cx1335cy916cxt0cyt0cxb2670cyb1833.jpg	10 min	4.5	20 min	Coût moyen	4
705	Cailles aux baies de genièvre	57 min	très facile	https://assets.afcdn.com/recipe/20181120/83970_w420h344c1cx3691cy2000cxt0cyt0cxb5466cyb3641.jpg	45 min	4.7	12 min	bon marché	4
706	cailles aux champignons et aux olives de Nice	1 h	Niveau moyen		30 min	4	30 min	Coût moyen	6
707	Cailles aux courgettes	40 min	facile	https://assets.afcdn.com/recipe/20150405/4314_w420h344c1cx1280cy768.jpg	25 min	4.3	15 min	bon marché	2
708	Cailles aux figues (sauce chocolatée)	50 min	Niveau moyen	https://assets.afcdn.com/recipe/20160922/15838_w420h344c1cx2000cy3000.jpg	20 min	3.6	30 min	Coût moyen	4
709	Cailles aux fruits secs	30 min	facile		20 min	4	10 min	Coût moyen	4
710	Cailles aux lardons et porto	45 min	très facile		30 min	4.3	15 min	bon marché	4
711	Cailles aux mandarines corses	35 min	très facile	https://assets.afcdn.com/recipe/20191024/101101_w420h344c1cx3000cy2000cxt0cyt0cxb6000cyb4000.jpg	30 min	4.7	5 min	bon marché	2
712	Cailles aux marrons	45 min	très facile	https://assets.afcdn.com/recipe/20131118/19214_w420h344c1cx1250cy1875.jpg	30 min	4.6	15 min	bon marché	6
713	Cailles aux noix	40 min	facile		20 min		20 min	Coût moyen	4
714	cailles aux petits oignons et champignons	30 min	facile	https://assets.afcdn.com/recipe/20100120/26540_w420h344c1cx256cy192.jpg	10 min	4.3	20 min	Coût moyen	4
715	Cailles aux pleurotes et foie gras	1h15	très facile		1 h	5	15 min	assez cher	4
716	Cailles aux poivrons	1h15	facile		1 h	4	15 min	bon marché	6
717	Cailles aux pruneaux et au foie gras	1 h	facile	https://assets.afcdn.com/recipe/20191106/102016_w420h344c1cx1400cy2685cxt0cyt0cxb3338cyb5000.jpg	30 min	4.8	30 min	Coût moyen	4
718	Cailles aux raisins faciles	30 min	très facile	https://assets.afcdn.com/recipe/20100120/21874_w420h344c1cx256cy192.jpg	25 min	4.7	5 min	Coût moyen	2
719	Cailles aux raisins	30 min	très facile	https://assets.afcdn.com/recipe/20100120/21895_w420h344c1cx256cy192.jpg	10 min	4.8	20 min	bon marché	4
720	Cailles aux saveurs d'automne	1h15	facile		30 min	5	45 min	Coût moyen	4
721	Cailles braisées au miel et aux raisins	45 min	facile	https://assets.afcdn.com/recipe/20130404/52330_w420h344c1cx256cy192.jpg	25 min	5	20 min	bon marché	4
722	Cailles farcies à l'auvergnate et petits légumes	1h25	Niveau moyen		1 h	5	25 min	Coût moyen	4
723	Cailles farcies à la coriandre sur lit de fruits de la passion	2 h	difficile		30 min		1h30	Coût moyen	2
724	Cailles farcies au foie gras et aux raisins sauternés	35 min	facile		15 min	5	20 min	Coût moyen	4
725	Cailles farcies au foie gras, sauce aux raisins et sauternes	40 min	Niveau moyen		20 min		20 min	assez cher	4
726	Cailles farcies aux cèpes	50 min	facile	https://assets.afcdn.com/recipe/20150206/44676_w420h344c1cx1296cy972.jpg	30 min	5	20 min	Coût moyen	6
727	Cailles farcies aux marrons	1h30	facile		1 h		30 min	Coût moyen	4
728	Cailles farcies aux poires et foie gras	45 min	facile		20 min		25 min	assez cher	2
729	Cailles grillées à la sauge	50 min	Niveau moyen	https://assets.afcdn.com/recipe/20100120/55947_w420h344c1cx256cy192.jpg	20 min	4.6	30 min	assez cher	4
730	Cailles marinées farcies à la vietnamienne	1h35	Niveau moyen		1h05	5	30 min	Coût moyen	6
731	Cailles panées aux artichauts	45 min	facile		30 min	2	15 min	Coût moyen	4
732	Cailles rôties à l'ananas flambées au rhum	1h45	facile		1h15	3	30 min	Coût moyen	4
733	Cailles rôties aux échalotes	30 min	très facile	https://assets.afcdn.com/recipe/20101021/2590_w420h344c1cx256cy192.jpg	20 min	4.7	10 min	bon marché	6
734	Caillette aux blettes	4 h	facile	https://assets.afcdn.com/recipe/20100120/11962_w420h344c1cx256cy192.jpg	2 h	5	2 h	bon marché	20
735	Caillettes ardéchoises aux herbes	1h05	facile		45 min		20 min	bon marché	10
736	Caillettes aux herbes	40 min	Niveau moyen	https://assets.afcdn.com/recipe/20100120/50440_w420h344c1cx256cy192.jpg	20 min	4.5	20 min	bon marché	12
737	Cake à l'indienne	1 h	facile	https://assets.afcdn.com/recipe/20150110/54719_w420h344c1cx1632cy1224.jpg	45 min	4.2	15 min	bon marché	8
738	Cake à la Bretonne au Sarrasin	55 min	facile	https://assets.afcdn.com/recipe/20150227/6509_w420h344c1cx1632cy1224.jpg	45 min	4.7	10 min	bon marché	6
739	Cake à la farine de chataîgne et aux tomates séchées	1 h	facile		30 min	4	30 min	Coût moyen	4
740	Cake à la julienne	1h15	très facile	https://assets.afcdn.com/recipe/20151007/42881_w420h344c1cx960cy1280.jpg	45 min	3.8	30 min	bon marché	6
741	Cake à la tomate et au carré frais	55 min	très facile	https://assets.afcdn.com/recipe/20100120/8568_w420h344c1cx256cy192.jpg	45 min	4	10 min	bon marché	4
742	Cake à la truite fumée et au chèvre	1 h	facile		45 min	4.9	15 min	bon marché	8
743	Cake à la viande hachée	1h05	très facile		45 min	3.2	20 min	bon marché	4
744	Cake au bleu et aux raisins	50 min	très facile		35 min	4.1	15 min	Coût moyen	5
745	Cake au canard au parmesan et à l'abricot	1 h	facile	https://assets.afcdn.com/recipe/20120901/51338_w420h344c1cx192cy256.jpg	45 min	5	15 min	Coût moyen	6
746	Cake au chèvre et à la tomate confite	1 h	très facile	https://assets.afcdn.com/recipe/20130718/25088_w420h344c1cx996cy1504.jpg	40 min	4.7	20 min	bon marché	6
747	Cake au jambon et aux courgette	1h15	facile	https://assets.afcdn.com/recipe/20190706/94846_w420h344c1cx1112cy834cxt0cyt0cxb2224cyb1668.jpg	50 min	5	25 min	bon marché	4
748	Cake au jambon et aux saucisses de strasbourg	55 min	facile	https://assets.afcdn.com/recipe/20101227/32308_w420h344c1cx256cy192.jpg	45 min	4.1	10 min	bon marché	6
749	Cake au thon à la catalane	1 h	très facile		45 min	5	15 min	bon marché	6
750	cake aux 2 saumons	1 h	facile	https://assets.afcdn.com/recipe/20140509/19362_w420h344c1cx1632cy1224.jpg	40 min	4.5	20 min	Coût moyen	4
751	Cake aux 3 poivrons et au saumon fumé	1h20	facile	https://assets.afcdn.com/recipe/20100120/21425_w420h344c1cx256cy192.jpg	1 h	4.7	20 min	Coût moyen	6
752	cake aux carottes et au lard	1h20	très facile		1 h	3.7	20 min	bon marché	6
753	Cake aux coquillettes et au fromage	1h05	facile		45 min	3.2	20 min	bon marché	6
754	Cake aux courgettes et poivron	1h05	très facile		45 min	4.4	20 min	bon marché	8
755	Cake aux courgettes, patates douces et garam massala	1h30	facile		45 min	3	45 min	Coût moyen	6
756	Cake aux deux fromages et au poulet	1h15	très facile		1 h		15 min	bon marché	6
757	Cake aux foies de volaille	55 min	facile		45 min	4.8	10 min	bon marché	4
758	Cake aux fruits à la map (machine à pain)	1h10	très facile	https://assets.afcdn.com/recipe/20100120/58724_w420h344c1cx256cy192.jpg	1 h	4.3	10 min	bon marché	6
759	Cake aux fruits	1 h	très facile	https://assets.afcdn.com/recipe/20191113/102364_w420h344c1cx314cy177cxt0cyt0cxb629cyb354.png	45 min		15 min	bon marché	6
760	Cake aux légumes de printemps, poulet rôti et fines herbes	1 h	très facile	https://assets.afcdn.com/recipe/20140604/6511_w420h344c1cx1024cy616.jpg	45 min	4	15 min	bon marché	6
761	Cake aux moules et poireaux	1 h	facile	https://assets.afcdn.com/recipe/20131031/41031_w420h344c1cx480cy358.jpg	30 min		30 min	bon marché	3
762	Cake aux olives vertes et noires	1h20	très facile	https://assets.afcdn.com/recipe/20170219/10364_w420h344c1cx2016cy1322.jpg	35 min	2	45 min	bon marché	6
763	Cake aux orties	1h10	très facile	https://assets.afcdn.com/recipe/20170426/42600_w420h344c1cx1500cy844.jpg	45 min	4	25 min	bon marché	4
764	Cake aux petits pois et lardons	1h20	très facile	https://assets.afcdn.com/recipe/20100120/432_w420h344c1cx192cy256.jpg	50 min	4.1	30 min	bon marché	8
765	Cake aux saucisses	45 min	très facile	https://assets.afcdn.com/recipe/20160422/49751_w420h344c1cx1560cy2080.jpg	35 min	4.2	10 min	bon marché	6
766	Cake aux tomates séchées et câpres	1h05	facile		45 min	4.3	20 min	bon marché	6
767	Cake chocolat et poires pochées au vin rouge	50 min	facile	https://assets.afcdn.com/recipe/20191108/102104_w420h344c1cx1060cy707cxt0cyt0cxb2121cyb1414.jpg	30 min		20 min	bon marché	6
768	Cake citron pavot au lait concentré sucré	40 min	très facile	https://assets.afcdn.com/recipe/20190509/92137_w420h344c1cx318cy423cxt0cyt0cxb637cyb847.jpg	25 min	4	15 min	bon marché	8
769	Cake de courgettes à la tomme de chèvre	1h20	très facile		50 min	4	30 min	bon marché	6
770	Cake de Cyrielle aux pistaches et jambon	1 h	très facile		45 min	5	15 min	bon marché	6
771	Cake de volaille au vin rouge et à la coriandre	1 h	très facile		40 min	3.5	20 min	bon marché	4
772	Cake d'épinard aux deux fromages	50 min	facile	https://assets.afcdn.com/recipe/20140821/35136_w420h344c1cx1280cy720.jpg	40 min		10 min	bon marché	1
773	Cake fou aux tomates séchées !	1 h	facile	https://assets.afcdn.com/recipe/20181206/84659_w420h344c1cx1280cy960cxt0cyt0cxb2560cyb1920.jpg	45 min	3.7	15 min	bon marché	6
774	cake léger aux tomates séchées et parmesan	55 min	très facile	https://assets.afcdn.com/recipe/20130909/26005_w420h344c1cx1872cy2808.jpg	45 min	3.8	10 min	bon marché	6
775	Cake méditerranéen gratiné au parmesan et à la coppa	1h15	facile		45 min		30 min	bon marché	1
776	Cake moelleux aux saucisses et au fromage	1 h	très facile		40 min	4	20 min	bon marché	6
777	Cake salé aux tomates séchées, chèvre et graines	1h10	facile	https://assets.afcdn.com/recipe/20170523/24707_w420h344c1cx1500cy1000.jpg	45 min	4.9	25 min	bon marché	8
778	Cake salé de pâtes aux lardons et aux échalotes	45 min	très facile		25 min		20 min	bon marché	10
779	Cake salé noisettes, pancetta, gorgonzola, ciboulette	1h15	très facile	https://assets.afcdn.com/recipe/20190418/91058_w420h344c1cx1424cy2136cxt0cyt0cxb11866cyb17800.jpg	50 min	5	25 min	bon marché	6
780	Cake au thon, à la tomate et à l'oignion	40 min	facile	https://assets.afcdn.com/recipe/20130609/10441_w420h344c1cx256cy192.jpg	30 min	4	10 min	bon marché	4
781	Cake végétarien aux épices	1 h	très facile	https://assets.afcdn.com/recipe/20170428/8046_w420h344c1cx307cy230.jpg	45 min	4.6	15 min	bon marché	4
782	Calamars à l'américaine	1h10	très facile		45 min	4.8	25 min	bon marché	4
839	Canard cuisiné à la sauce smitane	2h40	facile		2 h		40 min	Coût moyen	4
783	Calamars à l'andalouse	1h30	très facile	https://assets.afcdn.com/recipe/20160720/57910_w420h344c1cx3000cy2000.jpg	1 h	4.5	30 min	Coût moyen	4
784	Calamars à l'armoricaine	1h15	facile	https://assets.afcdn.com/recipe/20181107/83675_w420h344c1cx2136cy1424cxt0cyt0cxb4272cyb2848.jpg	1 h	4.7	15 min	Coût moyen	4
785	Calamars à l'encre et soufflé de persil	1h30	Niveau moyen		30 min	4.3	1 h	bon marché	4
786	Calamars à la basquaise	1h20	très facile	https://assets.afcdn.com/recipe/20150727/46185_w420h344c1cx1296cy972.jpg	1 h	4.7	20 min	assez cher	6
787	Calamars à la bolognaise	25 min	facile	https://assets.afcdn.com/recipe/20171115/75057_w420h344c1cx2304cy1728cxt0cyt0cxb4608cyb3456.jpg	20 min	4.2	5 min	bon marché	4
788	Calamars à la provençale et au blé	40 min	très facile	https://assets.afcdn.com/recipe/20140214/39907_w420h344c1cx1296cy864.jpg	25 min		15 min	bon marché	4
789	Calamars à la tomate et au paprika	1 h	très facile	https://assets.afcdn.com/recipe/20150609/53449_w420h344c1cx1632cy918.jpg	50 min	5	10 min	bon marché	6
790	Calamars à la tomate parfumé	1 h	facile		45 min		15 min	bon marché	4
791	Calamars à ma façon	20 min	facile	https://assets.afcdn.com/recipe/20150222/46894_w420h344c1cx80cy80.jpg	10 min		10 min	bon marché	4
792	Calamars aux épinards et aux tomates	1h05	facile		45 min	5	20 min	Coût moyen	6
793	Calamars aux fromages	45 min	facile	https://assets.afcdn.com/recipe/20140202/6831_w420h344c1cx878cy738.jpg	40 min	3.5	5 min	bon marché	4
794	Calamars aux graines de fenouil	1 h	facile		45 min		15 min	bon marché	6
795	Calamars  (calmars) farcis à la viande	2h15	facile	https://assets.afcdn.com/recipe/20190122/86546_w420h344c1cx1280cy960cxt0cyt0cxb2560cyb1920.jpg	1 h	4	1h15	Coût moyen	8
796	Calamars farcis à la purée d'épinards	1h20	facile		1 h		20 min	Coût moyen	4
797	calamars farcis à la sauce tomate vietnamiens	40 min	facile		25 min	4.4	15 min	Coût moyen	4
798	calamars farcis à la viande	1h15	facile		30 min		45 min	Coût moyen	8
799	Calamars farcis aux épinards et sauce tomate	1h15	Niveau moyen	https://assets.afcdn.com/recipe/20130328/56003_w420h344c1cx256cy192.jpg	15 min		1 h	bon marché	4
800	Calamars farcis aux herbes	1 h	facile		30 min	3	30 min	bon marché	4
801	Calamars farcis aux moules	2 h	facile		1 h		1 h	Coût moyen	4
802	Calamars sautés à la minute aux piments et dés de tomates	22 min	très facile	https://assets.afcdn.com/recipe/20100913/14371_w420h344c1cx256cy192.jpg	7 min	4.6	15 min	bon marché	4
803	Calmar (seiches) à la tomate et aux champignons	25 min	très facile	https://assets.afcdn.com/recipe/20140322/57589_w420h344c1cx1280cy960.jpg	15 min	4.4	10 min	bon marché	1
804	Calmars à la provençale faciles	1h45	facile		1h30	4.3	15 min	Coût moyen	6
805	Calmars à la sicilienne de mon papa (4ème rencontre Marmiton)	2h15	très facile	https://assets.afcdn.com/recipe/20100120/34226_w420h344c1cx192cy256.jpg	2 h	4.8	15 min	bon marché	8
806	Calmars à la thaïlandaise	20 min	très facile		10 min	1	10 min	bon marché	4
807	Calmars aux câpres et au vinaigre balsamique	22 min	très facile		2 min	4.4	20 min	Coût moyen	5
808	Calmars aux épinards et aux oignons	50 min	facile		30 min		20 min	Coût moyen	10
809	calmars aux oignons	20 min	très facile	https://assets.afcdn.com/recipe/20170131/8750_w420h344c1cx2808cy1872.jpg	10 min	3	10 min	bon marché	2
810	Calmars farcis aux aubergines	55 min	Niveau moyen		30 min	5	25 min	Coût moyen	4
811	Calmars sautés à la provençale	25 min	très facile		20 min	4.3	5 min	bon marché	2
812	Calzone aux légumes	35 min	facile		15 min		20 min	bon marché	4
813	Camembert à la braise à l'armagnac	25 min	très facile		20 min	5	5 min	bon marché	2
814	Camembert gratiné façon morbiflette parfumé aux saveurs du Maroc	1h10	facile	https://assets.afcdn.com/recipe/20130428/41782_w420h344c1cx256cy192.jpg	40 min		30 min	bon marché	3
815	Campagnard aux herbes fraîches	1h15	facile		45 min		30 min	bon marché	6
816	Canapés de cailles aux raisins	35 min	facile		20 min	5	15 min	Coût moyen	4
817	Canapés gratinés à la béchamel	35 min	très facile		20 min	5	15 min	bon marché	4
818	Canard à l'ananas au wok	1h05	très facile		45 min	5	20 min	Coût moyen	4
819	Canard à l'orange et au miel	2h15	facile	https://assets.afcdn.com/recipe/20121111/57534_w420h344c1cx256cy192.jpg	2 h	4.9	15 min	Coût moyen	4
820	Canard à l'orange et aux tomates	1h45	facile	https://assets.afcdn.com/recipe/20160722/43112_w420h344c1cx2880cy1920.jpg	45 min	5	1 h	Coût moyen	4
821	Canard à l'orange facile	1h20	très facile	https://assets.afcdn.com/recipe/20121025/872_w420h344c1cx256cy192.jpg	1 h	4.4	20 min	Coût moyen	4
822	Canard à la badiane	35 min	très facile		15 min	4.7	20 min	Coût moyen	4
823	Canard à la bière	1h45	facile	https://assets.afcdn.com/recipe/20141120/41389_w420h344c1cx720cy720.jpg	1h30	4.4	15 min	Coût moyen	6
824	canard à la capsicum	1h40	très facile		1h30	4.8	10 min	bon marché	6
825	Canard à la coriandre avec rougail	1h05	Niveau moyen		35 min	5	30 min	Coût moyen	4
826	Canard à la crème du sud-ouest	50 min	Niveau moyen		40 min		10 min	Coût moyen	4
827	canard à la mangue	35 min	très facile	https://assets.afcdn.com/recipe/20130203/45402_w420h344c1cx256cy192.jpg	25 min		10 min	bon marché	4
828	Canard à la Normande	2h30	Niveau moyen		2 h	4.4	30 min	Coût moyen	4
829	Canard aux 5 épices et aux abricots	1h15	Niveau moyen		1 h	5	15 min	Coût moyen	4
830	Canard aux amandes	1h40	facile		1 h		40 min	Coût moyen	8
831	Canard aux cerises	1h15	facile	https://assets.afcdn.com/recipe/20100120/65998_w420h344c1cx192cy256.jpg	45 min	4.4	30 min	Coût moyen	4
832	Canard aux figues farci de son foie	2h15	difficile	https://assets.afcdn.com/recipe/20160911/7565_w420h344c1cx1280cy768.jpg	45 min	5	1h30	assez cher	8
833	Canard aux lentilles en curry	1 h	facile		50 min	5	10 min	Coût moyen	2
834	Canard aux navets, sauce brune	2h35	Niveau moyen		2h20		15 min	Coût moyen	6
835	Canard aux olives	1h20	facile	https://assets.afcdn.com/recipe/20120729/10292_w420h344c1cx256cy192.jpg	1 h	4.7	20 min	Coût moyen	6
836	Canard aux pêches de vigne et aux raisins	1 h	facile	https://assets.afcdn.com/recipe/20150925/54088_w420h344c1cx1500cy1125.jpg	30 min		30 min	Coût moyen	4
837	Canard aux pommes farci , sauce aux champignons de forêt	3 h	Niveau moyen		2 h	5	1 h	assez cher	8
840	Canard en cocotte aux navets	1h15	facile	https://assets.afcdn.com/recipe/20150307/52407_w420h344c1cx1224cy1632.jpg	1 h	4.4	15 min	Coût moyen	4
841	Canard en cocotte aux olives	1h05	facile	https://assets.afcdn.com/recipe/20120502/30526_w420h344c1cx1162cy1750.jpg	45 min	4.3	20 min	Coût moyen	4
842	Canard farci à la rhubarbe	1h30	facile	https://assets.afcdn.com/recipe/20160223/46463_w420h344c1cx250cy375.jpg	1 h	4.6	30 min	Coût moyen	5
843	Canard farci à la solognote	1h35	facile		1 h		35 min	Coût moyen	6
844	Canard farci aux amandes	1h30	facile	https://assets.afcdn.com/recipe/20120502/4191_w420h344c1cx192cy256.jpg	1 h	3	30 min	Coût moyen	5
845	Canard farci aux foies de volailles	1h35	facile		1 h		35 min	Coût moyen	8
846	Canard farci aux marrons et aux fruits secs	2h20	facile	https://assets.afcdn.com/recipe/20130113/29277_w420h344c1cx256cy192.jpg	2 h	4.9	20 min	Coût moyen	6
847	Canard farci aux pommes	2h20	facile		2 h		20 min	Coût moyen	5
848	Canard laqué chinois aux 5 parfums	1h40	difficile	https://assets.afcdn.com/recipe/20160726/63432_w420h344c1cx1500cy1000.jpg	1 h	5	40 min	assez cher	4
849	Canard rôti à la confiture mangue-miel	2h15	très facile	https://assets.afcdn.com/recipe/20130627/3400_w420h344c1cx1250cy1875.jpg	2 h	5	15 min	Coût moyen	5
850	Canard rôti aux tagliatelles de légumes	1h10	facile		30 min	3	40 min	Coût moyen	4
851	Canard sauvage aux olives vertes	1h30	Niveau moyen	https://assets.afcdn.com/recipe/20190130/86918_w420h344c1cx1024cy701cxt0cyt0cxb2048cyb1402.jpg	1 h	5	30 min	Coût moyen	4
852	canard sauvage aux petits pois	1h20	facile	https://assets.afcdn.com/recipe/20170324/16448_w420h344c1cx2880cy1920.jpg	1 h	5	20 min	Coût moyen	2
853	canard sauvage aux pommes	2h20	très facile	https://assets.afcdn.com/recipe/20141116/31432_w420h344c1cx1224cy1632.jpg	2 h	4.8	20 min	Coût moyen	2
854	Canard thaï aux jeunes pousses de poivre vert	40 min	très facile		10 min	5	30 min	Coût moyen	4
855	Cancoillotte (à base de metton)	25 min	très facile		15 min	5	10 min	Coût moyen	4
856	Caneton à la Bigarrade	1h30	Niveau moyen		1 h		30 min	Coût moyen	4
857	Canette à l'orange	1h10	très facile	https://assets.afcdn.com/recipe/20130416/49805_w420h344c1cx256cy192.jpg	1 h	4.6	10 min	bon marché	4
858	Canette épicée au four à ma façon	1h20	facile		1 h	4.4	20 min	bon marché	4
859	Canette aux pêches	1h15	très facile		1 h	4.8	15 min	Coût moyen	3
860	Canette farcie aux figues simplissime	1h15	facile	https://assets.afcdn.com/recipe/20171019/73609_w420h344c1cx2808cy1872cxt0cyt0cxb5616cyb3744.jpg	1 h	4.7	15 min	Coût moyen	4
861	Canette fondante aux olives	2h15	facile		2 h		15 min	Coût moyen	6
862	Canette aux pommes, poivrons et gingembre	1h45	facile	https://assets.afcdn.com/recipe/20140427/21530_w420h344c1cx640cy640.jpg	1h15	4.5	30 min	Coût moyen	4
863	Cannelés croustillants et bien dorés	1 j 2 h 46 min	facile	https://assets.afcdn.com/recipe/20190704/94729_w420h344c1cx351cy530cxt0cyt0cxb702cyb1060.jpg	1h05		26 min	Coût moyen	12
864	Cannelés lait concentré sucré	31 min	facile	https://assets.afcdn.com/recipe/20190509/92136_w420h344c1cx318cy423cxt0cyt0cxb637cyb847.jpg	16 min	4	15 min	bon marché	9
865	cannelloni à la bolognaise	1h45	facile	https://assets.afcdn.com/recipe/20160627/5440_w420h344c1cx2104cy1424.jpg	1 h	5	45 min	Coût moyen	4
866	cannelloni à la brousse et saumon fumé	20 min	très facile		5 min	3.5	15 min	bon marché	4
867	Cannelloni à la Florentine ( épinards et viande )	1h15	facile	https://assets.afcdn.com/recipe/20130201/46228_w420h344c1cx1250cy1875.jpg	30 min	4.4	45 min	Coût moyen	4
868	Cannelloni à la ricotta et aux 3 poivrons	1h30	très facile		1 h	3.8	30 min	bon marché	10
869	Cannelloni a la ricotta et aux épinards	1h30	Niveau moyen		30 min	4.1	1 h	Coût moyen	4
870	Cannelloni au chèvre et aux poireaux	1 h	facile		30 min	4	30 min	bon marché	4
871	Cannelloni au pesto et à la ricotta	40 min	facile		30 min	4.7	10 min	bon marché	4
872	Cannelloni aux champignons	1h05	Niveau moyen	https://assets.afcdn.com/recipe/20160206/38743_w420h344c1cx1296cy776.jpg	20 min	4	45 min	Coût moyen	2
873	Cannelloni aux courgettes, basilic et chèvre frais	30 min	très facile	https://assets.afcdn.com/recipe/20160926/1755_w420h344c1cx1872cy2808.jpg	15 min	4.6	15 min	bon marché	4
874	Cannelloni aux courgettes, basilic et fromage de chèvre	30 min	facile		20 min	4.8	10 min	bon marché	4
875	Cannelloni aux courgettes	1h30	très facile	https://assets.afcdn.com/recipe/20180323/78103_w420h344c1cx3000cy2000cxt0cyt0cxb6000cyb4000.jpg	1 h	3.9	30 min	bon marché	4
876	Cannelloni aux épinards et à la ricotta faciles	1h10	facile		30 min	4.3	40 min	bon marché	6
877	Cannelloni aux épinards et à la ricotta	2 h	facile	https://assets.afcdn.com/recipe/20160804/42485_w420h344c1cx1500cy1000.jpg	1 h	4.4	1 h	bon marché	6
878	Cannelloni aux épinards et au brocciu	1h10	facile	https://assets.afcdn.com/recipe/20150802/7511_w420h344c1cx1632cy1224.jpg	40 min		30 min	bon marché	4
879	Cannelloni aux épinards	1h30	facile	https://assets.afcdn.com/recipe/20170411/1782_w420h344c1cx2050cy1368.jpg	45 min	4.7	45 min	bon marché	2
880	Cannelloni aux légumes	40 min	facile		25 min	4	15 min	bon marché	6
881	Cannelloni de jambon farcis aux épinards et à la feta	50 min	facile		20 min	2	30 min	bon marché	4
882	Cannelloni gratinés farcis à la bolognaise	1h20	facile	https://assets.afcdn.com/recipe/20130619/7081_w420h344c1cx192cy256.jpg	20 min	4	1 h	bon marché	4
883	Cannelloni à l'ancienne	1h25	Niveau moyen	https://assets.afcdn.com/recipe/20130909/3221_w420h344c1cx2532cy1688.jpg	25 min	4.6	1 h	Coût moyen	6
884	Cannellonis à la Francesco	1h15	Niveau moyen		35 min	4	40 min	Coût moyen	6
885	Cannellonis à la ricotta et aux herbes	1h05	très facile	https://assets.afcdn.com/recipe/20100120/35128_w420h344c1cx256cy192.jpg	45 min	4.6	20 min	bon marché	2
886	Cannellonis à la viande et au chèvre	1 h	facile		20 min	4.6	40 min	Coût moyen	6
887	Cannellonis au boeuf à la piémontaise	2 h	Niveau moyen	https://assets.afcdn.com/recipe/20100906/38612_w420h344c1cx256cy192.jpg	30 min	4.8	1h30	bon marché	4
888	Cannellonis aux 2 légumes	45 min	Niveau moyen	https://assets.afcdn.com/recipe/20130719/29337_w420h344c1cx977cy1504.jpg	15 min	3.5	30 min	Coût moyen	4
889	Cannellonis aux aubergines	1 h	facile	https://assets.afcdn.com/recipe/20150813/11197_w420h344c1cx2376cy1584.jpg	30 min	4.7	30 min	Coût moyen	2
890	Cannellonis aux champignons, ricotta et herbes	1h05	Niveau moyen		20 min		45 min	Coût moyen	4
891	Cannellonis aux deux saumons	1 h	très facile		30 min		30 min	Coût moyen	6
892	Cannellonis aux fruits de mer	50 min	facile		30 min	3.5	20 min	assez cher	4
893	Cannellonis d'andouillettes à la moutarde	2 h	Niveau moyen		1 h	4	1 h	Coût moyen	4
894	Cannellonis de blettes au carrelet et aux champignons	1 h	Niveau moyen		30 min		30 min	bon marché	2
895	Cannellonis farcis à la viande et au jambon (19ème rencontre)	2h30	facile		1h30	4.9	1 h	Coût moyen	6
896	Cannellonis végétariens à la menthe	1 h	facile		40 min	4.3	20 min	bon marché	6
897	Cannette à la normande	1h10	facile		40 min	5	30 min	Coût moyen	2
898	Capellini aux légumes	25 min	facile		10 min	5	15 min	bon marché	4
899	Capitaine à la Mauricienne	40 min	facile		25 min	5	15 min	Coût moyen	4
900	Cappelletti à la grecque	30 min	très facile		15 min		15 min	bon marché	4
901	Cappellini aux tomates confites	8 min	très facile		3 min	4	5 min	Coût moyen	4
902	Capuccino de potiron aux épices d’hiver	50 min	facile	https://assets.afcdn.com/recipe/20190206/87308_w420h344c1cx1512cy1134cxt0cyt0cxb3024cyb2268.jpg	40 min		10 min	bon marché	4
903	Carangue cuit à plat et velouté aux feuilles de carry	1h15	facile		45 min		30 min	bon marché	4
904	Carbonade à la bière et au raisins secs	1h20	très facile		1 h	4.8	20 min	bon marché	6
905	Carbonade à la Chouffe (Belgique)	2h40	Niveau moyen	https://assets.afcdn.com/recipe/20161019/46207_w420h344c1cx2725cy2045.jpg	2 h	5	40 min	Coût moyen	4
906	Carbonade aux rognons (recette de Marie Denis)	1h30	très facile		1 h	4.3	30 min	bon marché	6
907	Carbonade aux spéculoos	1h25	très facile	https://assets.afcdn.com/recipe/20161017/35677_w420h344c1cx2647cy1764.jpg	1 h	4	25 min	bon marché	2
908	Carbonade de porc aux cerises	1h30	facile		1 h	4	30 min	bon marché	4
909	Carbonades flamandes à la bière	2h30	facile	https://assets.afcdn.com/recipe/20170407/18212_w420h344c1cx1456cy2184.jpg	2 h	4.8	30 min	Coût moyen	4
910	Carbonades flamandes à la moutarde	3h15	Niveau moyen		3 h	5	15 min	bon marché	4
911	Carbonara aux crevettes sans gluten et sans lactose	30 min	facile	https://assets.afcdn.com/recipe/20171003/72682_w420h344c1cx2464cy1632cxt0cyt0cxb4928cyb3264.jpg	15 min	5	15 min	bon marché	4
912	Carbonnade à l'Irlandaise	3h20	facile	https://assets.afcdn.com/recipe/20150915/6794_w420h344c1cx500cy295.jpg	3 h	4.5	20 min	bon marché	4
913	Carbonnade de boeuf à la bière de tonton Hervé (autocuiseur)	1h05	très facile		35 min	3.1	30 min	bon marché	4
914	Carbonnade flamande à ma façon	3h15	Niveau moyen	https://assets.afcdn.com/recipe/20130328/53837_w420h344c1cx1000cy1504.jpg	3 h	4.8	15 min	bon marché	4
915	Carbonnades à la bière	3h10	Niveau moyen	https://assets.afcdn.com/recipe/20151124/45539_w420h344c1cx1224cy1632.jpg	3 h	4.7	10 min	Coût moyen	6
916	carbonnades aux choux de bruxelles	32 min	très facile		2 min	4.9	30 min	bon marché	4
917	Carbonnades aux pommes et poires	1h35	facile	https://assets.afcdn.com/recipe/20190222/88208_w420h344c1cx1728cy2304cxt0cyt0cxb3456cyb4608.jpg	1h20	4.6	15 min	bon marché	6
918	Carbonnades de boeuf à la bière brune	3 h	Niveau moyen		2 h		1 h	bon marché	4
919	Carbonnades de boeufs à la moutarde et confiture	5h25	facile		5 h	5	25 min	Coût moyen	2
920	Carbonnades flamandes à la Chimay bleue	2h30	facile	https://assets.afcdn.com/recipe/20121125/1455_w420h344c1cx256cy192.jpg	2 h	4.2	30 min	bon marché	4
921	Carbonnades flamandes aux pommes de terre	3h20	facile		3 h	5	20 min	bon marché	2
922	Cardons à l'ancienne	1h25	Niveau moyen		55 min	4	30 min	Coût moyen	6
923	Cardons gratinés aux anchois	1h15	facile		40 min	4.5	35 min	Coût moyen	8
924	Cari de crevettes à la noix de coco	35 min	facile	https://assets.afcdn.com/recipe/20130219/65134_w420h344c1cx256cy192.jpg	20 min	4.4	15 min	Coût moyen	4
925	Cari de poulet à la sauce tomate	55 min	très facile		45 min	4.5	10 min	bon marché	2
926	Cari de poulet aux patates douces	1h05	facile		45 min		20 min	bon marché	6
927	Cari de poulet (comme à la Réunion)	50 min	très facile	https://assets.afcdn.com/recipe/20130524/6376_w420h344c1cx1150cy867.jpg	30 min	4.5	20 min	bon marché	4
928	Cari poulet aux pommes de terres	1h15	facile		1 h	4	15 min	bon marché	4
929	Carne de porco alentejana (Emincé de porc aux vongoles) recette portugaise	2h30	facile	https://assets.afcdn.com/recipe/20120903/14553_w420h344c1cx256cy192.jpg	30 min	5	2 h	Coût moyen	4
930	Carottes à l'italienne	40 min	facile		10 min	3.7	30 min	bon marché	6
931	Carottes à la carbolognaise	1 h	très facile	https://assets.afcdn.com/recipe/20130220/44082_w420h344c1cx256cy192.jpg	30 min	4.7	30 min	bon marché	4
932	Carottes à la carbonara	30 min	très facile	https://assets.afcdn.com/recipe/20160922/14382_w420h344c1cx2000cy3000.jpg	20 min	4.7	10 min	bon marché	2
933	Carottes à la crème d'ail	30 min	très facile		15 min	4.6	15 min	bon marché	6
934	Carottes à la crème parfumées au thym	45 min	très facile	https://assets.afcdn.com/recipe/20170428/53426_w420h344c1cx360cy560.jpg	30 min	4.8	15 min	bon marché	4
935	Carottes à la crème persillée	30 min	facile	https://assets.afcdn.com/recipe/20100120/43173_w420h344c1cx256cy192.jpg	20 min	4.5	10 min	Coût moyen	4
936	Carottes à la moutarde au four	35 min	très facile	https://assets.afcdn.com/recipe/20100120/44026_w420h344c1cx256cy192.jpg	20 min	3.8	15 min	bon marché	1
937	Carottes au curry et aux petits lardons	30 min	très facile	https://assets.afcdn.com/recipe/20180409/78444_w420h344c1cx1024cy768cxt0cyt0cxb2048cyb1536.jpg	20 min	4.8	10 min	bon marché	2
938	carottes aux 2 fromages et à l'oseille	1 h	très facile		30 min	4.8	30 min	bon marché	2
939	Carottes aux lardons façon vichy	35 min	très facile	https://assets.afcdn.com/recipe/20160412/50828_w420h344c1cx214cy380.jpg	20 min	4.5	15 min	bon marché	3
940	Carottes confites à l'orange et à la cannelle	1h30	très facile		1 h	4	30 min	bon marché	4
941	Carottes fondantes à la crème et au vin blanc	30 min	très facile	https://assets.afcdn.com/recipe/20150215/65774_w420h344c1cx1960cy1102.jpg	15 min	4.3	15 min	Coût moyen	4
942	Carottes râpées aux brocolis et bacon, en sauce	20 min	très facile		10 min	4	10 min	bon marché	1
943	Carpaccio de boeuf aux aubergines	5 min	très facile		0		5 min	bon marché	4
944	Carpaccio de saumon aux baies roses et échalotes	2h30	facile	https://assets.afcdn.com/recipe/20160608/12139_w420h344c1cx1224cy1632.jpg	2 h	5	30 min	Coût moyen	4
945	Carpaccio de thon à l'avocat et au sésame	15 min	facile		0		15 min	assez cher	4
946	Carpaccio saumon, Vodka POLIAKOV, pomme	20 min	très facile	https://assets.afcdn.com/recipe/20151130/24140_w420h344c1cx1500cy1000.jpg	0		20 min	bon marché	4
947	Carpe à l'alsacienne	1 h	facile		30 min	4.5	30 min	Coût moyen	4
948	Carré d'agneau à l'antiboise	40 min	facile	https://assets.afcdn.com/recipe/20130406/7711_w420h344c1cx256cy192.jpg	30 min		10 min	assez cher	4
949	Carré d'agneau à l'orientale et légumes poêlés	30 min	facile		15 min	4	15 min	Coût moyen	4
950	Carré d’agneau aux agrumes	50 min	facile		30 min	4.3	20 min	bon marché	4
951	carré d'agneau aux fèves	1h05	facile		30 min		35 min	assez cher	6
952	Carré d'agneau aux figues et sa sauce jardinière	1 h	Niveau moyen		20 min		40 min	assez cher	5
953	Carré d'agneau aux herbes	1 h	très facile	https://assets.afcdn.com/recipe/20130328/28974_w420h344c1cx1000cy1334.jpg	45 min	4.7	15 min	assez cher	8
954	Carré d’agneau en croûte d'herbes et de graines au jus de thym et aux légumes nouveaux	1h45	Niveau moyen		1h10	3.5	35 min	Coût moyen	8
955	Carré de porc à l'ancienne	1h10	facile		30 min	4	40 min	Coût moyen	6
956	Carré de porc à l'orange	1h35	Niveau moyen		1h15	5	20 min	Coût moyen	6
957	Carré de veau à la moutarde à l'ancienne et aux pruneaux	1h10	facile		40 min		30 min	assez cher	6
958	Carré de veau gratiné aux champignons	55 min	facile		20 min	5	35 min	Coût moyen	4
959	Carrelet aux poireaux et au cidre	55 min	très facile		20 min	4.2	35 min	Coût moyen	4
960	Carrot cake au lait concentré sucré	2h05	facile	https://assets.afcdn.com/recipe/20181205/84596_w420h344c1cx240cy168cxt0cyt0cxb480cyb337.jpg	40 min	5	25 min	bon marché	10
961	Casimir (veau au curry et aux fruits)	1h30	facile		1 h	4.8	30 min	Coût moyen	4
962	Casserole de légumes aux lentilles	35 min	très facile	https://assets.afcdn.com/recipe/20120908/50235_w420h344c1cx256cy192.jpg	25 min	4.8	10 min	bon marché	4
963	Cassolette d'Achatines aux girolles	40 min	très facile		10 min		30 min	Coût moyen	6
964	Cassolette de lotte et de Saint-Jacques à la crème	40 min	facile		20 min	3.3	20 min	Coût moyen	6
965	Cassolette de Saumon à l'érable	35 min	très facile		15 min	4.7	20 min	Coût moyen	4
966	Cassolettes aux saumon et cabillaud	30 min	facile	https://assets.afcdn.com/recipe/20100120/27258_w420h344c1cx192cy256.jpg	20 min	4.2	10 min	bon marché	4
967	Cassolettes de pétoncle à la Jlau	35 min	facile	https://assets.afcdn.com/recipe/20130226/54052_w420h344c1cx256cy192.jpg	15 min	4	20 min	Coût moyen	2
968	Cassolettes de poulet aux petits légumes	1 h	très facile	https://assets.afcdn.com/recipe/20140316/14166_w420h344c1cx1632cy1224.jpg	35 min	4	25 min	Coût moyen	4
969	Cassoulet à L'ancienne façon lolo	5 h	Niveau moyen	https://assets.afcdn.com/recipe/20140323/42443_w420h344c1cx1632cy1224.jpg	4 h	5	1 h	bon marché	10
970	Cassoulet à l'ancienne	6h10	facile	https://assets.afcdn.com/recipe/20161220/7328_w420h344c1cx2880cy1920.jpg	5h30	5	40 min	assez cher	8
971	Cassoulet à la tomate	2h10	facile	https://assets.afcdn.com/recipe/20141211/18177_w420h344c1cx320cy240.jpg	2 h	4.6	10 min	bon marché	6
972	Cassoulet à ma façon au Companion	55 min	facile	https://assets.afcdn.com/recipe/20191121/102761_w420h344c1cx1920cy2880cxt0cyt0cxb3840cyb5760.jpg	30 min		25 min	Coût moyen	4
973	Cassoulet à ma façon au Cookeo	45 min	facile	https://assets.afcdn.com/recipe/20191121/102759_w420h344c1cx3483cy1891cxt0cyt0cxb5760cyb3840.jpg	20 min		25 min	Coût moyen	4
974	Cassoulet à ma façon au Cooking Chef	55 min	facile	https://assets.afcdn.com/recipe/20191121/102762_w420h344c1cx3808cy1583cxt1518cyt0cxb5616cyb3744.jpg	30 min		25 min	Coût moyen	4
975	Cassoulet à ma façon au Monsieur Cuisine	55 min	facile	https://assets.afcdn.com/recipe/20191121/102763_w420h344c1cx2316cy1833cxt0cyt0cxb5760cyb3840.jpg	30 min		25 min	Coût moyen	4
976	Cassoulet à ma façon au Thermomix	55 min	facile	https://assets.afcdn.com/recipe/20191121/102764_w420h344c1cx2808cy1872cxt0cyt0cxb5616cyb3744.jpg	30 min		25 min	Coût moyen	4
977	Cassoulet de cabillaud	50 min	très facile	https://assets.afcdn.com/recipe/20191009/100383_w420h344c1cx1632cy2464cxt0cyt0cxb3264cyb4928.jpg	30 min	5	20 min	bon marché	4
978	Cassoulet végétarien aux légumes variés	1h15	facile	https://assets.afcdn.com/recipe/20150312/32429_w420h344c1cx1250cy1875.jpg	1 h	4.1	15 min	bon marché	8
979	Cataplana aux fruits de mer	1h40	Niveau moyen	https://assets.afcdn.com/recipe/20130623/32606_w420h344c1cx256cy192.jpg	40 min	4.8	1 h	assez cher	4
980	Cervelas à la crème fromagère	20 min	facile		10 min		10 min	bon marché	4
981	cervelas à la  sauce tomate	20 min	facile		10 min	3.5	10 min	bon marché	4
982	Cervelles de porc aux câpres	45 min	facile	https://assets.afcdn.com/recipe/20130129/14211_w420h344c1cx256cy192.jpg	15 min	5	30 min	bon marché	2
983	Ceviche à la sole	20 min	facile		20 min		20 min	bon marché	6
984	Ch'ti poulet à la bière facile	1h15	facile	https://assets.afcdn.com/recipe/20140101/16636_w420h344c1cx1824cy1368.jpg	1 h	4.5	15 min	bon marché	6
985	Chair à saucisse au vin blanc	40 min	très facile		20 min	4	20 min	bon marché	4
986	Chair à Saucisse façon Louloute	35 min	facile		20 min		15 min	bon marché	4
987	Chakchouka à l'escalope de dinde	30 min	très facile	https://assets.afcdn.com/recipe/20170131/1426_w420h344c1cx1779cy2669.jpg	15 min	3.5	15 min	bon marché	4
988	Champ aux asperges et queues d'écrevisses	45 min	très facile	https://assets.afcdn.com/recipe/20111219/35730_w420h344c1cx256cy192.jpg	30 min		15 min	bon marché	4
989	Champignons à la crème persillée	35 min	facile		20 min	4	15 min	Coût moyen	5
990	Champignons de Paris farcis à la va vite	1h05	très facile	https://assets.afcdn.com/recipe/20180201/77286_w420h344c1cx2050cy1280cxt0cyt0cxb4100cyb2560.jpg	45 min	4	20 min	bon marché	4
991	Champignons en sauce à ma façon	20 min	très facile		10 min		10 min	bon marché	1
992	Champignons farcis à la chair à saucisse	50 min	très facile	https://assets.afcdn.com/recipe/20141111/47270_w420h344c1cx1779cy1506.jpg	30 min	4.9	20 min	bon marché	2
993	Champignons farcis à la provençale	30 min	très facile	https://assets.afcdn.com/recipe/20150613/51522_w420h344c1cx1224cy1632.jpg	10 min		20 min	bon marché	4
994	Champignons farcis à la ricotta, thon, tomates	50 min	très facile		40 min	4	10 min	bon marché	8
995	Champignons farcis à la saucisse et au riz	1h15	facile		45 min		30 min	bon marché	8
996	Champignons farçis à la tomme et au romarin	35 min	facile		15 min		20 min	Coût moyen	6
997	Champignons farcis à la viande	40 min	facile		20 min	4	20 min	bon marché	4
998	Champignons farcis à ma façon...	50 min	facile		30 min		20 min	bon marché	4
999	Champignons farcis aux lardons, tomates et poireaux	45 min	facile	https://assets.afcdn.com/recipe/20130226/67025_w420h344c1cx256cy192.jpg	30 min	4.8	15 min	bon marché	2
\.


--
-- Data for Name: recipe_ingredient; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipe_ingredient (ingredient_id, recipe_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
16	2
21	2
14	2
15	2
18	2
13	2
19	2
20	2
17	2
31	3
23	3
24	3
26	3
27	3
28	3
22	3
30	3
20	3
25	3
29	3
21	3
32	4
33	4
34	4
35	4
36	4
37	4
38	4
39	4
40	4
41	4
42	4
43	5
36	5
44	5
45	5
46	5
47	5
48	5
49	5
50	5
51	5
20	5
21	5
52	6
46	6
54	6
53	6
55	6
20	6
21	6
57	7
23	7
56	7
58	7
47	7
59	7
60	7
61	7
62	7
63	7
20	7
21	7
64	8
23	8
65	8
66	8
67	8
68	8
69	8
20	8
21	8
70	9
71	9
72	9
73	9
74	9
75	9
76	9
77	9
78	9
79	9
20	9
21	9
80	10
81	10
36	10
82	10
83	10
84	10
85	10
86	10
87	10
20	10
21	10
90	11
97	11
89	11
94	11
96	11
92	11
93	11
88	11
91	11
20	11
95	11
21	11
58	12
98	12
99	12
100	12
59	12
101	12
20	12
21	12
102	12
36	13
103	13
106	13
107	13
105	13
109	13
104	13
108	13
37	13
110	13
111	13
112	13
20	13
21	13
114	14
23	14
113	14
115	14
116	14
57	14
117	14
118	14
20	14
21	14
119	15
21	15
120	15
121	15
122	15
123	15
124	15
125	15
126	16
127	16
128	16
129	16
130	16
131	16
132	16
133	16
134	16
135	16
136	17
137	17
138	17
139	17
140	17
141	17
142	17
143	17
144	17
145	17
146	17
147	17
148	18
87	18
149	18
23	18
150	18
151	18
152	18
153	18
154	18
155	18
156	18
157	18
168	19
167	19
166	19
159	19
171	19
169	19
161	19
165	19
160	19
162	19
158	19
164	19
170	19
163	19
172	20
173	20
174	20
89	20
175	20
176	20
177	20
178	20
179	20
180	20
181	20
182	20
79	20
20	20
21	20
183	21
184	21
36	21
74	21
185	21
186	21
41	21
21	21
20	21
187	21
188	21
178	22
189	22
190	22
191	22
192	22
193	22
38	22
194	23
23	23
37	23
195	23
196	23
197	23
198	23
199	23
200	23
201	23
202	23
203	23
20	23
21	23
204	24
205	24
206	24
207	24
208	24
209	24
210	24
87	24
55	24
112	24
211	24
20	24
21	24
212	25
36	25
213	25
214	25
215	25
216	25
217	25
218	25
210	25
219	25
102	25
39	25
38	25
220	26
221	26
222	26
223	26
176	26
224	26
225	26
226	27
227	27
106	27
213	27
228	27
215	28
229	28
74	28
230	28
210	28
77	28
21	29
233	29
235	29
231	29
238	29
237	29
232	29
236	29
234	29
239	29
78	29
244	30
240	30
243	30
20	30
242	30
241	30
21	30
245	30
246	31
247	31
210	31
248	31
249	31
76	31
252	32
250	32
87	32
251	32
253	32
254	32
255	32
256	32
20	32
21	32
258	33
252	33
257	33
259	33
210	33
97	33
260	33
261	33
257	34
262	34
213	34
210	34
263	35
44	35
264	35
21	35
20	35
265	35
266	36
58	36
267	36
268	36
269	36
270	36
20	36
271	37
272	37
273	37
20	37
21	37
274	37
275	38
276	38
277	38
74	38
278	38
284	39
280	39
281	39
287	39
289	39
285	39
21	39
283	39
279	39
288	39
286	39
282	39
20	39
290	40
291	40
292	40
293	40
46	40
20	40
21	40
296	41
298	41
210	41
294	41
299	41
295	41
297	41
38	41
39	41
46	41
37	42
300	42
301	42
302	42
46	42
303	42
304	42
305	42
306	42
307	43
308	43
20	43
21	43
174	43
309	43
310	44
311	44
312	44
86	44
313	44
314	44
20	44
21	44
319	45
316	45
317	45
318	45
315	45
320	46
321	46
322	46
74	46
323	46
324	47
87	47
36	47
74	47
325	47
326	47
327	47
328	47
329	47
21	47
330	47
331	47
333	48
332	48
28	48
334	48
335	49
336	49
337	49
338	49
95	49
339	49
187	49
20	49
21	49
340	49
341	50
342	50
343	50
344	50
305	50
345	50
346	50
37	50
347	50
348	50
349	51
350	51
346	51
351	51
352	51
353	51
354	51
355	51
366	52
356	52
361	52
364	52
363	52
365	52
358	52
357	52
362	52
359	52
360	52
307	53
367	53
368	53
369	53
370	53
371	53
372	53
291	53
182	53
78	53
79	53
58	53
373	53
46	53
20	53
21	53
374	54
375	54
376	54
377	54
378	54
379	54
380	54
381	54
382	54
21	54
383	55
384	55
55	55
385	55
291	55
20	55
21	55
387	56
388	56
386	56
20	56
389	56
390	56
21	56
313	57
391	57
392	57
393	57
105	57
106	57
394	57
20	57
21	57
395	58
396	58
89	58
397	58
55	58
398	58
20	58
399	58
407	59
403	59
401	59
400	59
406	59
402	59
404	59
283	59
405	59
408	59
46	59
409	59
410	59
21	59
20	59
411	59
412	60
413	60
414	60
415	60
416	60
33	60
417	60
418	60
78	60
419	60
79	60
29	60
20	60
21	60
420	61
421	61
55	61
308	61
74	61
20	61
21	61
422	62
423	62
424	62
425	62
74	62
174	62
227	62
426	62
427	62
428	62
429	62
430	62
20	62
21	62
431	63
432	63
369	63
227	63
433	63
434	63
435	63
55	63
291	63
436	63
20	63
21	63
438	64
441	64
442	64
439	64
440	64
443	64
78	64
444	64
437	64
20	64
21	64
447	65
448	65
446	65
445	65
227	65
304	65
46	65
260	65
382	65
451	66
453	66
452	66
449	66
450	66
444	67
455	67
454	67
419	67
46	67
457	68
456	68
242	68
458	68
21	68
20	68
46	68
459	68
460	68
461	69
462	69
36	69
182	69
463	69
464	69
465	69
360	69
21	69
419	69
466	70
467	70
468	70
20	70
469	70
21	70
74	70
470	71
471	71
472	71
473	71
474	72
475	72
476	72
20	72
21	72
77	72
477	73
478	73
479	73
425	73
20	73
21	73
477	74
480	74
481	74
20	74
21	74
482	74
483	74
477	75
484	75
485	75
486	76
487	76
488	76
489	76
490	76
491	76
20	76
21	76
492	77
493	77
237	77
494	77
495	77
496	78
486	78
174	78
227	78
58	78
497	78
441	78
498	78
499	78
500	79
501	79
502	79
20	79
21	79
46	79
479	79
503	80
504	80
485	80
505	80
506	80
507	80
508	80
509	80
510	80
20	80
21	80
511	81
174	81
74	81
512	81
513	81
514	81
20	81
21	81
515	81
516	82
517	82
518	82
519	82
520	82
521	82
38	82
522	82
50	82
523	82
524	83
525	83
490	83
20	83
21	83
112	84
529	84
526	84
528	84
530	84
527	84
531	84
20	84
21	84
532	84
533	84
180	84
106	84
534	84
13	85
535	85
57	85
536	85
537	85
538	85
539	85
20	85
21	85
545	86
543	86
547	86
540	86
542	86
544	86
373	86
541	86
546	86
549	86
550	86
548	86
76	86
21	86
403	87
551	87
552	87
553	87
46	87
77	87
21	87
554	88
555	88
556	88
174	88
557	88
20	88
21	88
558	89
559	89
560	89
561	89
20	89
21	89
562	90
485	90
563	90
89	90
37	90
564	90
565	90
441	90
566	90
567	90
21	90
20	90
112	90
568	91
36	91
37	91
569	91
570	91
571	91
20	91
21	91
212	91
102	91
554	92
572	92
573	92
574	92
575	92
425	92
576	92
86	92
577	92
578	92
554	93
579	93
580	93
174	93
581	93
582	93
583	93
46	94
584	94
585	94
174	94
109	94
209	94
20	94
21	94
586	94
554	95
37	95
196	95
20	95
21	95
336	95
463	95
209	95
587	95
227	96
588	96
589	96
590	96
106	96
591	96
592	96
20	96
21	96
593	97
594	97
595	97
598	98
596	98
289	98
597	98
599	98
76	98
600	98
21	98
20	98
601	99
74	99
602	99
41	99
603	99
604	99
258	99
20	99
605	99
610	100
609	100
606	100
20	100
607	100
608	100
21	100
611	100
291	101
612	101
613	101
614	101
20	101
615	102
616	102
37	102
617	102
618	102
619	102
46	102
21	102
20	102
620	102
509	103
621	103
622	103
442	103
623	103
411	103
20	103
21	103
76	103
32	104
624	104
625	104
20	104
21	104
620	104
223	105
627	105
626	105
222	105
628	105
58	105
629	105
630	105
631	105
632	105
21	105
20	105
633	106
635	106
634	106
633	107
636	107
37	107
637	107
638	107
46	107
304	107
20	107
21	107
633	108
639	108
206	108
640	108
46	108
20	108
21	108
641	108
21	109
642	109
644	109
643	109
645	109
641	109
46	109
646	109
647	109
648	110
528	110
649	110
174	110
37	110
650	110
20	110
21	110
46	110
633	111
651	111
652	111
653	111
654	111
655	111
633	112
656	112
657	112
658	112
659	112
660	112
661	112
304	112
20	112
21	112
620	113
662	113
663	113
664	113
174	113
665	113
20	113
21	113
37	113
237	113
46	113
626	114
666	114
667	114
668	114
37	114
20	114
21	114
670	115
168	115
669	115
672	115
671	115
673	115
674	115
675	115
676	115
633	116
670	116
671	116
677	116
678	116
40	116
168	116
674	116
675	116
679	116
676	116
672	116
633	117
680	117
509	117
681	117
682	117
683	117
20	117
21	117
641	117
685	118
684	118
106	118
687	118
686	118
21	118
688	118
662	119
689	119
690	119
691	119
692	119
46	119
444	119
436	119
174	120
694	120
693	120
695	120
696	120
697	120
509	120
698	120
699	120
254	120
20	120
21	120
700	120
701	120
106	120
633	121
702	121
703	121
37	121
109	121
704	121
705	121
706	121
32	122
707	122
139	122
58	122
708	122
709	122
710	122
46	122
20	122
21	122
711	122
633	123
712	123
426	123
713	123
714	123
35	123
509	123
46	123
20	123
21	123
633	124
715	124
683	124
716	124
680	124
32	125
717	125
298	125
512	125
718	125
719	125
20	125
21	125
29	125
633	126
720	126
721	126
304	126
722	126
174	126
74	126
723	126
509	126
724	126
21	126
20	126
725	126
633	127
726	127
727	127
728	127
174	127
109	127
729	127
730	127
731	127
732	127
733	127
176	127
20	127
21	127
662	128
734	128
23	128
735	128
180	128
736	128
737	128
504	128
20	128
21	128
633	129
36	129
670	129
509	129
738	129
739	129
740	129
633	130
36	130
741	130
681	130
20	130
21	130
641	130
670	131
742	131
139	131
419	131
37	131
509	131
729	131
743	131
744	131
20	131
21	131
745	131
746	132
747	132
748	132
749	132
750	132
77	132
512	132
20	132
21	132
626	133
751	133
752	133
753	133
20	133
21	133
32	134
754	134
37	134
176	134
755	134
20	134
626	135
330	135
756	135
757	135
758	135
87	135
174	135
95	135
74	135
20	135
21	135
759	135
760	135
32	136
761	136
762	136
763	136
764	136
59	136
765	136
766	136
20	136
21	136
633	137
767	137
768	137
522	137
106	137
641	137
293	137
769	137
20	137
21	137
770	137
771	138
89	138
772	138
176	138
582	138
773	138
369	138
774	138
670	139
776	139
775	139
777	139
778	139
227	139
779	139
769	139
20	139
21	139
780	139
781	139
782	139
783	140
784	140
775	140
785	140
786	140
787	140
20	140
21	140
788	141
662	141
789	141
790	141
717	141
791	141
512	141
792	141
665	141
20	141
21	141
46	141
793	142
794	142
314	142
2	142
795	142
796	142
97	142
797	142
798	142
799	143
804	143
805	143
802	143
803	143
801	143
806	143
800	143
767	144
808	144
809	144
807	144
810	144
811	144
444	144
46	144
20	144
21	144
369	145
812	145
813	145
814	145
815	145
262	145
816	145
453	145
97	145
817	145
235	145
818	146
819	146
820	146
484	146
821	146
20	146
21	146
822	146
823	146
825	147
20	147
824	147
21	147
293	147
74	147
826	147
827	147
828	147
829	147
830	147
831	147
832	148
72	148
760	148
833	148
46	148
20	148
21	148
834	148
565	149
835	149
836	149
837	149
838	149
86	149
382	149
839	149
314	150
840	150
841	150
690	150
842	150
419	150
843	151
844	151
155	151
845	151
846	151
847	151
848	151
36	151
82	151
849	151
850	151
851	151
852	151
853	151
854	151
46	151
855	151
20	151
21	151
507	152
55	152
856	152
857	152
858	152
859	152
860	152
861	152
862	152
863	153
864	153
36	153
865	153
866	153
867	153
868	153
419	153
46	153
869	154
870	154
871	154
176	154
36	154
37	154
419	154
872	154
873	155
879	155
876	155
874	155
878	155
465	155
875	155
877	155
79	155
880	155
20	155
881	156
57	156
882	156
59	156
701	156
883	156
20	156
21	156
848	157
884	157
885	157
886	157
887	157
888	157
889	158
890	158
891	158
892	158
20	158
174	159
894	159
893	159
895	159
896	159
337	159
897	159
900	160
901	160
898	160
899	160
904	161
902	161
38	161
903	161
905	162
174	162
37	162
906	162
907	162
908	162
909	162
910	162
911	162
912	162
913	162
20	162
77	163
915	163
916	163
917	163
914	163
918	164
919	164
920	164
921	164
922	164
923	164
924	164
20	164
21	164
925	164
918	165
926	165
734	165
927	165
777	165
928	165
57	165
677	165
20	165
21	165
174	166
931	166
929	166
930	166
248	166
106	166
21	166
20	166
932	167
425	167
933	167
934	167
497	167
36	167
935	167
112	167
20	167
21	167
936	167
937	167
932	168
938	168
939	168
940	168
941	168
942	168
769	168
943	168
944	168
945	169
63	169
106	169
946	169
947	169
948	169
949	169
951	170
174	170
953	170
950	170
952	170
954	170
955	170
78	170
20	170
21	170
956	171
957	171
958	171
269	171
959	171
960	171
961	171
962	171
21	171
963	172
964	172
965	172
192	173
966	173
967	173
109	173
968	173
509	173
969	173
658	173
970	173
971	173
972	173
973	173
974	173
201	173
975	173
47	173
976	174
547	174
977	174
582	174
978	174
979	174
314	174
981	175
980	175
238	175
983	175
982	175
984	175
57	176
985	176
713	176
74	176
986	176
86	176
436	176
20	176
21	176
987	177
988	177
989	177
990	177
991	177
20	177
21	177
46	177
139	178
992	178
550	178
993	178
86	178
994	178
578	178
18	178
20	178
21	178
995	179
18	179
20	179
21	179
996	179
97	179
115	179
997	179
998	179
942	179
999	179
1001	180
774	180
1003	180
1000	180
1002	180
37	180
1004	180
1005	180
59	180
20	180
21	180
1006	181
942	181
196	181
72	181
111	181
174	181
74	181
1007	181
358	181
1008	181
20	181
21	181
1011	182
1010	182
108	182
1009	182
1012	182
36	182
441	182
106	182
21	182
20	182
1013	183
58	183
1014	183
646	183
291	183
21	183
20	183
237	184
1015	184
1016	184
419	184
174	184
1017	184
668	184
1018	184
46	184
20	184
21	184
490	184
1019	185
1020	185
36	185
1021	185
1022	185
21	185
480	186
1023	186
1024	186
1025	186
38	186
39	186
1023	187
480	187
1026	187
1027	187
21	187
20	187
425	187
1030	188
20	188
1028	188
1029	188
21	188
1031	188
1032	189
36	189
1033	189
1034	189
96	189
1035	189
95	189
20	189
21	189
361	189
1036	190
1037	190
1038	190
1039	190
1040	190
112	190
1034	190
1041	190
1042	190
21	190
20	190
77	190
866	191
1043	191
1044	191
650	191
174	191
1045	191
1046	191
436	191
20	191
21	191
516	192
1047	192
509	192
646	192
1048	192
1049	192
1050	192
1051	192
1052	192
1053	192
1054	192
1055	192
1056	192
1057	193
859	193
1058	193
1059	193
96	193
21	193
1060	193
1061	193
897	193
419	193
403	194
1062	194
505	194
174	194
1063	195
646	195
1064	195
1065	195
299	195
1066	195
38	195
579	195
436	195
282	196
1067	196
1068	196
270	196
1069	196
677	196
1070	196
1071	196
951	196
1072	196
36	196
737	196
1073	196
20	196
21	196
369	197
1074	197
1062	197
774	197
1075	197
20	197
21	197
1076	197
1077	197
36	197
1078	197
304	197
46	197
1079	198
1034	198
1080	198
20	198
21	198
74	198
1081	199
36	199
1082	199
1083	199
76	199
20	199
21	199
1084	200
484	200
1085	200
21	200
1086	200
1087	200
1088	200
1090	201
74	201
1089	201
513	201
210	201
55	201
1091	201
38	201
39	201
1092	202
1093	202
730	202
1094	202
1095	202
1097	203
1102	203
1096	203
1041	203
1098	203
1101	203
1103	203
1099	203
1100	203
1104	203
1105	203
20	203
21	203
1106	204
1107	204
1108	204
50	204
196	204
1109	204
74	204
21	204
1110	205
507	205
403	205
1111	205
1112	205
1113	205
21	205
952	205
1024	205
502	205
1114	205
1115	205
1116	206
1117	206
1118	206
1119	206
646	206
1120	206
1121	206
1122	206
298	207
1123	207
1124	207
1125	207
1126	207
1127	208
1128	208
1129	208
1130	208
1121	208
646	208
1131	208
816	208
1132	208
859	208
856	208
1133	208
1134	208
1135	208
1136	208
1054	208
1137	208
1138	209
364	209
1140	209
1139	209
1141	209
1142	209
1144	210
1143	210
1145	210
509	210
816	210
1146	210
1147	211
1148	211
1149	211
1150	211
1151	211
1152	211
1091	211
46	211
1153	211
21	211
1154	212
1155	212
1156	212
677	212
38	212
39	212
1157	213
174	213
1158	213
864	213
20	213
21	213
650	213
1159	213
274	213
29	213
459	213
460	213
1160	213
1161	213
490	213
295	214
1162	214
58	214
1163	214
176	214
1154	214
1166	215
1165	215
1164	215
21	215
1167	215
302	215
1168	215
36	215
1169	215
1170	215
1171	215
776	216
1172	216
1173	216
33	216
602	216
444	216
78	216
20	216
21	216
46	216
1154	217
1174	217
1175	217
174	217
1176	217
1177	217
1066	217
1178	217
1179	217
1024	217
1180	217
1154	218
1174	218
1175	218
174	218
1176	218
1177	218
1066	218
1178	218
1179	218
1024	218
1180	218
1154	219
1174	219
1175	219
174	219
1176	219
1177	219
1066	219
1178	219
1179	219
1024	219
1180	219
1154	220
1174	220
1175	220
174	220
1176	220
1177	220
1066	220
1178	220
1179	220
1024	220
1180	220
1154	221
1174	221
1175	221
174	221
1176	221
1177	221
1066	221
1178	221
1179	221
1024	221
1180	221
1154	222
1175	222
174	222
1174	222
1181	222
1177	222
1178	222
1066	222
1182	222
419	222
20	222
21	222
1154	223
36	223
1183	223
1184	223
74	223
1185	223
1186	223
46	223
1187	224
1188	224
1189	224
37	224
174	224
1190	224
21	224
106	224
903	225
1191	225
1192	225
1193	225
1194	225
21	225
1164	226
1195	226
314	226
46	226
670	226
20	226
21	226
1196	227
1198	227
1197	227
1154	227
1199	227
822	227
230	227
914	227
1200	227
20	227
21	227
910	228
37	228
38	228
1201	228
1202	228
1203	228
1204	228
174	229
1205	229
778	229
1206	229
1207	229
37	229
21	229
20	229
46	229
903	230
1208	230
1209	230
1210	230
1211	230
1212	230
20	230
21	230
1213	230
1157	231
1214	231
1215	231
1216	231
1217	231
1218	231
20	231
21	231
925	232
1219	232
1220	232
647	232
18	232
1062	232
21	232
20	232
1154	233
1221	233
1222	233
1223	234
1224	234
1226	234
20	234
1225	234
21	234
534	234
1154	235
1227	235
1228	235
1229	235
1154	236
1230	236
291	236
1231	236
1232	236
59	236
20	236
21	236
1187	237
1233	237
1234	237
59	237
1235	237
1236	237
21	237
20	237
1154	238
1237	238
1238	238
55	238
1239	238
1242	239
1241	239
512	239
1240	239
1243	239
21	239
20	239
1244	240
1245	240
1246	240
1066	240
509	240
1247	240
1094	240
105	240
96	240
20	240
21	240
1154	241
1248	241
174	241
1249	241
1250	241
444	241
46	241
291	241
1251	241
1256	242
1254	242
1253	242
1255	242
1252	242
74	242
1257	242
59	242
1258	242
20	242
21	242
1154	243
1066	243
1178	243
565	243
1182	243
1259	243
1260	243
1261	243
1262	243
1263	243
20	243
21	243
1265	244
1264	244
55	244
1266	244
1267	244
419	244
1268	244
769	244
1066	244
599	244
20	244
21	244
1154	245
1269	245
111	245
1270	245
105	245
20	245
21	245
1154	246
1271	246
1272	246
1273	246
59	246
38	246
20	246
1274	246
1275	246
1276	246
1154	247
1277	247
1278	247
1279	247
1280	247
1281	247
20	247
21	247
1282	247
1283	248
1284	248
1285	248
1286	248
176	248
1287	248
58	248
560	248
1288	248
20	248
21	248
1220	249
1289	249
1290	249
444	249
46	249
20	249
21	249
78	249
620	249
1291	250
1154	250
1292	250
677	250
89	250
1293	250
21	250
20	250
1295	251
1294	251
724	251
1296	251
1297	251
1298	251
206	251
1299	251
46	251
20	251
21	251
1154	252
1300	252
681	252
1301	252
1302	252
1303	252
717	252
1154	253
1304	253
1305	253
1306	253
89	253
582	253
509	253
67	253
394	253
1307	254
776	254
72	254
1308	254
1309	254
444	254
20	254
21	254
641	254
1310	255
1311	255
36	255
1312	255
33	255
1313	255
174	256
1314	256
1315	256
37	256
109	256
529	256
1299	256
592	256
1159	256
1316	256
29	256
20	256
21	256
447	256
1317	257
463	257
72	257
108	257
174	257
46	257
20	257
21	257
1318	258
1319	258
1320	258
174	258
118	258
1129	258
20	258
21	258
1164	259
26	259
1321	259
174	259
473	259
1322	259
55	259
1324	260
560	260
1323	260
59	260
18	260
20	260
21	260
1325	261
36	261
1326	261
633	261
1327	261
1328	261
1329	261
1330	261
868	261
1331	261
1332	262
1333	262
1334	262
1335	262
463	262
1336	262
174	262
1337	262
305	262
399	262
1338	262
106	262
1339	262
1340	262
20	262
21	262
265	263
1343	263
1341	263
1342	263
1344	263
1345	263
1346	263
21	263
29	263
74	263
1242	263
1347	264
1348	264
1349	264
1350	264
1351	264
55	264
1352	264
1353	264
952	264
935	264
942	264
1062	264
123	264
1354	264
646	264
361	264
1355	265
1356	265
1357	265
1066	265
509	265
1358	265
1359	265
1360	266
1361	266
1362	266
1363	266
1364	266
1365	266
20	266
21	266
1366	267
1367	267
36	267
1066	267
1368	267
175	267
569	267
42	267
1369	268
1370	268
174	268
585	268
401	268
1371	268
1073	268
1372	268
1373	268
38	268
39	268
1374	269
1375	269
308	269
174	269
46	269
21	269
20	269
1376	270
1377	270
951	270
36	270
1378	270
827	270
1379	270
1381	271
1380	271
550	271
1382	271
1383	271
1384	271
1385	271
1386	271
1387	271
298	271
1388	271
112	271
1389	271
21	271
1393	272
42	272
1391	272
1396	272
1394	272
1390	272
1392	272
1395	272
79	272
1397	272
1398	272
1399	272
787	272
646	272
1013	273
1400	273
371	273
1401	273
55	273
298	273
20	273
21	273
1268	273
1406	274
1407	274
1405	274
1402	274
1404	274
1024	274
1403	274
1408	274
1409	274
1410	274
1411	274
1412	274
569	274
21	274
20	274
1413	274
1414	274
1415	275
227	275
1072	275
951	275
560	275
569	275
1416	275
1417	275
1418	275
1062	275
38	275
39	275
1420	276
951	276
1419	276
1379	276
371	276
1073	276
1421	276
1261	276
89	276
97	276
21	276
20	276
1422	277
1423	277
1424	277
1425	277
952	277
1426	277
579	277
1427	277
298	277
1133	277
1428	277
1429	277
1430	278
1072	278
563	278
1431	278
46	278
1432	278
1062	278
497	279
1433	279
1377	279
174	279
74	279
1034	279
1434	279
112	279
512	279
291	279
1351	279
1435	279
283	279
1436	279
1062	279
1386	279
1437	280
563	280
1438	280
1439	280
1440	280
1441	280
952	280
1442	280
358	280
1443	280
1444	280
37	280
18	280
1445	280
298	280
787	280
314	280
20	280
21	280
1446	281
1447	281
1388	281
1448	281
1449	282
174	282
37	282
481	282
1450	282
1451	282
20	282
21	282
1452	282
308	282
46	282
419	282
512	283
1454	283
1453	283
646	283
399	283
76	283
20	283
21	283
298	284
1455	284
1456	284
36	284
291	284
1457	284
112	284
1458	284
38	284
39	284
1453	285
776	285
529	285
777	285
633	285
78	285
20	285
21	285
36	285
1459	285
1460	286
1461	286
77	286
1462	286
21	286
1463	286
112	286
1464	286
1066	286
1453	287
1465	287
155	287
174	287
668	287
444	287
79	287
1274	287
29	287
20	287
21	287
1347	288
1466	288
1467	288
1468	288
776	288
1469	288
1133	288
55	288
1073	288
78	288
499	288
304	288
20	288
21	288
46	288
1449	289
563	289
92	289
1470	289
1471	289
1472	289
816	289
1473	289
737	289
1066	289
20	289
21	289
1449	290
155	290
73	290
174	290
227	290
1372	290
1474	290
1475	290
1476	290
1477	290
109	290
1478	290
646	290
1129	290
1479	290
288	290
20	290
21	290
1480	290
787	290
1481	290
42	290
358	290
419	290
1482	291
308	291
1483	291
1072	291
1353	291
550	291
112	291
77	291
76	291
298	291
942	291
20	291
21	291
1484	292
1485	292
1486	292
112	292
1487	292
565	292
1488	292
105	292
1489	292
1490	292
298	292
787	292
1491	293
497	293
952	293
1492	293
174	293
1493	293
112	293
291	293
1494	293
1495	293
1066	293
77	293
20	293
21	293
293	293
1496	293
1497	293
1498	294
1499	294
419	294
444	294
1500	294
509	294
1501	294
314	294
20	294
21	294
1503	295
1502	295
55	295
484	295
1062	295
1504	295
291	295
20	295
21	295
1505	296
951	296
1506	296
174	296
227	296
1507	296
1508	296
1509	296
1510	296
1511	297
1512	297
1513	297
1514	297
1495	297
20	297
21	297
1515	297
76	297
1516	297
1517	297
497	298
848	298
1518	298
1519	298
1520	298
1521	298
951	299
848	299
33	299
1522	299
1523	299
1524	300
1525	300
206	300
1526	300
1527	300
1528	300
1529	300
883	300
1530	300
39	300
174	301
1532	301
1531	301
419	301
20	301
21	301
46	301
1533	301
1534	302
82	302
1535	302
512	302
174	302
87	302
737	302
20	302
21	302
1537	303
72	303
1536	303
36	303
37	303
1538	303
419	303
1539	303
20	303
21	303
1540	304
1541	304
36	304
37	304
1542	304
304	304
1020	304
46	304
1543	304
20	304
21	304
1544	305
624	305
1545	305
1546	305
1547	305
21	305
1548	306
777	306
1549	306
1550	306
176	306
37	307
1551	307
1552	307
1553	307
69	307
20	307
21	307
1554	308
89	308
1555	308
1556	308
902	308
1221	308
20	308
21	308
1554	309
580	309
1557	309
1515	309
769	309
1558	310
1559	310
1560	310
122	310
46	310
20	310
21	310
78	310
79	310
1561	311
1562	311
777	311
174	311
1563	311
1564	311
20	311
21	311
1565	312
1566	312
1567	312
1568	312
1569	312
1570	312
1571	312
499	312
1572	312
112	312
1573	313
1574	313
1575	313
237	313
20	313
21	313
46	313
917	314
1576	314
1577	314
1451	314
1420	314
174	314
78	314
79	314
1578	314
1579	314
1580	315
1072	315
1581	315
1582	315
1583	315
1584	315
513	315
1585	315
1586	315
1216	315
1587	315
167	315
21	315
1588	316
463	316
1589	316
1590	316
174	316
1477	316
1020	316
1508	316
1591	316
78	316
1592	316
1053	316
499	316
20	316
21	316
1593	316
1594	317
1595	317
1377	317
401	317
1420	317
1596	317
37	317
1597	317
361	317
1598	317
1599	318
1600	318
1072	318
1591	318
155	318
1601	318
1231	318
1602	318
1603	318
21	318
1389	318
563	319
1377	319
1604	319
1605	319
112	319
1420	319
500	319
1606	319
1607	319
1608	319
1609	319
1610	319
1611	320
1612	320
139	320
58	320
1613	320
112	320
39	320
1614	320
1615	320
1616	321
1617	321
1618	321
72	321
1619	321
139	321
37	321
358	321
1620	321
1591	321
1621	321
1622	321
1623	321
1085	321
21	321
425	322
206	322
1624	322
1625	322
1091	322
927	322
1626	322
1627	322
1628	322
1629	322
1630	322
1631	323
1632	323
72	323
1633	323
602	323
37	323
47	323
1634	323
1635	323
1636	323
1637	323
677	323
1638	323
21	323
1639	324
96	324
1640	324
36	324
1641	324
1642	324
1178	324
1643	324
1644	324
20	324
21	324
1645	325
1647	325
41	325
1646	325
1648	325
38	325
39	325
1639	326
1649	326
105	326
1650	326
1651	326
20	326
21	326
1652	327
1653	327
1654	327
1655	327
1656	327
36	327
37	327
1657	327
1658	327
1659	327
304	327
79	327
1660	327
1661	327
1662	327
20	327
868	327
1663	328
1664	328
1665	328
196	328
1666	328
1667	328
1668	328
1669	328
1670	328
1671	328
20	328
21	328
1672	329
1501	329
174	329
1673	329
1674	329
1675	329
1676	329
1677	330
463	330
36	330
776	330
227	330
1678	330
1679	330
46	330
20	330
21	330
1680	330
1681	330
1682	331
174	331
109	331
1683	331
1684	331
38	331
699	331
1685	332
1686	332
74	332
1177	332
1687	332
1688	332
21	332
1639	333
778	333
184	333
1689	333
58	333
1690	333
499	333
42	333
1691	333
1692	333
1693	333
97	333
677	333
1066	333
20	333
21	333
174	334
1694	334
1695	334
777	334
529	334
1696	334
77	334
1697	335
1698	335
44	335
1699	335
1700	335
1701	335
1702	335
1703	335
1704	335
1705	335
1706	335
1707	335
1708	336
1709	336
1710	336
1712	337
1717	337
1714	337
1716	337
1715	337
1713	337
1711	337
1718	338
1348	338
1643	338
36	338
848	338
1719	338
74	338
1720	338
112	338
1721	338
87	338
1722	338
20	338
21	338
1723	339
1724	339
139	339
37	339
1725	339
1216	339
1726	339
96	339
1727	340
74	340
1728	340
46	340
1729	340
1730	340
1731	340
1732	340
1733	340
1734	340
1695	341
776	341
1735	341
1736	341
1737	342
1694	342
156	342
37	342
89	342
1738	342
46	342
21	342
20	342
1739	343
871	343
1674	343
1740	343
1005	343
1741	343
160	343
1742	343
1743	343
1744	343
1745	344
1746	344
1741	344
1747	344
308	344
1748	344
1749	344
733	344
1750	344
20	344
21	344
1752	345
1751	345
36	345
1753	345
1754	345
21	345
1755	345
1756	346
1757	346
1758	346
1759	346
36	346
1760	346
1761	346
1762	346
20	346
21	346
277	346
196	346
563	347
1763	347
1764	347
74	347
36	347
46	347
1011	347
20	347
21	347
112	347
1506	347
1395	347
1765	347
1216	348
1766	348
1767	348
1768	348
1769	348
1770	348
1771	348
1772	348
1288	348
1773	348
1774	348
1775	349
1776	349
1777	349
1778	349
668	349
1779	349
1780	349
1781	349
79	349
1782	349
1783	350
23	350
1784	350
1785	351
1786	351
529	351
236	351
1787	351
382	351
21	351
1788	352
78	352
641	352
1789	352
23	352
1790	352
1791	352
1792	352
1793	352
1794	352
299	352
1795	352
1796	352
174	352
1797	352
155	352
46	352
1798	353
1379	353
176	353
1799	353
1800	353
74	353
21	353
1801	354
952	354
497	354
1802	354
174	354
1803	354
798	354
46	354
20	354
21	354
1804	355
1645	355
336	355
1805	355
1806	355
795	355
1807	355
1808	355
1809	355
86	355
155	356
1810	356
1812	356
1811	356
1813	356
1814	356
112	356
1815	356
1816	356
1817	356
1818	356
1819	356
1820	356
288	356
105	356
237	357
1821	357
1822	357
36	357
1823	357
46	357
79	357
78	357
20	357
21	357
1824	358
1825	358
1828	358
951	358
1827	358
1826	358
845	358
78	358
79	358
1829	358
1830	358
1831	358
1832	358
46	358
1833	358
21	358
1834	358
1835	358
1836	359
1837	359
1838	359
1617	359
1839	359
20	359
21	359
174	359
497	359
37	359
1840	359
1841	359
499	359
1842	359
1844	360
1843	360
790	360
1845	360
1062	360
677	360
951	360
174	360
1760	360
42	360
74	360
1846	360
1847	361
602	361
1848	361
254	361
96	361
87	362
499	362
1849	362
206	362
1850	362
1851	362
20	362
21	362
1852	362
1853	362
33	363
1854	363
1855	363
36	363
1856	363
1857	363
106	363
1858	363
21	363
20	363
1859	364
36	364
1860	364
1861	364
668	364
79	364
78	364
1862	364
1863	364
20	364
21	364
37	364
563	364
1865	365
55	365
1864	365
441	365
20	365
21	365
1866	366
23	366
254	366
1671	366
20	366
21	366
1867	367
155	367
826	367
1868	367
1869	367
1870	367
1871	367
1858	367
1442	367
1872	367
1062	367
1873	368
174	368
1394	368
852	368
79	368
78	368
1874	368
20	368
21	368
106	368
1639	369
1875	369
1876	369
1877	369
1878	369
1879	369
1880	369
1881	369
1882	370
1883	370
1741	370
1884	370
1885	370
1886	370
1887	370
1888	370
942	370
513	370
20	370
21	370
336	371
1892	371
1890	371
1889	371
1891	371
1893	371
646	371
1899	372
1895	372
1894	372
1897	372
1896	372
1901	372
78	372
1900	372
1898	372
79	372
1902	372
1903	373
72	373
951	373
36	373
37	373
1904	373
1905	373
1906	373
40	373
489	373
1907	373
1908	374
1909	374
1910	374
1911	374
1912	374
1913	374
1914	374
791	375
563	375
174	375
37	375
1915	375
1916	375
20	375
21	375
1917	375
1918	375
777	376
1919	376
1920	376
1921	376
314	376
1922	376
1923	376
1924	376
1925	376
1926	376
20	376
21	376
791	377
1927	377
1928	377
36	377
37	377
190	377
1929	377
1930	377
1931	377
1932	377
1933	377
321	378
1934	378
1935	378
1936	378
1937	378
1938	379
797	379
1586	379
1939	379
1940	379
1941	379
1942	379
1943	379
20	379
174	380
1944	380
1945	380
1261	380
111	380
1946	380
1947	380
1140	380
1948	380
1789	380
21	380
20	380
1949	381
662	381
1950	381
1951	381
174	381
1952	381
1953	381
1954	381
1832	381
293	381
20	381
21	381
1955	382
1956	382
1957	382
1958	382
1959	382
1005	382
1960	382
1961	382
87	382
1962	382
1591	382
1909	383
89	383
174	383
1963	383
497	383
1964	383
1965	384
1327	384
1966	384
1967	384
21	384
76	384
1968	384
1969	384
1970	385
167	385
1971	385
504	385
1972	385
481	385
1973	385
20	385
21	385
397	385
1974	386
23	386
37	386
1975	386
1976	386
168	386
299	386
1977	386
668	386
1978	386
513	386
174	387
1979	387
1980	387
1981	387
1982	387
1983	387
1984	387
1985	387
174	388
1986	388
1987	388
509	388
1988	388
59	388
1989	388
382	388
21	388
1990	388
74	388
304	388
1991	389
36	389
227	389
497	389
1992	389
1993	389
668	389
1994	389
1328	390
952	390
174	390
497	390
1995	390
534	390
304	390
20	390
21	390
1996	390
1997	391
1998	391
696	391
1999	391
2000	391
2001	391
106	391
20	391
21	391
2002	392
2003	392
2004	392
646	392
2005	392
2006	392
2007	392
1121	392
1130	392
610	392
1142	392
2008	393
174	393
2009	393
2010	393
2011	393
2012	393
2013	393
199	393
21	393
2014	394
2015	394
167	394
444	394
465	394
443	394
2016	394
2017	394
2018	395
2019	395
2020	395
89	395
2021	395
2022	395
2023	395
269	395
2024	395
20	395
21	395
2025	396
463	396
952	396
1814	396
320	396
129	396
1165	396
2026	396
2027	396
2028	396
372	396
138	396
2029	396
2030	397
1426	397
550	397
2031	397
569	397
176	397
2032	397
2033	397
89	397
2034	397
2029	397
2021	398
1187	398
2035	398
2036	398
2037	398
1167	398
291	398
38	398
39	398
2039	399
2041	399
381	399
2038	399
2040	399
2042	399
230	399
2043	400
2044	400
2045	400
2046	400
1597	400
388	400
78	400
443	400
444	400
2047	400
20	400
21	400
2048	400
2049	400
2050	400
2052	401
105	401
2051	401
1489	401
381	401
20	401
21	401
29	401
2053	401
2029	402
2054	402
2055	402
2056	402
2057	402
187	402
2058	402
1041	402
227	402
419	402
174	403
2060	403
2059	403
2061	403
2062	403
2063	403
2064	403
2065	403
20	403
2066	403
497	404
2067	404
2068	404
174	404
112	404
2069	404
790	404
1416	404
1384	404
441	404
774	404
2070	404
20	404
21	404
29	404
2071	404
2072	404
2073	404
2074	405
2075	405
2076	405
89	405
2077	405
2078	405
209	406
2079	406
2080	406
55	406
1133	406
381	406
2081	406
2082	406
2083	406
2084	406
2085	406
46	406
2074	406
2088	407
2089	407
2086	407
827	407
2087	407
1062	407
582	407
973	407
20	407
21	407
2090	408
2091	408
2062	408
2092	408
2093	408
2094	408
2095	408
20	408
21	408
444	408
419	408
469	408
2096	409
1187	409
2097	409
2098	409
2099	409
399	409
2100	409
283	409
1428	409
534	409
20	409
29	409
2101	409
2103	410
2102	410
2104	410
2105	410
1066	411
2108	411
2106	411
2107	411
2109	411
2110	411
2112	412
1416	412
2111	412
2113	412
677	412
952	412
2114	412
2115	412
2116	412
509	412
105	412
1489	412
2093	412
2117	412
2118	412
20	412
21	412
29	412
2101	413
2119	413
2120	413
174	413
1590	413
1945	413
1589	413
2121	413
112	413
1814	413
209	413
2122	413
2123	413
646	413
425	413
1372	413
20	413
21	413
2124	414
2125	414
2126	414
848	414
463	414
291	414
2127	414
106	414
1312	414
20	414
21	414
36	415
2128	415
2131	415
2130	415
2129	415
2132	415
20	415
21	415
1676	415
76	415
2133	416
2134	416
2135	416
1024	416
20	416
21	416
2136	417
2137	417
288	417
2138	417
1915	417
2139	418
703	418
2140	418
2141	418
2142	418
2143	418
46	418
2146	419
443	419
2145	419
2144	419
523	419
77	419
20	419
21	419
2139	420
2147	420
2148	420
105	420
106	421
174	421
2149	421
2150	421
2151	421
20	421
21	421
262	422
2152	422
23	422
210	422
425	422
2153	422
291	423
2154	423
2155	423
737	423
2139	424
790	424
174	424
62	424
668	424
2156	424
20	424
21	424
78	424
79	424
2157	424
2158	424
2139	425
2159	425
2160	425
2161	425
139	425
20	425
21	425
2162	426
2163	426
2164	426
269	426
20	426
2166	427
2165	427
302	427
87	427
1066	427
20	427
21	427
2170	428
2175	428
2167	428
1072	428
2168	428
2169	428
2173	428
2174	428
2172	428
2171	428
1231	428
1493	428
2176	428
2177	428
2178	428
2179	428
2180	428
2181	428
2182	428
2183	428
1828	428
2184	428
774	428
2185	428
434	428
2186	429
36	429
744	429
2187	429
60	429
58	429
2188	429
2189	429
2190	429
2191	429
106	430
2192	430
2193	430
2194	430
2195	430
2196	430
2197	430
358	430
2198	430
382	430
21	430
2199	430
2200	430
2201	430
2202	430
2203	430
2204	431
397	431
23	431
2205	431
2206	431
550	431
112	431
2207	431
106	431
2208	431
20	431
21	431
2210	432
2209	432
39	432
38	432
2211	432
59	432
55	432
509	432
2212	432
230	433
2213	433
2214	433
2215	433
1597	434
2216	434
463	434
952	434
1420	434
174	434
29	434
769	434
2217	434
677	434
859	434
1121	434
587	434
2218	435
2219	435
1694	435
2220	435
951	435
36	435
1477	435
2221	435
1462	435
20	435
21	435
2222	435
2223	436
497	436
174	436
952	436
2224	436
2225	436
2226	437
2227	437
2228	437
1072	437
758	437
2229	437
2230	437
665	437
2231	437
2232	438
2233	438
2234	438
2235	438
2236	438
2237	438
2238	438
2239	438
2240	438
2241	439
74	439
670	439
2242	439
2243	439
20	439
21	439
2244	439
2245	439
97	439
2246	440
812	440
21	440
2247	440
199	440
2248	440
2249	440
592	440
2250	440
2251	440
2252	440
2253	440
2254	440
2255	440
1010	440
2256	441
509	441
2257	441
419	441
174	441
2258	441
2259	441
1034	441
2260	441
2261	441
2262	441
112	441
2263	441
1953	441
2264	441
2265	441
2266	441
2267	441
2268	442
174	442
74	442
59	442
509	442
2269	442
2270	442
21	442
20	442
2271	442
112	442
237	442
230	442
2272	442
2273	442
2274	443
2275	443
509	443
2276	443
2277	443
29	443
20	443
21	443
2278	444
2279	444
509	444
180	444
2280	444
21	444
109	444
336	444
168	444
2281	444
20	444
2282	445
434	445
174	445
74	445
2283	445
646	445
2284	445
812	445
2285	445
20	445
21	445
2286	446
129	446
37	446
1450	446
2287	446
21	446
20	446
2288	446
2290	447
2289	447
1858	447
2291	447
174	447
2292	447
2293	447
20	447
21	447
2294	448
2295	448
36	448
509	448
358	448
2296	448
1501	448
1878	448
411	448
1861	448
587	448
436	448
20	448
21	448
2297	448
2298	448
2299	448
2300	448
2301	448
369	448
29	448
2302	448
2303	448
2304	448
2271	448
1274	448
79	448
78	448
2305	448
2306	448
2307	449
2308	449
2309	449
2310	449
87	449
1133	449
2311	449
2312	449
2313	449
2314	450
1242	450
2315	450
174	450
1617	450
2316	451
668	451
509	451
1495	451
2317	451
2318	451
2319	451
2320	451
2321	451
20	451
21	451
2322	452
403	452
758	452
668	452
1328	452
46	452
2323	452
2324	452
20	452
21	452
2325	452
2326	453
2327	453
509	453
37	453
237	453
2328	454
2329	454
2330	454
1501	454
2331	454
227	455
2332	455
2258	455
2333	455
2334	455
779	455
77	455
29	455
20	455
2337	456
2336	456
509	456
2335	456
2338	456
76	456
20	456
21	456
2343	457
2346	457
2340	457
2341	457
2344	457
2342	457
2345	457
2339	457
2347	458
2348	458
2349	458
2280	458
2350	458
2351	458
698	458
2352	458
1911	458
2353	458
2245	458
369	458
399	458
2354	458
2355	458
330	458
2205	458
1808	458
23	458
1012	458
2010	458
419	458
2358	459
2357	459
2356	459
37	459
2359	459
394	459
2360	459
176	459
2157	459
20	459
21	459
72	459
227	459
106	459
174	460
2361	460
1014	460
2362	460
729	460
2363	460
2364	460
2365	460
2366	460
76	460
2367	461
1219	461
870	461
419	461
76	461
2368	461
2369	461
2370	461
20	461
21	461
1645	462
529	462
777	462
2371	462
369	462
2372	462
2373	462
174	462
37	462
399	462
51	462
187	462
2377	463
2376	463
2375	463
2378	463
2374	463
20	463
21	463
76	463
2379	463
96	463
2380	463
133	463
1741	463
2381	463
2382	463
2383	463
2384	463
2385	463
702	464
174	464
2386	464
509	464
20	464
21	464
2387	464
2028	464
2388	464
1458	464
702	465
849	465
2389	465
2390	465
174	465
74	465
59	465
20	465
21	465
463	466
1596	466
174	466
1254	466
2391	466
2392	466
2393	466
509	466
21	466
20	466
46	466
1631	467
2394	467
2395	467
2396	467
270	467
677	467
737	467
20	467
21	467
174	468
2397	468
74	468
2398	468
1929	468
2399	468
2353	468
2400	468
20	468
21	468
76	468
330	468
36	468
37	468
358	468
2401	468
106	468
2402	468
86	468
2403	468
1631	469
2404	469
2405	469
74	469
227	469
174	469
509	469
512	469
373	469
2406	469
20	469
21	469
97	469
2409	470
2410	470
20	470
2408	470
2407	470
21	470
2132	470
187	470
46	470
2221	470
2411	471
671	471
2412	471
74	471
2413	471
382	471
2414	471
76	471
77	471
2297	472
36	472
74	472
2415	472
2416	472
1442	472
2417	472
20	472
21	472
2418	473
74	473
36	473
2419	473
2420	473
2421	473
610	473
2422	473
2030	474
2423	474
2424	474
2425	474
2426	474
2427	474
397	475
2428	475
859	475
1062	475
2429	475
302	475
20	475
21	475
42	475
2431	476
2430	476
463	476
174	476
74	476
1401	476
2432	476
112	476
20	476
21	476
29	476
2433	476
2434	477
2435	477
2276	477
2277	477
859	477
20	477
21	477
270	477
1062	477
2436	477
2437	478
441	478
2438	478
38	478
39	478
2439	478
2440	478
2441	478
42	478
2442	478
1034	478
174	478
2443	479
2444	479
36	479
744	479
2445	479
2446	479
2359	479
2089	479
859	479
399	479
2099	479
20	479
21	479
174	480
262	480
677	480
2447	480
2448	480
509	480
570	480
20	480
21	480
29	480
2449	481
727	481
2381	481
509	481
2450	481
237	481
419	481
20	481
21	481
2282	482
2451	482
2452	482
2453	482
2211	482
174	482
2454	482
46	482
2455	482
21	482
1695	483
2456	483
2457	483
2458	483
2459	483
509	483
2460	483
2262	483
230	483
2461	483
46	483
2462	484
702	484
2463	484
2464	484
1621	484
2465	484
2466	484
2467	484
138	484
2468	484
174	485
2469	485
74	485
369	485
2470	485
20	485
21	485
419	485
2471	485
2472	485
2473	485
59	485
2474	486
859	486
2475	486
295	486
641	486
419	486
2476	486
174	486
37	486
20	486
21	486
46	486
2477	487
509	487
2478	487
2479	487
2480	487
2481	487
2482	487
1911	487
2483	487
1066	487
2484	487
2397	488
2485	488
336	488
2486	488
74	488
646	488
2487	489
759	489
2488	489
74	489
106	489
2489	489
2490	490
2449	490
2491	490
671	490
509	490
20	490
21	490
2492	490
2493	491
2494	491
2495	491
646	491
2496	491
2364	491
237	491
21	491
20	491
702	492
2497	492
298	492
2498	492
381	492
578	492
2499	492
2500	492
499	492
2501	492
408	492
394	492
20	492
21	492
2282	493
2502	493
2503	493
2504	493
2505	493
2506	493
2507	493
369	493
2508	493
2509	493
2510	494
2511	494
139	494
41	494
2512	494
2513	494
260	494
20	494
2514	495
2515	495
2295	495
2516	495
646	495
174	495
20	495
21	495
174	496
2517	496
633	496
72	496
58	496
2518	496
2519	496
46	496
2520	497
724	497
509	497
2521	497
74	497
2522	497
36	497
777	497
2523	497
196	497
2524	497
641	497
20	497
21	497
2527	498
2525	498
2526	498
914	498
2528	498
299	498
20	498
21	498
46	498
710	498
291	498
2529	499
2530	499
36	499
2531	499
2532	499
2533	499
299	499
20	499
21	499
2536	500
2534	500
196	500
2535	500
2537	500
2538	500
210	500
1977	500
2539	500
2540	500
2541	500
2542	501
72	501
174	501
74	501
2543	501
677	501
38	501
39	501
328	502
2544	502
2545	502
2546	502
1971	502
827	502
213	502
59	502
2547	502
2548	502
419	502
2550	503
2556	503
2555	503
2554	503
112	503
2551	503
2552	503
2553	503
2549	503
2557	503
2558	503
756	503
174	503
20	503
21	503
76	503
2559	504
1689	504
2560	504
2561	504
2562	504
2563	504
2564	504
2565	504
20	504
21	504
2567	505
2569	505
2568	505
2566	505
58	505
2570	505
2571	505
20	505
21	505
182	505
2572	505
2573	505
2574	505
2575	505
2578	506
2576	506
2579	506
2577	506
1062	506
2580	506
403	506
411	506
2581	506
20	506
21	506
2582	506
2583	507
2584	507
2585	507
36	507
37	507
20	507
21	507
304	507
46	507
2586	507
2587	507
2588	508
2185	508
710	508
2589	508
2590	508
2591	508
2592	508
2593	508
2594	508
2595	508
579	508
20	508
21	508
2596	509
2597	509
2598	510
2599	510
1425	510
381	510
399	510
582	510
37	510
2600	510
2601	510
2602	511
2039	511
2603	511
2604	511
2605	511
2606	511
1662	511
2607	511
2608	511
1495	511
1289	511
419	511
2609	511
21	511
20	511
775	512
848	512
633	512
777	512
2610	512
2611	512
2612	512
2613	512
20	512
21	512
304	512
187	512
775	513
2614	513
2615	513
2616	513
174	513
509	513
807	514
266	514
2617	514
2618	514
89	514
509	514
230	514
20	514
21	514
2619	514
2620	514
775	515
2621	515
262	515
827	515
2622	515
314	515
239	515
20	515
2136	516
2623	516
262	516
2624	516
2625	516
783	517
807	517
174	517
37	517
776	517
463	517
1391	517
46	517
1234	517
20	517
21	517
2626	517
2627	517
1419	518
2628	518
859	518
2629	518
2630	518
2631	518
20	518
21	518
585	519
2026	519
1401	519
580	519
36	519
2633	520
684	520
2632	520
28	520
2634	520
2635	520
2636	520
175	520
2637	520
1091	520
1416	520
20	520
21	520
2638	521
2640	521
2641	521
641	521
2639	521
46	521
2642	521
20	521
21	521
2643	522
397	522
702	522
36	522
2644	522
38	522
39	522
2645	522
2646	522
2647	523
2648	523
2649	523
2650	523
2651	523
174	523
46	523
20	523
21	523
592	523
2321	523
623	523
1773	523
925	523
2654	524
509	524
2652	524
2653	524
174	524
2655	524
2656	524
2657	524
2565	524
2658	524
352	524
21	524
425	524
2659	525
174	525
2660	525
419	525
509	525
20	525
21	525
2633	525
2661	525
2185	525
2662	526
89	526
74	526
2663	526
2618	526
807	527
1157	527
2664	527
298	527
2665	528
1533	528
509	528
2666	528
2667	528
2668	528
2669	528
382	528
2633	528
2670	528
2671	528
2672	528
41	528
2673	529
2674	529
1034	529
58	529
20	529
21	529
106	529
2675	529
2633	529
670	530
2676	530
640	530
848	530
775	530
1362	530
20	530
21	530
2677	531
633	531
848	531
109	531
2678	531
2679	531
20	531
21	531
46	531
2680	532
2681	532
509	532
2057	532
2682	532
1034	532
37	532
196	532
469	532
20	532
21	532
2633	533
651	533
2683	533
2684	533
18	533
2323	533
509	533
522	533
21	533
20	533
2685	534
2686	534
2687	534
2688	534
807	535
2689	535
2690	535
2691	535
2692	535
2693	535
1877	535
2694	535
2695	535
2696	535
2697	535
775	536
2698	536
2699	536
2700	536
46	536
21	536
2702	537
2701	537
2704	537
2703	537
2705	538
2706	538
597	538
2707	538
277	538
106	538
21	538
20	538
2708	539
2709	539
403	539
2710	539
2711	539
120	539
1911	539
2712	539
1145	540
1416	540
254	540
2713	540
2714	540
2715	540
2716	541
2717	541
2629	541
79	541
2718	541
21	541
20	541
282	542
2719	542
1547	542
2720	542
38	542
39	542
779	542
2721	542
2725	543
2724	543
2723	543
942	543
2726	543
2722	543
18	543
2727	544
573	544
480	544
441	544
787	544
55	544
2728	544
2730	545
2729	545
403	545
2731	545
82	545
512	545
2732	545
112	545
1034	545
2733	545
2734	545
38	545
39	545
2735	546
2736	546
485	546
46	546
2738	547
2739	547
2476	547
2737	547
2740	547
174	547
20	547
21	547
2741	548
2742	548
2743	548
2612	548
690	548
174	548
20	548
21	548
2745	549
2744	549
1012	549
602	549
37	549
1977	549
485	549
2746	549
2626	549
20	549
21	549
2747	549
2753	550
2755	550
38	550
2748	550
2754	550
2749	550
2751	550
2750	550
2752	550
39	550
540	551
2756	551
1107	551
299	551
798	551
106	551
167	551
2757	552
2758	552
58	552
2759	552
2760	552
2761	552
2762	552
106	552
2763	553
2764	553
2765	553
2766	553
2767	553
2768	553
2769	553
2268	554
2770	554
2771	554
2772	554
20	554
21	554
2773	555
2774	555
1395	555
951	555
2160	555
2775	555
489	555
360	555
2047	555
78	555
2776	555
419	555
769	555
274	555
174	556
2777	556
37	556
38	556
39	556
2778	556
2779	556
40	556
2780	556
2781	556
2782	557
787	557
2783	557
2784	558
2785	558
1327	558
37	558
174	558
20	558
21	558
917	558
46	558
776	559
2786	559
2787	559
2788	559
109	559
46	559
21	559
2790	560
777	560
2789	560
1012	560
2791	560
1805	560
2792	560
2793	560
2794	560
304	560
361	560
20	560
2795	561
567	561
314	561
2796	561
20	561
21	561
2797	562
550	562
527	562
1977	562
2798	562
2799	562
2800	562
21	562
2801	563
2802	563
2803	563
2185	563
2804	563
2805	563
2806	563
1929	563
2807	563
2808	563
2809	563
2810	563
2811	563
2812	563
1265	563
21	563
20	563
23	564
2813	564
33	564
690	564
1662	564
2814	564
20	564
21	564
2815	565
36	565
109	565
176	565
2518	565
965	565
2816	565
20	565
21	565
2819	566
2818	566
176	566
2817	566
210	566
2820	566
20	566
21	566
2192	567
2821	567
2753	567
74	567
2822	567
2823	567
2824	567
2825	567
2826	567
78	567
2827	567
358	567
2828	567
2829	567
2832	568
2830	568
89	568
2831	568
37	568
2833	568
2834	568
254	568
2835	568
46	568
21	568
2836	568
227	568
2837	568
2838	568
1960	568
2839	568
2840	568
2841	569
139	569
509	569
1384	569
2842	569
2843	569
2844	569
167	569
2845	569
2846	569
2847	569
1258	569
2848	569
2849	570
2850	570
2851	570
2852	570
2853	570
2854	570
2855	570
2856	570
1740	570
1631	570
2857	570
2858	570
20	570
21	570
2859	570
2860	571
1933	571
228	571
87	571
20	571
74	571
2861	571
529	571
777	571
2862	572
2863	572
2864	572
737	572
2865	572
21	572
2741	573
2866	573
2867	573
529	573
777	573
36	573
848	573
633	573
2868	574
2869	574
2776	574
46	574
2870	575
36	575
2871	575
1320	575
2872	575
2873	575
1373	575
1247	575
2874	575
20	575
21	575
2875	576
2753	576
2354	576
2876	576
58	576
2878	577
2514	577
2877	577
2879	577
2880	577
46	577
942	577
1510	577
2881	578
572	578
2882	578
20	578
21	578
2883	578
2884	578
2885	578
1279	578
2886	578
2887	579
602	579
74	579
2423	579
2888	579
1420	579
2889	579
2890	580
662	580
2891	580
2892	580
2893	580
2894	580
2895	580
2896	580
2897	580
20	580
21	580
2898	581
777	581
529	581
859	581
2899	581
20	581
21	581
2900	582
106	582
2901	582
1906	582
2902	582
2745	582
737	582
2903	582
1387	582
2904	583
46	583
2906	583
2905	583
2907	584
44	584
512	584
2389	584
1287	584
2908	584
2909	584
96	584
1911	584
2146	585
2910	585
2911	585
2912	585
578	585
2913	586
2859	586
2305	586
560	586
513	586
20	586
21	586
2914	587
2915	587
2916	587
2917	587
2918	587
1357	587
211	587
1163	587
2920	588
2919	588
106	588
2921	588
72	589
2922	589
242	589
2923	589
59	589
291	589
2924	589
21	589
2925	589
2926	589
744	589
2927	589
2928	589
2929	589
2930	589
2931	589
903	590
2932	590
777	590
2185	590
288	590
2933	590
2934	590
2935	591
1678	591
2936	591
513	591
560	591
345	591
2937	591
460	591
29	591
2938	591
2939	592
1960	592
2940	592
2941	592
2942	592
631	592
2943	592
2944	592
248	592
2945	592
1164	593
2837	593
2946	593
744	593
2947	593
2948	593
2949	593
20	593
21	593
2952	594
2951	594
46	594
2950	594
2953	594
2954	594
2955	594
2956	594
2957	594
2958	595
777	595
2959	595
2960	595
2961	595
2962	595
2963	595
2964	595
38	595
2965	595
903	596
2636	596
2966	596
174	596
74	596
641	596
46	596
21	596
20	596
2967	597
529	597
777	597
1012	597
174	597
37	597
2968	597
610	597
46	597
20	597
21	597
1154	598
2969	598
2970	598
2192	598
179	598
1621	598
20	598
21	598
1264	599
174	599
2971	599
942	599
2972	599
210	599
20	599
21	599
910	600
811	600
2973	600
2974	600
21	600
20	600
1154	601
2975	601
2644	601
2192	601
2976	601
942	601
67	601
2418	602
2977	602
46	602
2192	602
2978	602
20	602
21	602
2979	602
2980	603
36	603
2180	603
942	603
2981	603
2982	603
2983	603
20	603
21	603
2984	604
737	604
2985	604
485	604
2986	604
2987	604
2988	604
252	604
1389	604
21	604
2989	605
2990	605
2991	605
58	605
2992	605
2993	605
46	605
2994	606
513	606
36	606
117	606
33	606
2995	606
2996	606
2997	606
291	606
1167	606
21	606
2999	607
2998	607
3000	607
755	608
1179	608
46	608
3001	608
58	608
20	608
21	608
3002	609
3003	609
3004	609
3005	609
3006	609
3007	609
3008	610
1915	610
102	610
227	610
3009	610
3010	610
1062	610
153	610
3011	610
20	610
21	610
3012	611
3013	611
3014	611
1533	611
3015	611
3016	611
176	611
3017	611
2538	611
2927	611
1167	611
74	611
3018	611
3019	611
21	611
20	611
291	612
3020	612
777	612
1012	612
529	612
3021	612
3022	612
196	612
20	612
21	612
291	613
3023	613
111	613
3024	613
609	613
20	613
21	613
174	614
3025	614
3026	614
3027	614
3028	614
1621	614
3029	614
3030	614
21	614
3031	615
107	615
72	615
46	615
304	615
419	615
274	615
187	615
3032	615
20	615
21	615
2397	616
2399	616
277	616
3033	616
270	616
179	616
3034	616
3035	616
105	616
3036	616
20	616
21	616
2772	616
3037	617
3038	617
2901	617
236	617
3039	617
74	617
3040	618
2554	618
308	618
36	618
89	618
3041	618
20	618
21	618
3042	619
3043	619
277	619
20	619
3044	620
174	620
3045	620
74	620
3046	620
3047	620
3048	620
20	620
21	620
174	621
3050	621
3051	621
3049	621
235	621
3052	622
2360	622
46	622
20	622
21	622
3053	622
403	623
36	623
529	623
3054	623
3055	623
3056	623
21	623
20	623
3060	624
3061	624
3059	624
3058	624
3063	624
3062	624
3057	624
3064	625
74	625
3065	625
32	625
46	625
3066	625
3067	625
3068	626
1442	626
3070	626
3069	626
975	626
46	626
21	626
20	626
3072	627
3071	627
1072	627
106	627
3073	627
3074	627
3075	627
21	627
20	627
859	628
3076	628
3077	628
302	628
3078	628
3079	628
3080	628
729	628
254	628
1066	628
509	628
3081	628
3082	628
3083	629
3084	629
859	629
302	629
3085	629
3086	629
3087	629
2461	629
3088	629
3089	629
2706	630
3090	630
3091	630
3092	630
3093	630
3094	630
1117	630
3095	630
283	631
3096	631
3097	631
3098	631
3099	631
3100	631
3101	631
3102	631
3103	631
2149	632
3104	632
130	632
3105	632
3106	632
253	632
3107	632
1133	632
3108	632
3109	632
1073	632
816	632
3110	633
3111	633
3112	633
2706	633
3096	633
291	633
3113	633
3114	633
3115	633
3116	633
3117	633
816	633
2703	633
1130	633
3118	633
3119	634
3120	634
1384	634
3121	634
3122	634
3123	634
3124	634
3125	634
859	635
3126	635
3076	635
2099	635
3127	635
3128	635
298	635
3096	635
3129	635
3130	635
795	635
3131	635
2745	635
2745	636
3132	636
3133	636
497	636
902	636
3134	636
792	636
3135	636
3136	636
1167	636
210	636
38	636
39	636
3137	637
3138	637
3139	637
3140	637
3141	637
3142	637
1462	638
3143	638
3144	638
463	638
174	638
3145	638
1784	638
305	638
646	638
237	638
441	638
3146	638
3147	638
3148	639
3149	639
3150	639
3151	639
670	639
671	639
3152	639
3153	639
59	639
3154	640
174	640
670	640
3147	640
3155	640
3156	640
509	640
3157	640
38	640
39	640
106	640
1965	640
3158	641
3159	641
509	641
813	641
3160	641
55	641
21	641
3161	641
3162	641
1233	641
3133	641
3163	641
3146	642
2764	642
3164	642
258	642
3165	642
3166	642
3167	642
3168	642
3169	643
670	643
870	643
3170	643
3171	643
3172	643
21	643
20	643
3173	643
3174	644
1167	644
369	644
2277	644
1986	644
2664	644
3175	644
3176	644
276	644
926	644
46	644
21	644
20	644
3181	645
3178	645
37	645
3177	645
3180	645
3179	645
3182	645
3183	645
3184	645
260	645
3185	645
3146	646
840	646
291	646
3186	646
3187	646
3188	646
394	646
1495	646
38	646
39	646
106	646
3190	647
295	647
3189	647
3191	647
698	647
106	647
602	647
3146	647
1983	647
1743	647
3192	647
174	648
3193	648
497	648
74	648
106	648
698	648
326	648
509	648
3194	648
1983	648
3195	648
3146	648
1743	648
3192	648
38	648
39	648
3196	649
3197	649
724	649
3198	649
670	649
3199	649
3200	649
3201	649
59	649
3202	649
3203	650
3204	650
3205	650
1327	650
3206	650
3207	650
512	650
3208	650
1858	650
21	650
3209	651
3210	651
3147	651
602	651
3211	651
3212	651
3213	651
698	651
3214	651
3215	652
59	652
3216	652
3217	652
2683	652
3218	652
3219	652
3220	652
36	652
1493	652
72	652
3221	652
3222	652
3223	652
20	652
21	652
3224	652
1073	653
3225	653
3226	653
3227	653
469	653
1906	653
21	653
20	653
58	653
705	653
3228	654
624	654
308	654
21	654
20	654
3229	655
3230	655
3231	655
89	655
37	655
369	655
2456	655
3232	655
3233	655
46	655
20	655
21	655
3234	656
3235	656
2482	656
2028	656
227	656
2655	656
46	656
883	656
29	656
20	656
21	656
3236	656
364	656
174	657
3238	657
3239	657
3237	657
74	657
737	657
2012	657
3240	657
3241	657
3242	657
3243	658
3244	658
2433	658
3245	658
74	658
20	658
21	658
29	658
1058	659
3246	659
288	659
579	659
3247	659
21	659
3248	660
239	660
1893	660
769	660
227	660
20	660
21	660
2157	660
3249	660
227	661
3250	661
670	661
983	661
3251	661
3252	661
20	661
425	662
3253	662
1066	662
1742	662
230	662
277	662
3254	662
20	662
21	662
3255	663
599	663
209	663
227	663
18	663
895	663
20	663
21	663
3256	664
1915	664
3257	664
3258	664
3259	664
3260	664
3261	664
3262	664
3236	664
3263	665
787	665
1971	665
55	665
1299	665
237	665
20	665
21	665
3264	666
57	666
106	666
3265	666
95	666
701	666
20	666
21	666
3266	667
3267	667
89	667
209	667
3268	667
3269	667
2033	667
260	667
46	667
3270	667
3265	668
790	668
109	668
227	668
291	668
3271	668
3272	668
55	668
419	668
20	668
21	668
3273	669
3274	669
2187	669
744	669
57	669
3275	669
1442	669
499	669
20	669
21	669
3276	670
3278	670
3277	670
108	670
3279	670
3280	670
1877	670
3281	670
2093	670
3282	670
3283	670
3284	670
3285	670
3286	670
21	670
3287	671
3288	671
2743	671
2901	671
816	671
3289	671
3290	671
260	671
3293	672
620	672
3292	672
3291	672
29	672
20	672
21	672
77	672
18	672
3294	672
2093	672
2601	672
3295	673
3296	673
3297	673
3298	673
3299	673
3300	673
769	673
419	673
227	673
20	673
21	673
23	674
3301	674
3303	674
3305	674
3304	674
3302	674
58	674
3306	674
20	674
46	674
2901	674
3265	675
3307	675
3308	675
3309	675
3310	675
2323	675
3311	675
20	675
1163	675
3312	675
3313	675
369	675
3314	675
3315	675
3316	675
328	675
3317	675
3318	675
679	675
3319	675
55	675
399	675
3320	675
277	675
3291	676
1426	676
3321	676
578	676
291	676
304	676
20	676
21	676
3322	676
3323	677
3324	677
2883	677
227	677
401	677
86	677
1073	677
46	677
1510	677
3325	678
3326	678
3253	678
46	678
20	678
21	678
304	678
3327	679
3328	679
3329	679
3330	679
2713	679
3331	679
737	679
3332	679
20	679
21	679
3333	680
3334	680
3335	680
3336	680
3337	680
2047	680
20	680
21	680
187	680
76	680
3338	681
3339	681
3340	681
690	681
174	681
227	681
230	681
505	681
74	681
3341	681
361	681
20	681
2805	681
196	681
3342	681
3253	682
174	682
37	682
2389	682
777	682
2753	682
3343	682
3344	682
106	682
20	682
21	682
209	683
3345	683
227	683
3346	683
105	683
1489	683
270	683
291	683
3253	684
397	684
174	684
3347	684
1133	684
235	684
3348	684
3349	684
20	684
21	684
3350	684
399	685
3351	685
114	685
139	685
3352	685
2323	685
578	685
570	685
20	685
21	685
3353	686
109	686
3354	686
3355	686
3356	686
20	686
21	686
41	686
3358	687
3360	687
3357	687
3361	687
20	687
3362	687
3359	687
3363	687
3364	688
3365	688
3366	688
509	688
436	688
382	688
21	688
3291	689
3367	689
1394	689
1072	689
1014	689
1760	689
1094	689
1416	689
3368	689
3369	689
3370	689
3371	689
20	689
3372	690
139	690
3373	690
394	690
78	690
867	690
20	690
21	690
3374	690
3375	691
3377	691
3376	691
3378	692
848	692
174	692
2743	692
3379	692
201	692
3380	692
20	692
3381	693
36	693
3382	693
2740	693
46	693
20	693
21	693
3387	694
3384	694
3385	694
3386	694
499	694
3383	694
3388	694
3378	695
3389	695
1306	695
3390	695
3391	695
3392	695
3393	695
3394	695
20	695
21	695
3395	695
3396	696
89	696
2003	696
59	696
3397	696
3378	697
624	697
18	697
3398	697
3399	697
2373	698
3400	698
55	698
3401	698
21	698
20	698
3402	699
36	699
89	699
72	699
3403	699
3404	699
3405	699
304	699
20	699
21	699
3406	700
72	700
37	700
1357	700
345	700
3407	700
3408	700
3409	700
3410	700
3378	701
3411	701
3412	701
3413	701
227	701
37	701
3414	701
942	701
46	701
20	701
21	701
3415	702
3416	702
3417	702
585	702
1918	702
174	702
3378	703
89	703
3418	703
1942	703
3419	703
1933	703
3420	703
3421	703
20	703
272	703
3423	704
3424	704
3422	704
112	704
3425	704
3426	704
3427	704
20	704
21	704
3428	705
1174	705
3035	705
44	705
499	705
42	705
3429	705
3430	705
3431	705
3433	706
89	706
3432	706
3434	706
3435	706
3436	706
3437	706
3438	706
3439	706
3440	706
20	706
21	706
1357	706
650	706
2381	706
46	706
82	707
3441	707
46	707
20	707
21	707
759	707
3442	708
2477	708
3443	708
2349	708
1066	708
3444	708
3445	708
3446	708
3447	708
3378	709
3448	709
3449	709
3450	709
2655	709
3451	709
1066	709
20	709
21	709
512	709
3452	709
106	709
3453	709
3378	710
1034	710
1760	710
3454	710
3455	710
18	710
46	710
20	710
21	710
3441	711
3456	711
269	711
20	711
21	711
3432	712
3457	712
2554	712
3458	712
23	712
3459	712
3378	713
3460	713
3461	713
3462	713
269	713
3463	713
2277	713
1832	713
21	713
20	713
3409	714
3464	714
1334	714
425	714
3465	714
1915	714
20	714
21	714
3378	715
3466	715
3467	715
3468	715
74	715
2704	715
18	715
20	715
21	715
3432	716
3469	716
3470	716
447	716
917	716
769	716
3471	716
77	716
76	716
20	716
21	716
3378	717
3472	717
3473	717
3474	717
36	717
1420	717
95	717
3360	717
269	717
2839	717
3475	717
358	717
20	717
21	717
3378	718
3476	718
254	718
3477	718
3478	718
3422	719
3479	719
112	719
3425	719
111	719
3480	719
20	719
21	719
3378	720
3481	720
3482	720
3483	720
3484	720
3485	720
3486	720
495	720
3487	721
3488	721
174	721
3489	721
425	721
3490	721
3491	721
59	721
21	721
382	721
3378	722
36	722
563	722
2187	722
3492	722
3493	722
304	722
3494	722
176	722
441	722
20	722
21	722
3500	723
3501	723
3499	723
3502	723
3498	723
3497	723
3495	723
3496	723
3400	724
3503	724
3504	724
3505	724
3506	724
3507	724
3324	724
20	724
21	724
3508	725
3511	725
3510	725
3509	725
3506	725
3507	725
3324	725
20	725
21	725
3432	726
415	726
3512	726
369	726
3513	726
480	726
419	726
97	726
3514	726
209	726
3515	726
1219	726
20	726
21	726
3378	727
3512	727
3516	727
1034	727
304	727
3324	727
3517	727
46	727
3518	728
3519	728
3520	728
3521	728
509	728
20	728
3523	729
21	729
3522	729
20	729
3524	729
3525	729
419	729
77	729
46	729
3526	729
3432	730
3527	730
87	730
3528	730
3529	730
139	730
58	730
20	730
3530	730
3531	730
3532	730
3378	731
3533	731
369	731
436	731
76	731
20	731
21	731
3534	732
55	732
3535	732
3536	732
3537	732
3425	732
3538	732
3539	732
3540	732
3541	733
89	733
3542	733
1387	733
2531	733
981	733
106	733
20	733
21	733
489	733
3545	734
3548	734
3544	734
3546	734
3543	734
3547	734
3549	734
419	734
2613	734
3550	735
3551	735
3552	735
3553	735
3554	735
509	735
174	735
37	735
20	735
21	735
78	735
3557	736
3555	736
3556	736
2670	736
3558	736
82	736
3559	736
78	736
29	736
3560	736
46	736
3546	736
3512	736
3561	736
729	737
3562	737
859	737
812	737
3563	737
857	737
20	737
21	737
3564	737
3565	737
389	737
41	737
859	738
3566	738
3567	738
3568	738
3569	738
277	738
3570	738
1741	738
129	739
3571	739
3572	739
3573	739
3574	739
3570	739
3575	739
3576	739
3577	739
28	739
859	740
3578	740
3579	740
3580	740
3581	740
3582	740
3570	740
38	740
3583	740
857	741
3584	741
3585	741
369	741
2811	741
3586	741
3587	741
382	741
21	741
111	742
3562	742
859	742
812	742
3588	742
857	742
3589	742
20	742
3590	742
3591	742
3592	743
3581	743
812	743
1233	743
3593	743
36	743
859	743
1167	743
304	743
3570	743
20	743
21	743
3594	744
3595	744
3596	744
3597	744
859	744
3598	744
3570	744
859	745
2741	745
3578	745
55	745
3570	745
590	745
3599	745
3600	745
89	745
812	746
3570	746
859	746
729	746
786	746
20	746
21	746
3601	746
649	746
2665	746
3602	746
3603	747
859	747
3570	747
3604	747
59	747
3605	747
3606	747
649	747
3607	747
3608	747
20	747
3609	747
74	747
3294	748
897	748
2706	748
3610	748
3611	748
1073	748
55	748
857	748
382	748
21	748
812	749
859	749
3612	749
3613	749
3570	749
3614	749
3615	749
1547	749
20	749
3616	750
507	750
3617	750
859	750
729	750
3618	750
3619	750
3620	750
3617	751
3621	751
3622	751
3623	751
3624	751
3603	751
3625	751
1662	751
3626	751
3627	751
1112	751
3628	751
3630	752
403	752
3629	752
55	752
469	752
3631	753
859	753
3632	753
253	753
3603	753
3633	754
206	754
3634	754
106	754
3635	754
3636	754
859	754
3637	754
20	754
3638	754
176	754
3639	754
1547	754
403	755
812	755
3562	755
848	755
174	755
3640	755
1320	755
3641	755
3499	755
2805	755
3570	755
20	755
21	755
3578	756
3642	756
3643	756
403	756
196	756
228	756
3644	756
1112	756
227	757
3645	757
1416	757
3646	757
3647	757
1186	757
3412	757
3648	757
812	757
3570	757
859	757
3574	757
649	757
3649	758
3650	758
3651	758
3652	758
2636	758
3653	758
302	758
509	758
3654	758
679	758
3655	758
129	759
3656	759
859	759
3578	759
3657	759
3658	759
3659	759
21	759
3661	760
3660	760
812	760
859	760
3570	760
3662	760
3663	760
20	760
21	760
3664	761
534	761
36	761
3665	761
859	761
3666	761
812	761
3667	761
3668	761
419	761
20	761
21	761
769	761
812	762
857	762
403	762
176	762
3669	762
3275	762
2429	762
77	762
3670	762
403	763
3671	763
3672	763
388	763
1058	763
3578	763
3673	763
174	763
3674	763
21	763
1549	764
3675	764
859	764
765	764
3676	764
21	764
3294	764
3570	764
436	764
3677	764
3678	764
741	764
3679	764
77	764
3680	764
1119	765
3681	765
774	765
859	765
3682	765
3570	765
138	765
812	766
3683	766
859	766
649	766
2089	766
2644	766
3684	766
3685	766
895	766
20	766
21	766
3123	767
859	767
3686	767
3058	767
3687	767
3688	767
3689	767
3690	767
1841	767
816	767
210	767
3691	767
798	767
403	768
3692	768
513	768
3352	768
507	768
857	768
3693	768
3694	768
516	769
759	769
3695	769
2706	769
2089	769
3696	769
3570	769
304	769
20	769
21	769
516	770
3570	770
403	770
609	770
95	770
3697	770
3698	770
3699	770
3700	770
20	770
21	770
3701	771
3603	771
3657	771
859	771
3702	771
3024	771
3703	771
1671	771
786	771
3704	771
3705	771
3706	771
3707	771
3708	771
3578	772
859	772
3709	772
235	772
3710	772
3711	772
1533	772
288	772
21	772
20	772
3578	773
857	773
859	773
649	773
3684	773
3712	773
3713	773
3714	773
2644	773
729	773
38	773
3294	774
3677	774
857	774
3715	774
3716	774
765	774
729	774
3717	774
20	774
21	774
3718	775
3570	775
403	775
765	775
3574	775
1162	775
21	775
20	775
3719	775
3720	775
3721	775
3722	775
812	776
369	776
3723	776
3724	776
3725	776
78	776
2776	776
1789	776
1345	776
176	776
1288	776
857	776
382	776
3726	776
3727	776
3728	776
3729	776
3730	776
37	776
3731	776
665	776
641	776
3732	777
3570	777
403	777
3709	777
3733	777
3734	777
3735	777
3736	777
3737	777
3738	777
3739	778
3740	778
2554	778
1034	778
369	778
2429	778
20	778
21	778
516	779
403	779
3570	779
3709	779
3741	779
792	779
3742	779
3743	779
3744	779
21	779
20	779
859	780
3578	780
857	780
3709	780
729	780
670	780
2649	780
174	780
769	780
20	780
21	780
2740	780
859	781
812	781
3570	781
2752	781
3745	781
649	781
3746	781
1312	781
39	781
3747	782
174	782
89	782
109	782
74	782
419	782
293	782
291	782
55	782
111	782
3748	782
76	782
20	782
21	782
3749	783
59	783
3750	783
111	783
358	783
20	783
3747	784
36	784
37	784
2732	784
3751	784
3752	784
46	784
20	784
21	784
3753	784
3754	785
3755	785
82	785
196	785
2093	785
1062	785
123	785
859	785
3756	785
20	785
21	785
3757	786
117	786
3758	786
3759	786
304	786
499	786
60	786
3760	786
1858	786
3761	786
20	786
1282	786
46	786
3747	787
72	787
36	787
37	787
660	787
3762	787
3763	787
20	787
21	787
3764	787
3765	788
1511	788
2444	788
176	788
37	788
3766	788
3767	788
382	788
21	788
3768	789
36	789
58	789
3769	789
3770	789
3771	789
46	789
20	789
21	789
3772	790
3773	790
139	790
3774	790
1621	790
3775	790
2657	790
21	790
3780	791
38	791
3779	791
3778	791
3776	791
3777	791
41	791
3781	792
3782	792
72	792
1466	792
37	792
1761	792
1014	792
46	792
77	792
20	792
21	792
3783	793
3784	793
3785	793
3786	793
3787	793
74	793
3788	793
46	793
3789	794
3759	794
36	794
108	794
3790	794
1644	794
21	794
20	794
46	794
3791	794
2324	794
3792	794
3793	794
3794	794
3795	795
3796	795
369	795
2521	795
3797	795
58	795
3798	795
3799	795
3800	795
20	795
21	795
3801	796
3802	796
3710	796
2245	796
811	796
3803	796
1420	796
497	796
174	796
326	796
397	796
1010	796
20	796
21	796
3804	797
3805	797
3806	797
3807	797
1911	797
1976	797
167	797
3307	797
3808	797
87	797
2421	797
3809	797
3810	798
2488	798
2307	798
497	798
3811	798
3812	798
36	798
82	798
3813	798
419	798
274	798
3815	799
3816	799
1328	799
3814	799
46	799
174	799
1034	799
37	799
2132	799
78	799
21	799
20	799
42	800
3821	800
3820	800
3817	800
3819	800
3818	800
1174	800
174	800
3822	800
3823	800
369	800
3824	800
441	800
20	800
21	800
106	800
3664	801
3825	801
3826	801
3827	801
36	801
209	801
670	801
46	801
20	801
21	801
78	801
79	801
3828	802
59	802
3829	802
3830	802
20	802
21	802
3831	802
3832	802
3833	802
305	802
3834	802
3835	803
3836	803
74	803
3837	803
1078	803
20	803
21	803
3747	804
3838	804
578	804
304	804
3839	804
46	804
3840	804
777	804
3841	804
3842	805
3843	805
550	805
1689	805
1761	805
613	805
3844	805
358	805
1338	805
3845	805
3846	805
46	805
20	805
21	805
3747	806
2753	806
3847	806
59	806
3848	806
3849	806
20	806
21	806
3850	807
37	807
419	807
213	807
3851	807
509	807
106	807
3852	807
3747	808
3853	808
23	808
765	808
37	808
1170	808
20	808
21	808
3855	809
1933	809
3854	809
1942	809
3856	809
2465	809
532	809
3857	810
626	810
46	810
86	810
20	810
21	810
295	810
3858	810
3859	811
3860	811
37	811
3861	811
196	811
20	811
21	811
3862	812
776	812
3863	812
109	812
89	812
3864	812
671	812
646	812
304	812
20	812
21	812
3867	813
3865	813
444	813
3866	813
463	814
3868	814
3869	814
36	814
697	814
3870	814
3871	814
2280	814
3872	814
20	814
21	814
3874	815
859	815
3873	815
3875	815
3876	815
3718	815
729	815
857	815
1014	815
849	815
3877	815
3878	815
20	815
3378	816
2952	816
3879	816
425	816
3880	816
3881	816
3174	816
3471	816
20	816
21	816
3883	817
3882	817
647	817
3884	818
3885	818
2349	818
3886	818
3887	818
3888	818
3889	818
3890	818
20	818
21	818
3891	818
3892	818
3893	818
3894	818
3895	818
174	819
3896	819
2743	819
3897	819
1299	819
2164	819
288	819
3898	819
358	819
78	819
293	819
20	819
21	819
3899	820
3900	820
33	820
3901	820
3902	820
3082	820
3903	820
138	820
2990	821
3904	821
479	821
1762	821
20	821
21	821
3905	822
509	822
508	822
3906	822
3907	822
3813	822
2937	822
3908	822
3909	822
798	822
737	822
2444	823
3910	823
36	823
1877	823
3911	823
78	823
20	823
21	823
3912	823
3915	824
20	824
3916	824
3914	824
3913	824
21	824
3917	825
2741	825
463	825
36	825
47	825
3918	825
74	825
3919	825
2912	825
3920	825
51	825
3921	826
41	826
2805	826
336	826
74	826
3922	826
3923	826
3924	826
2439	826
3925	826
372	827
3927	827
3926	827
411	827
3928	828
480	828
3929	828
3930	828
3931	828
3932	829
3933	829
2743	829
3934	829
3935	829
3936	829
1416	829
3937	829
3938	829
3939	829
20	829
21	829
3940	830
3941	830
174	830
3942	830
3943	830
2180	830
3944	830
3849	830
444	830
20	830
21	830
3896	831
3945	831
3946	831
425	831
291	831
3947	831
112	831
20	831
21	831
3948	832
3949	832
3950	832
20	832
21	832
29	832
3951	832
3952	833
3193	833
89	833
37	833
299	833
1337	833
1915	833
490	833
20	833
21	833
3953	834
3954	834
1489	834
38	834
39	834
3777	834
3955	834
3956	834
3957	835
3958	835
87	835
1761	835
3959	835
174	835
58	835
78	835
79	835
419	835
3960	835
20	835
21	835
3917	836
3961	836
3962	836
3963	836
2441	836
1441	836
248	836
3849	836
3964	837
3965	837
509	837
20	837
21	837
29	837
2756	837
89	837
3194	837
3966	837
3967	837
3968	837
3969	837
3970	837
3971	837
3972	837
3973	837
3974	837
2737	838
3975	838
3976	838
3977	838
711	838
3978	839
3979	839
20	839
21	839
36	839
3980	839
774	839
3981	839
29	839
308	839
89	839
3982	839
3978	840
3983	840
3984	840
112	840
111	840
3985	840
20	840
21	840
3896	841
23	841
37	841
3986	841
2444	841
3987	841
419	841
78	841
76	841
20	841
21	841
3988	842
3989	842
3990	842
89	842
2389	842
490	842
3991	842
57	843
3992	843
3274	843
1416	843
509	843
3993	843
78	843
2776	843
460	843
74	843
36	843
3994	843
20	843
21	843
3995	844
3996	844
3997	844
89	844
74	844
3998	844
3999	844
55	844
20	844
21	844
4000	845
4001	845
4002	845
4003	845
4004	845
4005	845
4006	845
480	845
4007	845
4008	845
4009	845
509	845
419	845
3896	846
20	846
21	846
4010	846
2477	846
174	846
4011	846
4012	846
4013	846
4014	846
509	846
4015	846
4016	846
1451	846
4017	846
4018	846
4019	846
4020	846
4021	847
36	847
174	847
1877	847
253	847
4022	847
4023	847
499	847
68	847
20	847
21	847
40	848
4024	848
4025	848
174	848
74	848
2850	848
4026	848
4027	848
20	848
21	848
4028	849
42	849
4029	849
4030	849
4031	849
4032	849
4033	849
4034	849
20	849
21	849
4035	850
155	850
4036	850
4037	850
4038	850
2037	850
210	850
1216	850
291	850
4039	850
4040	850
2389	850
20	850
497	851
4041	851
174	851
74	851
112	851
1761	851
425	851
62	851
4042	851
20	851
21	851
4043	851
1254	852
4044	852
174	852
1927	852
94	852
209	852
77	852
3978	853
4045	853
174	853
4046	853
3022	853
1644	853
513	853
21	853
4047	854
4048	854
4049	854
4050	854
4051	854
4052	854
453	854
4053	854
4054	855
4055	855
587	855
21	855
3896	856
497	856
336	856
326	856
1066	856
4056	856
4057	856
4058	857
210	857
352	857
4059	857
4060	857
20	857
21	857
3904	858
4061	858
4062	858
308	858
4063	858
174	858
4064	858
4065	858
20	858
46	858
4066	859
4067	859
2740	859
21	859
20	859
3904	860
4068	860
4069	860
727	860
20	860
21	860
4070	861
4071	861
2049	861
4072	861
3904	862
777	862
174	862
262	862
4073	862
2164	862
55	862
308	862
883	862
21	862
20	862
4074	863
130	863
3294	863
811	863
298	863
582	863
4075	863
4076	863
4077	863
3692	864
509	864
646	864
1445	864
4078	864
105	864
3099	864
4079	865
4080	865
647	865
4081	865
1667	865
4082	865
4083	865
20	865
4084	865
106	865
4087	866
509	866
4085	866
4086	866
4088	866
4089	866
443	866
46	866
641	866
4090	867
4091	867
3710	867
649	867
4092	867
509	867
36	867
512	867
58	867
20	867
21	867
4093	867
4094	868
4095	868
4096	868
174	868
74	868
509	868
4097	868
620	868
382	868
21	868
187	868
641	868
425	868
1372	868
4098	868
4099	868
2429	868
4100	869
4096	869
4101	869
758	869
509	869
29	869
4102	869
21	869
4103	870
4104	870
1072	870
1024	870
1489	870
585	870
2360	870
4105	870
4106	871
717	871
4107	871
1301	871
2613	871
4108	871
2672	871
3679	871
4109	871
4113	872
647	872
4112	872
4110	872
4111	872
20	872
21	872
776	873
4114	873
74	873
4115	873
4116	873
4117	873
646	873
599	873
46	873
20	873
21	873
776	874
4115	874
717	874
4118	874
4119	874
109	874
20	874
21	874
46	874
1694	875
4120	875
4121	875
59	875
369	875
4122	875
1066	875
4123	875
4124	875
4125	875
20	875
21	875
4126	876
4127	876
4128	876
1328	876
550	876
3599	876
4129	876
105	876
29	876
4130	876
20	876
21	876
4131	877
106	877
3307	877
4132	877
4133	877
4134	877
4135	877
4136	877
59	877
4137	877
4138	877
4139	877
4140	877
2427	877
4141	877
3558	878
4142	878
4143	878
4144	878
3647	878
4145	878
1162	878
37	878
196	878
4146	879
3551	879
174	879
74	879
4147	879
4148	879
20	879
21	879
29	879
77	879
509	879
4109	879
4149	880
4150	880
1328	880
46	880
444	880
443	880
304	880
2132	880
2399	880
620	880
2650	880
20	880
21	880
4151	880
4152	881
776	881
196	881
4153	881
4154	881
737	881
20	881
21	881
174	882
4155	882
497	882
1444	882
37	882
670	882
2560	882
4156	882
1027	882
254	882
46	882
20	882
21	882
495	882
3679	882
55	882
1133	882
381	882
29	882
419	883
641	883
20	883
21	883
4157	883
46	883
4158	883
2298	883
4159	883
4160	883
369	883
174	883
550	883
4161	883
4162	883
29	883
4163	883
77	883
4164	883
4165	883
58	883
1985	883
1547	883
4171	884
509	884
4166	884
4170	884
4167	884
4169	884
4168	884
3161	884
4172	884
641	884
46	884
4173	884
20	884
21	884
717	885
4174	885
641	885
419	885
769	885
2476	885
78	885
1162	885
4092	885
4175	886
1631	886
4176	886
4177	886
174	886
4178	886
897	886
304	886
20	886
21	886
46	886
4179	886
4180	887
1420	887
4181	887
336	887
497	887
369	887
4182	887
4183	887
620	887
20	887
21	887
436	887
237	887
1219	887
372	887
29	887
4184	888
2361	888
4185	888
20	888
21	888
419	888
425	888
1372	888
2093	888
4186	889
633	889
4187	889
174	889
74	889
670	889
4188	889
46	889
20	889
21	889
3293	889
4189	889
4190	889
4191	890
4128	890
4193	890
4192	890
4194	890
4195	890
883	890
4196	890
38	890
39	890
4197	891
4201	891
4200	891
1221	891
4198	891
4199	891
21	891
1138	892
4202	892
4203	892
787	892
4204	892
20	892
21	892
3236	892
4205	892
4206	892
4207	892
4208	892
1242	892
4085	893
4209	893
4210	893
1233	893
36	893
227	893
37	893
111	893
4211	893
4212	893
21	893
20	893
4214	894
4215	894
4216	894
345	894
4213	894
277	894
1915	894
21	894
20	894
444	894
77	894
174	895
4217	895
4218	895
4219	895
74	895
4220	895
4221	895
20	895
21	895
46	895
4149	895
36	895
4222	895
358	895
78	895
665	895
4223	895
1915	895
2435	895
4224	896
759	896
1231	896
4225	896
4226	896
174	896
3066	896
106	896
849	896
4080	896
20	896
21	896
4227	897
4228	897
4229	897
4230	897
20	897
21	897
4231	897
4232	898
425	898
1438	898
4233	898
4234	898
787	898
4235	898
106	898
20	898
4236	899
463	899
952	899
703	899
4237	899
2158	899
4238	899
4239	899
534	899
2455	899
21	899
4240	899
4242	900
4241	900
58	900
4243	900
21	900
20	900
305	900
46	900
4244	900
724	901
4245	901
4246	901
799	901
37	901
4247	901
20	901
21	901
4248	902
2743	902
155	902
4249	902
1870	902
4250	902
20	902
21	902
360	902
4251	903
46	903
305	903
2455	903
21	903
89	903
4252	903
4253	903
4254	903
4255	903
4256	903
4257	903
4258	903
1694	903
4259	903
4260	903
4261	903
2192	903
37	903
42	903
358	903
20	903
3680	903
4262	904
66	904
37	904
2192	904
4263	904
4264	904
3401	904
4265	904
1911	904
2427	904
4266	904
4267	904
308	904
4268	904
4269	905
36	905
4270	905
1591	905
358	905
1814	905
4271	905
237	905
2455	905
21	905
4272	906
414	906
4273	906
139	906
1479	906
917	906
237	906
78	906
79	906
20	906
21	906
4274	906
4275	907
239	907
89	907
1617	907
4276	907
4277	907
4278	907
20	907
21	907
1479	908
4279	908
4280	908
36	908
4281	908
77	908
917	908
20	908
21	908
23	909
4282	909
4283	909
1062	909
4284	909
4285	909
465	909
77	909
20	909
21	909
4286	910
36	910
55	910
408	910
78	910
79	910
1274	910
4287	910
4288	910
917	910
4289	910
1602	910
20	910
21	910
509	911
4290	911
4291	911
4292	911
4293	911
4294	911
36	911
550	911
512	911
20	911
21	911
106	911
4295	911
4296	912
1689	912
4297	912
4298	912
4299	912
87	912
139	913
4300	913
4301	913
20	913
21	913
1644	913
4302	913
4303	914
2925	914
1479	914
4304	914
4305	914
4306	914
20	914
21	914
4307	914
1167	914
4308	914
106	914
4309	914
499	914
42	914
4310	914
4311	915
951	915
36	915
4312	915
4313	915
4314	915
3761	915
4315	915
288	915
97	915
4316	916
4317	916
4318	916
4319	916
237	916
4320	916
4321	916
4322	916
4323	916
77	916
20	916
21	916
4324	917
326	917
262	917
4325	917
36	917
534	917
230	917
77	917
76	917
1985	917
78	917
79	917
419	917
20	917
21	917
1501	917
4326	918
4327	918
4328	918
4329	918
4330	918
4331	918
1501	918
1971	918
4332	918
78	918
58	918
867	918
21	918
20	918
4333	919
4334	919
1971	919
4335	919
4336	919
4337	919
4338	919
20	919
21	919
29	919
2248	919
123	920
4339	920
4340	920
677	920
1466	920
1549	920
4341	920
1062	920
4342	920
4343	920
20	920
288	920
4344	920
4345	920
4346	921
36	921
4347	921
4348	921
917	921
4349	921
4350	921
3960	921
4351	921
4352	922
4353	922
106	922
336	922
4354	922
3090	922
381	922
4355	922
20	922
4357	923
74	923
4356	923
174	923
283	923
473	923
4358	923
399	923
196	923
4359	923
485	923
20	923
21	923
4362	924
4360	924
4361	924
37	924
174	924
33	924
174	925
4363	925
37	925
76	925
4364	925
1159	925
907	925
78	925
2416	925
587	925
4365	925
4366	926
139	926
550	926
4367	926
1742	926
4368	926
41	926
4369	926
4370	926
38	926
39	926
4371	927
4372	927
4373	927
4374	927
4375	927
498	927
1442	927
4376	927
20	927
21	927
2229	928
37	928
1596	928
36	928
636	928
106	928
1442	928
4377	928
168	928
20	928
21	928
4378	928
4279	929
4379	929
578	929
58	929
20	929
21	929
187	929
274	929
358	929
291	929
4380	929
4381	930
734	930
23	930
539	930
425	930
4382	930
18	930
78	930
138	930
1394	931
2554	931
1746	931
174	931
2134	931
4383	931
1597	931
4384	931
39	931
59	931
2565	931
4385	932
298	932
174	932
1177	932
565	932
20	932
21	932
1643	933
4386	933
174	933
58	933
4387	933
1495	933
1274	933
382	933
21	933
4388	934
4389	934
1129	934
4390	934
4391	934
21	934
1643	935
425	935
646	935
1279	935
650	935
587	935
382	935
21	935
269	936
1167	936
167	936
730	936
4392	936
4288	936
4393	936
460	936
20	936
21	936
4394	937
277	937
4395	937
1091	937
20	937
21	937
36	938
4396	938
585	938
4397	938
235	938
4398	938
4399	938
20	938
4400	939
563	939
1549	939
512	939
20	939
4401	939
4402	939
4403	940
4405	940
4406	940
4404	940
174	941
4407	941
74	941
4408	941
4409	941
1495	941
534	941
29	941
20	941
21	941
4410	942
3040	942
4411	942
4412	942
592	942
20	942
21	942
4413	943
4414	943
495	943
304	943
4415	944
46	944
4416	944
4417	944
2047	944
1034	944
382	944
196	945
4420	945
4418	945
4419	945
2343	945
21	945
20	945
4421	946
4422	946
4423	946
3285	946
4424	946
2704	946
39	946
4425	947
4426	947
512	947
36	947
37	947
46	947
399	947
2249	947
1401	947
4427	947
20	947
21	947
57	948
74	948
36	948
419	948
196	948
4428	948
4429	948
20	948
21	948
4430	949
4431	949
4432	949
4433	949
4434	949
4435	949
4436	949
20	949
21	949
174	949
633	949
848	949
109	949
4437	949
2607	949
4438	949
4439	949
254	949
2937	949
223	949
4440	949
4441	950
4442	950
291	950
2743	950
4443	950
210	950
74	950
174	950
419	950
2891	950
289	950
4444	950
53	951
4445	951
4446	951
4447	951
4448	951
302	951
76	951
21	951
20	951
78	952
4450	952
4449	952
4451	952
79	952
2305	952
4452	952
4453	952
436	952
2221	952
21	952
20	952
77	952
4454	953
89	953
1014	953
4455	953
4456	953
2498	953
78	953
4457	953
1814	954
4460	954
4458	954
4461	954
4462	954
4459	954
550	954
4463	954
4464	954
4465	954
288	954
4466	954
4467	954
4468	954
4447	954
4469	954
174	954
4470	954
1602	954
1394	954
4471	954
4472	954
4473	954
4474	954
153	954
463	954
1395	954
534	954
58	954
499	954
4475	954
4476	954
951	955
82	955
1072	955
1420	955
4477	955
4478	955
55	955
174	955
89	955
4479	955
4480	955
55	956
4481	956
4482	956
4483	956
304	956
20	956
21	956
4484	957
4485	957
3561	957
4486	957
4487	957
36	957
1874	957
594	957
4488	958
4489	958
298	958
4490	958
105	958
4491	958
291	958
2498	958
2089	958
4492	958
4493	958
20	958
21	958
4494	959
4495	959
4496	959
3282	959
298	959
1386	959
4497	960
4498	960
798	960
1948	960
403	960
522	960
857	960
3603	960
3692	960
4499	960
4500	960
4501	961
89	961
2258	961
4502	961
1452	961
1189	961
4503	961
4504	961
20	961
21	961
914	961
4505	961
4506	961
502	962
4507	962
4508	962
4509	962
4510	962
2132	962
187	962
46	962
665	962
4511	962
4109	962
21	962
777	963
4513	963
4512	963
44	963
1034	963
4514	963
448	963
4515	963
4516	963
18	963
46	963
20	963
21	963
1282	963
4518	964
1377	964
4517	964
4519	964
4520	964
2893	964
4521	964
4522	964
21	964
20	964
647	964
4523	965
86	965
2457	965
4524	965
4525	965
4526	965
4527	965
4528	965
4529	965
4530	965
4531	965
20	965
21	965
4532	965
4534	966
2026	966
4533	966
4535	966
209	966
485	966
2047	966
459	966
20	966
21	966
646	966
282	966
174	967
4536	967
97	967
209	967
1878	967
308	967
1005	967
444	967
21	967
20	967
647	967
3764	967
4537	968
2683	968
4538	968
790	968
4539	968
4540	968
4541	968
20	968
21	968
4547	969
4542	969
4546	969
4548	969
4554	969
4543	969
2455	969
4544	969
4545	969
4553	969
4550	969
4552	969
4551	969
4549	969
21	969
4555	970
497	970
174	970
182	970
112	970
4556	970
20	970
21	970
4557	970
4558	970
4559	970
4560	970
4561	970
36	970
951	970
37	970
4562	970
4567	971
4565	971
4566	971
4563	971
20	971
4564	971
4568	972
4570	972
37	972
4571	972
4572	972
4569	972
2390	972
2280	972
4573	972
1180	972
4568	973
4569	973
4570	973
4571	973
4572	973
37	973
2390	973
2280	973
4573	973
1180	973
4568	974
4569	974
4570	974
4571	974
4572	974
37	974
2390	974
2280	974
4573	974
1180	974
4568	975
4569	975
4570	975
4571	975
4572	975
37	975
2390	975
2280	975
4573	975
1180	975
4568	976
4569	976
4570	976
4571	976
4572	976
37	976
2390	976
2280	976
4573	976
1180	976
4575	977
36	977
4574	977
37	977
4576	977
112	977
3208	977
512	977
4577	978
4578	978
1072	978
401	978
845	978
23	978
2330	978
4579	978
37	978
112	978
182	978
20	978
21	978
174	979
529	979
777	979
37	979
4580	979
4581	979
585	979
4582	979
4583	979
2900	979
488	979
4584	979
21	979
274	979
1412	980
4585	980
649	980
38	980
39	980
4587	981
4586	981
1667	981
20	981
21	981
4589	982
270	982
4588	982
3851	982
4590	982
1372	982
302	982
4591	982
2937	982
4592	982
21	982
20	982
4593	983
4594	983
4595	983
4596	983
3269	983
38	983
4597	983
4598	984
1574	984
3316	984
4599	984
585	984
174	984
802	984
4600	984
4601	985
1891	985
4602	985
174	985
4603	985
2444	986
4604	986
330	986
4605	986
44	986
38	986
39	986
4606	987
4607	987
4608	987
37	987
4609	987
4610	987
1886	987
106	987
21	987
1391	988
4611	988
4612	988
4613	988
1219	988
239	988
270	988
942	989
4614	989
89	989
74	989
4615	989
4387	989
1279	989
650	989
46	989
20	989
21	989
4619	990
4621	990
4618	990
4617	990
4620	990
436	990
4616	990
4623	991
4624	991
4622	991
4625	992
227	992
74	992
174	992
3512	992
2353	992
509	992
46	992
419	992
20	992
21	992
4626	993
4627	993
4539	993
4628	993
4629	993
4630	993
4631	993
4632	993
1228	993
304	993
59	993
4633	994
4634	994
4635	994
4636	994
641	994
20	994
21	994
1456	995
2488	995
4637	995
4638	995
102	995
883	995
39	995
3465	995
2911	996
4639	996
1918	996
4640	996
20	996
21	996
1456	997
4641	997
1986	997
174	997
509	997
4642	997
4643	997
20	997
21	997
2911	998
37	998
109	998
4644	998
2360	998
20	998
21	998
1456	999
3206	999
4645	999
784	999
4646	999
4647	999
4648	999
20	999
46	999
647	999
\.


--
-- Data for Name: user_input; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_input (id, name) FROM stdin;
\.


--
-- Name: association_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.association_id_seq', 1, false);


--
-- Name: ingredient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ingredient_id_seq', 4648, true);


--
-- Name: matching_recipe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.matching_recipe_id_seq', 1, false);


--
-- Name: recipe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recipe_id_seq', 999, true);


--
-- Name: user_input_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_input_id_seq', 1, false);


--
-- Name: association association_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.association
    ADD CONSTRAINT association_pkey PRIMARY KEY (id);


--
-- Name: ingredient ingredient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingredient
    ADD CONSTRAINT ingredient_pkey PRIMARY KEY (id);


--
-- Name: matching_recipe matching_recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.matching_recipe
    ADD CONSTRAINT matching_recipe_pkey PRIMARY KEY (id);


--
-- Name: recipe recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (id);


--
-- Name: user_input user_input_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_input
    ADD CONSTRAINT user_input_pkey PRIMARY KEY (id);


--
-- Name: ingredient ingredient_user_input_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingredient
    ADD CONSTRAINT ingredient_user_input_id_fkey FOREIGN KEY (user_input_id) REFERENCES public.user_input(id);


--
-- Name: matching_recipe matching_recipe_association_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.matching_recipe
    ADD CONSTRAINT matching_recipe_association_id_fkey FOREIGN KEY (association_id) REFERENCES public.association(id);


--
-- Name: matching_recipe matching_recipe_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.matching_recipe
    ADD CONSTRAINT matching_recipe_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES public.recipe(id);


--
-- Name: recipe_ingredient recipe_ingredient_ingredient_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_ingredient
    ADD CONSTRAINT recipe_ingredient_ingredient_id_fkey FOREIGN KEY (ingredient_id) REFERENCES public.ingredient(id);


--
-- Name: recipe_ingredient recipe_ingredient_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_ingredient
    ADD CONSTRAINT recipe_ingredient_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES public.recipe(id);


--
-- PostgreSQL database dump complete
--

