#!/bin/bash
export FLASK_APP="cooker/app.py"
export FLASK_ENV="development"
docker stop pg-cooker
docker run --rm --name pg-cooker -e POSTGRES_PASSWORD=cooker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data -v $(pwd)/cooker/data:/var/dump postgres:latest
source ./tools/restore_db.sh
flask run --host=0.0.0.0