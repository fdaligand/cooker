# restore new db
echo "drop exisiting database"
docker exec pg-cooker psql -U postgres -c "drop database cooker;"
echo "create database for cooker app"
docker exec pg-cooker psql -U postgres -c "create database cooker;"
echo "Load data"
docker exec pg-cooker psql -U postgres cooker -f ./var/dump/db_dump_full.sql