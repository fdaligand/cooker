import os
from collections import defaultdict

import pytest
from click.testing import CliRunner
from sqlalchemy import desc, asc

from cooker import models
from cooker.models import Ingredient, Recipe, Association, save_user_input_ingredients, \
    save_matching_recipe, MatchingRecipe
from cooker.models import db
import itertools


@pytest.fixture(scope="module")
def not_used():
    """ Load light db if not loaded"""
    # os.environ['FLASK_APP'] = "../cooker/app.py"
    # os.environ["FLASK_ENV"]="development"
    # runner = CliRunner()
    # result = runner.invoke(init_db)
    yield
    # remove db use for
    # os.remove('/tmp/test.db')


def test_find_receipe(db):


    ing1 = Ingredient.query.filter(Ingredient.name.like('%chocolat%')).all()
    ing2 = Ingredient.query.filter(Ingredient.name.like('%sucre%')).all()
    receipe_ing1 = set([recipe.id for recipe in itertools.chain(*[ing.recipe for ing in ing1])])
    receipe_ing2 = set([recipe.id for recipe in itertools.chain(*[ing.recipe for ing in ing2])])
    matching = receipe_ing1 & receipe_ing2
    assert len(matching) == 5

def test_find_matching_ingredients_from_user_input(db):

    inputs = ['banane', 'chocolat']
    possible_ingredients = models.find_ingredients(inputs)
    ingredients = models.save_user_input_ingredients(inputs, possible_ingredients)
    assert 'banane' in ingredients
    assert isinstance(ingredients['banane'], list)
    assert len(ingredients['banane']) > 0
    assert 'chocolat' in ingredients
    assert len(ingredients['chocolat']) > 0
    assert isinstance(ingredients['chocolat'], list)

@pytest.mark.skip
def test_get_recipe_by_score(db):

    inputs = ['agneau', 'riz']
    possible_ingredients = models.find_ingredients(inputs)
    ingredients = models.save_user_input_ingredients(inputs, possible_ingredients)
    recipes = models.save_matching_recipe(ingredients)
    sorted_recipes = models.get_recipe_by_score(recipes)
    assert isinstance(sorted_recipes[0]['recipe'], Recipe)
    assert isinstance(sorted_recipes[0]['ingredients'], str)
    assert isinstance(sorted_recipes[0]['score'], tuple)

def test_multiple_ingredient_in_recipe(db):

    inputs = ['lait', 'chocolat', 'oeuf', 'sucre']
    ingredients = models.save_user_input_ingredients(inputs)
    recipes = models.save_matching_recipe(ingredients)
    sorted_recipes = models.get_recipe_by_score(recipes)
    assert False

def test_get_best_recipe_from_multi_user_input():
    inputs = ['beurre', 'sel', 'poivre', 'poulet', 'riz', 'porc', 'pomme']
    all_ingredients = models.find_ingredients(inputs)
    recipes = defaultdict(dict)
    for rank in range(3,len(all_ingredients)):
        # get all recipe that match rank for n ingredient
        possible_association = list(itertools.combinations(inputs, rank))
        for assoc in possible_association:
            assoc_ingredient = models.save_user_input_ingredients(assoc, all_ingredients)
            recipes[assoc] = models.save_matching_recipe(assoc_ingredient)
    recipes_flatten = itertools.chain(*[recipes[key] for key in recipes])
    ranked_recipe = models.get_recipe_by_score(recipes_flatten)
    assert False

def test_filter_best_recipe_from_association(db):
    inputs = ['oeuf', 'beurre', 'lait', 'farine', 'boursin', 'tomate', 'courgettes']
    possible_ingredients = models.find_ingredients(inputs)
    ingredient_association = models.get_possible_association(possible_ingredients)

    recipes = defaultdict(dict)
    for assoc in ingredient_association:
        for recipe in ingredient_association.get(assoc):
            if recipe['recipe'].id in recipes:
                if recipes[recipe['recipe'].id]['score'] < recipe['score']:
                    recipe[recipe['recipe'].id] = recipe
            else:
                recipes[recipe['recipe'].id] = recipe
    assert False


def test_matching_recipe(db):
    input_ingredients = ['agneau', 'riz', 'poivron']
    possible_ingredients = models.find_ingredients(input_ingredients)
    association = Association(user_inputs=input_ingredients)
    matching_ingredient = save_user_input_ingredients(input_ingredients, possible_ingredients)
    association.extract_all_ingredients(matching_ingredient)
    save_matching_recipe(matching_ingredient, association)




def test_retrieve_association():

    ingredients = ['sel','beurre','poivre', 'crème']
    ordered_recipes = db.session.query(MatchingRecipe)\
        .join(Association)\
        .filter(Association.user_inputs.contained_by(ingredients))\
        .order_by(desc(MatchingRecipe.matching_score), desc(MatchingRecipe.missing_ingredient))\
        .all()
    assert ordered_recipes[0].matching_score > ordered_recipes[1].matching_score

def test_lazy_loading_strategy():
    ingredients = ['sel', 'beurre', 'poivre', 'crème']
    recipes_by_ingredient = defaultdict(dict)





