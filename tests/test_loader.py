import os

from cooker.loader import JsonLoader

TEST_FILE = os.path.join(os.path.dirname(__file__), '..', 'cooker', 'data', 'recipes_light.json')

def test_load_data():

    loader = JsonLoader(TEST_FILE)
    loader.load_datas()
    assert len(loader.datas) == 1000

def test_load_date_with_wrong_path_assert_log(caplog):

    loader = JsonLoader('wrong_path')
    loader.load_datas()
    assert "wrong_path is not a valid file path, no datas will be loaded" in [log.msg for log in caplog.records ]