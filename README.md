## Problème

> **J'attends des amis pour dîner mais je n'ai pas le temps de faire des courses.
> Sachant ce que j'ai dans mon frigo, qu'est-ce que je peux préparer ?**

## Objectif

Réaliser une application web qui permette de répondre au problème ci-dessus.

## Format

Le test se déroule en plusieurs phases:
 - Analyse et réflexion, implémentation d'une première version minimaliste
   - on laissera volontairement de côté les fonctionnalités non vitales
 - Rendu intermédiaire de cette démo minimaliste, discussion autour de l'implémentation, des projets et possibilités d'améliorations pour l'étape suivante
 - Implémentation du reste des fonctionnalités pour rendre une application finie

#### Impératifs tech
- [ ] MySQL / PostgreSQL ou autre base MySQL-Compatible.
- [ ] Un back-end qui réponde aux requêtes
- [ ] Une interface web (aussi minimaliste soit elle)

##### en bonus
- [ ] Ruby on Rails
- [ ] React

## Données

Le fichier `recipes.json` contient environ 9500 recettes de cuisines extraites du site www.marmiton.org.

Chaque ligne contient une entrée au format json, extraite grâce au paquet [python-marmiton](https://pypi.org/project/python-marmiton/). Le format de ces entrées est décrit dans le [README du paquet](https://github.com/remaudcorentin-dev/python-marmiton#marmitonget-return-a-dictionary-like-).

**NB:** la clé `steps` a éte supprimée de ces entrées, pour réduire le poids du fichier.

## Livrable

- la codebase : dans un repo git, ou à défaut dans un fichier ZIP
- la structure de base de données
- l'application : en ligne sur heroku ou un serveur perso. A defaut, une vidéo de démonstration et des instructions pour pourvoir l'éxécuter localement.


## Implémentation 

### Choix techniques

#### Backend 

- [Flask](https://flask.palletsprojects.com/en/1.1.x/) : Framework web Python léger (comparer à Django) idéal pour une petite application. 
- Base de donnée : Postgresql 
- ORM : [SQLAlchemy](https://www.sqlalchemy.org/) : J'utilise un ORM pour avoir un code plus clair et m'abstaire de la base de donnée.

#### FrontEnd

> J'ai fait le choix de ne pas trop investir sur la partie front-end pour le moment, n'ayant pas fait de front depuis un petit moment.  
> Cela fait partie des points à améliorer pour la V2. 
- [Jinja2]() : Moteur de template livré avec Flask

### Structure de base de donnée

![database_schema](doc/img/schemaV2.svg)


### Run 

#### Pré requis
* python 3.7
* docker

Pour lancer l'application en local, suivez les étapes suivantes 

 1. Créer un virtualenv python, l'activer et installer les requirements 
```
virtualenv -p python3.7 venv
source venv/bin/activate
cd <repo_git>
pip install -r requirements.txt
``` 
 2. Lancer le script de démarrage
```bash
./tools/run_app.sh
```

3. Aller sur [http://localhost:5000](http://localhost:5000)

### Amelioration
### Global 
- Dockeriser l'app
- Utiliser [tox](https://tox.readthedocs.io/en/latest/) pour automatiser les test unitaire 
#### FrontEnd
- Faire un vrai frontEnd ! 

#### Backend 
- Extraire les vrais nom d'ingrédient des quantités
- Faire un backend API (pour utilisation avec React)

#### database tips and tricks 

```shell script
# run postgres docker 
docker run --rm --name pg-cooker -e POSTGRES_PASSWORD=cooker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data -v $(pwd)/cooker/data:/var/dump postgres:latest
# dump db from docker 
docker exec pg-cooker pg_dump -U postgres cooker > cooker/data/db_dump_light.sql
# create cooker database 
docker exec pg-cooker psql -U postgres postgres -c "create database cooker;"
# populate db 
docker exec pg-cooker psql -U postgres cooker -f ./var/dump/db_dump_light.sql
```